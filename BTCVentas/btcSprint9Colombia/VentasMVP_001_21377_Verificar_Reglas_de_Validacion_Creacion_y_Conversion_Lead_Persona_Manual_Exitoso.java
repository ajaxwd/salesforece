package btcSprint9Colombia;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class VentasMVP_001_21377_Verificar_Reglas_de_Validacion_Creacion_y_Conversion_Lead_Persona_Manual_Exitoso {
	
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "VentasMVP_001_21377_Verificar_Reglas_de_Validacion_Creacion_y_Conversion_Lead_Persona_Manual_Exitoso.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		mensaje = "<p>"+"Prueba en Ejecucion : " + sNombrePrueba + "</p>";
		Funciones.mensajesLog(mensaje);

		int posicionExcel = 1;

		utilidades.DatosInicialesYDrivers.File_TestData = "VentasMVP_001_21377_Verificar_Reglas_de_Validacion_Creacion_y_Conversion_Lead_Persona_Manual_Exitoso.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
//			driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);
		// String currentURL = driver.getCurrentUrl();

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {
			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String modeloInteres = "";
			String ingresoLocal = "";

			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			ingresoLocal = Funciones.getExcel(driver, "Local", posicionExcel);
						
			Thread.sleep(5000);

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;			
			while (cierraLlamado == 0) {	
			Thread.sleep(2000);	
			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
			} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			Thread.sleep(2000);

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearLead(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Crear Lead ";
			msj = "ClickBtnCrearLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(10000);

			WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
			waitSelectEstadoLead.until(ExpectedConditions.visibilityOf(crearLead.Personal.selectEstadoLead(driver)));
			crearLead.Personal.selectEstadoLead(driver).click();
			Thread.sleep(3000);
			crearLead.Personal.nuevoEstadoLead(driver).click();
			mensaje = "Flujo : Seleccion Estado de Lead :" + "NUEVO";
			msj = "SeleccionEstado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String rut = "";
			String tipoDocumentoIdentidad = "";
			switch (paisEjecucion) {

			case "Colombia":

				waitSelectEstadoLead
				.until(ExpectedConditions.elementToBeClickable(crearLead.Personal.selectDocumentoIdentidad(driver)));
				crearLead.Personal.selectDocumentoIdentidad(driver).sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				tipoDocumentoIdentidad = "Cédula de Ciudadanía";
				Thread.sleep(2000);
				crearLead.Personal.tipoDocumentoIdentidad(driver, tipoDocumentoIdentidad).click();
				mensaje = "Flujo : Seleccion Documento Identidad : " + tipoDocumentoIdentidad;
				mensaje = "Flujo : Seleccion Documento Identidad :" + "Cédula de Ciudadanía";
				msj = "SeleccionDocumentoIdentidad";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				rut = utilidades.Funciones.numeroAleatorio(111111, 999988888);
				crearLead.Personal.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
				msj = "IngresoRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);				
				break;
			default:
				break;
			}

			Thread.sleep(3000);
			
			crearLead.Personal.selectTratamiento(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.srTratamiento(driver).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			crearLead.Personal.selectOrigen(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.webOrigen(driver).click();
			mensaje = "Flujo : Ingreso de Origen : " + "WEB";
			msj = "IngresoOrigen";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			crearLead.Personal.inputNombre(driver).sendKeys(nombreAleatorio);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputApellidoPaterno(driver).sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputApellidoMaterno(driver).sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			crearLead.Personal.selectPrioridad(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.medioInteresPrioridad(driver).click();
			mensaje = "Flujo : Seleccion Prioridad :" + "Medio Interes";
			msj = "SeleccionPrioridad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Nombre
			String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
			referidoAleatorio = referidoAleatorio + " " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputReferidoPor(driver).sendKeys(referidoAleatorio);
			mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
			msj = "IngresoNombreRef";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			correo = correo + correoAzar + "@derco.cl";
			correo = correo.toLowerCase();
			crearLead.Personal.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 10; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 9);
				numMovil = numMovil + azar;
			}
			crearLead.Personal.inputMovil(driver).sendKeys(numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 7; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 6);
				numTelefono = numTelefono + azar;
			}

			switch (paisEjecucion) {

			case "Colombia":
//				numTelefono = "2" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
				msj = "IngresoTelefono";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;
			case "Per�":
				numTelefono = "0" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : "  + numTelefono;
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;
			default:
				break;
			}
			
			Thread.sleep(4000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputMarcaInteres(driver));

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
			"Hoja1");
			
			String[] arregloMarcaInteres = new String[modeloInteres.length()];
			for (int i = 0; i < modeloInteres.length(); i++) {
				arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
			}
			for (int i = 0; i < modeloInteres.length(); i++) {
				crearLead.Personal.inputMarcaInteres(driver).sendKeys(arregloMarcaInteres[i]);
				Thread.sleep(500);
			}

			Thread.sleep(4000);

			WebElement elementoMarcaInteresPreProd = driver
			.findElement(By.xpath("//div[@title='" + modeloInteres + "']"));
			elementoMarcaInteresPreProd.click();
			mensaje = "Flujo : Seleccion Producto :" + modeloInteres;
			msj = "IngresoProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			Thread.sleep(1500);
			
			switch (paisEjecucion) {

			case "Colombia":
								
				break;

			case "Per�":
				String[] arregloIngresoLocal = new String[ingresoLocal.length()];
				for (int i = 0; i < ingresoLocal.length(); i++) {
					arregloIngresoLocal[i] = Character.toString(ingresoLocal.charAt(i));
				}
				crearLead.Personal.inputPais(driver).click();
				for (int i = 0; i < ingresoLocal.length(); i++) {
					crearLead.Personal.inputLocal(driver).sendKeys(arregloIngresoLocal[i]);
					Thread.sleep(500);
				}

				mensaje = "Flujo : Ingreso Campo Local : " + ingresoLocal;
				Funciones.mensajesLog(mensaje);

				Thread.sleep(2000);
				crearLead.Personal.localSeleccion(driver, ingresoLocal).click();
				mensaje = "Flujo : Seleccion de Local  : " + ingresoLocal;
				msj = "SeleccionLocal";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;
			default:
				break;
			}
			
			Thread.sleep(1500);
			
			String calleAleatorio = "";
			String calleAleatorioNumero = "";
			
			switch (paisEjecucion) {

			case "Colombia":
				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearLead.Personal.inputCalle(driver).sendKeys(calleAleatorio + " " + calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio + " " + calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				crearLead.Personal.inputComplementoDireccion(driver).sendKeys("Casa Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;
			case "Per�":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}
				crearLead.Personal.inputDireccion(driver).sendKeys(calleAleatorio );
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);
				
				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: "+calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				Thread.sleep(1500);
				
				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				Thread.sleep(1500);
				
				crearLead.Personal.inputReferenciaDireccion(driver).sendKeys(calleAleatorio + " Casa Interior");
				mensaje = "Flujo : Campo Referencia Direccion :" + calleAleatorio + " Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;
			default:
				break;
			}

			String regionIngresar = "";
			String regionIngresarAlternativo = "";
			String provinciaIngresar = "";
			String comunaIngresar = "";

			Thread.sleep(1000);
			
			Funciones.funcionMoverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));

			switch (paisEjecucion) {
			case "Colombia":

				regionIngresar = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				regionIngresarAlternativo = "Buscar Regiones...";
				comunaIngresar = "Buscar Comunas...";
				break;

			case "Per�":

				regionIngresar = "Buscar Departamentos...";
				regionIngresarAlternativo = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				comunaIngresar = "Buscar Distritos...";
				break;

			default:
				break;
			}

			String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			crearLead.Personal.inputPais(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				crearLead.Personal.inputPais(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			
			crearLead.Personal.paisSeleccionLead(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			String[] arregloRegionEjecucion = new String[regionEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloRegionEjecucion[i] = Character.toString(regionEjecucion.charAt(i));
			}
			crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).click();
			for (int i = 0; i < regionEjecucion.length(); i++) {
				crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).sendKeys(arregloRegionEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Region/Departamento : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			
			crearLead.Personal.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region/Departamento  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloProvinciaEjecucion = new String[provinciaEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloProvinciaEjecucion[i] = Character.toString(provinciaEjecucion.charAt(i));
			}
			crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(arregloProvinciaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Provincia : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			
			crearLead.Personal.provinciaSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion de Provincia  : " + provinciaEjecucion;
			msj = "SeleccionProvincia";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloComunaEjecucion = new String[comunaEjecucion.length()];
			for (int i = 0; i < comunaEjecucion.length(); i++) {
				arregloComunaEjecucion[i] = Character.toString(comunaEjecucion.charAt(i));
			}
			crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				jse.executeScript("window.scrollBy (0,150)");
				crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(arregloComunaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Comuna/Distrito : " + comunaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("(//lightning-icon[@class='itemIcon slds-icon slds-icon--x-small slds-m-left--x-small slds-icon-text-default slds-button__icon slds-icon-utility-search slds-icon_container forceIcon']/following::span[contains(@title,'"+comunaEjecucion+"')])[2]")));
			Thread.sleep(1000);

			crearLead.Personal.comunaSeleccion(driver,comunaIngresar,comunaEjecucion).click();
			mensaje = "Flujo : Seleccion de Comuna/Distrito : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			crearLead.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			Thread.sleep(3000);
			
			try {
				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta")) {
					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta pagina :  " +crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText()+ "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append( "\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(10000);
			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearLead.Personal.txtNombrePaginaInicio(driver)));
			String comparar = crearLead.Personal.txtNombrePaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creacion correcta de nuevo lead personal";
			Funciones.mensajesLog(mensaje);

			if (comparar.contains(nombreAleatorio + " " + apellidoPaternoAleatorio)) {
				mensaje = "Ok : Coincide Nombre Ingresado " + nombreAleatorio + " " + apellidoPaternoAleatorio
				+ " con Nombre desplegado en Cracion de Lead " + comparar;
				msj = "OkCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
				+ "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Lead " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}
			try {
				
				int sw = 0;
				int posicion = 0;
				posicion = posicion + 1;
				
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK:; Se Crea Con exito LEAD Personal " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(3000);

				try {					
					new WebDriverWait(driver, 30, 100).until(ExpectedConditions
					.elementToBeClickable(principalLead.PrincipalLead.btnEstadoCalificado(driver)))
					.click();
					mensaje = "Flujo : Se presiona Boton 'Calificar' en menu superior ";
					msj = "ClickBtnCalificar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Thread.sleep(3000);

					new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(
					principalLead.PrincipalLead.btnCalificarLeadMenuSuperior(driver)))
					.click();
					mensaje = "Flujo : Se presiona Boton 'Marcar como Estado de lead actual' en menu superior ";
					msj = "ClickBtnMarcar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Thread.sleep(3000);

					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
					principalLead.PrincipalLead.btnConvertirMenuSuperior(driver))).click();

					mensaje = "Flujo : Se presiona Boton 'Convertir' en menu superior ";
					msj = "ClickBtnConvertir";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					mensaje = "Flujo : Se Califica Lead ";
					Funciones.mensajesLog(mensaje);

				} catch (NoSuchElementException | ScreenshotException e) {

					new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(
					principalLead.PrincipalLead.btnDosAccionesMenuSuperior(driver)))
					.click();
					mensaje = "Flujo : Se presiona Boton 'Dos Acciones Mas' en menu superior ";
					msj = "ClickBtnDosAccionesMas";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Thread.sleep(3000);
					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
					principalLead.PrincipalLead.btnConvertirDosAccionesMenuSuperior(driver)))
					.click();
					mensaje = "Flujo : Se presiona Boton 'Convertir' en menu superior ";
					msj = "ClickBtnConvertir";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(3000);

				new WebDriverWait(driver, 20, 100)
				.until(ExpectedConditions.elementToBeClickable(
				principalLead.PrincipalLead.chkNoCrearOportinidadTrasConvertir(driver)))
				.click();
				mensaje = "Flujo : Se tickea checkbox 'No crear una oportunidad tras la conversi�n' ";
				msj = "ClickChkNoCrearOportunidad";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(5000);

				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
				principalLead.PrincipalLead.btnConvertirPopUpConvertir(driver))).click();
				mensaje = "Flujo : Se presiona Boton 'Convertir' en Pop Up 'Convertir' ";
				msj = "ClickBtnConvertirPopUp";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(3000);
				
				try {
					Thread.sleep(5000);
					String comprobar = "";

					WebElement comprobarObjeto = driver.findElement(By.xpath(
					"/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div[1]/div[1]/span"));
					comprobar = comprobarObjeto.getText();

					if (comprobar.contains("Se ha convertido su lead")) {
						mensaje = "OK : Registro Se encuentra en Estado 'Convertido' ";
						msj = "OkregistroConvertido";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(1500);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						new WebDriverWait(driver, 20, 100).until(ExpectedConditions
						.elementToBeClickable(principalLead.PrincipalLead.btnIrALeads(driver)))
						.click();
						ValorEncontrado = true;
						sw = 1;
					} else {
						mensaje = "Error: Registro No Se encuentra en Estado 'Convertido'  ";
						msj = "ErrorRegistroNoCovertido";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(1500);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
						sw = 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					mensaje = mensaje + "<p style='color:red;'>"
					+ " Error : No encuentra Elemento de estado en pop up Convertir" + "<p>";
					msj = "ErrorNoExisteElementoPopUp";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}	
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
				+ "ERROR: Prueba Fallida ; No se crea LAED Personal de Manera Correcta" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException |ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEnEjecucion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
