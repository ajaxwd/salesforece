package btcSprint9Colombia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class VentasMVP_004_22076_Verificar_Reglas_De_Validacion_Creacion_Cuenta_Empresa {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "VentasMVP_004_22076_Verificar_Reglas_De_Validacion_Creacion_Cuenta_Empresa.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		mensaje = "<p>" + "Escenario en Ejecucion : " + sNombrePrueba + "</p>";
		Funciones.mensajesLog(mensaje);

		int posicionExcel = 1;
		//utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
			+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		/* utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx"; */
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String modeloInteres = "";
			
			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)));
			paginaPrincipal.MenuPrincipal.btnCuentas(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]|(//h2[contains(text(),'Llamada')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					Funciones.mensajesLog(mensaje);
					System.out.println(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}
			
			Thread.sleep(5000);

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions
			.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.btnCrearCuenta(driver)));
			principalCuenta.MenuPrincipalCuenta.btnCrearCuenta(driver).click();
			mensaje = "Flujo : Click Boton Crear Cuenta ";
			msj = "ClickBtnCrearCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalCuenta.MenuPrincipalCuenta.rdCuentaEmpresa(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Cuenta Empresa";
			msj = "ClickRadioCrearPersona";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalCuenta.MenuPrincipalCuenta.btnSiguienteCrearCuenta(driver).click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";

			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(10000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearLead.Empresa.inputDocumentoIdentidad(driver)))
			.getText();

			String rut = "";
			/* String tipoDocumentoIdentidad = "RUC";
			crearLead.Empresa.inputDocumentoIdentidad(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.tipoDocumentoIdentidad(driver, tipoDocumentoIdentidad).click();
			mensaje = "Flujo : Seleccion Documento Identidad : " + tipoDocumentoIdentidad;
			msj = "SeleccionDocumentoIdentidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			// Cadena de caracteres aleatoria Para RUC
			rut = utilidades.Funciones.generadorDeRut(500000000, 990000000);
			String aleatorioNum = Funciones.numeroAleatorio(1, 8);				
			String vectorRut[] = rut.split("-");
			System.err.println("*" + vectorRut[0] + "*");
			System.err.println("*" + vectorRut[1] + "*");
			rut = vectorRut[0];
			rut = rut + Integer.parseInt(aleatorioNum);
			if (rut.length()<11){
				aleatorioNum = Funciones.numeroAleatorio(1, 8);
				rut = rut + Integer.parseInt(aleatorioNum);
			}
			crearLead.Empresa.inputDocumentoIdentidad(driver).sendKeys(rut);
			mensaje = "Flujo : Ingreso de RUC Aleatorio Empresa : " + rut;
			msj = "IngresoDni";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			String giroEmpresa = Funciones.getExcel(driver, "Giro", posicionExcel);
			
			crearCuenta.Empresa.inputGiroEmpresa(driver).click();
			Thread.sleep(500);
			crearCuenta.Empresa.inputGiroEmpresa(driver).sendKeys(giroEmpresa);
			mensaje = "Flujo : Ingreso de Giro : "+giroEmpresa;
			msj = "IngresoGiro";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			/* Thread.sleep(2000);

			crearCuenta.Empresa.selectClasificacionFlotas(driver).click();
			Thread.sleep(2000);
			crearCuenta.Empresa.clasificacionFlotasSeleccion(driver, "A").click();

			mensaje = "Flujo : Ingreso de Flota : " + "A";
			msj = "IngresoFlota";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			String empresaAleatorioComparar = empresaAleatorio + " Cia Ltda";
			crearCuenta.Empresa.inputRazonSocial(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			mensaje = "Flujo : Ingreso de Compañia Aleatorio : " + empresaAleatorio + " Cia Ltda";
			msj = "IngresoCompania";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);
			String detalleDeCuenta = ExcelUtils.getCellData(posicionExcel, 31);
			System.err.println("Cuenta " + detalleDeCuenta);

			Thread.sleep(3000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			driver.findElement(By.xpath("//*[contains(text(),'Nacionalidad')]")));
			Thread.sleep(3000);

			String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			principalCuenta.MenuPrincipalCuenta.inputNacionalidad(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				crearCuenta.Empresa.inputNacionalidad(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Ingreso Campo Nacionalidad : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);

			crearCuenta.Empresa.nacionalidadSeleccion(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Nacionalidad  : " + paisEjecucion;
			msj = "SeleccionNacionalidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			String correo = "";
			correo = "contactenos";
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearCuenta.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			numTelefono = "0" + numTelefono;
			crearCuenta.Empresa.inputTelefonoEmpresa(driver).sendKeys(numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			Thread.sleep(1500);

			String calleAleatorio = "";
			String calleAleatorioNumero = "";

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearCuenta.Empresa.inputDireccion(driver).sendKeys(calleAleatorio);
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: " + calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearCuenta.Empresa.inputReferenciaDireccion(driver).sendKeys("Sector Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Sector Interior";
				msj = "IngresoReferenciaDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String regionIngresar = "";
			String regionIngresarAlternativo = "";
			String provinciaIngresar = "";
			String comunaIngresar = "";

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			regionIngresar = "Buscar Departamentos...";
			regionIngresarAlternativo = "Buscar Regiones...";
			provinciaIngresar = "Buscar Provincias...";
			comunaIngresar = "Buscar Comunas...";

			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			principalCuenta.MenuPrincipalCuenta.inputPais(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				principalCuenta.MenuPrincipalCuenta.inputPais(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);
			
			int largo = crearLead.Personal.paisSelecciones(driver, paisEjecucion).size();
			crearLead.Personal.paisSelecciones(driver, paisEjecucion).get(largo - 1).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo));
			Thread.sleep(1000);

			String[] arregloRegionEjecucion = new String[regionEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloRegionEjecucion[i] = Character.toString(regionEjecucion.charAt(i));
			}
			crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).click();
			for (int i = 0; i < regionEjecucion.length(); i++) {
				crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).sendKeys(arregloRegionEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Region/Departamento : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
		
			crearLead.Personal.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region/Departamento  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloProvinciaEjecucion = new String[provinciaEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloProvinciaEjecucion[i] = Character.toString(provinciaEjecucion.charAt(i));
			}
			crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(arregloProvinciaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Provincia : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
	
			crearLead.Personal.provinciaSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion de Provincia  : " + provinciaEjecucion;
			msj = "SeleccionProvincia";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloComunaEjecucion = new String[comunaEjecucion.length()];
			for (int i = 0; i < comunaEjecucion.length(); i++) {
				arregloComunaEjecucion[i] = Character.toString(comunaEjecucion.charAt(i));
			}
			crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				jse.executeScript("window.scrollBy (0,150)");
				crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(arregloComunaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			
			mensaje = "Flujo : Ingreso Campo Comuna/Distrito : " + comunaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(
			"(//lightning-icon[@class='itemIcon slds-icon slds-icon--x-small slds-m-left--x-small slds-icon-text-default slds-button__icon slds-icon-utility-search slds-icon_container forceIcon']/following::span[contains(@title,'"
			+ comunaEjecucion + "')])[2]")));
			Thread.sleep(1000);

			crearLead.Personal.comunaSeleccion(driver, comunaIngresar, comunaEjecucion).click();
			mensaje = "Flujo : Seleccion de Comuna/Distrito : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {

				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta página.")) {

					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta página :  "
					+ crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText() + "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(10000);

			mensaje = "Flujo : Se revisa creacion correcta de nueva Cuenta Empresa";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(10000);

			try {

				String textoError = driver.findElement(By.xpath("//span[@class='genericError uiOutputText']"))
				.getText();
				String contenidoError = driver.findElement(By.xpath(
				"//span[@class='genericError uiOutputText'][contains(text(),'"+textoError+"')]//following::li"))
				.getText();
				System.err.println("Cont Error " + contenidoError);
				if (textoError.contains("Revise")) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Creacion "
					+ contenidoError + "<p>";
					msj = "ErrorExistenCreacion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					driver.close();
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}

			} catch (NoSuchElementException | ScreenshotException e1) { }

			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions
			.visibilityOf(crearCuenta.Empresa.txtNombrePaginaInicioCuenta(driver)));
			
			String compararNombre = crearCuenta.Empresa.txtNombrePaginaInicioCuenta(driver).getText();
			String compararRut = crearCuenta.Empresa.txtRutPaginaInicioCuenta(driver).getText();
			String compararTelefono = crearCuenta.Empresa.txtTelefonoPaginaInicioCuenta(driver)
			.getText();

			System.err.println("*" + compararNombre + "*");
			System.err.println("*" + empresaAleatorioComparar + "*");
			System.err.println("*" + compararRut + "*");
			System.err.println("*" + rut + "*");
			System.err.println("*" + compararTelefono + "*");
			System.err.println("*" + numTelefono + "*");

			if (compararNombre.contains(empresaAleatorioComparar) && compararRut.contains(rut)
				&& compararTelefono.contains(numTelefono)) {
				mensaje = "Ok : Guardado exitoso";
				msj = "OkExito";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
				+ "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Cuenta " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito CUENTA Empresa " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
				"Hoja1");

				recorreExcell = 0;
				inicioExcell = 0;
				String setNombreLead = "";
				while (recorreExcell == 0) {
					if ("Nombre Empresa Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setNombreLead = compararNombre;
						ExcelUtils.setCellData(setNombreLead, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Nombre Persona Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
						+ "</p>";
						Funciones.mensajesLog(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				recorreExcell = 0;
				inicioExcell = 0;
				while (recorreExcell == 0) {
					if ("Nombre Persona Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						ExcelUtils.setCellData("Cuenta Empresa", posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Nombre Persona Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
						+ "</p>";
						Funciones.mensajesLog(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}
				
				
				recorreExcell = 0;
				inicioExcell = 0;
				String setRut = "";
				while (recorreExcell == 0) {
					if ("Rut Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setRut = rut;
						ExcelUtils.setCellData(setRut, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Rut Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
						+ "</p>";
						Funciones.mensajesLog(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				recorreExcell = 0;
				inicioExcell = 0;
				String setTelefono = "";
				while (recorreExcell == 0) {
					if ("Telefono Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setTelefono = numTelefono;
						ExcelUtils.setCellData(setTelefono, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Telefono Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
						Funciones.mensajesLog(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				String ruta = "C:\\SalesForce\\listadoCreacionCuentasVolumen.txt";
				File archivo = new File(ruta);
				FileWriter fw = null;
				BufferedWriter bw = null;
				// flag true, indica adjuntar informaci�n al archivo.

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
				Date date = new Date();
				String dateTime = dateFormat.format(date);
				String data = setRut + "  " + setNombreLead + "  " + setTelefono + "  " + dateTime + "\r\n";
				
				if (archivo.exists()) {
					try {
						// bw = new BufferedWriter(new FileWriter(archivo));
						fw = new FileWriter(archivo.getAbsoluteFile(), true);
						bw = new BufferedWriter(fw);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						bw.write(data);
						// bw.newLine();
						bw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.err.println("No Existe el Archivo de Texto 'listadoCreacionCuentasVolumen.txt'");
				}

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
				+ "ERROR: Prueba Fallida ; No se crea CUENTA Empresa de Manera Correcta" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEnEjecucion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
