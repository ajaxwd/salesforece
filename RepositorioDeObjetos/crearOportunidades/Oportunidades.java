package crearOportunidades;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Oportunidades {
	
	private static WebElement element = null;
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputNombreOportunidad
	 * @info *** Campo Nombre Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputNombreOportunidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//input[@data-interactive-lib-uid='4']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputNombre
	 * @info *** elemento input Nombre/Raz�n Social en Oportunidad; se tiene que llenar y elegir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputNombreRazon(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Cuentas...']"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param nombreRazonSeleccion
	 * @info *** Selecci�n elemento Nombre/Raz�n Social en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement nombreRazonSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[2]"));
		element = driver.findElement(By.xpath("//div[contains(@title,'267487848')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 06 | AXITY
	 * @param labelTituloNuevaOPortunidad
	 * @info *** Titulo Nueva OPortunidad en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement labelTituloNuevaOPortunidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']"));
		String ruta = "//h2[contains(text(),'Oportunidad')]";
		element = driver.findElement(By.xpath(ruta));
		return element;		
	}	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputFechaCierre
	 * @info *** elemento input Fecha de Cierre en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputFechaCierre(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='6']"));
		return element;
	}

	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectPrioridad
	 * @info *** Campo Prioridad; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectPrioridad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		String ruta = "(//span[contains(text(),'Prioridad')]/following::div/div/div/div/a[contains(text(),'--Ninguno--')])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param medioInteres
	 * @info *** elemento Medio Interés en Prioridad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement altoInteres(WebDriver driver) {
		//a[@title='Alto interés']
		String ruta = "//a[contains(text(),'Alto inter')]";
		element = driver.findElement(By.xpath(ruta));									
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectEtapa
	 * @info *** Campo Etapa; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectEtapa(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='6']"));
		element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param propuesta
	 * @info *** elemento propuesta en Etapa Oportunidad
	 * @return WebElement 
	 */
	public static WebElement detencion(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[9]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Detección']"));		
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param opcionRetoma
	 * @info *** Opci�n Retoma, Oportunidad
	 * @return WebElement 
	 */
	public static WebElement opcionRetoma(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputProbabilidad
	 * @info *** Campo probabilidad, Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputProbabilidad(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectDivisaOportunidad
	 * @info *** Campo Divisa de la Oportunidad; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectDivisaOportunidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		String ruta = "//span[contains(text(),'Divisa de la oportunidad')]/following::a[@class='select']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param pesoChileno
	 * @info *** elemento Peso Chileno en Divisa de la Oportunidad 
	 * @return WebElement 
	 */
	public static WebElement pesoChileno(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[11]/div[1]/ul[1]/li[3]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='CLP - Peso chileno']"));		
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param divisa
	 * @info *** elemento divisa en Divisa de la Oportunidad 
	 * @return WebElement 
	 */
	public static WebElement divisa(WebDriver driver, String divisa) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[11]/div[1]/ul[1]/li[3]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='"+divisa+"']"));		
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputTotal
	 * @info *** Campo Total, Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputTotal(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='12']"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectOrigen
	 * @info *** Campo Origen; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param web
	 * @info *** elemento Web en Origen Oportunidad 
	 * @return WebElement 
	 */
	public static WebElement web(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Web']"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputOrigenCampana
	 * @info *** elemento input Origen de la campa�a principal Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputOrigenCampana(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[7]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**Pendiente debido a Falta de Datos
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param nombreRazonSeleccion
	 * @info *** Selecci�n elemento Origen de la campa�a principal Oportunidad
	 * @return WebElement 
	 */
	public static WebElement origenCampanaSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath(""));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputCampa�aDerco
	 * @info *** Campo Campa�a Derco Center, Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputCampanaDerco(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[7]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Campañas...']"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputReferido
	 * @info *** Campo Referido Por, Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputReferido(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[8]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "(//span[contains(text(),'Referido por')]/following::input[@maxlength='255'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputEntidadFinanciera
	 * @info *** elemento input Entidad financiera Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputEntidadFinanciera(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param VentaParaSeleccion
	 * @info *** Selecci�n elemento Venta Para en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement entidadFinancieraSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
		element = driver.findElement(By.xpath("//div[@title='225483187']"));
		return element;
	}
	
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputVentaPara
	 * @info *** elemento input Venta Para en Oportunidad; se tiene que llenar y elegir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputVentaPara(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
												
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param VentaParaSeleccion
	 * @info *** Selecci�n elemento Venta Para en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement ventaParaSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[4]/a[1]/div[2]/div[1]"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputCompra
	 * @info *** elemento input Compra y. En Oportunidad; se tiene que llenar y elegir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputCompra(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
												
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param compraSeleccion
	 * @info *** Selecci�n elemento Compra y. en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement compraSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[2]/a[1]"));
		return element;
	}
	
	
	
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectMarcaInteres
	 * @info *** Campo Marca de Interes; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectMarcaInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		
												
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param susuki
	 * @info *** elemento Susuki en Marca de Interes Oportunidad
	 * @return WebElement 
	 */
	public static WebElement susuki(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[13]/div[1]/ul[1]/li[2]/a[1]"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectModeloInteres
	 * @info *** Campo Modelo de inter�s; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectModeloInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
												
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param swift
	 * @info *** elemento Swift en Modelo de Interes Oportunidad
	 * @return WebElement 
	 */
	public static WebElement swift(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[14]/div[1]/ul[1]/li[24]/a[1]"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectVersion
	 * @info *** Campo Version; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectVersion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));		
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param gaAc
	 * @info *** elemento GA AC Version Oportunidad
	 * @return WebElement 
	 */
	public static WebElement gaAc(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[15]/div[1]/ul[1]/li[3]/a[1]"));
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param selectMotivoPerdidaOp
	 * @info *** Campo Motivo de p�rdida de oportunidad; aca se presenta como select pero no lo es en realidad Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectMotivoPerdidaOp(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
												
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param terminoPlazo
	 * @info *** elemento Termino de Plazo en Motivo de Perdida Oportunidad
	 * @return WebElement 
	 */
	public static WebElement terminoPlazo(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[16]/div[1]/ul[1]/li[2]/a[1]"));
		return element;
	}
	
	
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputOtroMotivoPerdida
	 * @info *** elemento input Otro motivo de p�rdida en Oportunidad; se tiene que llenar y elegir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputOtroMotivoPerdida(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/textarea[1]"));
											
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param inputCompetenciaElegida
	 * @info *** elemento input Competencia elegida en Oportunidad; se tiene que llenar y elegir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputCompetenciaElegida(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
												
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param btnGuardar
	 * @info *** Boton Guardar Registro en Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[3]/span[1]"));
		element = driver.findElement(By.xpath("//button[@title='Guardar']//span[contains(@class,'label bBody')][contains(text(),'Guardar')]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param txtNombreOpPaginaInicio
	 * @info *** Texto Nombre Oportunidad  
	 * @return WebElement 
	 */
	public static WebElement txtNombreOpPaginaInicio(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]"));
		element = driver.findElement(By.xpath("//div[2]/h1/div"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 03 05 | AXITY
	 * @param inputBuscarProductos
	 * @info *** Input Buscar Productos
	 * @return WebElement 
	 */
	public static WebElement inputBuscarProductos(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Productos...']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputVinOportunidad
	 * @info *** Input Vin Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputVinOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'VIN')]/following::input[@maxlength='20']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputCilindradaOportunidad
	 * @info *** Input Cilindrada Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputCilindradaOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Cilindrada')]/following::input[@maxlength='10'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputMarcaOportunidad
	 * @info *** Input Marca Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputMarcaOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Marca')]/following::input[@maxlength='150'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputKilometrajeOportunidad
	 * @info *** Input Kilometraje Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputKilometrajeOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Kilometraje')]/following::input[@maxlength='10'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputLocalOportunidad
	 * @info *** Input Local Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputLocalOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Local')]/following::input[@maxlength='255'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputCombustibleOportunidad
	 * @info *** Input Combustible Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputCombustibleOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Combustible')]/following::input[@maxlength='15'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputPatenteOportunidad
	 * @info *** Input Patente Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputPatenteOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Patente')]/following::input[@maxlength='6'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputReservaOportunidad
	 * @info *** Input Reserva Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputReservaOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Reserva')]/following::input[@maxlength='15'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputPrecioOportunidad
	 * @info *** Input Precio Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputPrecioOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Precio')]/following::input[@maxlength='15'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputColorOportunidad
	 * @info *** Input Color Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputColorOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Color')]/following::input[@maxlength='15'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputAnioOportunidad
	 * @info *** Input A�o Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputAnioOportunidad(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Año')]/following::input[@maxlength='4'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param selectTransmisionOportunidad
	 * @info *** Input Anio Oportunidad
	 * @return WebElement 
	 */
	public static WebElement selectTransmisionOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Transmisión')]/following::a[@class='select'])[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param elementoTransmision
	 * @info *** elemento Transmision en Transmision de la Oportunidad 
	 * @return WebElement 
	 */
	public static WebElement elementoTransmision(WebDriver driver, String transmision) {
		element = driver.findElement(By.xpath("//a[@title='"+transmision+"']"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputModeloOportunidad
	 * @info *** Input A�o Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputModeloOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Modelo')]/following::input[@maxlength='150'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 30 | AXITY
	 * @param inputVersionOportunidad
	 * @info *** Input Version Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputVersionOportunidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Versi�n')]/following::input[@maxlength='150'])[1]"));
		return element;
	}
	
}
