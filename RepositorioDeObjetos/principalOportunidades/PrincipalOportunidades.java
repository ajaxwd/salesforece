package principalOportunidades;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PrincipalOportunidades {

	private static WebElement element = null;

	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param linkPrimerRegistroDesplegado
	 * @info *** Link Primer Registro Desplegado en Pagina Principal Oportunidades
	 * @return WebElement
	 */
	public static WebElement linkPrimerRegistroDesplegado(WebDriver driver) {
		//"/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/span[1]/a[1]";
		String ruta = "//*[@id='brandBand_1']/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[1]/th/span/a";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnNuevaCotizacion
	 * @info *** Boton Nueva Cotizacion Registro Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnNuevaCotizacion(WebDriver driver) {
		//a[@title='Nueva cotización']
		//a[contains(text(),'Nueva cotización')]
		String ruta = "//a/div[contains(text(),'Nueva cotización')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param txtNombreCotizacion
	 * @info *** Texto Nombre Nueva Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement txtNombreCotizacion(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param inputFormasDePago
	 * @info *** Input Formas de Pago Nueva Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement inputFormasDePago(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//span[contains(text(),'Forma de pago')]/following::input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']"));
		String ruta = "//span[contains(text(),'Forma de pago')]//following::input[@placeholder='Buscar Formas de pago...']";
		element = driver.findElement(By.xpath(ruta));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnGuardarCotizacion
	 * @info *** Boton Guardar Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarCotizacion(WebDriver driver) {
		String ruta = "/html/body/div[5]/div[2]/div/div[2]/div/div[3]/button[2]/span";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 09 05 | AXITY
	 * @param lnkCotizacionCreada
	 * @info *** Link de Cotizacion Creada en Oportunidad
	 * @return WebElement 
	 */
	public static WebElement lnkCotizacionCreada(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/a[1]/div[1]"));
		String ruta = "//span[@class='toastMessage slds-text-heading--small forceActionsText']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnCotizaciones
	 * @info *** Boton Cotizaciones OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnCotizaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@title='Cotizaciones']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param inputBuscar
	 * @info *** input Buscar
	 * @return WebElement 
	 */
	public static WebElement inputBuscar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//input[@placeholder='Buscar en Salesforce']"));
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Oportunidades y más...']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnCotizacioneCreada
	 * @info *** Boton Cotizaciones Creada
	 * @return WebElement 
	 */
	public static WebElement btnCotizacioneCreada(WebDriver driver, String nombreCotizacion) {
		//System.err.println("Elemento "+"//a[@class='slds-truncate outputLookupLink slds-truncate forceOutputLookup'][contains(@title,'"+nombreCotizacion+"')]");
		System.err.println("Elemento "+"//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')][contains(text(),'"+nombreCotizacion+"')]");
		//element = driver.findElement(By.xpath("//a[@class='slds-truncate outputLookupLink slds-truncate forceOutputLookup'][contains(@title,'"+nombreCotizacion+"')]"));	
		//element = driver.findElement(By.xpath("(//a[contains(@title,'"+nombreCotizacion+"')])[2]"));
		String ruta = "//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')][contains(text(),'"+nombreCotizacion+"')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnAgregarProductos
	 * @info *** Boton Agregar Productos Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnAgregarProductos(WebDriver driver) {
		//element = driver.findElement(By.linkText("Agregar productos"));
		//element = driver.findElement(By.xpath("//div[@title='Agregar productos']"));
		element = driver.findElement(By.xpath("//div[@title='Agregar productos']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 10 | AXITY
	 * @param btnModificarProductos
	 * @info *** Boton Modificar Productos Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnModificarProductos(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@title='Agregar productos']"));
		element = driver.findElement(By.linkText("Modificar productos"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnNuevoDescuento
	 * @info *** Boton Nuevo Descuento Productos Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnNuevoDescuento(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[2]/article/div/div/div/ul/li/a"));
		element = driver.findElement(By.xpath("//div[@class='container']//div[2]//article[1]//div[1]//div[1]//div[1]//ul[1]//li[1]//a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param selectListaDePrecio
	 * @info *** Select Lista de Precios Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement selectListaDePrecio(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@class='select']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnGuardarSeleccionarListaDePrecio
	 * @info *** Boton Guardar Seleccionar Lista De Precio  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarSeleccionarListaDePrecio(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[2]/div/div[3]/button[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param inputBusquedaProducto
	 * @info *** Input Busqueda Producto Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement inputBusquedaProducto(WebDriver driver) {

		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	

	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnSiguienteAgregarProducto
	 * @info *** Boton Siguiente Agregar Producto  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnSiguienteAgregarProducto(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Siguiente')]"));
		return element;
	}

	/**
	 * @author Francisco Anticoy | 2018 06 09 | AXITY
	 * @param inputNombreCotizacion
	 * @info *** Input Campo Nombre Cotizacion
	 * @return WebElement 
	 */
	public static WebElement inputNombreCotizacion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'Nombre de cotización')]//following::input[@class=' input'])[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 10 09 | AXITY
	 * @param inputNombreCotizacionSCN
	 * @info *** Input Campo Nombre Cotizacion Escaenario
	 * @return WebElement 
	 */
	public static WebElement inputNombreCotizacionSCN(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	

	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param inputCantidadProducto
	 * @info *** Input Cantidad Producto Cotizacion Oportunidades Boton
	 * @return WebElement 
	 */
	public static WebElement inputCantidadProducto(WebDriver driver) {
		//element = driver.findElement(By.xpath("//tr//td[2]//span[1]//span[2]//button[1]"));
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/span[1]/span[2]/button[1]"));
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param inputCantidadProductoIngreso
	 * @info *** Input Cantidad Producto Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputCantidadProductoIngreso(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//td[2]/div/div/div/div/div/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 06 | AXITY
	 * @param btnGuardarAgregarProducto
	 * @info *** Boton Guardar Agregar Producto  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarAgregarProducto(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[3]/div/button[3]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 07 | AXITY
	 * @param btnGuardarAgregarDcto
	 * @info *** Boton Guardar Agregar Dcto  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarAgregarDcto(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Descuentos')]//following::a[@title='Nuevo'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 07 | AXITY
	 * @param inputTipoDescuento
	 * @info *** Input tipo descuento Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputTipoDescuento(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Descuentos...']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 07 | AXITY
	 * @param inputPorcentajeDescuento
	 * @info *** Input porcentaje descuento Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputPorcentajeDescuento(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='8']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 07 | AXITY
	 * @param btnGuardarAgregarDescuento
	 * @info *** Boton Guardar Agregar Producto  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarAgregarDescuento(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@title='Guardar']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 07 | AXITY
	 * @param txtProductoCotizacionValidar
	 * @info *** Texto Producto Cotizacion Validar  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement txtProductoCotizacionValidar(WebDriver driver) {
		element = driver.findElement(By.xpath("//tbody/tr/th/div/a"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 07 | AXITY
	 * @param txtDescuentoCotizacionValidar
	 * @info *** Texto Descuento Cotizacion Validar  Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement txtDescuentoCotizacionValidar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[2]/article/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr/td/span"));
		return element;
	}


	/**
	 * @author Francisco Anticoy | 2018 07 09 | AXITY
	 * @param btnInputCantidadProducto
	 * @info *** Boton de Input Cantidad Producto 
	 * @return WebElement 
	 */
	public static WebElement btnInputCantidadProducto(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/span[1]/span[2]/button[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 06 09 | AXITY
	 * @param btnGuardarProductosCotizacion
	 * @info *** Click Boton 'Guardar' Productos Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnGuardarProductosCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[3]"));
		return element;
	}

	
	/**
	 * @author Francisco Anticoy | 2018 06 09 | AXITY
	 * @param txtValidarProducto
	 * @info *** Texto Productos de la Cotizacion
	 * @return WebElement 
	 */
	public static WebElement txtValidarProducto(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/article[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/div[1]/a[1]"));
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/article[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/div[1]/a[1]"));										
		return element;
	}
	
	
	/**
	 * @author Francisco Anticoy | 2018 06 09 | AXITY
	 * @param txtValidarProductoSCN
	 * @info *** Texto Productos de la Cotizacion Esacenario
	 * @return WebElement 
	 */
	public static WebElement txtValidarProductoSCN(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/article[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/div[1]/a[1]"));										
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 01 | AXITY
	 * @param selectSeleccionVistaLista
	 * @info *** Select de Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement selectSeleccionVistaLista(WebDriver driver) {
		String ruta = "//div[@class='triggerLinkTextAndIconWrapper slds-p-right--x-large']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 01 | AXITY
	 * @param selectTodasLasOportunidades
	 * @info *** Select Todas Las Oportunidades Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement selectTodasLasOportunidades(WebDriver driver) {
		element = driver.findElement(By.linkText("Todas las oportunidades"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param elementoVistosRecientemente
	 * @info *** Elemento 'Vistos recientemente' de Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement elementoVistosRecientemente(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(@class,'virtualAutocompleteOptionText')][contains(text(),'Vistos recientemente')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param linkRegistrarLlamada
	 * @info *** Elemento link Registrar Llamada Pagina Principal Oportunidades
	 * @return WebElement 
	 */
	public static WebElement linkRegistrarLlamada(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Registrar una llamada')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param btnAgregar
	 * @info *** Boton Agregar para Input Asunto
	 * @return WebElement 
	 */
	public static WebElement btnAgregar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@title='Agregar']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param inputAsunto
	 * @info *** Input Asunto Ingreso LLamada
	 * @return WebElement 
	 */
	public static WebElement inputAsunto(WebDriver driver) {
		element = driver.findElement(By.xpath("//div/div/div/div/div/lightning-grouped-combobox/div/div/lightning-base-combobox/div/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param elementoLLamadaTelefonica
	 * @info *** Elemento LLamada Telefonica Asunto
	 * @return WebElement 
	 */
	public static WebElement elementoLLamadaTelefonica(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[2]/section[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/lightning-grouped-combobox[1]/div[1]/div[1]/lightning-base-combobox[1]/div[1]/div[1]/div[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param inputComentarios
	 * @info *** Elemento LLamada Telefonica Asunto
	 * @return WebElement 
	 */
	public static WebElement inputComentarios(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 02 | AXITY
	 * @param btnGuardarLLamada
	 * @info *** Boton Guardar LLamada Telefonica
	 * @return WebElement 
	 */
	public static WebElement btnGuardarLLamada(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[contains(@class,'bottomBarRight slds-col--bump-left')]//span[contains(@class,'label bBody')][contains(text(),'Guardar')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 03 | AXITY
	 * @param resultadoTexto
	 * @info *** Texto Verde Resultado Creacion
	 * @return WebElement 
	 */
	public static WebElement resultadoTexto(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]"));
		element = driver.findElement(By.xpath("//img[contains(@title,'Llamada registrada')]//following::a[@class='subjectLink slds-truncate']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 05 | AXITY
	 * @param inputMonto
	 * @info *** Input Monto Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputMonto(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Monto')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 05 | AXITY
	 * @param inputMonto
	 * @info *** Input Monto Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputCotizacionDescuento(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Cotización')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 10 | AXITY
	 * @param txtNombreProductoVerificar
	 * @info *** Texto Producto Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement txtNombreProductoVerificar(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/span[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 10 | AXITY
	 * @param btnCerrarModificarProducto
	 * @info *** Boton Modificar Producto Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnCerrarModificarProducto(WebDriver driver) {
		element = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[2]/div/div[3]/div/button[1]/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param inputMontoDescuento
	 * @info *** Input Monto descuento Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement inputMontoDescuento(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='7']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param linkDetalles
	 * @info *** Link Detalles Cotizacion Oportunidades Ingreso
	 * @return WebElement 
	 */
	public static WebElement linkDetalles(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Detalles']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param inputBusquedaMarcaPreprod
	 * @info *** Input Busqueda Marca Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement inputBusquedaMarcaPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div/div[2]/div/div/div/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='4']"));.
		String ruta = "//label[@class='uiLabel-left form-element__label uiLabel']//span[contains(text(),'Marca')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param elementoBusquedaMarcaPreprod
	 * @info *** Elemento Busqueda Marca Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement elementoBusquedaMarcaPreprod(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param inputBusquedaProductoPreprod
	 * @info *** Input Busqueda Producto Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement inputBusquedaProductoPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div/div[2]/div/div/div/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='9']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param elementoBusquedaProductoPreprod
	 * @info *** Elemento Busqueda Producto Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement elementoBusquedaProductoPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param inputBusquedaModeloPreprod
	 * @info *** Input Busqueda Modelo Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement inputBusquedaModeloPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[2]/div/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//div[3]/div/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='6']"));
		String ruta = "//label[@class='uiLabel-left form-element__label uiLabel']//span[contains(text(),'Modelo /')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param elementoBusquedaModeloPreprod
	 * @info *** Elemento Busqueda Modelo Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement elementoBusquedaModeloPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/label[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param inputBusquedaAccesorioPreprod
	 * @info *** Input Busqueda Accesorio Cotizacion Oportunidades Pre Prod
	 * @return WebElement 
	 */
	public static WebElement inputBusquedaAccesorioPreprod(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[2]/div[2]/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//div[3]/div[2]/div/div/div[2]/div/input"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='7']"));
		String ruta = "//label[@class='uiLabel-left form-element__label uiLabel']//span[contains(text(),'Accesorio')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 10 | AXITY
	 * @param btnGuardarProducto
	 * @info *** Boton Gusradar Producto Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarProducto(WebDriver driver) {
		String ruta = "//button[contains(text(),'Guardar')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param lapizModificarVentaPara
	 * @info *** Lapiz Modificar Venta Para Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement lapizModificarVentaPara(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@title = 'Modificar Venta para']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param inputModificarVentaPara
	 * @info *** Input Modificar Venta Para Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement inputModificarVentaPara(WebDriver driver) {
		String ruta = "//div[2]/div/div/div[2]/div/div/div/div/div/div/div/div/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param btnGuardarModificarVentaPara
	 * @info *** Boton Guardar Venta Para Cotizacion OPortunidades
	 * @return WebElement 
	 */
	public static WebElement btnGuardarModificarVentaPara(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[4]/div/div[2]/div/div/button[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param imgCheckModificarVentaPara
	 * @info *** Imagen Ticket Venta Para Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement imgCheckModificarVentaPara(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//img[@class=' checked'])[3]"));
		element = driver.findElement(By.xpath("//div[2]/div[2]/div/div[2]/span/span/img"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param txtError
	 * @info *** Texto Error Para Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement txtError(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//img[@class=' checked'])[3]"));
		element = driver.findElement(By.xpath("//section/div/div/div/div/div/div/div"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 11 06 | AXITY
	 * @param detalleTxtError
	 * @info *** Detalle Texto Error Para Cotizacion Oportunidades
	 * @return WebElement 
	 */
	public static WebElement detalleTxtError(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//img[@class=' checked'])[3]"));
		element = driver.findElement(By.xpath("//section/div/div/div/div/div/div/span"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 05 02 | AXITY
	 * @param inputPrecioOportunidad
	 * @info *** Input Precio Oportunidad
	 * @return WebElement 
	 */
	public static WebElement inputPrecioOportunidad(WebDriver driver) {
		String ruta = "//label[contains(@class,'uiLabel-left form-element__label uiLabel')]/following::input[@min='-99999999999999']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 05 09 | AXITY
	 * @param txtNumeroCotizacion
	 * @info *** Texto Numero Cotizacion
	 * @return WebElement 
	 */
	public static WebElement txtNumeroCotizacion(WebDriver driver) {
		String ruta = "(//span[@title='Número de cotización']//following::span[@class='uiOutputText'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 09 | AXITY
	 * @param txtNombreCotizacionPaginaPrincipal
	 * @info *** Texto Nombre Cotizacion Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement txtNombreCotizacionPaginaPrincipal(WebDriver driver) {
//		element = driver.findElement(By.xpath("(//span[contains(text(),'Cotizaci�n')]//following::span[@class='uiOutputText'])[1]"));
		String ruta = "(//div[contains(text(),'Cotización')]//following::span[@class='uiOutputText'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param pestanaZoura
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement pestanaZoura(WebDriver driver) {
		String ruta = "//*[@id='Suscipcion__item']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param inputSeleccionSubcripcion
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement inputSeleccionSubcripcion(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param selectSubcripcion
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement selectSubcripcion(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]/option[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param inputProductoSubcripcion
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement inputProductoSubcripcion(WebDriver driver) {
		String ruta = "//*[@id='Suscipcion']/div/div[3]/div[1]/div/label/span//following::select/option[contains(text(),Seleccionar)]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param optionProducto
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement optionProducto(WebDriver driver) {
		//*[@id='Suscipcion']/div/div[3]/div[1]/div/label/span//following::select/option[contains(text(),Seleccionar)][1]//following::option[1]
		//*[@id='Suscipcion']/div/div[3]/div[1]/div/label/span//following::select/option[contains(text(),Seleccionar)][3]
		String ruta = "//*[@id='Suscipcion']/div/div[3]/div[1]/div/label/span//following::select/option[contains(text(),Seleccionar)][1]//following::option[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param inputPlan
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement inputPlan(WebDriver driver) {
		String ruta = "//*[@id='Suscipcion']/div/div[3]/div[2]/div/label/span//following::select/option[@value='Seleccionar']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrián Siñia | 2019 11 11 | AXITY
	 * @param selectPlan
	 * @info *** Seleccionar pestaña de Cotización Zuora
	 * @return WebElement 
	 */
	public static WebElement selectPlan(WebDriver driver, String Plan) {
		//*[@id='Suscipcion']/div/div[3]/div[2]/div/label/span//following::select/option[contains(text(),'" + Plan + "')]
		//*[@id='Suscipcion']/div/div[3]/div[2]/div/label/span//following::select/option[2]
		String ruta = "//*[@id='Suscipcion']/div/div[3]/div[2]/div/label/span//following::select/option[contains(text(),'" + Plan + "')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
}
