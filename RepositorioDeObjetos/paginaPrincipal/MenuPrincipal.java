package paginaPrincipal;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuPrincipal {
	
	private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnCuentas
	 * @info *** Boton Cuentas Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCuentas(WebDriver driver) {
		//element = driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/one-appnav/div/one-app-nav-bar/nav/ul/li[3]/a/span"));
		element = driver.findElement(By.xpath("//a[@title='Cuentas']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnCrearCuentas
	 * @info *** Boton Crear Cuentas Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCrearCuentas(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[1]"));
		element = driver.findElement(By.xpath("//a[@title='Nuevo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnCrearCampanas
	 * @info *** Boton Crear Campanas Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCrearCampanas(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[1]"));
		String ruta = "//a[@title='Campañas']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param rdCuentasPersonales
	 * @info *** Boton Radio Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement rdCuentasPersonales(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div[1]/fieldset/div[2]/div[3]/label/div[1]/span"));
		//element = driver.findElement(By.xpath("(//label/div/span)[1]"));
		//(.//*[normalize-space(text()) and normalize-space(.)='Cuentas Personales'])[1]/preceding::span[1]
		//(//span[@class='slds-radio--faux topdown-radio--faux'])[2]
		//(//span[@class='slds-radio--faux'])[1]
		String ruta = "(//span[@class='slds-radio--faux'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param rdCuentasEmpresas
	 * @info *** Boton Radio Cuenta Empresa
	 * @return WebElement 
	 */
	public static WebElement rdCuentasEmpresas(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[1]/label[1]/div[1]/span[1]"));
		//element = driver.findElement(By.xpath("(//label/div/span)[3]"));
		//(.//*[normalize-space(text()) and normalize-space(.)='Cuentas Empresas'])[1]/preceding::span[1]
		//(//span[@class='slds-radio--faux topdown-radio--faux'])[1]
		String ruta = "(//span[@class='slds-radio--faux'])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnSiguienteCrearCuenta
	 * @info *** Boton Siguiente Crear Cuenta
	 * @return WebElement 
	 */
	public static WebElement btnSiguienteCrearCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Siguiente')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 22 | AXITY
	 * @param btnLead
	 * @info *** Boton Lead Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/one-appnav/div/one-app-nav-bar/nav/ul/li[2]/a/span"));
		String ruta = "//a[@title='Leads']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 22 | AXITY
	 * @param btnCrearLead
	 * @info *** Boton Crear Lead Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCrearLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[1]"));
		String ruta = "//a[@title='Nuevo']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 22 | AXITY
	 * @param rdLeadPersona
	 * @info *** Boton Radio Lead Personal
	 * @return WebElement 
	 */
	public static WebElement rdLeadPersona(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[1]/label[1]/div[1]"));
		String ruta = "//span[contains(text(),'Lead Persona')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 22 | AXITY
	 * @param rdLeadEmpresa
	 * @info *** Boton Radio Lead Empresa
	 * @return WebElement 
	 */
	public static WebElement rdLeadEmpresa(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[3]/label[1]/div[1]/span[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Lead Empresa')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 22 | AXITY
	 * @param rdLeadUsado
	 * @info *** Boton Radio Lead Usado
	 * @return WebElement 
	 */
	public static WebElement rdLeadUsado(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[5]/label[1]/div[1]/span[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Lead Usado')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnSiguienteCrearLead
	 * @info *** Boton Siguiente Crear Lead
	 * @return WebElement 
	 */
	public static WebElement btnSiguienteCrearLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[2]/span[1]"));
		String ruta = "//button/span[contains(text(),'Siguiente')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param btnOportunidades
	 * @info *** Boton Crear Oportunidad 
	 * @return WebElement 
	 */
	public static WebElement btnOportunidades(WebDriver driver) {
		//element = driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/one-appnav/div/one-app-nav-bar/nav/ul/li[5]/a/span"));	
		String ruta = "//a[@title='Oportunidades']";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 08 21 | AXITY
	 * @param btnCrearOportunidad
	 * @info *** Boton Crear Oportunidad
	 * @return WebElement 
	 */
	public static WebElement btnCrearOportunidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[1]"));						
		element = driver.findElement(By.xpath("//div[@class='slds-truncate'][contains (text(),'Nuevo')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param btnVerPerfil
	 * @info *** Boton Imagen Ver Perfil
	 * @return WebElement 
	 */
	public static WebElement btnVerPerfil(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[1]/section[1]/header[1]/div[2]/span[1]/ul[1]/li[9]/button[1]/div[1]/span[1]/div[1]/span[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param txtNombrePerfil
	 * @info ***Texto Nombre Perfil
	 * @return WebElement 
	 */
	public static WebElement txtNombrePerfil(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/h1[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param btnCotizaciones
	 * @info ***Boton Cotizciones Menu Principal
	 * @return WebElement 
	 */
	public static WebElement btnCotizaciones(WebDriver driver) {
		//element = driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/one-appnav/div/one-app-nav-bar/nav/ul/li[6]/a/span"));
		element = driver.findElement(By.xpath("//a[@title='Cotizaciones']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 04 | AXITY
	 * @param textoNotificacionesVerde
	 * @info ***Texto de Notificaciones Verde Acciones Correctas
	 * @return WebElement 
	 */
	public static WebElement textoNotificacionesVerde(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]"));
		String ruta = "//span[@class='toastMessage forceActionsText']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 04 | AXITY
	 * @param textoNotificacionesVerde
	 * @info ***Texto de Notificaciones Verde Acciones Correctas
	 * @return WebElement 
	 */
	public static WebElement textoNotificacionesVerdeDos(WebDriver driver) {
		element = driver.findElement(By.xpath("//section/div/div/div/div"));
		//element = driver.findElement(By.xpath("//section/div/div/div[2]/div/div/div/span"));
		//element = driver.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 04 | AXITY
	 * @param textoNotificacionesEvaluar
	 * @info ***Texto de Notificaciones Verde Acciones Correctas
	 * @return WebElement 
	 */
	public static WebElement textoNotificacionesEvaluar(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='toastMessage forceActionsText']"));
		//element = driver.findElement(By.xpath("//section/div/div/div[2]/div/div/div/span"));
		//element = driver.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 04 | AXITY
	 * @param textoNotificacionesVerde
	 * @info ***Texto de Notificaciones Verde Acciones Correctas
	 * @return WebElement 
	 */
	public static WebElement textoNotificacionesVerdePequeno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]"));
		String ruta = "//span[@class='toastMessage slds-text-heading--small forceActionsText']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 22 | AXITY
	 * @param btnBorrarTodo
	 * @info ***Boton Borrar Todo Llamados Anteriores
	 * @return WebElement 
	 */
	public static WebElement btnBorrarTodo(WebDriver driver) {
		String ruta = "//*[@id='oneHeader']/div[2]/span/ul/li[7]/div[1]/div[1]/a|//*[@id='oneHeader']//a[contains(text(),'Borrar todo')]";
		element = driver.findElement(By.xpath(ruta));
		return element;	
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 18 | AXITY
	 * @param inputBuscarPrincipal
	 * @info ***Input Buscar Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement inputBuscarPrincipal(WebDriver driver) {
		String ruta = "//input[@title='Buscar Cuentas y más']";
		element = driver.findElement(By.xpath(ruta));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2019 06 18 | AXITY
	 * @param inputBuscarPaginaPrincipal
	 * @info ***Input Buscar Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement inputBuscarPaginaPrincipal(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar en Salesforce']"));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2019 06 18 | AXITY
	 * @param linkInicioPaginaPrincipal
	 * @info ***Input Buscar Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement linkInicioPaginaPrincipal(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Inicio']"));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2018 12 18 | AXITY
	 * @param inputBuscarOrdenPrincipal
	 * @info ***Input Buscar Ordenes Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement inputBuscarOrdenPrincipal(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar �rdenes de trabajo y m�s']"));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2018 12 18 | AXITY
	 * @param linkRegistroResultadoBusqueda
	 * @info ***Link Registro Resultado Busqueda
	 * @return WebElement 
	 */
	public static WebElement linkRegistroResultadoBusqueda(WebDriver driver, String nombreCuenta) {
//		element = driver.findElement(By.xpath("//a[@title='"+nombreCuenta+"']"));
//		element = driver.findElement(By.xpath("(//a[contains(text(),'"+nombreCuenta+"')])"));
		element = driver.findElement(By.linkText(nombreCuenta));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param txtNohayResutados
	 * @info ***Texto No hay Resutados Busqueda
	 * @return WebElement 
	 */
	public static WebElement txtNohayResutados(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='slds-text-heading--large noResultsTitle slds-p-bottom--large']"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param btnMenuAplicaciones
	 * @info ***Boton Menu Aplicaciones
	 * @return WebElement 
	 */
	public static WebElement btnMenuAplicaciones(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"));
		String ruta = "//one-app-launcher-header/button/div|//div[@class='slds-icon-waffle']";
		element = driver.findElement(By.xpath(ruta));
		return element;	}
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param btnMenuAplicacionesAlternativo
	 * @info ***Boton Menu Aplicaciones Alternativo
	 * @return WebElement 
	 */
	public static WebElement btnMenuAplicacionesAlternativo(WebDriver driver) {
		//element = driver.findElement(By.xpath("//one-app-launcher-header/button/div"));
		element = driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param linkSSTTMenuAplicaciones
	 * @info ***Link SSTT Menu Aplicaciones
	 * @return WebElement 
	 */
	public static WebElement linkSSTTMenuAplicaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='slds-app-launcher__tile-body']//a[@class='appTileTitle'][contains(text(),'Servicio T�cnico')]"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnordenesdetrabajo
	 * @info *** Boton Oredenes de Trabajo Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnordenesdetrabajo(WebDriver driver) {
		//element = driver.findElement(By.xpath("//*[@id=\"oneHeader\"]/div[3]/one-appnav/div/one-app-nav-bar/nav/ul/li[3]/a/span"));
		element = driver.findElement(By.xpath("//a[@title='�rdenes de trabajo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param linkServicioMenuAplicaciones
	 * @info ***Link Servicio Menu Aplicaciones
	 * @return WebElement 
	 */
	public static WebElement linkServicioMenuAplicaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("(//div[@class='slds-app-launcher__tile-body']//a[@class='appTileTitle'][contains(text(),'Servicio al Cliente')])[1]"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2019 10 02 | AXITY
	 * @param linkMarketingMenuAplicaciones
	 * @info ***Link Marketing Menu Aplicaciones
	 * @return WebElement 
	 */
	public static WebElement linkMarketingMenuAplicaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[contains(text(),'Marketing')]"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2014 01 04 | AXITY
	 * @param linkConsolaServicioMenuAplicaciones
	 * @info ***Link Consola Servicio Menu Aplicaciones
	 * @return WebElement 
	 */
	public static WebElement linkConsolaServicioMenuAplicaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("(//div[@class='slds-app-launcher__tile-body']//a[@class='appTileTitle'][contains(text(),'Consola de Servicio al Cliente')])[1]"));
		return element;	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnMostrarMenuNavegacionConsola
	 * @info *** Boton Mostrar Menu Navegacion Consola
	 * @return WebElement 
	 */
	public static WebElement btnMostrarMenuNavegacionConsola(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='slds-context-bar__icon-action']|//button[@class='slds-button slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 08 19 | AXITY
	 * @param btnMostrarMenuNavegacionConsolaAlternativo
	 * @info *** Boton Mostrar Menu Navegacion Consola Alternativo
	 * @return WebElement 
	 */
	public static WebElement btnMostrarMenuNavegacionConsolaAlternativo(WebDriver driver) {
		//element = driver.findElement(By.xpath("//button[@class='slds-button slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']"));
		element = driver.findElement(By.xpath("//button[@title='Mostrar men� de navegaci�n']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param btnContactos
	 * @info *** Boton Contactos Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnContactos(WebDriver driver) {
	element = driver.findElement(By.xpath("//a[@title='Contactos']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param btnCrearContactos
	 * @info *** Boton Crear Contactos Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCrearContactos(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Nuevo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 02 | AXITY
	 * @param btnNuevaCampana
	 * @info *** Boton Nueva Campa�a Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnNuevaCampana(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Nuevo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 02 | AXITY
	 * @param btnModificarCampana
	 * @info *** Boton Modificar Campa�a Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnModificarCampana(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Modificar']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param inputBuscarContactos
	 * @info *** Input Buscar Contactos Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement inputBuscarContactos(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar Contactos y m�s']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param seleccionCuenta
	 * @info *** Seleccion Cuenta Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement seleccionCuenta(WebDriver driver, String cuenta) {
		String ruta = "(//h2//a[contains(text(),'Cuentas')]//following::a[@title='"+cuenta+"'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param linkRevisionNumeroCaso
	 * @info *** Link Revision Numero Caso
	 * @return WebElement 
	 */
	public static WebElement linkRevisionNumeroCaso(WebDriver driver, String numeroDeCaso) {
		element = driver.findElement(By.xpath("//article[contains(@class,'slds-card slds-card_boundary related_list_card_border_top forceRelatedListCardDesktop')]//*[contains(@title,'Caso')]//following::a[contains(text(),'"+numeroDeCaso+"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param linkBuscarNumeroCaso
	 * @info *** Link Buscar Numero Caso
	 * @return WebElement 
	 */
	public static WebElement linkBuscarNumeroCaso(WebDriver driver, String numeroDeCaso) {
		element = driver.findElement(By.xpath("//a[contains(text(),'Casos')]//following::a[contains(text(),'"+numeroDeCaso+"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param linkContactoEncontrado
	 * @info *** Link Contactos Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement linkContactoEncontrado(WebDriver driver, String buscarContacto) {
		element = driver.findElement(By.xpath("(//span[@class='slds-grid slds-grid--align-spread']//a[@title='"+buscarContacto+"'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param linkActividad
	 * @info ***Link Actividad
	 * @return WebElement 
	 */
	public static WebElement linkActividad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a[@title='Actividad'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 19 | AXITY
	 * @param linkNumeroCaso
	 * @info ***Link Numero Caso
	 * @return WebElement 
	 */
	public static WebElement linkNumeroCaso(WebDriver driver , String numeroDeCaso) {
		element = driver.findElement(By.xpath("(//h2//a[contains(text(),'Casos')]//following::a[@title='"+numeroDeCaso+"'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 19 | AXITY
	 * @param linkActivo
	 * @info ***Link Numero Caso
	 * @return WebElement 
	 */
	public static WebElement linkActivo(WebDriver driver , String nombreActivo) {
		element = driver.findElement(By.xpath("(//h2//a[contains(text(),'Activos')]//following::a[@title='"+nombreActivo+"'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 19 | AXITY
	 * @param linkCuenta
	 * @info ***Link Cuenta
	 * @return WebElement 
	 */
	public static WebElement linkCuenta(WebDriver driver , String cuenta) {
		element = driver.findElement(By.xpath("(//h2//a[contains(text(),'Cuentas')]//following::a[@title='"+cuenta+"'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 21 | AXITY
	 * @param btnEstadoEnProceso
	 * @info *** Boton Estado En Proceso Menu Caso
	 * @return WebElement 
	 */
	public static WebElement btnEstadoEnProceso(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='En Proceso']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 21 | AXITY
	 * @param btnEstadoPropuestaEnviada
	 * @info *** Boton Estado Propuesta Enviada Menu Caso
	 * @return WebElement 
	 */
	public static WebElement btnEstadoPropuestaEnviada(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Propuesta Enviada']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param linkMarcarEstado
	 * @info *** Link Marcar Estado
	 * @return WebElement 
	 */
	public static WebElement linkMarcarEstado(WebDriver driver) {
		//element = driver.findElement(By.xpath("//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction current uiButton']"));
		//element = driver.findElement(By.xpath("//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction active uiButton']"));
		element = driver.findElement(By.xpath("//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction active uiButton']|//button[@class='slds-button slds-button--brand slds-path__mark-complete stepAction current uiButton']"));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 25 | AXITY
	 * @param menuNavegacion
	 * @info *** Mostrar menu de navegacion
	 * @return WebElement 
	 */
	public static WebElement menuNavegacion(WebDriver driver) {
		String ruta = "//div/div/button[@type='button'][@title='Mostrar menú de navegación']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 25 | AXITY
	 * @param seleccionarMenu
	 * @info *** Mostrar menu de navegacion
	 * @return WebElement 
	 */
	public static WebElement seleccionarMenu(WebDriver driver, String opcionMenu) {
		String ruta = "//span/span[contains(text(),'" + opcionMenu + "')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 11 25 | AXITY
	 * @param btnAplicaciones
	 * @info *** Mostrar menu de navegacion
	 * @return WebElement 
	 */
	public static WebElement btnAplicaciones(WebDriver driver) {
		String ruta = "//button[@class='slds-button']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 11 25 | AXITY
	 * @param linkAppVentas
	 * @info *** Mostrar menu de navegacion
	 * @return WebElement 
	 */
	public static WebElement linkAppVentas(WebDriver driver) {
		String ruta = "//a[contains(text(),'Ventas')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
}
