package principalLead;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PrincipalLead {
	
private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param selectSeleccionVistaLista
	 * @info *** Select de Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement selectSeleccionVistaLista(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='triggerLinkTextAndIconWrapper slds-p-right--x-large']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param elementoVistosRecientemente
	 * @info *** Elemento 'Vistos recientemente' de Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement elementoVistosRecientemente(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(@class,'virtualAutocompleteOptionText')][contains(text(),'Vistos recientemente')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param elementoTodosLosCandidatosAbiertos
	 * @info *** Elemento 'Todos Los Candidatos Abiertos' de Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement elementoTodosLosCandidatosAbiertos(WebDriver driver) {
		//element = driver.findElement(By.xpath("//span[contains(text(),'Todos los candidatos abiertos')]"));
		element = driver.findElement(By.xpath("//span[contains(@class,'virtualAutocompleteOptionText')][contains(text(),'Todos los leads abiertos')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param inputBuscarVistas
	 * @info *** Input 'BUSCAR' Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement inputBuscarVistas(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param primerElementoSeleccionBuscarVistas
	 * @info *** Selecciona el primer elemento desplegado en 'BUSCAR' Seleccion de Vista de Listas
	 * @return WebElement 
	 */
	public static WebElement primerElementoSeleccionBuscarVistas(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]/span[1]/mark[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param selectMostrar3AccionesMas
	 * @info *** Select boton Mostrar 3 acciones mas en registro
	 * @return WebElement 
	 */
	public static WebElement selectMostrar3AccionesMas(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[10]/span[1]/div[1]/a[1]/lightning-icon[1]/lightning-primitive-icon[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param lnkCambiarPropietario
	 * @info *** Link Cambiar Propietario 
	 * @return WebElement 
	 */
	public static WebElement lnkCambiarPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[contains(@title,'Cambiar propietario')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param inputBuscarNuevoPropietario
	 * @info *** Campo de busqueda nuevo propietario
	 * @return WebElement 
	 */
	public static WebElement inputBuscarNuevoPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 28 | AXITY
	 * @param btnEnviarNuevoPropietario
	 * @info *** Boton 'Enviar' nuevo propietario
	 * @return WebElement 
	 */
	public static WebElement btnEnviarNuevoPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/button[2]/span[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 29 | AXITY
	 * @param txtErrorUsuarioSinPermisos 
	 * @info *** Texto de Error, usario  No posee los permisos suficientes para cambiar el responsable del candidato
	 * @return WebElement 
	 */
	public static WebElement txtErrorUsuarioSinPermisos(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/p[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 29 | AXITY
	 * @param linkPrimerRegistroDesplegado
	 * @info *** Link Primer Registro Desplegado en Pagina Principal Lead
	 * @return WebElement 
	 */
	public static WebElement linkPrimerRegistroDesplegado(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/span[1]/a[1]"));
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/span[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 29 | AXITY
	 * @param linkPrimerRegistroDesplegadoCerrar
	 * @info *** Link Primer Registro Desplegado en Pagina Principal Lead despues de cerrar
	 * @return WebElement 
	 */
	public static WebElement linkPrimerRegistroDesplegadoCerrar(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[1]/th/span/a"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 29 | AXITY
	 * @param btnModificarLead
	 * @info *** Boton Modificar Lead 
	 * @return WebElement 
	 */
	public static WebElement btnModificarLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='Modificar']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 29 | AXITY
	 * @param selectMarcadeInteres
	 * @info *** Select Marca de interes
	 * @return WebElement 
	 */
	public static WebElement selectMarcadeInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param selectModelodeInteres
	 * @info *** Select Marca de interes
	 * @return WebElement 
	 */
	public static WebElement selectModelodeInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param selectVersiondeInteres
	 * @info *** Select Marca de interes
	 * @return WebElement 
	 */
	public static WebElement selectVersiondeInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param btnGuardarModificaciones
	 * @info *** Boton Guardar Pantalla Modificar
	 * @return WebElement 
	 */
	public static WebElement btnGuardarModificaciones(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@title='Guardar']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param btnEstadoCalificado
	 * @info *** Boton Estado Calificado Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnEstadoCalificado(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Calificado']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param btnMarcarComoEstadoDelLead
	 * @info *** Boton 'Marcar Como Estado Del Lead' Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnMarcarComoEstadoDelLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("//span[contains(text(),'Marcar como Estado de lead actual')]"));
		element = driver.findElement(By.xpath("//div[@class='runtime_sales_pathassistantPathAssistantHeader--desktop runtime_sales_pathassistantPathAssistantHeader']//button[@type='button']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param txtErrorGuardadoRegistro
	 * @info *** Texto Error Guardado Registro Menu Lead
	 * @return WebElement 
	 */
	public static WebElement txtErrorGuardadoRegistro(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param txtMensajeErrorGuardadoRegistro
	 * @info *** Texto Mensaje Error Guardado Registro Menu Lead
	 * @return WebElement 
	 */
	public static WebElement txtMensajeErrorGuardadoRegistro(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='toastMessage forceActionsText']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 30 | AXITY
	 * @param btnCerrarMensajeErrorGuardadoRegistro
	 * @info *** Boton Cerrar Mensaje Error Guardado Registro Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnCerrarMensajeErrorGuardadoRegistro(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/button[1]/lightning-primitive-icon[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param btnPestanaDetalles
	 * @info *** Boton Pesta�a Detalle Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnPestanaDetalles(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Detalles']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param txtEstadoLeadDetalle
	 * @info *** Texto Estado Lead Detalle Menu Lead
	 * @return WebElement 
	 */
	public static WebElement txtEstadoLeadDetalle(WebDriver driver) {
		//element = driver.findElement(By.xpath("//a[contains(text(),'Calificado')]"));
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param btnCambiarPropietarioDelLead
	 * @info *** Boton Cambiar Propietario del Lead en Detalle Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnCambiarPropietarioDelLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//span/div/button"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnDosAccionesMasMenuSuperior
	 * @info *** Boton Dos Acciones Mas Menu Superior Lead
	 * @return WebElement 
	 */
	public static WebElement btnDosAccionesMasMenuSuperior(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[2]/ul[1]/li[4]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnConvertirMenuSuperior
	 * @info *** Boton Convertir en Menu Superior Lead
	 * @return WebElement 
	 */
	public static WebElement btnConvertirMenuSuperior(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='Convertir']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 13 | AXITY
	 * @param btnCalificarLeadMenuSuperior
	 * @info *** Boton Calificar en Menu Superior Lead
	 * @return WebElement 
	 */
	public static WebElement btnCalificarLeadMenuSuperior(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Marcar como Estado de lead actual')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnDosAccionesMenuSuperior
	 * @info *** Boton Dos Acciones Mas en Menu Superior Lead
	 * @return WebElement 
	 */
	public static WebElement btnDosAccionesMenuSuperior(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Mostrar 2 acciones m�s']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnConvertirDosAccionesMenuSuperior
	 * @info *** Boton Convertir Dos Acciones Mas en Menu Superior Lead
	 * @return WebElement 
	 */
	public static WebElement btnConvertirDosAccionesMenuSuperior(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[7]/div[1]/ul[1]/li[1]/a[1]"));
		//element = driver.findElement(By.name("Convertir"));
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[9]/div[1]/ul[1]/li[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param chkNoCrearOportinidadTrasConvertir
	 * @info *** Check Box No Crear Oportunidad Tras Convertir
	 * @return WebElement 
	 */
	public static WebElement chkNoCrearOportinidadTrasConvertir(WebDriver driver) {
		//element = driver.findElement(By.xpath("//input[@id='opptyCheckBox']"));
		element = driver.findElement(By.xpath("//div[@class='checkboxContainer runtime_sales_leadConvertOpportunityCheckBox']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 31 | AXITY
	 * @param txtPropietarioLead
	 * @info *** Texto Propietario Lead
	 * @return WebElement 
	 */
	public static WebElement txtPropietarioLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//span/div/div/div/a"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param txtVerificarEstadoLead
	 * @info *** Texto Propietario Lead En Vista Recientes
	 * @return WebElement 
	 */
	public static WebElement txtVerificarEstadoLead(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[8]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnConvertirPopUpConvertir
	 * @info *** Boton Convertir en Pop Up Convertir
	 * @return WebElement 
	 */
	public static WebElement btnConvertirPopUpConvertir(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Convertir')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param txtVerificarConvertirPopUp
	 * @info *** Texto Propietario Lead En Vista Recientes
	 * @return WebElement 
	 */
	public static WebElement txtVerificarConvertirPopUp(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 03 | AXITY
	 * @param btnIrALeads
	 * @info *** Boton Convertir en Pop Up Convertir
	 * @return WebElement 
	 */
	public static WebElement btnIrALeads(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Ir a Leads')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param btnEstadoCerrado
	 * @info *** Boton Estado Cerrado Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnEstadoCerrado(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='title'][contains(text(),'Cerrado')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param selectSubEstadoCerrado
	 * @info *** Select Sub Estado en Cerrado Menu Lead
	 * @return WebElement 
	 */
	public static WebElement selectSubEstadoCerrado(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='input-4']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param elementoNoUbicableSubEstadoCerrado
	 * @info *** Elemento No Ubicable Sub Estado en Cerrado Menu Lead
	 * @return WebElement 
	 */
	public static WebElement elementoNoUbicableSubEstadoCerrado(WebDriver driver) {
		element = driver.findElement(By.xpath("//lightning-base-combobox-item[@id='input-4-3']/span[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 04 | AXITY
	 * @param btnListoCerrado
	 * @info *** Boton Listo Estado Cerrado Menu Lead
	 * @return WebElement 
	 */
	public static WebElement btnListoCerrado(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@type='submit']"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2018 09 04 | AXITY
	 * @param txtNombreCandidatoLead
	 * @info *** Texto Nombre de Candidato Lead En Detalles
	 * @return WebElement 
	 */
	public static WebElement txtNombreCandidatoLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]"));
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2019 03 04 | AXITY
	 * @param inputBuscarLead
	 * @info *** Input Buscar Lead
	 * @return WebElement 
	 */
	public static WebElement inputBuscarLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar Leads y m�s']"));
		return element;
	}
	
	/**
	 * @author Francisco Anticoy | 2019 03 04 | AXITY
	 * @param inputBuscarPaginaLead
	 * @info *** Input Buscar Pagina Lead
	 * @return WebElement 
	 */
	public static WebElement inputBuscarPaginaLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar en esta lista...']"));
		return element;
	}
}
