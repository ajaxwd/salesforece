package crearLead;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Empresa {
	
	private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectEstadoLead
	 * @info *** Campo ESTADO LEAD; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectEstadoLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param nuevoEstadoLead
	 * @info *** Elemento NUEVO en ESTADO LEAD CREACION LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement nuevoEstadoLead(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[9]/div[1]/ul[1]/li[2]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputDocumentoIdentidad
	 * @info *** Campo DOCUMENTO IDENTIDAD Creacion Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputDocumentoIdentidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//span[contains(text(),'Documento de Identidad')]//following::input[@class=' input'][@maxlength='15']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	
	
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputCompania
	 * @info *** Campo COMPA�IA  CREAR LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCompania(WebDriver driver) {
		//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Compañia')]//following::input[1]
		String ruta = "//span[contains(text(),'Compañía')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputSitioWeb
	 * @info *** Campo SITIO WEB  CREAR LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputSitioWeb(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Sitio Web')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectPrioridad
	 * @info *** Campo PRIORIDAD; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectPrioridad(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]|//span[contains(text(),'Prioridad')]//following::a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param medioInteresPrioridad
	 * @info *** Elemento MEDIO INTERES en PRIORIDAD CREACION LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement medioInteresPrioridad(WebDriver driver) {
		//Modificaci�n Ambiente SalesForce 27-09-2018
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[10]/div[1]/ul[1]/li[3]/a[1]"));
		String ruta = "//a[@title='Medio interés']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectOrigen
	 * @info *** Campo ORIGEN; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]|//span[contains(text(),'Origen')]//following::a[1]"));
		element = driver.findElement(By.xpath("(//*[contains(text(),'Origen')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param webOrigen
	 * @info *** Elemento WEB en ORIGEN CREACION LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement webOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[11]/div[1]/ul[1]/li[10]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Web']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputNumeroEmpleados
	 * @info *** Campo NUEMRO EMPLEADOS  CREAR LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputNumeroEmpleados(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Número de empleados')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectTratamiento
	 * @info *** Campo TRATAMIENTO; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectTratamiento(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]|//span[contains(text(),'Tratamiento')]//following::a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param srTratamiento
	 * @info *** Elemento SR en TRATAMIENTO CREACION LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement srTratamiento(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Sr.']"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[10]/div[1]/ul[1]/li[2]/a[1]"));
		return element;
	}
			
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputNombre
	 * @info *** Campo Nombre Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputNombre(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[2]/input[1]|//input[@placeholder='Nombre']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputApellidoPaterno
	 * @info *** Campo Apellido Paterno Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputApellidoPaterno(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[3]/input[1]|//input[@placeholder='Apellido 1']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputApellidoMaterno
	 * @info *** Campo Apellido Materno Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputApellidoMaterno(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Apellido 2')]//following::input[1]"));
		return element;
	}
		
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputReferidoPor
	 * @info *** Campo REFERIDO POR Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputReferidoPor(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Referido por')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputCorreo
	 * @info *** Campo CORREO Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCorreo(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]|//input[@type='email']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 24 | AXITY
	 * @param inputMovil
	 * @info *** Campo MOVIL Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputMovil(WebDriver driver) {
		//Modificaci�n Ambiente SalesForce 27-09-2018
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/input[1]|(//span[contains(text(),'Móvil')])[2]//following::input[1]
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/input[1]|(//span[contains(text(),'Móvil')])[2]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));									
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputTelefono
	 * @info *** Campo TELEFONO Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputTelefono(WebDriver driver) {
	    //element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]|(//span[contains(text(),'Tel�fono')])[2]//following::input[1]"));	
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]|(//span[contains(text(),'Tel�fono')])[2]//following::input[1]"));
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]|(//span[text()='Teléfono'])[2]//following::input[1]"));
		return element;
	}
		
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectMarcaInteres
	 * @info *** Campo MARCA DE INTERES; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectMarcaInteres(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]|(//span[contains(text(),'Producto')])//following::input[@placeholder='Buscar Productos...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param mazdaMarcaInteres
	 * @info *** Elemento MAZDA en MARCA DE INTERES LEAD EMPRESA
	 * @return WebElement 
	 */
	
	public static WebElement selectModeloInteres(WebDriver driver, String modeloInteres) {
		element = driver.findElement(By.xpath("//a[@title=" + modeloInteres + "]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectModeloInteres
	 * @info *** Campo Modelo DE INTERES; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
//	public static WebElement selectModeloInteres(WebDriver driver) {
//		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
//		return element;
//	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param mazdaMX5ModeloInteres
	 * @info *** Elemento MAZDA MX 5 en MODELO DE INTERES LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement mazdaMX5ModeloInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Mazda MX-5']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param selectVersion
	 * @info *** Campo VERSION; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectVersion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param ver206MTVersion
	 * @info *** Elemento 2.0L 6MT en VERSION LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement ver206MTVersion(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='2.0L 6MT (Cuero Negro - techo de lona)']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputLocal
	 * @info *** Campo Local Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputLocal(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputDescripcionLocal
	 * @info *** Campo Descripcion Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputDescripcionLocal(WebDriver driver) {
		//Modificaci�n Ambiente SalesForce 27-09-2018
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/textarea[1]"));
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/textarea[1]";
		element = driver.findElement(By.xpath(ruta));									
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputConcesionario
	 * @info *** Campo CONCESIONARIO ; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputConcesionario(WebDriver driver) {
		//"//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[5]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"
		//Modificaci�n Ambiente SalesForce 27-09-2018
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//label/span[contains(text(),'Concesionario')]/following::input[@title='Buscar Organizaciones']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param concesionarioSeleccion
	 * @info *** Seleccion elemento CONCESIONARIO Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement concesionarioSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='D001']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputCalle
	 * @info *** Campo Calle Local Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCalle(WebDriver driver) {
		String ruta = "(//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Dirección')]//following::input[@class=' input'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputNumeroCalle
	 * @info *** Campo Numero Calle Local Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputNumeroCalle(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]|(//span[contains(text(),'Número')])[3]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputDptoOficinaDireccion
	 * @info *** Campo Dpto Oficina Direccion Local Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputDptoOficinaDireccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Depto/Casa/OF')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputComplementoDireccion
	 * @info *** Campo Complemento Direccion Local Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputComplementoDireccion(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]|//span[contains(text(),'Referencia Dirección')]//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputPais
	 * @info *** elemento input PAIS LEAD EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputPais(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]|//input[@title='Buscar Paises']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param paisSeleccion
	 * @info *** elemento PAIS en InputPais LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement paisSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='Perú']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputRegion
	 * @info *** elemento input REGION LEAD EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputRegion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]|//input[@title='Buscar Departamentos']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param regionSeleccion
	 * @info *** elemento REGION en InputRegion LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement regionSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		element = driver.findElement(By.xpath("//div[@title='Lima']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputProvincia
	 * @info *** elemento input Provincia LEAD EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputProvincia(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar Provincias']"));
		return element;
	}

		/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param ProvinciaSeleccion
	 * @info *** elemento input Provincia LEAD EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement ProvinciaSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//mark[contains(text(),'LIMA')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param inputComuna
	 * @info *** elemento input COMUNA LEAD EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputComuna(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]|//input[@title='Buscar Distritos']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param comunaSeleccion
	 * @info *** elemento COMUNA en inputComuna LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement comunaSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='LIMA']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param btnGuardar
	 * @info *** Boton Guardar LEAD EMPRESA
	 * @return WebElement 
	 */
	public static WebElement btnGuardar(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[3]/span[1]|//button[@title='Guardar']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 24 | AXITY
	 * @param txtNombrePaginaInicio
	 * @info *** Texto Nombre Creado Lead EMPRESA
	 * @return WebElement 
	 */
	public static WebElement txtNombrePaginaInicio(WebDriver driver) {
		//Modificaci�n Ambiente SalesForce 27-09-2018
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]"));
		String ruta = "//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]|//h1/div/span";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 03 04 | AXITY
	 * @param inputMarcaInteres
	 * @info *** Campo Marca Interes Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputMarcaInteres(WebDriver driver) {
		String ruta = "//input[@placeholder='Buscar Productos...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
}

