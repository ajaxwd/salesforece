package crearLead;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Personal {
	
	private static WebElement element = null;
	private static List<WebElement> elements = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectEstadoLead
	 * @info *** Campo ESTADO LEAD; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectEstadoLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("//a[contains(text(),'Nuevo')]"));
		//element = driver.findElement(By.xpath("(//*[contains(text(),'Estado de lead')]/following::a[@class='select'][contains(text(),'Nuevo')])[1]"));
		String ruta = "//div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select']//div[@class='uiMenu']//div[@class='uiPopupTrigger']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param nuevoEstadoLead
	 * @info *** Elemento NUEVO en ESTADO LEAD CREACION LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement nuevoEstadoLead(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[9]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Nuevo'][contains(text(),'Nuevo')]"));
		//element = driver.findElement(By.xpath("(//a[@title='Nuevo'])[1]"));
		//element = driver.findElement(By.linkText("Nuevo"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputDocumentoIdentidad
	 * @info *** Campo DOCUMENTO IDENTIDAD Creacion Lead Persona
	 * @return WebElement 
	 */
	public static WebElement inputDocumentoIdentidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='11']"));
		//element = driver.findElement(By.xpath("//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']/following::input[@class=' input'][@maxlength='15']"));
		String ruta = "//span[contains(text(),'Documento de Identidad')]/following::input[@class=' input'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectTratamiento
	 * @info *** Campo TRATAMIENTO; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectTratamiento(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		element = driver.findElement(By.xpath("(//*[contains(text(),'Tratamiento')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 04 01 | AXITY
	 * @param selectDocumentoIdentidad
	 * @info *** Campo DOCUMENTO IDENTIDAD; aca se presenta como select pero no lo es 
	 * @return WebElement 
	 */
	public static WebElement selectDocumentoIdentidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		String ruta = "(//*[contains(text(),'Tipo de Documento')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 04 01 | AXITY
	 * @param dniDocumentoIdentidad
	 * @info *** elemento DOCUMENTO IDENTIDAD; 
	 * @return WebElement 
	 */
	public static WebElement tipoDocumentoIdentidad(WebDriver driver, String tipoDocumento) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='"+tipoDocumento+"']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param srTratamiento
	 * @info *** Elemento SR en TRATAMIENTO CREACION LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement srTratamiento(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[10]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Sr.']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectOrigen
	 * @info *** Campo ORIGEN; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		String ruta = "(//*[contains(text(),'Origen')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param webOrigen
	 * @info *** Elemento WEB en ORIGEN CREACION LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement webOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[11]/div[1]/ul[1]/li[10]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Chat']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputNombre
	 * @info *** Campo Nombre Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputNombre(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[2]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Nombre']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputApellidoPaterno
	 * @info *** Campo Apellido Paterno Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputApellidoPaterno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[3]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Apellido 1']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputApellidoMaterno
	 * @info *** Campo Apellido Materno Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputApellidoMaterno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		element = driver.findElement(By.xpath("(//*[contains(text(),'Apellido 2')]/following::input[@maxlength='40'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectPrioridad
	 * @info *** Campo PRIORIDAD; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectPrioridad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//div/span/span[contains(text(),'Prioridad')]//following::div/div/div/div/a[contains(text(),'--Ninguno--')]
		//(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]
		String ruta = "//div/span/span[contains(text(),'Prioridad')]//following::div/div/div/div/a[contains(text(),'--Ninguno--')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param medioInteresPrioridad
	 * @info *** Elemento MEDIO INTERES en PRIORIDAD CREACION LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement medioInteresPrioridad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[12]/div[1]/ul[1]/li[3]/a[1]"));
		String ruta = "//a[@title='Medio interés']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputReferidoPor
	 * @info *** Campo REFERIDO POR Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferidoPor(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='18']|//span[contains(text(),'Referido por')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputCorreo
	 * @info *** Campo CORREO Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputCorreo(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='20']"));
		element = driver.findElement(By.xpath("//*[contains(text(),'Correo')]/following::input[@type='email']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputMovil
	 * @info *** Campo MOVIL Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputMovil(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='21']"));
		//(//*[contains(text(),'Móvil')]/following::input[@maxlength='40'][@type='tel'])[1]
		String ruta ="//*[contains(text(),'vil')]/following::input[@maxlength='40'][@type='tel'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @info *** Campo TELEFONO Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputTelefono(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='22']"));
		String ruta = "(//*[contains(text(),'Teléfono')]/following::input[@maxlength='40'][@type='tel'])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 03 04 | AXITY
	 * @param inputMarcaInteres
	 * @info *** Campo Marca Interes Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputMarcaInteres(WebDriver driver) {
		String ruta = "//input[@placeholder='Buscar Productos...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectMarcaInteres
	 * @info *** Campo MARCA DE INTERES; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectMarcaInteres(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='22']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param mazdaMarcaInteres
	 * @info *** Elemento MAZDA en MARCA DE INTERES LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement mazdaMarcaInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[13]/div[1]/ul[1]/li[6]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputIdSap
	 * @info *** Campo ID SAP Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputIdSap(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectModeloInteres
	 * @info *** Campo Modelo DE INTERES; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectModeloInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param mazdaMX5ModeloInteres
	 * @info *** Elemento MAZDA MX 5 en MODELO DE INTERES LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement mazdaMX5ModeloInteres(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[14]/div[1]/ul[1]/li[5]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectVersion
	 * @info *** Campo VERSION; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectVersion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param ver206MTVersion
	 * @info *** Elemento 2.0L 6MT en VERSION LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement ver206MTVersion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[15]/div[1]/ul[1]/li[2]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputLocal
	 * @info *** Campo Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputLocal(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Centros...']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param localSeleccion
	 * @info *** elemento Local en InputRegion LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement localSeleccion(WebDriver driver, String ingresoLocal) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		element = driver.findElement(By.xpath("//div[contains(@title,'"+ingresoLocal+"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputDescripcionLocal
	 * @info *** Campo Descripcion Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputDescripcionLocal(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/textarea[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectConcesionarioWeb
	 * @info *** Campo CONCESIONARIO WEB; aca se presenta como select pero no lo es en realidad CREAR LEAD
	 * @return WebElement 
	 */
	public static WebElement selectConcesionarioWeb(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param ansaConcesionarioWeb
	 * @info *** Elemento ANSA en CONCESIONARIO WEB LEAD PERSONA
	 * @return WebElement 
	 */
	public static WebElement ansaConcesionarioWeb(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[16]/div[1]/ul[1]/li[7]/a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputCalle
	 * @info *** Campo Calle Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputCalle(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='27']"));
		//"(//span[contains(text(),'Calle')]/following::input[@maxlength='100'])[1]"
		String ruta = "//span[contains(text(),'Dirección')]/following::input[@maxlength='40'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputComplementoDireccion
	 * @info *** Campo Complemento Direccion Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputComplementoDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='29']"));
		String ruta = "(//span[contains(text(),'Referencia de dirección')]/following::input[@maxlength='250'])[1] | (//span[contains(text(),'Referencia de dirección')]/following::input[@maxlength='40'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputPais
	 * @info *** elemento input PAIS LEAD Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputPais(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Paises...']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param nacionalidadSeleccion
	 * @info *** elemento Nacionalidad en InputPais LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement nacionalidadSeleccion(WebDriver driver, String paisEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("\"//div[@title='"+paisEjecucion+"']//*[@class='data-match']"));
		//"//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body']//div[@title='"+paisEjecucion+"']";
		String ruta = "//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body']//div[@title='"+paisEjecucion+"']//mark[text()='"+paisEjecucion+"']";
		element = driver.findElement(By.xpath(ruta));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param paisSeleccion
	 * @info *** elemento PAIS en InputPais LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement paisSeleccion(WebDriver driver, String paisEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		element = driver.findElement(By.xpath("(//a//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body'])[2] | (//a//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body'])"));
		//element = driver.findElement(By.xpath("//div[@title='"+paisEjecucion+"']//*[@class='data-match']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param paisSeleccionLead
	 * @info *** elemento PAIS en InputPais LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement paisSeleccionLead(WebDriver driver, String paisEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("(//a//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body'])[2] | (//a//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body'])"));
		element = driver.findElement(By.xpath("//div[@title='"+paisEjecucion+"']//*[@class='data-match']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param regionIngresarAlternativo 
	 * @param regionIngresar 
	 * @param inputRegion
	 * @info *** elemento input REGION LEAD Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputRegion(WebDriver driver, String regionIngresar, String regionIngresarAlternativo) {
		//, String regionIngresar, String regionIngresarAlternativo
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//String ruta = "//input[@placeholder='"+regionIngresar+"']|//input[@placeholder='"+regionIngresarAlternativo+"']";
		String ruta = "//input[@placeholder='" + regionIngresar + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param regionSeleccion
	 * @info *** elemento REGION en InputRegion LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement regionSeleccion(WebDriver driver, String regionEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		String ruta = "//div[contains(@title,'"+regionEjecucion+"')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputProvincia
	 * @info *** elemento input Provincia LEAD Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputProvincia(WebDriver driver, String provinciaIngresar) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "//input[@placeholder='"+provinciaIngresar+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param provinciaSeleccion
	 * @info *** elemento Provincia en inputProvincia LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement provinciaSeleccion(WebDriver driver, String provinciaEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("//mark[contains(text(),'Santiago')]"));
//		element = driver.findElement(By.xpath("//div[@title='"+provinciaEjecucion+"']|//mark[contains(text(),'"+provinciaEjecucion+"')]"));
		String ruta = "(//div[@title='"+provinciaEjecucion+"'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputComuna
	 * @info *** elemento input COMUNA LEAD Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputComuna(WebDriver driver, String comunaIngresar) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//input[@title='Buscar Comunas']
		//input[@placeholder='"+comunaIngresar+"']
		String ruta = "//input[@placeholder='"+comunaIngresar+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param comunaSeleccion
	 * @info *** elemento COMUNA en inputComuna LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement comunaSeleccion(WebDriver driver, String comunaIngresar, String comunaEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("//mark[contains(text(),'Santiago')]"));
		//element = driver.findElement(By.xpath("//*/div[2]/div[2][contains(text(),'"+comunaEjecucion+"')]"));
		String ruta = "//input[@placeholder='"+comunaIngresar+"']//following::div[@class='primaryLabel slds-truncate slds-lookup__result-text'][@title='"+comunaEjecucion+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnGuardar
	 * @info *** Boton Guardar LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement btnGuardar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[3]/span[1]"));
		String ruta = "//button[@title='Guardar']//span[contains(text(),'Guardar')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param txtNombrePaginaInicio
	 * @info *** Texto Nombre Creado Lead Personal 
	 * @return WebElement 
	 */
	public static WebElement txtNombrePaginaInicio(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]"));
		String ruta = "//h1/div/span";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Adrian Siñiga | 2019 12 23 | AXITY
	 * @param inputDireccion
	 * @info *** Campo Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		//(//span[contains(text(),'Dirección')]/following::input[@maxlength='100'])[1]|(//span[contains(text(),'Calle')]/following::input[@maxlength='100'])[1]
		String ruta = "//span[contains(text(),'Calle')]/following::input[@class=' input'][@maxlength='40']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputNumeroDireccion
	 * @info *** Campo Numero Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumeroDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "(//span[contains(text(),'Número')]/following::input[@maxlength='10'])[1]|(//span[contains(text(),'Número')])[2]//following::input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputNumeroDireccionAlt
	 * @info *** Campo Numero Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumeroDireccionAlt(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "(//span[contains(text(),'Número')])[2]//following::input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputDeptoInterior
	 * @info *** Campo Depto/Interior Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputDeptoInterior(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'Depto/Interior')]/following::input[@maxlength='10'])[1]|(//span[contains(text(),'Depto/Casa/OF')]/following::input[@maxlength='10'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputReferenciaDireccion
	 * @info *** Campo Referencia Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "(//span[contains(text(),'Referencia Dirección')]/following::input[@maxlength='250'])[1]|(//span[contains(text(),'Referencia de dirección')])//following::input[@maxlength='40'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputReferenciaDireccionAlt
	 * @info *** Campo Referencia Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccionAlt(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "(//span[contains(text(),'Complemento dirección')])//following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 02 | AXITY
	 * @param txtMensajeErrorEnPagina
	 * @info *** Mensaje Error En Pagina Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement txtMensajeErrorEnPagina(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "//span[contains(text(),'Revise los errores de esta página.')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 02 | AXITY
	 * @param txtContenidoMensajeErrorEnPagina
	 * @info *** Contenido Mensaje Error En Pagina Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement txtContenidoMensajeErrorEnPagina(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		element = driver.findElement(By.xpath("(//li[@class='form-element__help'])[1]"));
		return element;
	}
	
//	
//	
//	public static WebElement Cmb_EstadoLead(WebDriver driver) throws InterruptedException {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]")));
//		Thread.sleep(2000);
//		return element  ;
//	}
//	
//	public static WebElement Cmb_EstadoLeadSeleccionar(WebDriver driver) throws InterruptedException {
//		element = driver.findElement(By.xpath("/html[1]/body[1]/div[9]/div[1]/ul[1]/li[5]/a[1]")); //Opcion 'Calificado' seleccionada
//		return element ;
//	}
//	
//		public static WebElement TxT_DocumentoIdentidad(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement Cmb_Tratamiento(WebDriver driver) throws InterruptedException {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]")));
//		Thread.sleep(2000);
//		return element;
//	}	
//	
//	public static WebElement Cmb_TratamientoSeleccionar(WebDriver driver ) {
//		element = driver.findElement(By.xpath("/html[1]/body[1]/div[10]/div[1]/ul[1]/li[2]/a[1]")); //Opcion Sr. selecionada 
//		return element;
//	}
//
//	public static WebElement Cmb_Origen(WebDriver driver) throws InterruptedException {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]")));
//		Thread.sleep(2000);
//		return element;
//	}
//	
//	public static WebElement Cmb_OrigenSeleccionar(WebDriver driver) throws InterruptedException {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[11]/div[1]/ul[1]/li[3]/a[1]")));//opcion Chat seleccionada
//		return element;
//	}
//	
//	public static WebElement TxT_Nombre(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[2]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement TxT_Apellido(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[3]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement TxT_SegundoApellido(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement TxT_Referido(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//		
//	public static WebElement TxT_Email(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//
//	public static WebElement TxT_Movil(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//
//	public static WebElement TxT_Telefono(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//	
//	 /*-------- <Informacion de direccion> -------*/
//	
//	public static WebElement TxT_Direccion(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement TxT_ComplementoDireccion(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]")));
//		return (element) ;
//	}
//	
//	public static WebElement Btn_Submit(WebDriver driver) {
//		element = driver.findElement((By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[3]\r\n")));
//		return (element) ;
//	}

	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param paisSeleccion
	 * @info *** elemento PAIS en InputPais LEAD Personal
	 * @return WebElement 
	 */
	public static List<WebElement> paisSelecciones(WebDriver driver, String paisEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("\"//div[@title='"+paisEjecucion+"']//*[@class='data-match']"));
		String ruta = "(//a//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body'])";
		elements = driver.findElements(By.xpath(ruta));
		return elements;
	}
}
