package crearLead;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Maquinaria {
	
	private static WebElement element = null;
	private static List<WebElement> elements = null;
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param selectEstadoLead
	 * @info Seleccionar campo compania
	 * @return WebElement 
	 */
	public static WebElement selectCompania(WebDriver driver) {
		String ruta = "//span[contains(text(),'Compa��a')]/following::input[@maxlength='255'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param selectPreferencia
	 * @info Seleccionar campo compania
	 * @return WebElement 
	 */
	public static WebElement selectPreferencia(WebDriver driver) {
		String ruta = "//*[contains(text(),'Preferencia medio de contacto')]/following::div[@class='uiMenu']//div[@class='uiPopupTrigger']//div/div/a[contains(text(),'--Ninguno--')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param PreferenciaContacto
	 * @info Elemento medio de preferencia
	 * @return WebElement 
	 */
	public static WebElement PreferenciaContacto(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Email'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param elementoMarcaInteres
	 * @info Elemento medio de preferencia
	 * @return WebElement 
	 */
	public static WebElement elementoMarcaInteres(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Landini'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param UnidadNegocio
	 * @info Redirige al label Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement UnidadNegocio(WebDriver driver) {
		String ruta = "//*[contains(text(),'Unidad de negocio')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param elementoNegocio
	 * @info Redirige alelemento de la Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement elementoNegocio(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Agr�cola'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param Familia
	 * @info Redirige al label Familia
	 * @return WebElement 
	 */
	public static WebElement Familia(WebDriver driver) {
		String ruta = "//*[contains(text(),'Familia')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param elementoFamilia
	 * @info Redirige al elemento de la Familia
	 * @return WebElement 
	 */
	public static WebElement elementoFamilia(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Tractores'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param elementoFamiliaPeru
	 * @info Redirige al elemento de la Familia
	 * @return WebElement 
	 */
	public static WebElement elementoFamiliaPeru(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Implementos'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param elementoMarcaInteresPreProd
	 * @info Redirige al elemento de la marca de ineteres
	 * @return WebElement 
	 */
	public static WebElement elementoMarcaInteresPreProd(WebDriver driver, String modeloInteres) {
		String ruta = "//div[@title='" + modeloInteres + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param inputNegocio
	 * @info Redirige al label Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement inputNegocio(WebDriver driver) {
		String ruta = "//div/div/div/div/span/span[text()='Unidad de negocio']/following::div[1]/div/div/div/a[text()='--Ninguno--'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param inputFamilia
	 * @info Redirige al label Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement inputFamilia(WebDriver driver) {
		String ruta = "//div/div/div/div/span/span[text()='Familia']/following::div[1]/div/div/div/a[text()='--Ninguno--'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param inputMarcaInteres
	 * @info Redirige al label Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement inputMarcaInteres(WebDriver driver) {
		String ruta = "//div/div/div/div/span/span[text()='Marca de inter�s']/following::div[1]/div/div/div/a[text()='--Ninguno--'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param seleccionarNegocio
	 * @info Redirige al label Unidad de negocio
	 * @return WebElement 
	 */
	public static WebElement seleccionarNegocio(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Agr�cola'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Si�iga | 2019 09 26 | AXITY
	 * @param seleccionarFamilia
	 * @info Seleccionar familia
	 * @return WebElement 
	 */
	public static WebElement seleccionarFamilia(WebDriver driver) {
		String ruta = "//li/a[@role='menuitemradio'][text()='Tractores'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
}
