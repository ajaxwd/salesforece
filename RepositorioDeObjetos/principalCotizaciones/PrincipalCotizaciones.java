package principalCotizaciones;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PrincipalCotizaciones {
	
	private static WebElement element = null;
	private static List<WebElement> elements = null;
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param btnSimularAmicar
	 * @info ***Boton Simular Amicar en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnSimularAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Simular Amicar')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 09 | AXITY
	 * @param selectTipoMoneda
	 * @info ***Select Tipo de Moneda Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement selectTipoMoneda(WebDriver driver) {
		String ruta = "(//*[contains(text(),'Tipo de Moneda')])[1]//following::select[@class='slds-select'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param selectTipoVehiculo
	 * @info ***Select Tipo de Vehiculo Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement selectTipoVehiculo(WebDriver driver) {
		element = driver.findElement(By.name("tipoVehiculo"));
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 02 | AXITY
	 * @param selectMarcaVehiculo
	 * @info ***Select Marca de Vehiculo Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement selectMarcaVehiculo(WebDriver driver) {
		element = driver.findElement(By.name("codMarcaAmicar"));
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 02 | AXITY
	 * @param selectModeloVehiculo
	 * @info ***Select Modelo de Vehiculo Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement selectModeloVehiculo(WebDriver driver) {
		element = driver.findElement(By.name("codModeloAmicar"));
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/select[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param selectTipoCreditoAmicar
	 * @info ***Select Tipo de Credito Amicar Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement selectTipoCreditoAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[4]/div/div/div/select"));
		//element = driver.findElement(By.xpath("//span[contains(text(),'Tipo cr�dito Amicar')]//following::select[@class='slds-select']|(//span[contains(text(),'Tipo cr�dito Amicar')]//following::select)[1]"));
		String ruta = "(//span[contains(text(),'Tipo crédito Amicar')]//following::select)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param inputPieSimularAmicar
	 * @info ***Input Pie  Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement inputPieSimularAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[5]/div/input"));
		//(//span[contains(text(),'Pie')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'] | //span[contains(text(),'Cuota inicial')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'])[1]
		String ruta = "//div[5]/div/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñia | 2019 12 26 | AXITY
	 * @param inputCuotaInicial
	 * @info ***Input Pie  Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement inputCuotaInicial(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[5]/div/input"));
		//(//span[contains(text(),'Pie')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'] | //span[contains(text(),'Cuota inicial')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'])[1]
		String ruta = "(//span[contains(text(),'Cuota inicial')]/following::input)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param inputCuotasSimularAmicar
	 * @info ***Input Cuotas  Simular Financiamiento
	 * @return WebElement 
	 */
	public static WebElement inputCuotasSimularAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[10]/div/input"));
		//element = driver.findElement(By.xpath("(//span[contains(text(),'Cuotas')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'])[2]"));
		//label[@class='uiLabel-left form-element__label uiLabel']//span[contains(text(),'Cuotas')]/following::input[@class='input uiInputSmartNumber'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text']
		//span[contains(text(),'Cuotas')]//following::input[@class='input uiInputSmartNumber'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'][3]
		String ruta = "//label[@class='uiLabel-left form-element__label uiLabel']//span[contains(text(),'Cuotas')]/following::input[@class='input uiInputSmartNumber'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param inputCuotasSimularAmicarOtro
	 * @info ***Input Cuotas  Simular Otro Pais Financiamiento
	 * @return WebElement 
	 */
	public static WebElement inputCuotasSimularAmicarOtro(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[10]/div/input"));
		//(//span[contains(text(),'Plazo')]/following::input[@class=' input'][@min='-99999999999999'][@max='99999999999999'][@step='1'][@type='text'])[3]|(//span[contains(text(),'Plazo')]/following::input[@class=' input'])[1]
		String ruta = "//span[contains(text(),'Plazo')]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	

	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param btnBonoAmicar
	 * @info ***Boton Bono Amicar en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnBonoAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Bono Amicar')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param btnSimular
	 * @info ***Boton Simular en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnSimular(WebDriver driver) {
		element = driver.findElement(By.xpath("(//button[contains(text(),'Simular')])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param btnDetallesCotizacion
	 * @info ***Boton Detalles en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnDetallesCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Detalles')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param txtPieDetalle
	 * @info ***Texto Pie Detalle
	 * @return WebElement 
	 */
	public static WebElement txtPieDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param txtCuotasDetalle
	 * @info ***Texto Cuotas Detalle
	 * @return WebElement 
	 */
	public static WebElement txtCuotasDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param txtTipoCreditoDetalle
	 * @info ***Texto Tipo Credito Detalle
	 * @return WebElement 
	 */
	public static WebElement txtTipoCreditoDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 09 05 | AXITY
	 * @param txtVerificarCotizacion
	 * @info ***Texto Estado Cotizacion Detalle
	 * @return WebElement 
	 */
	public static WebElement txtVerificarCotizacion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[3]/div[2]/div/div[2]/span"));
		element = driver.findElement(By.xpath("(//span[@class='test-id__field-label'][contains(text(), 'Estado')]//following::span)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 01 | AXITY
	 * @param txtCotizacionDetalle
	 * @info ***Texto Cotizacion Detalle
	 * @return WebElement 
	 */
	public static WebElement txtCotizacionDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@title='Cotizaciones']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 05 | AXITY
	 * @param btnNuevoDescuento
	 * @info ***Boton Descuentos / Tramites de la Cotizaci�n
	 * @return WebElement 
	 */
	public static WebElement btnNuevoDescuento(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@title='Cotizaciones']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param btnPagoContado
	 * @info ***Boton Pago Contado en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnPagoContado(WebDriver driver) {
		String ruta = "//button[contains(text(),'Pago contado')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param numeroCotizacion
	 * @info ***Texto Numero de Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement numeroCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[11]/div[2]/div[1]/div[2]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param pieCotizacion
	 * @info ***Texto Pie de Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement pieCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param cuotasCotizacion
	 * @info ***Texto Cuotas de Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement cuotasCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param impuestoPagareCotizacion
	 * @info ***Texto Impuesto Pagare Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement impuestoPagareCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param tipoCreditoAmicarCotizacion
	 * @info ***Texto tipo Credito Amicar Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement tipoCreditoAmicarCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param montoSeguroDesgravamenCotizacion
	 * @info ***Texto Monto Seguro Desgravamen Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement montoSeguroDesgravamenCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param totalAFinanciarCotizacion
	 * @info ***Texto total A Financiar Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement totalAFinanciarCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param valorCuotaCotizacion
	 * @info ***Texto Valor Cuota Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement valorCuotaCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[5]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param valorCae
	 * @info ***Texto Valor Cae Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement valorCae(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[6]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param costoTotal
	 * @info ***Texto Costo Total Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement costoTotal(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[7]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param totalRecargos
	 * @info ***Texto Total Recargos Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement totalRecargos(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[7]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param valorFuturoMinimoGarantizado
	 * @info ***Texto Valor Futuro Minimo Garantizado Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement valorFuturoMinimoGarantizado(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[8]/div[1]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param fechaPrimeraCuota
	 * @info ***Texto fecha PrimeraCuota Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement fechaPrimeraCuota(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[8]/div[2]/div[1]/div[2]/span[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param btnCrearPdf
	 * @info ***Boton Crear PDF en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnCrearPdf(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Crear PDF')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param btnGuardarCotizacion
	 * @info ***Boton Guardar Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnGuardarCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Guardar cotización')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param txtConfirmaGuardarCotizacion
	 * @info ***Texto Confirma Guardar Cotizacion en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement txtConfirmaGuardarCotizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 17 | AXITY
	 * @param btnAgregarProductos
	 * @info ***Boton Agregar Productos en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnAgregarProductos(WebDriver driver) {
		String ruta = "//button[contains(text(),'Agregar Productos')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 23 | AXITY
	 * @param txtCuadroDialogo
	 * @info ***Texto de dialogo para verificar acciones
	 * @return WebElement 
	 */
	public static WebElement txtCuadroDialogo(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html/body/div[5]/div[1]/section/div[1]/div/div/div/div/div/span"));
		//element = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String ruta = "//span[@class='toastMessage forceActionsText']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 12 | AXITY
	 * @param btnEvaluarAmicar
	 * @info ***Boton Evaluar Amicar en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Evaluar Amicar')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param btnInfoClientes
	 * @info ***Boton Info Clientes en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement btnInfoClientes(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@data-tab-value=\"infoDeClientes\"][contains(text(),'Informaci�n de clientes')]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectCodigoFyi
	 * @info ***Select Codigo F&I en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectCodigoFyi(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[2]/div/select"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectRegionEvaluarAmicar
	 * @info ***Select Region en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRegionEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[6]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Regi�n')]//following::select)[1]|//div[@id='infoDeClientes']/div/div[6]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 08 22 | AXITY
	 * @param selectRegionEvaluarAmicarAlternativo
	 * @info ***Select Region en Pagina de Evaluar Amicar Alternativo
	 * @return WebElement 
	 */
	public static WebElement selectRegionEvaluarAmicarAlternativo(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[6]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Departamento')]//following::select)[1]|//div[@id='infoDeClientes']/div/div[6]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectRegionEmpresaEvaluarAmicar
	 * @info ***Select Region Empresa en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRegionEmpresaEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[6]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Regi�n')]//following::select)[1]|(//*[@id='infoDeClientes']//span[contains(text(),'Departamento')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param inputFechaConstitucionEvaluarAmicar
	 * @info ***Input Fecha Constitucion Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputFechaConstitucionEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Fecha de Constituci�n')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param inputRubroEvaluarAmicar
	 * @info ***Input Rubro  Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputRubroEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Descripci�n Rubro')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param inputNumeroTrabajadoresEvaluarAmicar
	 * @info ***Input Numero Trabajadores Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputNumeroTrabajadoresEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'N� Trabajadores')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param selectRubroEvaluarAmicar
	 * @info ***Select rubro en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRubroEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[6]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Rubro')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectComunaEvaluarAmicar
	 * @info ***Select Comuna en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectComunaEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[7]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Comuna')]//following::select)[1]|//div[@id='infoDeClientes']/div/div[7]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 08 22 | AXITY
	 * @param selectComunaEvaluarAmicarAlternativo
	 * @info ***Select Alternativo Comuna en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectComunaEvaluarAmicarAlternativo(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[7]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Provincia')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectComunaEvaluarAmicar
	 * @info ***Select Comuna en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectDistritoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[7]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Distrito')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 08 22 | AXITY
	 * @param selectDistritoEmpleadorEvaluarAmicar
	 * @info ***Select Distrito Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectDistritoEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[7]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Distrito empleador')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectComunaEmpresaEvaluarAmicar
	 * @info ***Select Comuna Empresa en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectComunaEmpresaEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Comuna')]//following::select)[1]|(//*[@id='infoDeClientes']//span[contains(text(),'Provincia')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectNacionalidadEvaluarAmicar
	 * @info ***Select Nacionalidad en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectNacionalidadEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[11]/div[1]/select[1]"));
//		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[11]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Nacionalidad')]//following::select)[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectGeneroEvaluarAmicar
	 * @info ***Select Genero en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectGeneroEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[12]/div[1]/select[1]"));
		//element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[12]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'G�nero')]//following::select)[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectEstadoCivilEvaluarAmicar
	 * @info ***Select Estado Civil en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectEstadoCivilEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[13]/div[1]/select[1]"));
		//element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[13]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Estado civil')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectRegimenMatrimonialEvaluarAmicar
	 * @info ***Select Regimen Matrimonial en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRegimenMatrimonialEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[14]/div[1]/select[1]"));
		//element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[14]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Regimen matrimonial')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectEstudiosEvaluarAmicar
	 * @info ***Select Estudios en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectEstudiosEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[15]/div[1]/select[1]"));
//		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[15]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Estudios')]//following::select)[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectTipoResidenciaEvaluarAmicar
	 * @info ***Select Tipo Residencia en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectTipoResidenciaEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[16]/div[1]/select[1]"));
//		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div/div[16]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Tipo de residencia')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectTipoTrabajadorEvaluarAmicar
	 * @info ***Select Tipo Trabajador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectTipoTrabajadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/select[1]"));
//		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[2]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Tipo trabajador')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectTipoActividadEvaluarAmicar
	 * @info ***Select Tipo de Actividad en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectTipoActividadEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/select[1]"));
//		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[3]/div/select"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Tipo actividad')]//following::select)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputCargoEvaluarAmicar
	 * @info ***Input Cargo en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputCargoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[4]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[4]/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputFechaIngresoEvaluarAmicar
	 * @info ***Input Fecha Ingreso en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputFechaIngresoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[5]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[5]/div/div/input"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputNombreEmpleadorEvaluarAmicar
	 * @info ***Input Nombre Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputNombreEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[7]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[7]/div/input"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputDireccionEmpleadorEvaluarAmicar
	 * @info ***Input de Direccion Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputDireccionEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[8]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[8]/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectRubroEmpleadorEvaluarAmicar
	 * @info ***Select Rubro Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRubroEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[9]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[9]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectRegionEmpleadorEvaluarAmicar
	 * @info ***Select Region Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectRegionEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[10]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[10]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectComunaEmpleadorEvaluarAmicar
	 * @info ***Select Comuna Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectComunaEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[11]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[11]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTelefonoEmpleadorEvaluarAmicar
	 * @info ***Input Telefono Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[12]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[12]/div/input|(//*[@id='infoDeClientes']//span[contains(text(),'Tel�fono Fijo Contacto')]//following::input)[1]|(//*[@id='infoDeClientes']//span[contains(text(),'Fono empleador')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTelefonoComercialrEvaluarAmicar
	 * @info ***Input Telefono Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoComercialEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Tel. Comercial 1')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTelefonoComercialAlternativoEvaluarAmicar
	 * @info ***Input Telefono Empleador Alternativo en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoComercialAlternativoEvaluarAmicar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Tel. Comercial 2')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTelefonoAlternativoEmpleadorEvaluarAmicar
	 * @info ***Input Telefono Alternativo Empleador en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoAlternativoEmpleadorEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[13]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//div[@id='infoDeClientes']/div[2]/div[13]/div/input|(//*[@id='infoDeClientes']//span[contains(text(),'Celular')]//following::input)[1]"));
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Fono alternativo')]//following::input)[1]|(//*[@id='infoDeClientes']//span[contains(text(),'Celular')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param btnIngresosyEgresos
	 * @info ***Boton Ingresos y Egresos en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement btnIngresosyEgresos(WebDriver driver) {
		//element = driver.findElement(By.xpath("//a[@id='IngresosEgresos__item']"));
		element = driver.findElement(By.xpath("//*[@data-tab-value=\"IngresosEgresos\"][contains(text(),'Ingresos y egresos')]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectTipoContratoEvaluarAmicar
	 * @info ***Select Tipo Contrato en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectTipoContratoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div/div[2]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param selectTipoRentaEvaluarAmicar
	 * @info ***Select Tipo Renta en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement selectTipoRentaEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/select[1]"));
		element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div/div[3]/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputDiaDePagoEvaluarAmicar
	 * @info ***Input Dia De Pago Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputDiaDePagoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div/div[4]/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputSueldoLiquidoEvaluarAmicar
	 * @info ***Input Sueldo Liquido Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputSueldoLiquidoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div/div[6]/div/input"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Sueldo liquido')]//following::input[1]|//span[contains(text(),'Sueldo Bruto')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputSueldoNetoEvaluarAmicar
	 * @info ***Input Sueldo Liquido Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputSueldoNetoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div/div[6]/div/input"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Sueldo Neto')]//following::input[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputArriendoDividendoEvaluarAmicar
	 * @info ***Input Arriendo Dividendo Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputArriendoDividendoEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//div[@id='IngresosEgresos']/div[2]/div[2]/div/input"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Arriendo - Dividendo')]//following::input[1]|//span[contains(text(),'Hipoteca')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param btnInformacionComercial
	 * @info ***Boton Informacion Comercial en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement btnInformacionComercial(WebDriver driver) {
		//element = driver.findElement(By.xpath("//a[@id='infoComercial__item']"));
		element = driver.findElement(By.xpath("//*[@data-tab-value=\"infoComercial\"][contains(text(),'Informaci�n comercial')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputNombreReferido
	 * @info ***Input Nombre Referido en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputNombreReferido(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/input[1]"));
		element = driver.findElement(By.xpath("//div[@id='infoComercial']/div[3]/div/table/tbody/tr/td[2]/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param inputNombreContacto
	 * @info ***Input Nombre Contacto en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputNombreContacto(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[@id='infoDeClientes']//span[contains(text(),'Nombre Contacto')]//following::input)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 07 09 | AXITY
	 * @param inputCargoContacto
	 * @info ***Input Cargo Contacto en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputCargoContacto(WebDriver driver) {
		String ruta = "(//*[@id='infoDeClientes']//span[contains(text(),'Cargo Contacto')]//following::input)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputDireccionReferido
	 * @info ***Input Direccion Referido en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputDireccionReferido(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[3]/input[1]"));
		String ruta = "//div[@id='infoComercial']/div[3]/div/table/tbody/tr/td[3]/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTipoRelacionReferido
	 * @info ***Input Tipo Relacion Referido en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTipoRelacionReferido(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[4]/input[1]"));
		String ruta = "//div[@id='infoComercial']/div[3]/div/table/tbody/tr/td[4]/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param inputTelefonoReferido
	 * @info ***Input Telefono Referido en Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoReferido(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[5]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/input[1]"));
		String ruta = "//div[@id='infoComercial']/div[3]/div/table/tbody/tr/td[5]/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param btnGuardarEvaluarAmicar
	 * @info ***Boton Guardar Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement btnGuardarEvaluarAmicar(WebDriver driver) {
		//element = driver.findElement(By.xpath(""));
		String ruta = "//button[contains(text(),'Guardar')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 24 | AXITY
	 * @param btnEnviarEvaluacion
	 * @info ***Boton Enviar Evaluacion Pagina de Evaluar Amicar
	 * @return WebElement 
	 */
	public static WebElement btnEnviarEvaluacion(WebDriver driver) {
		String ruta = "//button[contains(text(),'Enviar evaluación')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 30 | AXITY
	 * @param btnPagoCredito
	 * @info ***Boton Pago Credito en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement btnPagoCredito(WebDriver driver) {
		String ruta = "//button[contains(text(),'Pago crédito')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 30 | AXITY
	 * @param inputPrecioListaNeto
	 * @info ***Input Precio Lista Neto en Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement inputPrecioListaNeto(WebDriver driver) {
		String ruta = "/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/span[1]/span[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 30 | AXITY
	 * @param txtMensajeEnvioEvaluacion
	 * @info ***Texto Mensaje Envio Evaluacion Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static WebElement txtMensajeEnvioEvaluacion(WebDriver driver) {
		String ruta = "div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage,div.slds-theme--error.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage";
		element = driver.findElement(By.cssSelector(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 30 | AXITY
	 * @param txtMensajeEnvioEvaluacionColeccion
	 * @info ***Coleccion Texto Mensaje Envio Evaluacion Pagina de Cotizacion
	 * @return WebElement 
	 */
	public static List<WebElement> txtMensajeEnvioEvaluacionColeccion(WebDriver driver) {
		String ruta = "div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage,div.slds-theme--error.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage";
		elements = driver.findElements(By.cssSelector(ruta));
		return elements;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 30 | AXITY
	 * @param txtMensajeEnvioEvaluacionDetalle
	 * @info ***Texto Mensaje Envio Evaluacion Pagina de Cotizacion Detalle
	 * @return WebElement 
	 */
	public static WebElement txtMensajeEnvioEvaluacionDetalle(WebDriver driver) {
		String ruta = "span.toastMessage.forceActionsText";
		element = driver.findElement(By.cssSelector(ruta));
		return element;
	}
}
