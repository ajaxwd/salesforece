package login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {
	
	
	private static WebElement element = null;

	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputIngresarUsuario
	 * @info *** Input Ingreso de Usuario
	 * @return WebElement
	 */
	public static WebElement inputIngresarUsuario(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='username']"));
		if (element == null) {
			System.out.println("nulo");
	    }
		return element;
	}

	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputIngresarContrsena
	 * @info *** Input Ingreso de Contraseņa
	 * @return WebElement
	 */
	public static WebElement inputIngresarContrsena(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='password']"));
		return element;
	}
	
	
	/**
	* @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnIniciarSesion
	 * @info *** Boton Iniciar Sesion
	 * @return WebElement
	 */
	public static WebElement btnIniciarSesion(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='Login']"));
		return element;
	}

	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param txtErrorIngreso
	 * @info *** Texto Error Ingreso Usuario o Contraseņa
	 * @return WebElement 
	 */
	public static WebElement txtErrorIngreso(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='error']/font/font[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param imgUsuarioPaginaPrincipal
	 * @info *** Imagen Usuario en Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement imgUsuarioPaginaPrincipal(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/header[1]/div[2]/span[1]/ul[1]/li[9]/button[1]/div[1]/span[1]/div[1]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param lnkCerrarSesion
	 * @info *** Link Cerrar Sesion Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement lnkCerrarSesion(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/a[2]"));
		return element;
	}
	
	
}
