package crearCuenta;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Personal {
	
	private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputDocumentoIdentidad
	 * @info *** Campo Documento Identidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputDocumentoIdentidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='5']"));
		element = driver.findElement(By.xpath("//input[@class=' input'][@maxlength='15']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputGiro
	 * @info *** Campo Giro Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputGiro(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='6']"));
		//(//input[@class=' input'][@maxlength='100'])[1]
		element = driver.findElement(By.xpath("(//input[@class=' input'][@maxlength='40'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectTratamiento
	 * @info *** Campo TRATAMIENTO; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectTratamiento(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("(//a[contains(text(),'--Ninguno--')])[1]"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'Tratamiento')]//following::a[@class='select'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param rolSR
	 * @info *** elemento SR en TRATAMIENTO Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement rolSR(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[9]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='Sr.']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param rolTratamiento
	 * @info *** elemento Tratamiento en TRATAMIENTO Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement rolTratamiento(WebDriver driver, String tratamiento) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[9]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[@title='"+tratamiento+"']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectGenero
	 * @info *** Campo GENERO; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectGenero(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//(//*[contains(text(),'Género')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]
		String ruta = "//*[contains(text(),'Género')]/following::a[@class='select'][contains(text(),'--Ninguno--')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param generoMasculino
	 * @info *** elemento MASCULINO en GENERO Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement generoMasculino(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[10]/div[1]/ul[1]/li[2]/a[1]"));
		//a[@title='Masculino']
		String ruta = "//a[@title='Masculino'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param generoIngreso
	 * @info *** elemento Ingreso en GENERO Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement generoIngreso(WebDriver driver, String genero) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[10]/div[1]/ul[1]/li[2]/a[1]"));
		//a[@title='"+genero+"']
		//a[@title='"+genero+"'][1]
		//a[contains(text(),'"+genero+"')]
		//li/a[contains(text(),'Masculino')]
		element = driver.findElement(By.xpath("//li/a[contains(text(),'"+ genero +"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNombre
	 * @info *** Campo Nombre Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNombre(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[2]/input[1]"));
		//input[@placeholder='Nombres']
		String ruta = "//input[@placeholder='Nombres']";
		element = driver.findElement(By.xpath(ruta));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputApellidoPaterno
	 * @info *** Campo Apellido Paterno Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputApellidoPaterno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[3]/input[1]"));
		//input[@placeholder='Apellido 1']
		String ruta = "//input[@placeholder='Apellido 1']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputApellidoMaterno
	 * @info *** Campo Apellido Materno Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputApellidoMaterno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='11']"));
		String ruta = "(//input[@class=' input'][@maxlength='40'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputFechaNacimiento
	 * @info *** Campo Fecha Nacimiento Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputFechaNacimiento(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='12']"));
		element = driver.findElement(By.xpath("//div[@class='form-element']/input[@class=' input']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNacionalidad
	 * @info *** elemento input Nacionalidad Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputNacionalidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(//input[@placeholder='Buscar Paises...'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param nacionalidadSeleccion
	 * @info *** elemento NACIONALIDAD en InputNacionalidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement nacionalidadSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]"));
		element = driver.findElement(By.xpath("//div[@title='Chile']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectEstadoCivil
	 * @info *** Campo ESTADO CIVIL; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectEstadoCivil(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//span/span[contains(text(),'Estado Civil')]/following::a[contains(text(),'--Ninguno--')][1]
		//(//a[contains(text(),'--Ninguno--')])[1]
		String ruta = "//span/span[contains(text(),'Estado Civil')]/following::a[contains(text(),'--Ninguno--')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param soltero
	 * @info *** elemento SOLTERO en ESTADO CIVIL Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement soltero(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[11]/div[1]/ul[1]/li[2]/a[1]"));
		element = driver.findElement(By.xpath("//a[contains(@title,'Soltero')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputIdExternoSap
	 * @info *** Campo ID Externo SAP Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputIdExternoSap(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='15']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectPreferenciaComunicacion
	 * @info *** Campo PREFERENCIA DE COMUNICACION; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectPreferenciaComunicacion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		//element = driver.findElement(By.xpath("//a[contains(text(),'--Ninguno--')]"));
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//following::a[@class='select'])[3]"));
		//element = driver.findElement(By.xpath("(//span[contains(text(),'V�a de preferencia para comunicaci�n')]//following::a[@class='select'])[1]"));
		String ruta = "//span[contains(text(),'de contacto')][@class='test-id__section-header-title section-header-title slds-p-horizontal--small slds-truncate']//following::a[@class='select']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param viaTelefono
	 * @info *** elemento VIA TELEFONO en PREFERENCIA DE COMUNICACION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement viaTelefono(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[12]/div[1]/ul[1]/li[3]/a[1]"));
		//element = driver.findElement(By.xpath("//a[contains(@class,'select')][contains(text(),'Tel�fono')]"));
		String ruta = "//a[contains(text(),'Teléfono')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param elementoViaEmail
	 * @info *** elemento VIA EMAIL en PREFERENCIA DE COMUNICACION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement elementoViaEmail(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[12]/div[1]/ul[1]/li[3]/a[1]"));
		//element = driver.findElement(By.xpath("//a[contains(@class,'select')][contains(text(),'Tel�fono')]"));
		element = driver.findElement(By.xpath("//a[contains(text(),'E-Mail')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNumeroMovil
	 * @info *** Campo NUMERO DE MOVIL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumeroMovil(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Móvil'])[1]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 12 | AXITY
	 * @param inputNumeroMovilModificado
	 * @info *** Campo NUMERO DE MOVIL MODIFICADO en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumeroMovilModificado(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='test-id__section-header-title section-header-title slds-p-horizontal--small slds-truncate']//following::input[@type='tel'][1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputCorreo
	 * @info *** Campo CORREO en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputCorreo(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@type='email']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNumTelefono
	 * @info *** Campo NUMERO DE TELEFONO en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumTelefono(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Tel�fono'])[2]/following::input[1]"));
		element = driver.findElement(By.xpath("//div[2]/div[2]/div/div/div/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNumTelefonoModificado
	 * @info *** Campo NUMERO DE TELEFONO MODIFICADO en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumTelefonoModificado(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='test-id__section-header-title section-header-title slds-p-horizontal--small slds-truncate']//following::input[@type='tel'][2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputPaisDireccion
	 * @info *** elemento input PAIS DIRECCION Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputPaisDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(//input[@placeholder='Buscar Paises...'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param paisDireccionSeleccion
	 * @info *** Seleccion elemento PAIS DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement paisDireccionSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		element = driver.findElement(By.xpath("//div[@title='Chile']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputCalleAleatorio
	 * @info *** Campo CALLE en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputCalleAleatorio(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Calle'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputNumeroDireccion
	 * @info *** Campo NUMERO DIRECCION en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputNumeroDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Número'])[1]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputDeptoCasaOficina
	 * @info *** Campo DEPARTAMENTO/CASA/OFICINA  en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputDeptoCasaOficina(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Depto/Casa/OF'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputComplementoDireccion
	 * @info *** Campo COMPLEMENTO DE DIRECCION en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputComplementoDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Complemento dirección'])[1]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputRegionDireccion
	 * @info *** elemento input REGION DIRECCION Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputRegionDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Regiones...']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param regionDireccionSeleccion
	 * @info *** Seleccion elemento REGION DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement regionDireccionSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]"));
		element = driver.findElement(By.xpath("//div[@title='RM - Santiago']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputComunaDireccion
	 * @info *** elemento input COMUNA DIRECCION Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputComunaDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@title='Buscar Comunas']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param comunaDireccionSeleccion
	 * @info *** Seleccion elemento COMUNA DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement comunaDireccionSeleccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]"));
		element = driver.findElement(By.xpath("//div[@title='Santiago']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputProvinciaDireccion
	 * @info *** elemento input Provincia DIRECCION Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaDireccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@title='Buscar Provincias']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param comunaDireccionSeleccion
	 * @info *** Seleccion elemento Provincia DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement provinciaDireccionSeleccion(WebDriver driver, String provinciaEjecucion) {
		//div/mark[text()='Antioquia']
		//div[@title='" + provinciaEjecucion + "']
		String ruta = "//div/div[@title='"+ provinciaEjecucion +"']/mark[@class='data-match'][text()='" + provinciaEjecucion + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputCalleLaboral
	 * @info *** Campo CALLE LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputCalleLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[2]/div[1]/div[1]/textarea[1]"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Calle laboral'])[1]/following::textarea[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputCodigoPostalLaboral
	 * @info *** Campo CODIGO POSTAL LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputCodigoPostalLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[3]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Código postal laboral'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputComunaLaboral
	 * @info *** Campo COMUNA LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputComunaLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[4]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comuna laboral'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputProvinciaLaboral
	 * @info *** Campo PROVINCIA LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[4]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Estado o provincia laboral'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputPaisLaboral
	 * @info *** Campo PAIS DE DIRECCION LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputPaisLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/fieldset[1]/div[1]/div[5]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pa�s laboral'])[1]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputReferenciaDireccionLaboral
	 * @info *** Campo REFERENCIA DE DIRECCION LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccionLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Dir. Laboral - Referencia'])[1]/following::input[1]|(.//*[normalize-space(text()) and normalize-space(.)='Depto/Casa/OF'])[2]/following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputDescripcionDireccionLaboral
	 * @info *** Campo DESCRIPCION DE DIRECCION LABORAL en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputDescripcionDireccionLaboral(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/textarea[1]"));
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Dir. Laboral - Descripci�n'])[1]/following::textarea[1]|(.//*[normalize-space(text()) and normalize-space(.)='Complemento direcci�n'])[2]/following::textarea[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputBusinessPartner
	 * @info *** Campo BUSSINES PARTNER en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputBusinessPartner(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[5]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param btnGuardar
	 * @info *** Boton Guardar Registro en Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement btnGuardar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral test-saveButton uiButton--default uiButton--brand uiButton']//span[@dir='ltr'][contains(text(),'Guardar')]"));
		//element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Guardar'])[1]/following::button[1]"));
		String ruta = "//button[@title='Guardar']//span[contains(@class,'label bBody')][contains(text(),'Guardar')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param txtRutPaginaInicio
	 * @info *** Texto RUT Creado Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement txtRutPaginaInicio(WebDriver driver) {
		String ruta = "//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/ul[1]/li[1]/p[2]/span[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param btnModificarCuenta
	 * @info *** Boton Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement btnModificarCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Modificar']"));
		return element;
	}	
	
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param inputNacionalidadModificarCuenta
	 * @info *** Input Nacionalidad Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement inputNacionalidadModificarCuenta(WebDriver driver) {
//		element = driver.findElement(By.xpath("//div[@id='brandBand_1']/div/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/input"));
//		element = driver.findElement(By.xpath("//div[@id='brandBand_1']/div/div/div/div/div/div[2]/div/div/div[2]/div/div/div[2]/input"));
		element = driver.findElement(By.xpath("//input[@placeholder='search..']|//span[contains(text(),'Nacionalidad')]//following::input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'][1]"));
		return element;
	}	
	
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoNacionalidadModificarCuenta
	 * @info *** Select Elemento Nacionalidad Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectNacionalidadModificarCuenta(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@id='brandBand_1']/div/div/div/div/div/div[2]/div/div[2]/div/div/div/select"));
		element = driver.findElement(By.xpath("(//*[contains(text(),'G�nero')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoNacionalidadModificarCuenta
	 * @info *** Select Elemento Nacionalidad Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoNacionalidadModificarCuenta(WebDriver driver, String nacionalidadModificada) {
		//element = driver.findElement(By.xpath("//span[@id='listbox-option-unique-id-01']/span[2]/span"));
		//element = driver.findElement(By.xpath("//span[@id='listbox-option-unique-id-01']/span[2]/span"));
		String ruta = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+nacionalidadModificada+"')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectGeneroModificar
	 * @info *** Select GENERO Modificar Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectGeneroModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[2]/div/div/div/select"));
		return element;
	}
	
//	/**
//	 * @author Daniel Salinas | 2018 10 15 | AXITY
//	 * @param inputFechaNacimientoModificar
//	 * @info *** Campo Fecha Nacimiento Modificar Cuenta Personal
//	 * @return WebElement 
//	 */
//	public static WebElement inputFechaNacimientoModificar(WebDriver driver) {
//		element = driver.findElement(By.xpath("//div/div/div/div/div[2]/div/div[2]/div[2]/div/input"));
//		return element;
//	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectEstadoCivilModificar
	 * @info *** Select ESTADO CIVIL Modificar Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectEstadoCivilModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[3]/div/div/select"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param inputPaisModificar
	 * @info *** elemento input PAIS MODIFICAR Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputPaisModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[7]/div/div/div/div[2]/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param paisElementoModificar
	 * @info *** Seleccion elemento PAIS DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement paisElementoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//li/span/span[2]/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param inputRegionModificar
	 * @info *** elemento input Region MODIFICAR Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputRegionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[7]/div[2]/div/div/div[2]/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param regionElementoModificar
	 * @info *** Seleccion elemento Region DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement regionElementoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//li/span/span[2]/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param inputComunaModificar
	 * @info *** elemento input Comuna MODIFICAR Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputComunaModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[4]/div/div/div[2]/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param comunaElementoModificarCuidad
	 * @info *** Seleccion elemento Ciudad DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement comunaElementoModificarCuidad(WebDriver driver) {
		element = driver.findElement(By.xpath("//li/span/span[2]/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param inputCiudadModificar
	 * @info *** elemento input Ciudad MODIFICAR Cuenta Personal; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputCiudadModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[3]/div/div/div[2]/input"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param comunaElementoModificar
	 * @info *** Seleccion elemento Comuna DIRECCION Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement comunaElementoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//li/span/span[2]/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 18 | AXITY
	 * @param inputNumeroDireccionModificar
	 * @info *** Input Numero Direccion DIRECCION Cuenta Personal Modificar
	 * @return WebElement 
	 */
	public static WebElement inputNumeroDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div[1]/div/div[2]/div/div[8]/lightning-input[2]/div"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param btnCrearEnSap
	 * @info *** Boton Crear En Sap
	 * @return WebElement 
	 */
	public static WebElement btnCrearEnSap(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(@title,'Brand action')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputGiroModificar
	 * @info *** Input Giro Modificar
	 * @return WebElement 
	 */
	public static WebElement inputGiroModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='accGiro']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 02 19 | AXITY
	 * @param inputRutModificar
	 * @info *** Input Rut Modificar
	 * @return WebElement 
	 */
	public static WebElement inputRutModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::input[1]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputNombreModificar
	 * @info *** Input Nombre Modificar
	 * @return WebElement 
	 */
	public static WebElement inputNombreModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='accName']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputFechaNacimientoModificar
	 * @info *** Input Fecha Nacimiento  Modificar
	 * @return WebElement 
	 */
	public static WebElement inputFechaNacimientoModificar(WebDriver driver) {
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='15']"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='10']"));
		//element = driver.findElement(By.xpath("(//*[contains(text(),'Fecha de nacimiento')]/following::input[@class=' input'])[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Fecha de nacimiento')]/following::input[@class='field input']|//span[contains(text(),'Fecha de nacimiento')]/following::input[@class=' input']"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputApellidoPaternoModificar
	 * @info *** Input Apellido Paterno Modificar
	 * @return WebElement 
	 */
	public static WebElement inputApellidoPaternoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-input'])[4]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputApellidoPaternoModificarEnvio
	 * @info *** Input Apellido Paterno Modificar Envio
	 * @return WebElement 
	 */
	public static WebElement inputApellidoPaternoModificarEnvio(WebDriver driver, String apellidoPaternoAntiguo) {
		element = driver.findElement(By.xpath("//input[@value='']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputApellidoMaternoModificar
	 * @info *** Input Apellido Paterno Modificar
	 * @return WebElement 
	 */
	public static WebElement inputApellidoMaternoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-input'])[5]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputCorreoModificar
	 * @info *** Input Correo Modificar
	 * @return WebElement 
	 */
	public static WebElement inputCorreoModificar(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//input[@class='slds-input'])[7]"));
		String ruta = "//label[contains(text(),'Correo electrónico')]/following::input[1]|(//label[contains(text(),'Correo empresa')]/following::input)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputMovilModificar
	 * @info *** Input Movil Modificar
	 * @return WebElement 
	 */
	public static WebElement inputMovilModificar(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//input[@class='slds-input'])[8]"));
		String ruta = "//label[contains(text(),'Móvil')]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputTelefonoModificar
	 * @info *** Input Telefono Modificar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoModificar(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//input[@class='slds-input'])[9]"));
		String ruta = "//label[contains(text(),'Teléfono')]/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 12 02 | AXITY
	 * @param inputTelefono
	 * @info *** Input Telefono 
	 * @return WebElement 
	 */
	public static WebElement inputTelefono(WebDriver driver) {
		String ruta = "//label/span[contains(text(),'Teléfono')]/following::input[1]";		
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputCalleModificar
	 * @info *** Input Calle Modificar
	 * @return WebElement 
	 */
	public static WebElement inputCalleModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label[contains(text(),'Calle')]//following::input[@class='slds-input'])[1]|(//input[@class='slds-input'])[10]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputNumeroCalleModificar
	 * @info *** Input Numero Calle Modificar
	 * @return WebElement 
	 */
	public static WebElement inputNumeroCalleModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label[contains(text(),'N�mero')]//following::input[@class='slds-input'])[1]|(//input[@class='slds-input'])[11]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputDptoModificar
	 * @info *** Input Dpto Modificar
	 * @return WebElement 
	 */
	public static WebElement inputDptoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label[contains(text(),'Depto/Casa/OF')]//following::input[@class='slds-input'])[1]|(//input[@class='slds-input'])[12]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputComplementoModificar
	 * @info *** Input Complemento Modificar
	 * @return WebElement 
	 */
	public static WebElement inputComplementoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label[contains(text(),'Complemento direcci�n')]//following::input[@class='slds-input'])[1]|(//input[@class='slds-input'])[13]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputRegionDireccionModificar
	 * @info *** Input Region Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputRegionDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[3]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoRegionModificarCuenta
	 * @info *** Select Elemento Region Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoRegionModificarCuenta(WebDriver driver, String regionModificado) {
		String ruta = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+regionModificado+"')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputProvinciaDireccionModificar
	 * @info *** Input Provincia Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaDireccionModificar(WebDriver driver) {
		String ruta = "(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[4]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoProvinciaModificarCuenta
	 * @info *** Select Elemento Provincia Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoProvinciaModificarCuenta(WebDriver driver, String provinciaModificado) {
		String ruta = "(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+provinciaModificado+"')])[2]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputComunaDireccionModificar
	 * @info *** Input Comuna Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputComunaDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[5]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoComunaModificarCuenta
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoComunaModificarCuenta(WebDriver driver, String comunaModificado) {
		//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+comunaModificado+"')]
		String ruta = "(//span/span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+comunaModificado+"')])[3]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param inputCuentaPrincipal
	 * @info *** elemento input cuenta
	 * @return WebElement 
	 */
	public static WebElement inputCuentaPrincipal(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Nombre / Razón Social')]//following::input[@placeholder='Buscar Cuentas...'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param inputCuentaPrincipalContacto
	 * @info *** elemento input cuenta
	 * @return WebElement 
	 */
	public static WebElement inputCuentaPrincipalContacto(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Nombre de la cuenta')]//following::input[@placeholder='Buscar Cuentas...'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param inputCuentaPrincipal
	 * @info *** elemento input cuenta
	 * @return WebElement 
	 */
	public static WebElement inputBuscarCuentaRelacion(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Cuenta')]//following::input[@placeholder='Buscar Cuentas...'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param cuentaPrincipalSeleccion
	 * @info *** elemento CuentaPrincipal en inputCuentaPrincipal Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement cuentaPrincipalSeleccionParametro(WebDriver driver, String cuentaPrincipal) {
		element = driver.findElement(By.xpath("//div[@title='"+cuentaPrincipal+"']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectEstado
	 * @info *** Campo Estado; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectEstado(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Estado')]//following::a[@class='select'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param selectEstado
	 * @info *** Campo Estado; aca se presenta como select pero no lo es en realidad Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement elementoEstado(WebDriver driver, String elementoEstado) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Estado')]//following::a[contains(text(),'"+elementoEstado+"')])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 10 | AXITY
	 * @param inputCargo
	 * @info *** Campo Cargo Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputCargo(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Cargo')]//following::input[@class=' input'])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 10 25 | AXITY
	 * @param inputTitulo
	 * @info *** Campo Cargo Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement inputTitulo(WebDriver driver) {
		//(//label/span[contains(text(),'tulo')]//following::input[@class=' input'][@type='text'])[1]
		//(//label/span[contains(text(),'tulo')]//following::input[@class=' input'])[1]
		String ruta = "(//label/span[contains(text(),'tulo')]//following::input[@class=' input'][@type='text'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param botonModificar
	 * @info *** Boton Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement botonModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a[@title='Modificar'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param botonEliminarCuenta
	 * @info *** Boton Eliminar Cuenta Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement botonEliminarCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@class='deleteAction']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputDireccionModificar
	 * @info *** Input Direccion Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//label[@class='uiLabel-top form-element__label uiLabel']//following::textarea[@placeholder='Calle de correo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputDireccionComercialModificar
	 * @info *** Input Direccion Comercial Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputDireccionComercialModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//label[@class='uiLabel-top form-element__label uiLabel']//following::textarea[@placeholder='Calle laboral']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputCodigoPostalCorreoModificar
	 * @info *** Input Codigo Postal Correo Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputCodigoPostalCorreoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='C�digo postal de correo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputCodigoPostalLaboralModificar
	 * @info *** Input Codigo Postal Laboral Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputCodigoPostalLaboralModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='C�digo postal laboral']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputComunaAModificar
	 * @info *** Input Comuna A Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputComunaAModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Comuna']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputEstadoCorreo
	 * @info *** Input Estado Correo Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputEstadoCorreo(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Estado o provincia de correo']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputComunaLaboralModificar
	 * @info *** Input Comuna Laboral a Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputComunaLaboralModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Comuna laboral']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param inputEstadoLaboralModificar
	 * @info *** Input Estado Laboral a Modificar Contacto
	 * @return WebElement 
	 */
	public static WebElement inputEstadoLaboralModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Estado o provincia laboral']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 11 | AXITY
	 * @param linkAgregarRelacion
	 * @info *** Link Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement linkAgregarRelacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='Agregar relaci�n']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param linkFuncionRelacion
	 * @info *** Link Agregar Funcion Relacion
	 * @return WebElement 
	 */
	public static WebElement linkFuncionRelacion(WebDriver driver, String aleatorioFuncion) {
		element = driver.findElement(By.xpath("//ul[@class='slds-listbox slds-listbox_vertical']//li["+aleatorioFuncion+"]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param botonFlechaAgregarRelacion
	 * @info *** Boton Flecha Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement botonFlechaAgregarRelacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@title='Mover selecci�n a Elegido']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param txtContactoVerificar
	 * @info *** Texto Contacto Verificar Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement txtContactoVerificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[@class='pillText'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param selectTipoContacto
	 * @info *** Select Tipo Contacto Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement selectTipoContacto(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a[@class='select'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param elementoTipoContacto
	 * @info *** Elemento Tipo Contacto Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement elementoTipoContacto(WebDriver driver, String elementoTipoContacto) {
		element = driver.findElement(By.xpath("(//a[@title='"+elementoTipoContacto+"'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param inputFechaInicio
	 * @info *** Input Fecha Inicio Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement inputFechaInicio(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Fecha de inicio')]//following::input[@class=' input'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param linkHoyFechaInicio
	 * @info *** Link Hoy Fecha Inicio Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement linkHoyFechaInicio(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='today slds-show--inline-block slds-text-link slds-p-bottom--x-small']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 13 | AXITY
	 * @param inputFechaFinalizacion
	 * @info *** Input Fecha Finalizaci�n Agregar Relacion
	 * @return WebElement 
	 */
	public static WebElement inputFechaFinalizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Fecha de finalizaci�n')]//following::input[@class=' input'])[1]"));
		return element;
	}

	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param selectCuenta
	 * @info *** Seleccionar primera cuenta
	 * @return WebElement 
	 */
	public static WebElement selectCuenta(WebDriver driver) {
		String ruta = "(//tr/th/span/a)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param selectRelacionado
	 * @info *** Seleccionar primera cuenta
	 * @return WebElement 
	 */
	public static WebElement selectRelacionado(WebDriver driver) {
		String ruta = "//span[contains(text(),'Relacionado')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param validarCampaña
	 * @info *** Valida si tiene campañas disponibles
	 * @return WebElement 
	 */
	public static WebElement validarCampania(WebDriver driver) {
		String ruta = "//a/span[contains(text(),'Historial de la campa')]//following::span";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param linkAgregarCampania
	 * @info *** Se selecciona link para poder agregar camapaña
	 * @return WebElement 
	 */
	public static WebElement linkAgregarCampania(WebDriver driver) {
		//a[@title='Agregar a campaña']
		//div[contains(text(),'Agregar a campa')]
		String ruta = "//a[@title='Agregar a campaña']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param inputBuscarCampania
	 * @info *** Se selecciona input buscar campaña
	 * @return WebElement 
	 */
	public static WebElement inputBuscarCampania(WebDriver driver) {
		String ruta = "/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div[3]/div/div/div[1]/div/input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param selectInputCompania
	 * @info *** Se selecciona seleccionar buscar campaña
	 * @return WebElement 
	 */
	public static WebElement selectInputCompania(WebDriver driver) {
		//div/div[contains(text(),'probacion2')]
		//div/div[contains(text(),'aprobacion2')]
		String ruta = "//div/div/mark[contains(text(),'aprobacion2')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param btnSiguiente
	 * @info *** Se selecciona seleccionar buscar campaña
	 * @return WebElement 
	 */
	public static WebElement btnSiguiente(WebDriver driver) {
		String ruta = "//span[contains(text(),'Siguiente')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author adrian Siñiga | 2019 11 21 | AXITY
	 * @param btnGuardarCampania
	 * @info *** Se selecciona boton para guardar la campaña
	 * @return WebElement 
	 */
	public static WebElement btnGuardarCampania(WebDriver driver) {
		String ruta = "(//button/span[contains(text(),'Guardar')])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	
	/**
	 * @author Adrian Siñiga | 2019 11 21 | AXITY
	 * @param validarCampoCampania
	 * @info *** Se valida que el campo este visible para el perfil
	 * @return WebElement 
	 */
	public static WebElement validarCampoCampania(WebDriver driver, String campo1) {
		String ruta = "//th[contains(text(),'" + campo1 + "')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 21 | AXITY
	 * @param inputTipoDocumento
	 * @info *** Seleccionar tipo de documento
	 * @return WebElement 
	 */
	public static WebElement inputTipoDocumento(WebDriver driver) {
		String ruta = "//span/span[contains(text(),'Tipo de Documento')]/following::a[contains(text(),'--Ninguno--')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 21 | AXITY
	 * @param selectTipoDocumento
	 * @info *** Seleccionar tipo de documento
	 * @return WebElement 
	 */
	public static WebElement selectTipoDocumento(WebDriver driver, String tipoDocumento) {
		String ruta = "//li/a[@title='" + tipoDocumento + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param rubroPersona
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement rubroPersona(WebDriver driver) {
		String ruta = "(//label/span[contains(text(),'Rubro')]/following::select/option[contains(text(),'--Ninguno--')])[1]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param selectRubroPersona
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectRubroPersona(WebDriver driver) {
		String ruta = "//option[contains(text(),'Agrícola')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
}
