package crearCuenta;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Empresa {

private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputDocumentoIdentidad
	 * @info *** Campo Documento Identidad Cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputDocumentoIdentidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//input[@data-interactive-lib-uid='5']
		//input[@data-interactive-lib-uid='7']
		String ruta = "//span[text()='Documento de Identidad']/following::input[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputRazonSocial
	 * @info *** Campo Razon Social crear Cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputRazonSocial(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='8']"));
		//span[contains(text(),'Nombre / Razón Social')]//following::input[@class='input uiInput uiInputText uiInput--default uiInput--input']
		//span[contains(text(),'Nombre / Razón Social')]//following::input[@class=' input']
		String ruta = "//label/span[contains(text(),'Nombre')]/following::input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputNacionalidad
	 * @info *** elemento input Nacionalidad Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputNacionalidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		String ruta = "(//input[@placeholder='Buscar Paises...'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param nacionalidadSeleccion
	 * @info *** elemento Nacionalidad en InputPais LEAD Personal
	 * @return WebElement 
	 */
	public static WebElement nacionalidadSeleccion(WebDriver driver, String paisEjecucion) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]/mark[1]"));
		//element = driver.findElement(By.xpath("\"//div[@title='"+paisEjecucion+"']//*[@class='data-match']"));
		String ruta = "//div[@class='slds-m-left--smalllabels slds-truncate slds-media__body']//div[@title='"+paisEjecucion+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param paisSeleccion
	 * @info *** elemento Nacionalidad en inputNacionalidad Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	/* public static WebElement paisSeleccion(WebDriver driver,String paisEjecucion) {
		String ruta = "//div[contains(@title,'" + paisEjecucion + "')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	 */
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputGiro
	 * @info *** Campo Giro Cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputGiro(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//span[text()='Giro / Actividad Económica']/following::input
		//input[@data-interactive-lib-uid='6']
		String ruta = "//span[text()='Giro / Actividad Económica']/following::input";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputClasificacionFlotas
	 * @info *** elemento input Clasificacion Flotas Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement selectClasificacionFlotas(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]"));
		String ruta = "//a[@class='select'][contains(text(),'--Ninguno--')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param clasificacionFlotasSeleccion
	 * @info *** elemento A en inputClasificacionFlotas Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement  clasificacionFlotasSeleccion(WebDriver driver, String clasificacion) {
		String ruta = "//a[@title='"+clasificacion+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}		
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputSitioWeb
	 * @info *** Campo SITIO WEB  Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputSitioWeb(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='9']"));
		String ruta = "//span[contains(text(),'Sitio Web')]//following::input[@class=' input'][@maxlength='255']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputCuentaPrincipal
	 * @info *** elemento input Nacionalidad Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputCuentaPrincipal(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Buscar Cuentas'])[1]/preceding::input[1]"));
		//element = driver.findElement(By.xpath("//div[@class='autocompleteWrapper slds-grow']//input[@placeholder='Buscar Cuentas...']"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Cuenta principal')]//following::input[@placeholder='Buscar Cuentas...']"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param cuentaPrincipalSeleccionElemento
	 * @info *** elemento CuentaPrincipal en inputCuentaPrincipal Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement cuentaPrincipalSeleccionElemento(WebDriver driver, String cuenta) {
		element = driver.findElement(By.xpath("//div[contains(@title,'"+cuenta+"')]"));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param cuentaPrincipalSeleccion
	 * @info *** elemento CuentaPrincipal en inputCuentaPrincipal Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement cuentaPrincipalSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[contains(@title,'225483187')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param cuentaPrincipalSeleccion
	 * @info *** elemento CuentaPrincipal en inputCuentaPrincipal Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement cuentaPrincipalSeleccionParametro(WebDriver driver, String cuentaPrincipal) {
		element = driver.findElement(By.xpath("//div[@title='"+cuentaPrincipal+"']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputCorreo
	 * @info *** Campo CORREO Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCorreo(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@type='email']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputTelefono
	 * @info *** Campo TELEFONO Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputTelefono(WebDriver driver) {
		String ruta = "//input[@type='tel']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputPais
	 * @info *** elemento input PAIS Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputPais(WebDriver driver) {
		String ruta = "(//div[@class='autocompleteWrapper slds-grow']//input[@placeholder='Buscar Paises...'])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param paisSeleccion
	 * @info *** elemento PAIS en InputPais Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement paisSeleccion(WebDriver driver, String paisEjecucion) {
		element = driver.findElement(By.xpath("//div[@title='" + paisEjecucion + "']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputRegion
	 * @info *** elemento input REGION Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputRegion(WebDriver driver) {
		String ruta = "//div[@class='autocompleteWrapper slds-grow']//input[@placeholder='Buscar Regiones...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param regionSeleccion
	 * @info *** elemento REGION en InputRegion Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement regionSeleccion(WebDriver driver, String regionEjecucion) {
		String ruta = "//div[@title='" + regionEjecucion+ "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param inputCcomunaDireccion
	 * @info *** elemento input Provincia DIRECCION Cuenta Empresa; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaDireccion(WebDriver driver, String lbCiudad) {
		//input[@title='Buscar Provincias'][@placeholder='Buscar Provincias...'][@role='combobox']
		//input[@title='Buscar Provincias']
		//div/input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup'][@title='Buscar Provincias'][@placeholder='Buscar Provincias...'][@role='combobox']
		//div/input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup'][@title='Buscar Provincias'][@placeholder='Buscar Provincias...'][1]
		String ruta = "//label/span[text()='"+ lbCiudad +"']/following::input[@placeholder='Buscar Provincias...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 21 | AXITY
	 * @param comunaDireccionSeleccion
	 * @info *** Seleccion elemento Provincia DIRECCION Cuenta Empresa
	 * @return WebElement 
	 */
	public static WebElement provinciaDireccionSeleccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@title='Santiago']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputComuna
	 * @info *** elemento input COMUNA Crear cuenta EMPRESA; se tiene que llenar y elejir sugerencia
	 * @return WebElement 
	 */
	public static WebElement inputComuna(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		element = driver.findElement(By.xpath("//input[@title='Buscar Comunas']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param comunaSeleccion
	 * @info *** elemento COMUNA en inputComuna Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement comunaSeleccion(WebDriver driver, String comunaEjecucion) {
		element = driver.findElement(By.xpath("//div[@title='" + comunaEjecucion + "']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputCalleAleatorio
	 * @info *** Campo CALLE en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCalleAleatorio(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='15']|//div[@class='slds-form slds-form_stacked slds-is-editing']//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']//span[contains(text(),'Calle')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputNumeroDireccion
	 * @info *** Campo NUMERO DIRECCION Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputNumeroDireccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='17']|//div[@class='slds-form slds-form_stacked slds-is-editing']//span[contains(text(),'N�mero')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputDeptoCasaOficina
	 * @info *** Campo DEPARTAMENTO/CASA/OFICINA  Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputDeptoCasaOficina(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='19']|//div[@class='slds-form slds-form_stacked slds-is-editing']//span[contains(text(),'Depto/Casa/OF')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputComplementoDireccion
	 * @info *** Campo COMPLEMENTO DE DIRECCION en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputComplementoDireccion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='21']|//div[@class='slds-form slds-form_stacked slds-is-editing']//span[contains(text(),'Complemento direcci�n')]//following::input[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputCalleComercial
	 * @info *** Campo CALLE Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCalleComercial(WebDriver driver) {
		String ruta = "//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']//textarea[@class='street compoundTLRadius compoundTRRadius compoundBorderBottom textarea']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputCodigoPostalComercial
	 * @info *** Campo CODIGO POSTAL Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCodigoPostalComercial(WebDriver driver) {
		String ruta = "//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@class='postalCode compoundBorderBottom input']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputComunaComercial
	 * @info *** Campo COMUNA Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputComunaComercial(WebDriver driver) {
		String ruta = "//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@class='city compoundBorderBottom compoundBorderRight input']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputProvinciaComercial
	 * @info *** Campo PROVINCIA Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaComercial(WebDriver driver) {
		String ruta = "//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@class='state compoundBorderBottom input']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputPaisEnvio
	 * @info *** Campo Pais Envio en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputPaisEnvio(WebDriver driver) {
		//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@class='country compoundBLRadius compoundBRRadius input']
		String ruta = "//label/span[contains(text(),'País de facturación')]/following::input[@placeholder='País de facturación']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputReferenciaDireccionComercial
	 * @info *** Campo REFERENCIA DE DIRECCION Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccionComercial(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='27']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputDescripcionDireccionComercial
	 * @info *** Campo DESCRIPCION DE DIRECCION Comercial en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputDescripcionDireccionComercial(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']//textarea[@data-interactive-lib-uid='28']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param inputIdExternoSap
	 * @info *** Campo ID Externo SAP Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputIdExternoSap(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']//input[@data-interactive-lib-uid='30']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param btnGuardar
	 * @info *** Boton Guardar Registro en Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement btnGuardar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral uiButton--default uiButton--brand uiButton forceActionButton']//span[@dir='ltr']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param txtNombrePaginaInicio
	 * @info *** Texto Nombre Creado Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement txtNombrePaginaInicio(WebDriver driver) {
		element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/div[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 27 | AXITY
	 * @param txtRutPaginaInicio
	 * @info *** Texto RUT Creado Crear cuenta EMPRESA
	 * @return WebElement 
	 */
	public static WebElement txtRutPaginaInicio(WebDriver driver) {
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputNombreModificar
	 * @info *** Input Nombre Modificar
	 * @return WebElement 
	 */
	public static WebElement inputNombreEmpresaModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='accNameE']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputGiroModificar
	 * @info *** Input Giro Modificar
	 * @return WebElement 
	 */
	public static WebElement inputGiroModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@name='accGiro']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param inputGiroEmpresa
	 * @info *** Campo input Giro EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputGiroEmpresa(WebDriver driver) {
		//span[contains(text(),'Giro')]//following::input[@class=' input']
		//span[contains(text(),'Giro')]//following::input[@class=' input'][@maxlength='100'])[1]
		String ruta = "//span[contains(text(),'Giro')]//following::input[@class=' input']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param inputCorreoEmpresa
	 * @info *** Campo input Correo EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputCorreoEmpresa(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Correo Empresa')]//following::input[@class=' input'][@type='email'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param inputTelefonoEmpresa
	 * @info *** Campo input Telefono EMPRESA
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoEmpresa(WebDriver driver) {
		String ruta = "(//*[contains(text(),'Teléfono')]/following::input[@maxlength='40'][@type='tel'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param inputDireccion
	 * @info *** Campo Direccion Cuenta Empresa
	 * @return WebElement 
	 */
	public static WebElement inputDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='27']"));
		//(//span[contains(text(),'Dirección')]/following::input[@maxlength='100'])[1]|(//span[contains(text(),'Calle')]/following::input[@maxlength='100'])[1]
		String ruta = "//label/span[contains(text(),'Dirección')]/following::input[@class=' input'][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param txtRutPaginaInicioCuenta
	 * @info *** Texto Rut Pagina Inicio
	 * @return WebElement 
	 */
	public static WebElement txtRutPaginaInicioCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Documento de Identidad')]//following::span[@class='uiOutputText'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param txtTelefonoPaginaInicioCuenta
	 * @info *** Texto Telefono Pagina Inicio Cuenta
	 * @return WebElement 
	 */
	public static WebElement txtTelefonoPaginaInicioCuenta(WebDriver driver) {
		String ruta = "(//span[contains(text(),'Teléfono')]//following::span[@class='uiOutputPhone'])[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 05 07 | AXITY
	 * @param txtNombrePaginaInicioCuenta
	 * @info *** Texto Telefono Pagina Inicio Cuenta
	 * @return WebElement 
	 */
	public static WebElement txtNombrePaginaInicioCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='custom-truncate uiOutputText']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputReferenciaDireccion
	 * @info *** Campo Complemento Direccion Local Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='29']"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'Referencia de dirección')]/following::input[@maxlength='40'])[1]|(//span[contains(text(),'Complemento dirección')]/following::input[@maxlength='40'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoNacionalidadModificarCuenta
	 * @info *** Select Elemento Nacionalidad Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectNacionalidadModificarCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[1]"));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoNacionalidadModificarCuenta
	 * @info *** Select Elemento Nacionalidad Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoNacionalidadModificarCuenta(WebDriver driver, String nacionalidadModificada) {
		//element = driver.findElement(By.xpath("//span[@id='listbox-option-unique-id-01']/span[2]/span"));
		//element = driver.findElement(By.xpath("//span[@id='listbox-option-unique-id-01']/span[2]/span"));
		String ruta = "//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+nacionalidadModificada+"')]";	
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputCorreoModificar
	 * @info *** Input Correo Modificar
	 * @return WebElement 
	 */
	public static WebElement inputCorreoModificar(WebDriver driver) {
		String ruta = "(//input[@class='slds-input'])[7]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputTelefonoModificar
	 * @info *** Input Telefono Modificar
	 * @return WebElement 
	 */
	public static WebElement inputTelefonoModificar(WebDriver driver) {
		String ruta = "(//input[@class='slds-input'])[8]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputCalleModificar
	 * @info *** Input Calle Modificar
	 * @return WebElement 
	 */
	public static WebElement inputCalleModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-input'])[8]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputNumeroCalleModificar
	 * @info *** Input Numero Calle Modificar
	 * @return WebElement 
	 */
	public static WebElement inputNumeroCalleModificar(WebDriver driver) {
		String ruta = "(//input[@class='slds-input'])[10]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputDptoModificar
	 * @info *** Input Dpto Modificar
	 * @return WebElement 
	 */
	public static WebElement inputDptoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-input'])[11]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputComplementoModificar
	 * @info *** Input Complemento Modificar
	 * @return WebElement 
	 */
	public static WebElement inputComplementoModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-input'])[12]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputRegionDireccionModificar
	 * @info *** Input Region Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputRegionDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[3]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoRegionModificarCuenta
	 * @info *** Select Elemento Region Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoRegionModificarCuenta(WebDriver driver, String regionModificado) {
		element = driver.findElement(By.xpath("//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+regionModificado+"')]"));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputProvinciaDireccionModificar
	 * @info *** Input Provincia Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputProvinciaDireccionModificar(WebDriver driver) {
		String ruta = "(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[4]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoProvinciaModificarCuenta
	 * @info *** Select Elemento Provincia Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoProvinciaModificarCuenta(WebDriver driver, String provinciaModificado) {
		String ruta = "(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+provinciaModificado+"')])[2]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2018 12 14 | AXITY
	 * @param inputComunaDireccionModificar
	 * @info *** Input Comuna Direccion Modificar
	 * @return WebElement 
	 */
	public static WebElement inputComunaDireccionModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@class='slds-lookup__search-input slds-input leftPaddingClass input uiInput uiInputText uiInput--default uiInput--input'])[5]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 10 16 | AXITY
	 * @param selectElementoComunaModificarCuenta
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectElementoComunaModificarCuenta(WebDriver driver, String comunaModificado) {
		String ruta = "(//span[@class='slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+comunaModificado+"')])[3]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param inputCiudadFacturacion
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement inputCiudadFacturacion(WebDriver driver) {
		String ruta = "//span[contains(text(),'Ciudad de facturación')]/following::input[@placeholder='Ciudad de facturación']";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param inputEstado
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement inputEstado(WebDriver driver) {
		String ruta = "//label/span[contains(text(),'Estado')]/following::input[@placeholder='Estado o provincia de facturación']";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param codigoPostal
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement codigoPostal(WebDriver driver) {
		String ruta = "//label/span[contains(text(),'Código postal de facturación')]/following::input[@placeholder='Código postal de facturación']";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}

	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param rubroEmpresa
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement rubroEmpresa(WebDriver driver) {
		String ruta = "//span/span[contains(text(),'Rubro')]/following::a[contains(text(),'--Ninguno--')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 11 29 | AXITY
	 * @param selectRubroEmpresa
	 * @info *** Select Elemento Comuna Modificar Cuenta Personal 
	 * @return WebElement 
	 */
	public static WebElement selectRubroEmpresa(WebDriver driver) {
		String ruta = "//li/a[contains(text(),'Agrícola')]";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
	
}
