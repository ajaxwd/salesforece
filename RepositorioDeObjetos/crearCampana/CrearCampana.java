package crearCampana;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CrearCampana {
	
	private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param inputNombreCampana
	 * @info *** Input Nombre Campaña
	 * @return WebElement 
	 */
	public static WebElement inputNombreCampana(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Nombre de la campaña')]//following::input[@class=' input'])[1]"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param selectTipoCampaña
	 * @info *** Select Tipo Campaña
	 * @return WebElement 
	 */
	public static WebElement selectTipoCampaña(WebDriver driver) {
		String ruta = "(//div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select'])[3]//span[contains(text(),'Tipo')]//following::a[@class='select'][1]";
		element = driver.findElement(By.xpath(ruta));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param elementoSelectTipoCampaña
	 * @info *** Elemento Select Tipo Campaña
	 * @return WebElement 
	 */
	public static WebElement elementoSelectTipoCampaña(WebDriver driver, String tipoCampana) {
		element = driver.findElement(By.xpath("(//a[@title='"+tipoCampana+"'])[1]"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param selectSubTipoCampaña
	 * @info *** Select Sub Tipo Campaña
	 * @return WebElement 
	 */
	public static WebElement selectSubTipoCampaña(WebDriver driver) {
		element = driver.findElement(By.xpath("(//div[@class='uiInput uiInputSelect forceInputPicklist uiInput--default uiInput--select'])[4]//span[contains(text(),'Sub-tipo de campaña')]//following::a[@class='select'][1]"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param elementoSelectSubTipoCampaña
	 * @info *** Elemento Select Tipo Campaña
	 * @return WebElement 
	 */
	public static WebElement elementoSelectSubTipoCampaña(WebDriver driver, String subtipo) {
		element = driver.findElement(By.xpath("(//a[@title='"+subtipo+"'])[1]"));		
		return element;
	}


	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param selectPais
	 * @info *** Select Pais
	 * @return WebElement 
	 */
	public static WebElement selectPais(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'País')]//following::a[1]"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param elementoSelectPais
	 * @info *** Elemento Select Pais
	 * @return WebElement 
	 */
	public static WebElement elementoSelectPais(WebDriver driver, String pais) {
		element = driver.findElement(By.xpath("(//a[@title='"+pais+"'])[1]"));		
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param selectUnidadDeNegocio
	 * @info *** Select UnidadDeNegocio
	 * @return WebElement 
	 */
	public static WebElement selectUnidadDeNegocio(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[contains(text(),'Unidad de negocio')]//following::a[1]"));		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param elementoSelectUnidadDeNegocio
	 * @info *** Elemento Select UnidadDeNegocio
	 * @return WebElement 
	 */
	public static WebElement elementoSelectUnidadDeNegocio(WebDriver driver, String unidadDeNegocio) {
		element = driver.findElement(By.xpath("(//a[@title='"+unidadDeNegocio+"'])[1]"));		
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param inputFechaInicio
	 * @info *** Input Fecha Inicio
	 * @return WebElement 
	 */
	public static WebElement inputFechaInicio(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label//span[contains(text(),'Fecha de inicio')]//following::input[@class=' input'])[1]"));		
		return element;
	}
	
	
	/**
	 * @author Daniel Salinas | 2019 10 07 | AXITY
	 * @param inputFechaFinalizacion
	 * @info *** Input Fecha Finalizacion
	 * @return WebElement 
	 */
	public static WebElement inputFechaFinalizacion(WebDriver driver) {
		element = driver.findElement(By.xpath("(//label//span[contains(text(),'Fecha de finalización')]//following::input[@class=' input'])[1]"));		
		return element;
	}
	
	
	
	
}
