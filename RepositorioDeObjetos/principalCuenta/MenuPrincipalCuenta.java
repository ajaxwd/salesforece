package principalCuenta;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuPrincipalCuenta {

	private static WebElement element = null;
	private static List<WebElement> elements = null;

	/**
	 * @author Daniel Salinas | 2018 10 08 | AXITY
	 * @param txtRutPersona
	 * @info ***Texto Rut Persona
	 * @return WebElement
	 */
	public static WebElement txtRutPersona(WebDriver driver) {
		// element =
		// driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div/div/div[1]/div[1]/header/div[2]/ul/li[1]/p[2]/span"));
//		element = driver.findElement(By.xpath(
//				"//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div[1]/div/div[1]/div[1]/header/div[2]/ul/li[1]/div/div/div/span"));
		//element = driver.findElement(By.xpath("//div/div/div/div/div/div[2]/span/span"));
		element = driver.findElement(By.xpath("(//lightning-icon[@class='tooltipIcon slds-button__icon slds-icon-utility-info slds-icon_container forceIcon']/following::span[@class='uiOutputText'][@data-aura-class='uiOutputText'])[1]"));
		
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 10 08 | AXITY
	 * @param txtTelefonoEmpresa
	 * @info ***Texto Telefono Empresa
	 * @return WebElement
	 */
	public static WebElement txtTelefonoEmpresa(WebDriver driver) {
		// element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[1]/div[1]/header/div[2]/ul/li[1]/p[2]/span"));
		//element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[5]/div[1]/div/div[1]/div[1]/header/div[2]/ul/li[1]/div/div/div/span"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'Teléfono')]//following::span[@class='uiOutputPhone'])[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 12 18 | AXITY
	 * @param txtRutDetallesCuenta
	 * @info ***Texto Rut Detalles Cuenta
	 * @return WebElement
	 */
	public static WebElement txtRutDetallesCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[@class='uiOutputText'])[4]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 12 18 | AXITY
	 * @param txtBPDetallesCuenta
	 * @info ***Texto BP Detalles Cuenta
	 * @return WebElement
	 */
	public static WebElement txtBPDetallesCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("(//span[@class='uiOutputText'])[14]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkRelacionado
	 * @info ***link Relacionado Cuenta
	 * @return WebElement
	 */
	public static WebElement linkRelacionado(WebDriver driver) {
		//element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Detalles'])[1]/following::span[2]"));
		String ruta = "(//a[@title='Detalles'][1]//following::a[@title='Relacionado'])[1]|//a[contains(@title,'Relacionado')]|//div[@class='column region-main']//li[@class='tabs__item uiTabItem']//a[@class='tabHeader']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkElementoRelacionado
	 * @info ***link Relacionado Cuenta
	 * @return WebElement
	 */
	public static WebElement linkElementoRelacionado(WebDriver driver) {
		element = driver.findElement(By.linkText("Relacionado"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkRelacionadoActivo
	 * @info ***link Relacionado Activo Cuenta
	 * @return WebElement
	 */
	public static WebElement linkRelacionadoActivo(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a[contains(@title,'Relacionado')])[2]|//a[@id='relatedListsTab__item'][contains(text(),'Relacionado')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkDetalles
	 * @info ***link Detalles Cuenta
	 * @return WebElement
	 */
	public static WebElement linkDetalles(WebDriver driver) {
		// element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and
		// normalize-space(.)='Detalles'])[1]/following::span[2]"));
		element = driver.findElement(By.xpath("//a[contains(@title,'Detalles')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param btnConsultarOt
	 * @info ***Boton Consultar OT
	 * @return WebElement
	 */
	public static WebElement btnConsultarOt(WebDriver driver) {
		//button[@title='Brand action'][contains(text(),'Crear en SAP')]
		//button[@title='Brand action'][contains(text(),'OT')]
		String ruta = "//button[@title='Brand action'][contains(text(),'Crear en SAP')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtMensajeConsultarOt
	 * @info ***Texto Mensaje Consultar OT
	 * @return WebElement
	 */
	public static WebElement txtMensajeConsultarOt(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkOrdenesUltimoAno
	 * @info ***Link Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement linkOrdenesUltimoAno(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//a[@class='slds-card__header-link baseCard__header-title-container'])[15]"));
		//element = driver.findElement(By.xpath("(//span[@title='�rdenes del �ltimo a�o']//following::a[@class='textUnderline outputLookupLink slds-truncate forceOutputLookup'])[1]"));
		element = driver.findElement(By.xpath("(//a[@class='slds-card__header-link baseCard__header-title-container']//following::span[@title='�rdenes del �ltimo a�o'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkOrdenesUltimoAnoVariante
	 * @info ***Link Ordenes Ultimo Ano refact
	 * @return WebElement
	 */
	public static WebElement linkOrdenesUltimoAnoVariante(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a//span[contains(text(),'�rdenes del �ltimo a�o')]//following::span[contains (text(),'(')])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkOrdenesUltimoAnoCantidad
	 * @info ***Link Ordenes Ultimo Ano Cntidad
	 * @return WebElement
	 */
	public static WebElement linkOrdenesUltimoAnoCantidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@class='container forceRelatedListSingleContainer'][14]//h2//a//span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small']"));
		//	element = driver.findElement(By.xpath("(//span[contains(text(),'�rdenes del �ltimo a�o')]/following::span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small'])[1]"));
		//*[contains(text(),'�rdenes del �ltimo a�o')]//following::table/tbody/tr[2]/th/div/a
		String ruta = "(//span[contains(text(),'Órdenes del último año')]/following::span)[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkRecorridoOrdenesUltimoAno
	 * @info ***Recorrido Link Ordenes Ultimo Ano 
	 * @return WebElement
	 */
	public static WebElement linkRecorridoOrdenesUltimoAno(WebDriver driver, int i) {
	element = driver.findElement(By.xpath("//*[contains(text(),'Órdenes del último año')]//following::table/tbody/tr["+i+"]/th/div/a"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkRecorridoOrdenesHistoricas
	 * @info ***Recorrido Link Ordenes Historicas 
	 * @return WebElement
	 */
	public static WebElement linkRecorridoOrdenesHistoricas(WebDriver driver, int i) {
	element = driver.findElement(By.xpath("//*[contains(text(),'Órdenes de Trabajo Históricas')]//following::table/tbody/tr["+i+"]/th/div/a"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkActivosCantidad
	 * @info ***Link Ordenes Ultimo Ano Cntidad
	 * @return WebElement
	 */
	public static WebElement linkActivosCantidad(WebDriver driver) {
	//element = driver.findElement(By.xpath("//div[@class='container forceRelatedListSingleContainer'][3]//h2//a//span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small']"));
	element = driver.findElement(By.xpath("(//span[@title='Activos']//following::span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small'])[1]"));	
	return element;
	}
		
	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkNumeroDeOrdenPrincipal
	 * @info ***Link numero de Orden Principal en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement linkNumeroDeOrdenPrincipal(WebDriver driver, String numeroOrden) {
		element = driver.findElement(By.xpath("//table[@class='forceRecordLayout slds-table slds-no-row-hover slds-table_cell-buffer slds-table_fixed-layout uiVirtualDataGrid--default uiVirtualDataGrid']//a[contains(text(),'" + numeroOrden + "')]"));
		// element = driver.findElement(By.xpath("//a[@title='"+numeroOrden+"']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtFechaAperturaPrincipal
	 * @info ***Texto Fecha Apertura Principal en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement txtFechaAperturaPrincipal(WebDriver driver, String fechaApertura) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'" + fechaApertura + "')])[1]"));
		// element = driver.findElement(By.xpath("//a[@title='"+numeroOrden+"']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreDelActivoPrincipal
	 * @info ***Texto Nombre Del Activo Principal en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement txtNombreDelActivoPrincipal(WebDriver driver, String nombreActivo) {
		// element =
		// driver.findElement(By.xpath("(//a[@title='"+nombreActivo+"'])[1]"));
		element = driver.findElement(By.xpath("(//a[contains(text(),'" + nombreActivo + "')])[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreDelActivoPrincipalHeader
	 * @info ***Texto Nombre Del Activo Principal Header
	 * @return WebElement
	 */
	public static WebElement txtNombreDelActivoPrincipalHeader(WebDriver driver, String nombreActivo) {
		element = driver.findElement(By.linkText(nombreActivo));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtMatriculaPrincipal
	 * @info ***Texto Matricula Principal en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement txtMatriculaPrincipal(WebDriver driver, String matricula) {
		element = driver.findElement(By.xpath("(//*[contains(text(),'" + matricula + "')])[1]"));
		// element = driver.findElement(By.xpath("//a[@title='"+numeroOrden+"']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtMatriculaPrincipalDetalle
	 * @info ***Texto Matricula Principal Detalles en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement txtMatriculaPrincipalDetalle(WebDriver driver, String matricula) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ matricula + "')]"));
		// element = driver.findElement(By.xpath("//a[@title='"+numeroOrden+"']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param linkNumeroDeOrden
	 * @info ***Link numero de Orden en Ordenes Ultimo Ano
	 * @return WebElement
	 */
	public static WebElement linkNumeroDeOrden(WebDriver driver, String numeroOrden) {
		// element =
		// driver.findElement(By.xpath("(//*[contains(text(),'"+numeroOrden+"')])[2]"));
		element = driver.findElement(By.xpath("//a[@title='" + numeroOrden + "']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNumeroDeOrden
	 * @info ***Texto numero de Orden en Ordenes
	 * @return WebElement
	 */
	public static WebElement txtNumeroDeOrden(WebDriver driver, String numeroOrden) {
		// element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and
		// normalize-space(.)='N�mero de Orden'])[1]/following::span[1]"));
		// element = driver.findElement(By.xpath("//div/div[2]/span"));
		element = driver.findElement(By.xpath("//span[contains(text(),'" + numeroOrden + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreDetalles
	 * @info ***Texto Nombre
	 * @return WebElement
	 */
	public static WebElement txtNombreDetalles(WebDriver driver, String nombreCuenta) {
		element = driver.findElement(By.xpath("(//*[contains(text(),'" + nombreCuenta + "')])[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreDetallesHeader
	 * @info ***Texto Nombre
	 * @return WebElement
	 */
	public static WebElement txtNombreDetallesHeader(WebDriver driver, String nombreCuenta) {
		element = driver.findElement(By.linkText(nombreCuenta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtClasePedido
	 * @info ***Texto Nombre
	 * @return WebElement
	 */
	public static WebElement txtClasePedido(WebDriver driver, String clasePedido) {
		element = driver.findElement(By.xpath("//span[contains(text(),'" + clasePedido + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtEstadoPedido
	 * @info ***Texto Estado Pedido
	 * @return WebElement
	 */
	public static WebElement txtEstadoPedido(WebDriver driver, String estadoPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ estadoPedido + "')]"));
		// element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and
		// normalize-space(.)='Estado'])[1]/following::span[1]"));
		// System.err.println("//*[contains(text(),'"+estadoPedido+"')]");
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtMotivoPedido
	 * @info ***Texto Motivo Pedido
	 * @return WebElement
	 */
	public static WebElement txtMotivoPedido(WebDriver driver, String motivoPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ motivoPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//*[contains(text(),'"+motivoPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtFechaActualizacionPedido
	 * @info ***Texto Fecha Actualizacion Pedido
	 * @return WebElement
	 */
	public static WebElement txtFechaActualizacionPedido(WebDriver driver, String fechaActualizacionPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ fechaActualizacionPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+fechaActualizacionPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtFechaCierrePedido
	 * @info ***Texto Fecha Cierre Pedido
	 * @return WebElement
	 */
	public static WebElement txtFechaCierrePedido(WebDriver driver, String fechaCierrePedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ fechaCierrePedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+fechaCierrePedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtFechaEntregaPedido
	 * @info ***Texto Fecha Entrega Pedido
	 * @return WebElement
	 */
	public static WebElement txtFechaEntregaPedido(WebDriver driver, String fechaEntregaPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ fechaEntregaPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+fechaEntregaPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreAsesorPedido
	 * @info ***Texto Nombre Asesor Pedido
	 * @return WebElement
	 */
	public static WebElement txtNombreAsesorPedido(WebDriver driver, String nombreAsesorPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'')]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'" + nombreAsesorPedido + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtNombreAsesorPedidoHeader
	 * @info ***Texto Nombre Asesor Pedido Header
	 * @return WebElement
	 */
	public static WebElement txtNombreAsesorPedidoHeader(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(.//*[normalize-space(text()) and normalize-space(.)='Nombre del Asesor'])[1]/following::div[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtCodigoCentroOtPedido
	 * @info ***Texto Codigo Centro Ot Pedido
	 * @return WebElement
	 */
	public static WebElement txtCodigoCentroOtPedido(WebDriver driver, String codigoCentroOtPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ codigoCentroOtPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+codigoCentroOtPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtlocalPedido
	 * @info ***Texto Local Pedido
	 * @return WebElement
	 */
	public static WebElement txtlocalPedido(WebDriver driver, String localPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ localPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+localPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtValorPedido
	 * @info ***Texto Valor Pedido
	 * @return WebElement
	 */
	public static WebElement txtValorPedido(WebDriver driver, String valorPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ valorPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+valorPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtRecallIdPedido
	 * @info ***Texto Recall Id Pedido
	 * @return WebElement
	 */
	public static WebElement txtRecallIdPedido(WebDriver driver, String recallIdPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ recallIdPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+recallIdPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtVinPedido
	 * @info ***Texto Vin Pedido
	 * @return WebElement
	 */
	public static WebElement txtVinPedido(WebDriver driver, String vinPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ vinPedido + "')]"));
		// element =
//		driver.findElement(By.xpath("//span[contains(text(),'VIN')]//following::span[contains(text(),'"+vinPedido+"')]"));
//		System.err.println("//span[contains(text(),'VIN')]//following::span[contains(text(),'"+vinPedido+"')]");
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtMarcaPedido
	 * @info ***Texto Marca Pedido
	 * @return WebElement
	 */
	public static WebElement txtMarcaPedido(WebDriver driver, String marcaPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ marcaPedido + "')]"));
//		// element =
//		 driver.findElement(By.xpath("(//span[contains(text(),'Marca')]//following::span[contains(text(),'"+marcaPedido+"')])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtModeloPedido
	 * @info ***Texto Modelo Pedido
	 * @return WebElement
	 */
	public static WebElement txtModeloPedido(WebDriver driver, String modeloPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ modeloPedido + "')]"));
		// element =
		//driver.findElement(By.xpath("(//span[contains(text(),'Modelo')]//following::span[contains(text(),'"+modeloPedido+"')])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtKilometrajePedido
	 * @info ***Texto Kilometraje Pedido
	 * @return WebElement
	 */
	public static WebElement txtKilometrajePedido(WebDriver driver, String kilometrajePedido) {
//		element = driver.findElement(By.xpath(
//				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
//						+ kilometrajePedido + "')]"));
		// element =driver.findElement(By.xpath("//span[contains(text(),'"+kilometrajePedido+"')]"));
		 element =driver.findElement(By.xpath("(//span[contains(text(),'Kilometraje')]//following::span[contains(./text(),'"+kilometrajePedido+"')])[3]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtRequerimientoPedido
	 * @info ***Texto Requerimiento Pedido
	 * @return WebElement
	 */
	public static WebElement txtRequerimientoPedido(WebDriver driver, String requerimientoPedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ requerimientoPedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+requerimientoPedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param txtDetallePedido
	 * @info ***Texto Detalle Pedido
	 * @return WebElement
	 */
	public static WebElement txtDetallePedido(WebDriver driver, String detallePedido) {
		element = driver.findElement(By.xpath(
				"//div[contains(@class,'slds-form-element__control slds-grid itemBody')]//span[contains(@class,'test-id__field-value slds-form-element__static slds-grow  is-read-only')]//span[contains(text(),'"
						+ detallePedido + "')]"));
		// element =
		// driver.findElement(By.xpath("//span[contains(text(),'"+detallePedido+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 08 | AXITY
	 * @param btnModificarOT
	 * @info ***Boton Modificar Detalle OT
	 * @return WebElement
	 */
	public static WebElement btnModificarOT(WebDriver driver) {
		// element = driver.findElement(By.xpath("//*[contains(@title,'Modificar')]"));
		element = driver.findElement(By.xpath("//a[contains(@role,'button')]//div[contains(@title,'Modificar')]"));
		element = driver.findElement(By.xpath(
				"//div[@class='slds-brand-band slds-brand-band_cover slds-brand-band_medium slds-template_default  forceBrandBand']//div[@class='slds-page-header slds-page-header_record-home s1FixedTop forceHighlightsStencilDesktop forceRecordLayout']//*[contains(text(),'Modificar')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param txtTratamientoDetalle
	 * @info ***Texto Tratamiento en Nombre Detalle
	 * @return WebElement
	 */
	public static WebElement txtTratamientoDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath("(//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='slds-form-element__control slds-grid itemBody']//span[@class='test-id__field-value slds-form-element__static slds-grow  is-read-only']//span[contains(@data-aura-class,'uiOutputText')])[3]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelIdentificacionDetalle
	 * @info ***Label Identificacion Detalle
	 * @return WebElement
	 */
	public static WebElement labelIdentificacionDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate has-tooltip full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelNombreDetalle
	 * @info ***Label Identificacion Detalle
	 * @return WebElement
	 */
	public static WebElement labelNombreDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelApellidoPaternoDetalle
	 * @info ***Label Apellido Paterno Detalle
	 * @return WebElement
	 */
	public static WebElement labelApellidoPaternoDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[30]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelApellidoMaternoDetalle
	 * @info ***Label Apellido Materno Detalle
	 * @return WebElement
	 */
	public static WebElement labelApellidoMaternoDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[4]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelGeneroDetalle
	 * @info ***Label Genero Detalle
	 * @return WebElement
	 */
	public static WebElement labelGeneroDetalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[3]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelFechaNacimiento
	 * @info ***Label Fecha Nacimiento Detalle
	 * @return WebElement
	 */
	public static WebElement labelFechaNacimiento(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[5]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelNacionalidad
	 * @info ***Label Nacionalidad Detalle
	 * @return WebElement
	 */
	public static WebElement labelNacionalidad(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[6]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelCorreo
	 * @info ***Label Correo Detalle
	 * @return WebElement
	 */
	public static WebElement labelCorreo(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[14]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelMovil
	 * @info ***Label Movil Detalle
	 * @return WebElement
	 */
	public static WebElement labelMovil(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[13]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelTelefono
	 * @info ***Label Telefono Detalle
	 * @return WebElement
	 */
	public static WebElement labelTelefono(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[15]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelPais
	 * @info ***Label Pais Detalle
	 * @return WebElement
	 */
	public static WebElement labelPais(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[16]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelCalle
	 * @info ***Label Calle Detalle
	 * @return WebElement
	 */
	public static WebElement labelCalle(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[17]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelRegion
	 * @info ***Label Region Detalle
	 * @return WebElement
	 */
	public static WebElement labelRegion(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[18]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelNumero
	 * @info ***Label Numero Detalle
	 * @return WebElement
	 */
	public static WebElement labelNumero(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[19]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelProvincia
	 * @info ***Label Provincia Detalle
	 * @return WebElement
	 */
	public static WebElement labelProvincia(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[20]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelDpto
	 * @info ***Label Dpto Detalle
	 * @return WebElement
	 */
	public static WebElement labelDpto(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[21]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelComuna
	 * @info ***Label Comuna Detalle
	 * @return WebElement
	 */
	public static WebElement labelComuna(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[22]"));
		// element = driver.findElement(By.xpath("//div[@class='slds-col slds-grid
		// slds-has-flexi-truncate full forcePageBlockItem
		// forcePageBlockItemView']//div[@class='slds-form-element
		// slds-form-element_readonly slds-grow slds-hint-parent
		// override--slds-form-element']//div[@class='test-id__field-label-container
		// slds-form-element__label']//span[contains(text(),'Comuna')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelFormaPago
	 * @info ***Label Forma de Pago Detalle
	 * @return WebElement
	 */
	public static WebElement labelFormaPago(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(//div[@class='slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(@class,'test-id__field-label')])[31]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelARevisarDetalles
	 * @info ***Link para revisar en Relacionado
	 * @return WebElement
	 */
	public static WebElement labelARevisarDetalles(WebDriver driver, String nombreLabel) {
		String ruta = "//*[contains(text(),'" + nombreLabel + "')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkARevisarRelacionado
	 * @info ***Link para revisar en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkARevisarRelacionado(WebDriver driver, String nombreLink) {
		String ruta = "//span[@title='" + nombreLink + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 06 18 | AXITY
	 * @param labelARevisarRelacionado
	 * @info ***Label para revisar en Relacionado
	 * @return WebElement
	 */
	public static WebElement labelARevisarRelacionado(WebDriver driver, String nombreLabel) {
		element = driver.findElement(By.xpath("//span[contains(text(),'"+nombreLabel+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkCampanaRelacionado
	 * @info ***Link Campaña en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkCampanaRelacionado(WebDriver driver) {
		String ruta = "//span[contains(text(),'campaña')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkActivoRelacionado
	 * @info ***Link Activo en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkActivoRelacionado(WebDriver driver) {
		//element = driver.findElement(By.xpath("//span[contains(text(),'Activos')]"));
		element = driver.findElement(By.xpath("(//span[contains(@title,'Activos')]//preceding::a[@class='slds-card__header-link baseCard__header-title-container'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkActivoRelacionadoVariante
	 * @info ***Link Activo en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkActivoRelacionadoVariante(WebDriver driver) {
		element = driver.findElement(By.xpath("(//a//following::span[contains(@title,'Activos')])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param labelResumenActivo
	 * @info ***Label Resumen Activo
	 * @return WebElement
	 */
	public static WebElement labelResumenActivo(WebDriver driver, String nombreLabel) {
		element = driver.findElement(By.xpath("//th[@title='" + nombreLabel + "']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkActivoRelacionadoSeleccion
	 * @info ***Link Activo en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkActivoRelacionadoSeleccion(WebDriver driver, String nombreActivo) {
		//element = driver.findElement(By.xpath("//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//a[contains(@title,'"+ nombreActivo + "')]|//span[contains(text(),'Activos')]//following::a[contains(text(), '"+ nombreActivo + "')]"));
		element = driver.findElement(By.xpath("(//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']//a[contains(@title,'"+ nombreActivo + "')])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkSeleccionActivo
	 * @info ***Link Seleccion Activo
	 * @return WebElement
	 */
	public static WebElement linkSeleccionActivo(WebDriver driver, String nombreActivo) {
		element = driver.findElement(By.xpath("//a[@title='"+ nombreActivo + "']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkActivoRelacionadoSeleccionVariante
	 * @info ***Link Activo en Relacionado
	 * @return WebElement
	 */
	public static WebElement linkActivoRelacionadoSeleccionVariante(WebDriver driver, String nombreActivo) {
		element = driver.findElement(By.xpath("(//span[contains(text(),'Activos')]//following::a[contains(text(), '"+ nombreActivo + "')])[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param linkRevisionPrincipalActivo
	 * @info ***Link Para revisar pagina de Activo
	 * @return WebElement
	 */
	public static WebElement linkRevisionPrincipalActivo(WebDriver driver, String nombreLink) {
		element = driver.findElement(By.xpath("//span[contains(text(),'" + nombreLink + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param labelRevisionPrincipalActivo
	 * @info ***Label Para revisar pagina de Activo
	 * @return WebElement
	 */
	public static WebElement labelRevisionPrincipalActivo(WebDriver driver, String nombreLabel) {
		element = driver.findElement(By
				.xpath("//div[@class='test-id__field-label-container slds-form-element__label']//span[contains(text(),'"+ nombreLabel + "')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param labelRevisionModificar
	 * @info ***Label Para revisar pagina Modificacion
	 * @return WebElement
	 */
	public static WebElement labelRevisionModificar(WebDriver driver, String nombreLabel) {
		element = driver.findElement(By
				.xpath("//*[contains(text(),'"+ nombreLabel + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param btnExpandirInformacionPropietario
	 * @info ***Boton Expandir Informacion Propietario Activo
	 * @return WebElement
	 */
	public static WebElement btnExpandirInformacionPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//h3[@class='slds-section__title  test-id__section-header-container']//button[@class='slds-button test-id__section-header-button slds-section__title-action']//span[contains(text(),'Informaci�n del propietario')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param btnModificarCuentaInformacionPropietario
	 * @info ***Boton Modificar Cuenta Informacion Propietario Activo
	 * @return WebElement
	 */
	public static WebElement btnModificarCuentanPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@title='Modificar Cuenta']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param txtPropietarioCuentaInformacion
	 * @info ***Texto Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement txtPropietarioCuentaInformacion(WebDriver driver, String nombreCuenta) {
		element = driver.findElement(By.xpath(
				"//div[@class='outputLookupContainer forceOutputLookupWithPreview']//a[@class=' textUnderline outputLookupLink slds-truncate forceOutputLookup'][contains(text(),'"
						+ nombreCuenta + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param btnBorrarPropietarioCuentaInformacion
	 * @info ***Boton Borrar Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement btnBorrarPropietarioCuentaInformacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@class='deleteAction']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param inputBuscarCuentaInformacion
	 * @info ***Input Buscar Cuenta Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement inputBuscarCuentaInformacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Buscar Cuentas...']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param seleccionElementoBuscarCuentaInformacion
	 * @info ***Seleccion Elemento Buscar Cuenta Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement seleccionElementoBuscarCuentaInformacion(WebDriver driver, String nuevoPropietario) {
		element = driver.findElement(By.xpath("//div[contains(@title,'" + nuevoPropietario + "')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param btnGuardarEditarCuentaInformacion
	 * @info ***Boton Guardar Editar Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement btnGuardarEditarCuentaInformacion(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='actionsContainer']//button[@title='Guardar']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param inputFechaEditarCuentaInformacion
	 * @info ***Input Fecha Editar Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement inputFechaEditarCuentaInformacion(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(.//*[normalize-space(text()) and normalize-space(.)='Fecha �ltimo Cambio de Propietario'])[2]/following::input[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param inputHoraEditarCuentaInformacion
	 * @info ***Input Hora Editar Propietario Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement inputHoraEditarCuentaInformacion(WebDriver driver) {
		element = driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Hora'])[1]/following::input[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param txtErrorGuardarCuentaInformacion
	 * @info ***Texto Error Guardar Cuenta Informacion
	 * @return WebElement
	 */
	public static WebElement txtErrorGuardarCuentaInformacion(WebDriver driver) {
		element = driver
				.findElement(By.xpath("//div[@class='inlineEdit desktop forcePageError']//ul[@class='errorsList']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 15 | AXITY
	 * @param txtSeguimiento48Horas
	 * @info ***Texto Seguimiento 48 Horas
	 * @return WebElement
	 */
	public static WebElement txtTareaSeguimiento48Horas(WebDriver driver, String tarea48Horas) {
		element = driver.findElement(By.xpath(
				"//div[@class='slds-col slds-truncate timelineGridItemLeft']//a[@title='"+tarea48Horas+"'][contains(text(),'"+tarea48Horas+"')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 18 | AXITY
	 * @param labelFechaVencimiento
	 * @info ***Label Fecha Vencimiento
	 * @return WebElement
	 */
	public static WebElement labelFechaVencimiento(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//div[@class='test-id__field-label-container slds-form-element__label']//span[@class='test-id__field-label'][contains(text(),'Fecha de vencimiento')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 18 | AXITY
	 * @param txtFechaVencimiento
	 * @info ***Texto Fecha Vencimiento
	 * @return WebElement
	 */
	public static WebElement txtFechaVencimiento(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"//div[@class='slds-form-element__control slds-grid itemBody']//span[@class='test-id__field-value slds-form-element__static slds-grow ']//span[@data-aura-class='uiOutputDate']"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 23 | AXITY
	 * @param labelOrdenDeTrabajoVista360
	 * @info ***Label Orden De Trabajo Vista 360
	 * @return WebElement
	 */
	public static WebElement labelOrdenDeTrabajoVista360(WebDriver driver, String nombreLabel) {
		//element = driver.findElement(By.xpath(
		//"//div[16]/article[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/table[1]/thead[1]/tr[1]//th[@title='"+ nombreLabel + "']"));
		//element = driver.findElement(By.xpath("//th[@title='"+ nombreLabel + "']::*[contains(text(),'Mantenciones')]"));
		String ruta = "//*[contains(text(),'Órdenes de Trabajo Históricas')]/following::th[@title='"+ nombreLabel + "']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param linkVerTodoOTHistoricas
	 * @info ***Link Ver Todo OT Historicas
	 * @return WebElement
	 */
	public static WebElement linkVerTodoOTHistoricas(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//span[@class='view-all-label'][contains(text(),'Ver todos')])[3]"));
		//element = driver.findElement(By.xpath("//div[16]/article[@class='slds-card slds-card_boundary forceRelatedListCardDesktop']//a//span[@class='view-all-label'][contains(text(),'Ver todos')]"));
		String ruta = "//*[contains(text(),'Órdenes de Trabajo Históricas')]/following::span[contains(text(),'Ver todos')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param linkOrdenarPorFecha
	 * @info ***link Ordenar Por Fecha
	 * @return WebElement
	 */
	public static WebElement linkOrdenarPorFecha(WebDriver driver) {
		element = driver.findElement(By.linkText("SortFecha Apertura"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtOrdenadoPorEnOrdenesTrabajoHistoricas
	 * @info ***Texto Ordenado Por En Ordenes Trabajo Historicas
	 * @return WebElement
	 */
	public static WebElement txtOrdenadoPorEnOrdenesTrabajoHistoricas(WebDriver driver) {
		element = driver.findElement(By.xpath(
				"(.//*[normalize-space(text()) and normalize-space(.)='Controles de vista de lista'])[3]/preceding::span[2]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param primeraOrdenOrdenesTrabajoHistoricasPrincipal
	 * @info ***Primera Orden en Ordenes Trabajo Historicas Principal
	 * @return WebElement
	 */
	public static WebElement primeraOrdenOrdenesTrabajoHistoricasPrincipal(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//tr[1]/th/span/a)[2]"));
		element = driver.findElement(By.xpath("(//div[@id='brandBand_1']/div/div/div[4]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/th/span/a)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param linkOrdenesHistoricaCantidad
	 * @info ***Texto Ordenes Trabajo Historicas CantidadvPrincipal
	 * @return WebElement
	 */
	public static WebElement linkOrdenesHistoricaCantidad(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@class='container forceRelatedListSingleContainer'][15]//h2//a//span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small']"));
		String ruta = "(//*[contains(text(),'Órdenes de Trabajo Históricas')]//following::span[@class='slds-card__header-title slds-shrink-none slds-m-right--xx-small'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtNoHayElementosParaMostrarOrdenesTrabajoHistoricasPrincipal
	 * @info ***Texto No Hay Elementos Para Mostrar Ordenes Trabajo Historicas
	 *       Principal
	 * @return WebElement
	 */
	public static WebElement txtNoHayElementosParaMostrarOrdenesTrabajoHistoricasPrincipal(WebDriver driver) {
		element = driver.findElement(By
				.xpath("(//div[@class='emptyContentInner']//*[contains(text(),'No hay elementos para mostrar.')])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtOrdenesTrabajoHistoricasPrincipal
	 * @info ***Texto Ordenes Trabajo Historicas Principal
	 * @return WebElement
	 */
	public static WebElement txtOrdenesTrabajoHistoricasPrincipal(WebDriver driver) {
		// element = driver.findElement(By.xpath("//span[@class='slds-card__header-title
		// slds-truncate slds-m-right--xx-small'][contains(text(),'�rdenes de Trabajo
		// Hist�ricas')]"));
		// element = driver.findElement(By.xpath("//a[@class='slds-card__header-link
		// baseCard__header-title-container']//*[contains(text(),'�rdenes de Trabajo
		// Hist�ricas')]"));
		// element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and
		// normalize-space(.)='�rdenes del �ltimo a�o'])[2]/following::span[2]"));		
		//element = driver.findElement(By.xpath("//div[14]/article/div/header/div[2]/h2/a"));
		String ruta = "//a//preceding::*[contains(text(),'Órdenes de Trabajo Históricas')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}


	/**
	 * @author Daniel Salinas | 2019 01 23 | AXITY
	 * @param labelOrdenesUltimoAnoVista360
	 * @info ***Label Ordenes Ultimo A�o Vista 360
	 * @return WebElement
	 */
	public static WebElement labelOrdenesUltimoAnoVista360(WebDriver driver, String nombreLabel) {
		// element =
		// driver.findElement(By.xpath("//div[13]/article[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/table[1]/thead[1]/tr[1]//th[@title='"+nombreLabel+"']"));
		//element = driver.findElement(	By.xpath("//div[12]/article/div[2]/div/div/div/div/div[3]/div/div/table/thead/tr/th[@title='"+ nombreLabel + "']"));
		String ruta = "(//table[@class='forceRecordLayout slds-table slds-no-row-hover slds-table_cell-buffer slds-table_fixed-layout uiVirtualDataGrid--default uiVirtualDataGrid']//th[@title='"+nombreLabel+"'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param linkVerTodoOrdenAnuales
	 * @info ***Link Ver Todo Ordenes del Ultimo A�o
	 * @return WebElement
	 */
	public static WebElement linkVerTodoOrdenAnuales(WebDriver driver) {
//		element = driver.findElement(By
//				.xpath("//div[12]/article[@class='slds-card slds-card_boundary forceRelatedListCardDesktop']//a//span[@class='view-all-label'][contains(text(),'Ver todos')]"));
//		element = driver.findElement(By
//				.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mostrar m�s'])[15]/following::span[1]"));
		String ruta = "//*[contains(text(),'Órdenes del último año')]/following::span[contains(text(),'Ver todos')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtOrdenadoPorEnOrdenesAnuales
	 * @info ***Texto Ordenado Por En Ordenes del Ultimo A�o
	 * @return WebElement
	 */
	public static WebElement txtOrdenadoPorEnOrdenesAnuales(WebDriver driver) {
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Controles de vista de lista'])[3]/preceding::span[2]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtMotivoPrincipal
	 * @info ***Texto Motivo Pagina Principal
	 * @return WebElement
	 */
	public static WebElement txtMotivoPrincipal(WebDriver driver, int posicion) {
		//element = driver.findElement(By.xpath("(//tr[" + posicion + "]/td[5]/span/span)[2]"));
		element = driver.findElement(By.xpath("//div[@id='brandBand_1']/div/div/div[4]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr[" + posicion + "]/td[5]/span/span"));
		//element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[" + posicion + "]/td[5]/span/span"));
		
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtMotivoPrincipalHistorica
	 * @info ***Texto Motivo Pagina Principal
	 * @return WebElement
	 */
	public static WebElement txtMotivoPrincipalHistorica(WebDriver driver, int posicion) {
		//element = driver.findElement(By.xpath("(//tr[" + posicion + "]/td[5]/span/span)[2]"));
		//element = driver.findElement(By.xpath("//div[@id='brandBand_1']/div/div/div[4]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr[" + posicion + "]/td[5]/span/span"));
		element = driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[" + posicion + "]/td[5]/span/span"));
		
		return element;
	}

	/**
	 * @author Daniel Salinas | 2019 01 24 | AXITY
	 * @param txtOrdenPrincipal
	 * @info ***Texto Orden Pagina Principal
	 * @return WebElement
	 */
	public static WebElement txtOrdenPrincipal(WebDriver driver, int posicion) {
		element = driver
				.findElement(By.xpath("//div[4]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr["
						+ posicion + "]/th/span/a"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 29 | AXITY
	 * @param primerElementoOrdenUltimoAno
	 * @info *** Primer Elemento Orden Ultimo A�o Principal Cuenta
	 * @return WebElement
	 */
	public static WebElement primerElementoOrdenUltimoAno(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[12]/article/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr[1]/th/div/a"));
		//element = driver.findElement(By.xpath("(//table[@class='forceRecordLayout slds-table slds-no-row-hover slds-table_cell-buffer slds-table_fixed-layout uiVirtualDataGrid--default uiVirtualDataGrid']/tbody/tr[1]/th/div/a)[2]"));
		element = driver.findElement(By.xpath("(//span[contains(text(),'�rdenes del �ltimo a�o')]//following::a[@class='textUnderline outputLookupLink slds-truncate forceOutputLookup'])[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 29 | AXITY
	 * @param btnModificarOrdenDeTRabajo
	 * @info *** Boton Modificar Orden De Trabajo
	 * @return WebElement
	 */
	public static WebElement btnModificarOrdenDeTrabajo(WebDriver driver) {
		element = driver.findElement(
				By.xpath("forceHighlightsStencilDesktop forceRecordLayout']//*[contains(text(),'Modificar')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 29 | AXITY
	 * @param txtRutModificar
	 * @info *** Texto Rut Modificacion
	 * @return WebElement
	 */
	public static WebElement txtRutModificar(WebDriver driver) {
		element = driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::input[1]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param btnCancelarModificar
	 * @info *** Texto Rut Modificacion
	 * @return WebElement
	 */
	public static WebElement btnCancelarModificar(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[contains(text(),'Cancelar')]"));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param kilometrajeActivoDetalle
	 * @info *** Kilometraje Activo Detalle
	 * @return WebElement
	 */
	public static WebElement kilometrajeActivoDetalle(WebDriver driver) {
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Kilómetraje'])[1]/following::span[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param fechaActualizacionKilometrajeActivoDetalle
	 * @info *** Fecha Kilometraje Activo Detalle
	 * @return WebElement
	 */
	public static WebElement fechaActualizacionKilometrajeActivoDetalle(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//span[@class='uiOutputDateTime'][@data-aura-class='uiOutputDateTime'])[2]"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Fecha Actualización del Kilometraje'])[1]/following::span[3]";
	    element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param creadoPorKilometrajeActivoDetalle
	 * @info *** Creado Por Kilometraje Activo Detalle
	 * @return WebElement
	 */
	public static WebElement creadoPorKilometrajeActivoDetalle(WebDriver driver) {
//		String test=driver.findElement(By.xpath("//div[@class='test-id__section-content slds-section__content section__content'][@aria-hidden='false'])[4]")).getAttribute("id");
//		//String test = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Creado por'])[1]/following::a[1]")).getAttribute("data-aura-rendered-by");
//		String test2 = driver.findElement(By.xpath("//*[@id='"+test+"']/div/div[1]/div[1]/div/div[2]/span/div/a/text()")).getText();
//		element = driver.findElement(By.xpath("//*[@id='"+test+"']/div/div[1]/div[1]/div/div[2]/span/div/a/text()"));
//		//element = driver.findElement(By.xpath("//a[@data-refid='recordId'][@data-proxy-id='aura-pos-lib-1']"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Creado por'])[1]/following::a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param actualizadoPorKilometrajeActivoDetalle
	 * @info *** Actualizado Por Kilometraje Activo Detalle
	 * @return WebElement
	 */
	public static WebElement actualizadoPorKilometrajeActivoDetalle(WebDriver driver) {
		//element = driver.findElement(By.xpath("//a[@data-refid='recordId'][@data-proxy-id='aura-pos-lib-4']"));
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='última modificación por'])[1]/following::a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 14 03 | AXITY
	 * @param linksCreadoyActualizadoPorKilometrajeActivoDetalle
	 * @info *** Links Creado y Actualizado Por Kilometraje Activo Detalle Lista mas de un elemento
	 * @return List<WebElement>
	 */
	public static List<WebElement> linksCreadoyActualizadoPorKilometrajeActivoDetalle(WebDriver driver) {
		//div[@data-component-id='force_detailPanel']//a[@class=' textUnderline outputLookupLink slds-truncate forceOutputLookup']
		String ruta = "//div[@class='outputLookupContainer forceOutputLookupWithPreview forceOutputModStampWithPreview']/following::a[@class=' textUnderline outputLookupLink slds-truncate outputLookupLink-005f4000000sn80AAA-659:3952;a forceOutputLookup']";
		elements = driver.findElements(By.xpath(ruta));
		return elements;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param txtCuentaActivoDetalle
	 * @info *** Texto Cuenta Activo Detalle
	 * @return WebElement
	 */
	public static WebElement txtCuentaActivoDetalle(WebDriver driver) {
		String ruta = "//ul[@class='slds-form slds-form_stacked slds-grid slds-page-header__detail-row']//li[@class='slds-page-header__detail-block forceHighlightsDesktopListRecordItem']//div[@class='slds-form-element slds-form-element_readonly slds-hint-parent']//a[@class=' textUnderline outputLookupLink slds-truncate forceOutputLookup']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param cuadroCambiosSinGuardar
	 * @info *** Cuadro Cambios Sin Guardar
	 * @return WebElement
	 */
	public static WebElement cuadroCambiosSinGuardar(WebDriver driver) {
		String ruta = "//h2[@class='title slds-text-heading--medium']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param txtNombrePaginaPrincipal
	 * @info *** Texto Nombre Pagina Principal
	 * @return WebElement
	 */
	public static WebElement txtNombrePaginaPrincipal(WebDriver driver) {
		String ruta = "//div[@class ='slds-page-header__title slds-m-right--small slds-truncate slds-align-middle']//span[@class ='uiOutputText'][contains(text(),'Sr')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}

	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param btnCerrarPopUpModificarPropirtario
	 * @info *** Boton Cerrar Pop Up ModificarPropirtario
	 * @return WebElement
	 */
	public static WebElement btnCerrarPopUpModificarPropirtario(WebDriver driver) {
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Revise los siguientes campos'])[1]/preceding::button[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param txtCerrarPopUpModificarPropirtario
	 * @info *** Texto Cerrar Pop Up ModificarPropirtario
	 * @return WebElement
	 */
	public static WebElement txtCerrarPopUpModificarPropirtar(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='inlineEdit desktop forcePageError']//div[@class='genericNotification']//span[@class='genericError uiOutputText']"));
		return element;
	}

	
	/**
	 * @author Daniel Salinas | 2018 01 30 | AXITY
	 * @param txtModificarPropietario
	 * @info *** Texto Cerrar Pop Up ModificarPropirtario
	 * @return WebElement
	 */
	public static WebElement txtModificarPropietario(WebDriver driver) {
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Informaci�n del propietario'])[1]/following::a[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param btnCasoNuevo
	 * @info *** Boton Caso Nuevo
	 * @return WebElement
	 */
	public static WebElement btnCasoNuevo(WebDriver driver) {
		String ruta = "(.//*[normalize-space(text()) and normalize-space(.)='Casos'])[2]/following::div[3]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 20 | AXITY
	 * @param txtNombreAntiguo
	 * @info *** Texto Nombre Antiguo
	 * @return WebElement
	 */
	public static WebElement txtNombreAntiguo(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[11]/article/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr/td[3])[1]"));
		String ruta = "(//table[@class='forceRecordLayout slds-table slds-no-row-hover slds-table_cell-buffer slds-table_fixed-layout uiVirtualDataGrid--default uiVirtualDataGrid']//tbody//td[3]//span[@data-aura-class='uiOutputText'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 20 | AXITY
	 * @param txtNombreNuevo
	 * @info *** Texto Nombre Nuevo
	 * @return WebElement
	 */
	public static WebElement txtNombreNuevo(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[11]/article/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr/td[4])[1]"));
		element = driver.findElement(By.xpath("(//table[@class='forceRecordLayout slds-table slds-no-row-hover slds-table_cell-buffer slds-table_fixed-layout uiVirtualDataGrid--default uiVirtualDataGrid']//tbody//td[4]//span[@data-aura-class='uiOutputText'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 20 | AXITY
	 * @param txtNombreAntiguoEmpresa
	 * @info *** Texto Nombre Antiguo Empresa
	 * @return WebElement
	 */
	public static WebElement txtNombreAntiguoEmpresa(WebDriver driver) {
		element = driver.findElement(By.xpath("(//td[3]/span/span)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 20 | AXITY
	 * @param txtNombreNuevoEmpresa
	 * @info *** Texto Nombre Nuevo Empresa
	 * @return WebElement
	 */
	public static WebElement txtNombreNuevoEmpresa(WebDriver driver) {
		element = driver.findElement(By.xpath("(//td[4]/span/span)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 01 14 | AXITY
	 * @param linkARevisarRelacionado
	 * @info ***Link revisar elementos Menu de Navegacion Desplegable
	 * @return WebElement
	 */
	public static WebElement linkARevisarMenuNavegacionDesplegable(WebDriver driver, String nombreLink) {
		element = driver.findElement(By.xpath("//span[@class='menuLabel slds-listbox__option-text slds-listbox__option-text_entity'][contains(text(),'"+nombreLink+"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param btnCrearCuenta
	 * @info *** Boton Crear Cuenta Pagina Principal
	 * @return WebElement 
	 */
	public static WebElement btnCrearCuenta(WebDriver driver) {
		String ruta = "//a/div[@title='Nuevo']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param rdCuentaPersona
	 * @info *** Boton Radio Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement rdCuentaPersona(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[1]/label[1]/div[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Cuentas Personales')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param rdCuentaEmpresa
	 * @info *** Boton Radio Cuenta Empresa
	 * @return WebElement 
	 */
	public static WebElement rdCuentaEmpresa(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/fieldset[1]/div[2]/div[1]/label[1]/div[1]"));
		element = driver.findElement(By.xpath("//span[contains(text(),'Cuentas Empresas')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param btnSiguienteCrearCuenta
	 * @info *** Boton Siguiente Crear Cuenta
	 * @return WebElement 
	 */
	public static WebElement btnSiguienteCrearCuenta(WebDriver driver) {
		//element = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/button[2]/span[1]"));
		element = driver.findElement(By.xpath("//button/span[contains(text(),'Siguiente')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param inputNombreCuenta
	 * @info ***Input Nombre Cuenta 
	 * @return WebElement
	 */
	public static WebElement inputNombreCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Nombres']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param inputApellidoPaterno
	 * @info ***Input Apellido Paterno Cuenta 
	 * @return WebElement
	 */
	public static WebElement inputApellidoPaterno(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Apellido 1']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param inputApellidoMaterno
	 * @info ***Input Apellido Materno Cuenta 
	 * @return WebElement
	 */
	public static WebElement inputApellidoMaterno(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[contains(text(),'Apellido 2')]/following::input[@maxlength='40'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param selectEstadoCivil
	 * @info *** Select ESTADO CIVIL  Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement selectEstadoCivilCuenta(WebDriver driver) {
		element = driver.findElement(By.xpath("(//*[contains(text(),'Estado Civil')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param elementoSelectEstadoCivil
	 * @info *** Select Elemento ESTADO CIVIL  Cuenta Personal
	 * @return WebElement 
	 */
	public static WebElement elementoSelectEstadoCivil(WebDriver driver, String selectEstadoCivil) {
		element = driver.findElement(By.xpath("//a[contains(@title,'"+selectEstadoCivil+"')]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param inputNacionalidad
	 * @info *** elemento input PAIS CUENTA
	 * @return WebElement 
	 */
	public static WebElement inputNacionalidad(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@placeholder='Buscar Paises...'])[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 03 | AXITY
	 * @param inputPais
	 * @info *** elemento input PAIS CUENTA
	 * @return WebElement 
	 */
	public static WebElement inputPais(WebDriver driver) {
		element = driver.findElement(By.xpath("(//input[@placeholder='Buscar Paises...'])[2]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param inputReferenciaDireccion
	 * @info *** Campo Referencia Direccion Lead Personal
	 * @return WebElement 
	 */
	public static WebElement inputReferenciaDireccion(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input[1]"));
		//element = driver.findElement(By.xpath("//input[@data-interactive-lib-uid='16']"));
		String ruta = "//*[contains(text(),'Referencia de dirección')]/following::input[@maxlength='40']|//*[contains(text(),'Referencia de dirección')]/following::input[@maxlength='40']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param selectViaComunicacion
	 * @info *** Campo Via Comunicacion; aca se presenta como select pero no lo es en realidad CREAR CUENTA
	 * @return WebElement 
	 */
	public static WebElement selectViaComunicacion(WebDriver driver) {
		//(//*[contains(text(),'Vía de preferencia para comunicación')]/following::a[@class='select'][contains(text(),'--Ninguno--')])[1]
		String ruta = "//*[contains(text(),'Vía de preferencia para comunicación')]/following::a[@class='select'][contains(text(),'--Ninguno--')][1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2018 08 23 | AXITY
	 * @param elementoViaComunicacion
	 * @info *** Elemento Via Comunicacion Crear Cuenta
	 * @return WebElement 
	 */
	public static WebElement elementoViaComunicacion(WebDriver driver, String viaComunicacion) {
		element = driver.findElement(By.xpath("//a[@title='"+viaComunicacion+"']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 08 | AXITY
	 * @param txtNombrePaginaInicioCuenta
	 * @info *** Texto Nombre Creado Lead Personal 
	 * @return WebElement 
	 */
	public static WebElement txtNombrePaginaInicioCuenta(WebDriver driver) {
		//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]
		//element = driver.findElement(By.xpath("//h1//div[@class='slds-page-header__title slds-m-right--small slds-truncate slds-align-middle']"));
		//element = driver.findElement(By.xpath("//div[@class='slds-page-header__title slds-m-right--small slds-align-middle fade-text']//span[@class='uiOutputText'][contains(text(),'Sr.')]"));
		String ruta = "//div[@class='slds-page-header__title slds-m-right--small slds-align-middle fade-text']|(//div//span[@class='uiOutputText'][contains(text(),'Sr.')])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 08 | AXITY
	 * @param txtRutPaginaInicioCuenta
	 * @info *** Texto Rut Creado Lead Personal 
	 * @return WebElement 
	 */
	public static WebElement txtRutPaginaInicioCuenta(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]"));
		element = driver.findElement(By.xpath("//li[@role='listitem']//span[@class='uiOutputText']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 04 08 | AXITY
	 * @param txtTelefonoPaginaInicioCuenta
	 * @info *** Texto Telefono Creado Lead Personal 
	 * @return WebElement 
	 */
	public static WebElement txtTelefonoPaginaInicioCuenta(WebDriver driver) {
		//element = driver.findElement(By.xpath("//html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/div[2]/h1[1]/span[1]"));
		element = driver.findElement(By.xpath("(//span[@class='uiOutputPhone'][@dir='ltr'])[3]"));
		return element;
	}
}
