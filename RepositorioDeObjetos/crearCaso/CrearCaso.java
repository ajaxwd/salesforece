package crearCaso;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CrearCaso {
	
private static WebElement element = null;
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectElementoCaso
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectElementoCaso(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		String ruta = "//span[contains(text(),'Negocio')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectElementoArea
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectElementoArea(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		String ruta = "//span[contains(text(),'Área')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectElementoOrigen
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectElementoOrigen(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[1]"));
		String ruta = "//span[contains(text(),'Origen del caso')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectSegundoElementoCaso
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectSegundoElementoCaso(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		String ruta = "//span[contains(text(),'Categoría')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectSubcategoria
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectSubcategoria(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		String ruta = "//span[contains(text(),'Subcategoría')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param selectSubTipo
	 * @info *** Campos CASO; aca se presenta como select pero no lo es en realidad 
	 * @return WebElement 
	 */
	public static WebElement selectSubTipo(WebDriver driver) {
		//element = driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//a[@class='select'][contains(text(),'--Ninguno--')])[2]"));
		String ruta = "//span[contains(text(),'Subtipo')]//following::div//a[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param elementoSelectCaso
	 * @info *** Elemento NEGOCIO
	 * @return WebElement 
	 */
	public static WebElement elementoSelectCaso(WebDriver driver, String seleccion) {
		String ruta = "//a[@title='"+seleccion+"']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param textAreaDescripcion
	 * @info *** Text Area Descripcion
	 * @return WebElement 
	 */
	public static WebElement textAreaDescripcion(WebDriver driver) {
		element = driver.findElement(By.xpath("(//textarea)[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param inputActivoCaso
	 * @info *** Input Activo Caso
	 * @return WebElement 
	 */
	public static WebElement inputActivoCaso(WebDriver driver) {
		//element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Activo'])[1]/following::input[1]"));
		//element = driver.findElement(By.xpath("//div[@class='autocompleteWrapper slds-grow']//input[@class='default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']"));
		String ruta = "//div[5]/div/div/div/div/div/div/div/div/div/div/div/input";
		element = driver.findElement(By.xpath(ruta));	
		return element;
	}
			
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param btnGuardarCaso
	 * @info *** Boton Guardar Caso
	 * @return WebElement 
	 */
	public static WebElement btnGuardarCaso(WebDriver driver) {
		String ruta = "//button[@title='Guardar']//span[contains(@class,'label bBody')][contains(text(),'Guardar')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}		

	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param txtMensajeGuardado
	 * @info *** Texto Guardar Caso
	 * @return WebElement 
	 */
	public static WebElement txtMensajeGuardado(WebDriver driver) {
		String ruta = "//section/div/div/div/div";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param txtMensajeGuardadoCampana
	 * @info *** Texto Guardar Campaña
	 * @return WebElement 
	 */
	public static WebElement txtMensajeGuardadoCampana(WebDriver driver) {
		String ruta = "//span[@class='toastMessage slds-text-heading--small forceActionsText']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param txtMensajeGuardadoModoficarCampana
	 * @info *** Texto Guardar Modoficar Campaña
	 * @return WebElement 
	 */
	public static WebElement txtMensajeGuardadoModoficarCampana(WebDriver driver) {
		String ruta = "//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 18 | AXITY
	 * @param linkMensajeGuardado
	 * @info *** Link Guardar Caso
	 * @return WebElement 
	 */
	public static WebElement linkMensajeGuardado(WebDriver driver) {
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='success'])[1]/following::div[3]"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 19 | AXITY
	 * @param linkCasos
	 * @info *** Link Casos
	 * @return WebElement 
	 */
	public static WebElement linkCasos(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@title='Casos']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param txtNuevo
	 * @info *** Texto Nuevo
	 * @return WebElement 
	 */
	public static WebElement txtNuevo(WebDriver driver) {
		element = driver.findElement(By.linkText("Nuevo"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param txtEnProceso
	 * @info *** Texto En Proceso
	 * @return WebElement 
	 */
	public static WebElement txtEnProceso(WebDriver driver) {
		element = driver.findElement(By.linkText("En Proceso"));
		return element;
	}		
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param linkActividad
	 * @info *** Link Actividad
	 * @return WebElement 
	 */
	public static WebElement linkActividad(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='windowViewMode-normal oneContent active lafPageHost']//span[@class='title'][contains(text(),'Actividad')]"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param linkNuevaTarea
	 * @info *** Link Nueva Tarea
	 * @return WebElement 
	 */
	public static WebElement linkNuevaTarea(WebDriver driver) {
		element = driver.findElement(By.xpath("//section[2]/div/div/div/div/ul/li[2]/a/span[2]"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param inputAsunto
	 * @info *** Input Asunto
	 * @return WebElement 
	 */
	public static WebElement inputAsunto(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div/div/div/div/div/lightning-grouped-combobox/div/div/lightning-base-combobox/div/div/input"));
		element = driver.findElement(By.xpath("//label[contains(text(),'Asunto')]//following::input[1]"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param selectPrimerContacto
	 * @info *** Select Primer Contacto
	 * @return WebElement 
	 */
	public static WebElement selectPrimerContacto(WebDriver driver) {
		//element = driver.findElement(By.xpath("//lightning-base-combobox-item[2]/span[2]/lightning-base-combobox-formatted-text"));
		element = driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='SAC - Primer Contacto']"));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param inputFechaVencimiento
	 * @info *** Input Fecha Vencimiento
	 * @return WebElement 
	 */
	public static WebElement inputFechaVencimiento(WebDriver driver) {
		element = driver.findElement(By.xpath("//div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime']//input[@type='text']\r\n" + 
				""));
		return element;
	}	
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param linkHoy
	 * @info *** Link Hoy Fecha Vencimiento
	 * @return WebElement 
	 */
	public static WebElement linkHoy(WebDriver driver) {
		element = driver.findElement(By.xpath("//span[@class='today slds-show--inline-block slds-text-link slds-p-bottom--x-small']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param btnGuardarTarea
	 * @info *** Boton Guardar Tarea
	 * @return WebElement 
	 */
	public static WebElement btnGuardarTarea(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[@class='bottomBarRight slds-col--bump-left']//span[contains(@class,'label bBody')][contains(text(),'Guardar')]"));
		//element = driver.findElement(By.xpath("(//div[2]/div[2]/button/span)[2]"));
		element = driver.findElement(By.xpath("//div[3]/div/div/div[2]/div[2]/button/span"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param btnMarcarEstado
	 * @info *** Boton Marcar Estado
	 * @return WebElement 
	 */
	public static WebElement btnMarcarEstado(WebDriver driver) {
		//element = driver.findElement(By.xpath("//div[3]/div/div/div/div[2]/div/div/div/div/div[2]/button/span"));
		element = driver.findElement(By.xpath("//div[@class='slds-grid slds-path__action runtime_sales_pathassistantPathAssistantHeader']//button[@type='button']"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param txtMensajeTarea
	 * @info *** Texto Mensaje Tarea
	 * @return WebElement 
	 */
	public static WebElement txtMensajeTarea(WebDriver driver) {
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='success'])[1]/following::span[1]"));
		return element;
	}
	
	/**
	 * @author Daniel Salinas | 2019 02 21 | AXITY
	 * @param linkEnProceso
	 * @info *** Link En Proceso
	 * @return WebElement 
	 */
	public static WebElement linkEnProceso(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[@title='En Proceso']"));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 12 10 | AXITY
	 * @param rdNuevoCaso
	 * @info *** Seleccionar radio de reclamo en la creacion de un caso
	 * @return WebElement 
	 */
	public static WebElement rdNuevoCaso(WebDriver driver) {
		String ruta = "(//label/div/span[@class='slds-radio--faux'])[1]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 12 10 | AXITY
	 * @param btnSiguiente
	 * @info *** Seleccionar boton siguiente en la creacion del reclamo
	 * @return WebElement 
	 */
	public static WebElement btnSiguiente(WebDriver driver) {
		String ruta = "//button/span[contains(text(),'Siguiente')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 12 10 | AXITY
	 * @param inputCentro
	 * @info *** Seleccionar combobox centro
	 * @return WebElement 
	 */
	public static WebElement inputCentro(WebDriver driver) {
		String ruta = "//label/span[contains(text(),'Centro')]/following::input[@placeholder='Buscar Centros...']";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
	
	/**
	 * @author Adrian Siñiga | 2019 12 10 | AXITY
	 * @param selectCentro
	 * @info *** Seleccionar combobox centro
	 * @return WebElement 
	 */
	public static WebElement selectCentro(WebDriver driver, String centro) {
		String ruta = "//div/mark[contains(text(),'"+ centro +"')]";
		element = driver.findElement(By.xpath(ruta));
		return element;
	}
}


