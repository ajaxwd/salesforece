package Maquinarias;

import org.openqa.selenium.WebDriver;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class Datos_Excel_B2B {
	
	static int recorreExcell = 0;
	static int inicioExcell = 0;
	static String paisEjecucion = "";
	static String regionEjecucion = "";
	static String provinciaEjecucion = "";
	static String comunaEjecucion = "";
	static String modeloInteres = "";
	static String ingresoLocal = "";
	static int posicionExcel = 1;
	
	public static void Excel_Escenario(String sNumeroEscenario) throws Exception{
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {

				posicionExcel = i;
				break;
			}
		}
	}
	
	public static String[] Campos_Excel(WebDriver driver) throws Exception{

		while (recorreExcell == 0) {
			if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				String mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
				utilidades.Funciones.mensajesLog(mensaje);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>" + "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}

		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Region".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				regionEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>" + "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}

		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Provincia".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				provinciaEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>" + "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}

		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Comuna".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				comunaEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
		
		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Marca de Interes".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				modeloInteres = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				System.err.println("Modelo " + modeloInteres);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
		
		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Local".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				ingresoLocal = ExcelUtils.getCellData(posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
		
		String[] CamposExcel = {paisEjecucion, 
				regionEjecucion, 
				provinciaEjecucion, 
				comunaEjecucion, 
				modeloInteres, 
				ingresoLocal};
		
		return CamposExcel;

	}

	public static void Excel_Validacion(WebDriver driver, String nombreAleatorio, String apellidoPaternoAleatorio, String rut, String numTelefono) throws Exception{
		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Nombre Persona Lead Creado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				String setNombreLead = nombreAleatorio + " " + apellidoPaternoAleatorio;
				ExcelUtils.setCellData(setNombreLead, posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Nombre Persona Lead Creadoo' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
		
		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Rut LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				String setRut = rut;
				ExcelUtils.setCellData(setRut, posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Rut LEAD' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
		
		recorreExcell = 0;
		inicioExcell = 0;
		while (recorreExcell == 0) {
			if ("Telefono LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
				String setTelefono = numTelefono;
				ExcelUtils.setCellData(setTelefono, posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Telefono LEAD' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				utilidades.Funciones.mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}

	}
}
