package Maquinarias;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

//@Test(groups = { "Primary" })
public class Crear_Lead_B2B {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	 @Test
	//@Test(groups = { "Primary" })
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		mensaje = "<p>"+"Escenario en Ejecucion : " + sNombrePrueba+"</p>";
		utilidades.Funciones.mensajesLog(mensaje);
		
		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				mensaje = "<p>"+"Numero del Escenario de Ejecucion : " + sNumeroEscenario +"</p>";
				utilidades.Funciones.mensajesLog(mensaje);
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(31, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(31, 3);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		String navegador = utilidades.DatosInicialesYDrivers.Browser;
		String url = utilidades.DatosInicialesYDrivers.URL;
		
		mensaje = "<p>URL: " + url + "</p>";
		utilidades.Funciones.mensajesLog(mensaje);
		
		//utilidades.Funciones.navegador(driver, navegador, url, nombreCapturaClase);
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			utilidades.Funciones.funcionPausa(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			utilidades.Funciones.funcionPausa(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			Funciones.funcionPausa(2000);
			break;

		case "chrome":
			utilidades.Funciones.funcionPausa(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			utilidades.Funciones.funcionPausa(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			break;

		case "IE32":
			utilidades.Funciones.funcionPausa(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			utilidades.Funciones.funcionPausa(4000);
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			break;

		case "IE64":
			utilidades.Funciones.funcionPausa(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			utilidades.Funciones.funcionPausa(4000);
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			break;

		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		//
		// Inicio de Sesion
		
		login.IniciarSesion.Execute(driver, nombreCapturaClase);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			String paisEjecucion = utilidades.Funciones.getExcel(driver, "Pais", posicionExcel);
			mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			String regionEjecucion = utilidades.Funciones.getExcel(driver, "Region", posicionExcel);
			String provinciaEjecucion = utilidades.Funciones.getExcel(driver, "Provincia", posicionExcel);
			String comunaEjecucion = utilidades.Funciones.getExcel(driver, "Comuna", posicionExcel);
			String modeloInteres = utilidades.Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			String ingresoLocal = utilidades.Funciones.getExcel(driver, "Local", posicionExcel);
			
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				utilidades.Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }
			
			int cierraLlamado = 0;			
			while (cierraLlamado == 0) {	
			utilidades.Funciones.funcionPausa(2000);	
			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Tel�fono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					utilidades.Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
			} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}
			
			//Selecciona menu principal opcion Leads
			utilidades.Funciones.funcionPausa(2000);
			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearLead(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Crear Lead ";
			msj = "ClickBtnCrearLead";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(3000);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(10000);

			String rut = "";
			String tipoDocumentoIdentidad = "";
			switch (paisEjecucion) {
			case "Chile":

				// Cadena de caracteres aleatoria Para Rut
				rut = utilidades.Funciones.generadorDeRut(9000000, 22000000);
				crearLead.Personal.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
				msj = "IngresoRut";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			case "Per�":

				WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
				waitSelectEstadoLead.until(ExpectedConditions.elementToBeClickable(crearLead.Personal.selectDocumentoIdentidad(driver)));
				crearLead.Personal.selectDocumentoIdentidad(driver).sendKeys(Keys.ENTER);
				utilidades.Funciones.funcionPausa(2000);
				tipoDocumentoIdentidad = "DNI";
				utilidades.Funciones.funcionPausa(2000);
				crearLead.Personal.tipoDocumentoIdentidad(driver, tipoDocumentoIdentidad).click();
				mensaje = "Flujo : Seleccion Documento Identidad : " + tipoDocumentoIdentidad;
				mensaje = "Flujo : Seleccion Documento Identidad :" + "DNI";
				msj = "SeleccionDocumentoIdentidad";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				// Cadena de caracteres aleatoria Para DNI
				rut = utilidades.Funciones.generadorDeRut(10000000, 22000000);
				String vectorRut[] = rut.split("-");
				System.err.println("*"+vectorRut[0]+"*");
				System.err.println("*"+vectorRut[1]+"*");
				rut=vectorRut[0];
				crearLead.Personal.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de DNI Aleatorio : " + rut;
				msj = "IngresoDni";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}

			utilidades.Funciones.funcionPausa(3000);
			crearLead.Personal.selectTratamiento(driver).click();
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Personal.srTratamiento(driver).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);			

			crearLead.Personal.selectOrigen(driver).click();
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Personal.webOrigen(driver).click();
			mensaje = "Flujo : Ingreso de Origen : " + "WEB";
			msj = "IngresoOrigen";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(1000);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			crearLead.Personal.inputNombre(driver).sendKeys(nombreAleatorio);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(3000);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputApellidoPaterno(driver).sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPat";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(3000);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputApellidoMaterno(driver).sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMat";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(3000);

			crearLead.Maquinaria.selectPreferencia(driver).click();
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Maquinaria.PreferenciaContacto(driver).click();
			mensaje = "Flujo : Seleccion Prioridad : Preferencia de medio de contacto";
			msj = "SeleccionPreferencia";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(2000);

			// Cadena de caracteres aleatoria Para Nombre
			String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
			referidoAleatorio = referidoAleatorio + " " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputReferidoPor(driver).sendKeys(referidoAleatorio);
			mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
			msj = "IngresoNombreRef";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(2000);

			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			correo = correo + correoAzar + "@derco.cl";
			correo = correo.toLowerCase();
			crearLead.Personal.inputCorreo(driver).clear();
			utilidades.Funciones.funcionPausa(1000);
			crearLead.Personal.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			String numMovil = utilidades.Funciones.numeroMovil();
			crearLead.Personal.inputMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numTelefono = utilidades.Funciones.numeroTelefono();

			switch (paisEjecucion) {
			case "Chile":
				
				numTelefono = "2" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
				msj = "IngresoTelefono";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			case "Per�":

				numTelefono = "0" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : "  + numTelefono;
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			default:
				break;
			}
			
			utilidades.Funciones.funcionPausa(4000);
			utilidades.Funciones.moverScroll(driver, crearLead.Maquinaria.UnidadNegocio(driver));
			crearLead.Maquinaria.inputNegocio(driver).click();
			
			crearLead.Maquinaria.elementoNegocio(driver).click();
			mensaje = "Flujo : Seleccion Unidad de negocio : Agr�cola";
			msj = "IngresoNegocio";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(1500);
			
			switch(paisEjecucion) {
			case "Chile":
				utilidades.Funciones.moverScroll(driver, crearLead.Maquinaria.Familia(driver));
				crearLead.Maquinaria.inputFamilia(driver).click();
				
				crearLead.Maquinaria.elementoFamilia(driver).click();
				mensaje = "Flujo : Seleccion Unidad de familia: Tractores";
				msj = "IngresoFamilia";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.funcionPausa(1500);
				
			case "Per�":
				utilidades.Funciones.moverScroll(driver, crearLead.Maquinaria.Familia(driver));
				crearLead.Maquinaria.inputFamilia(driver).click();
				
				crearLead.Maquinaria.elementoFamiliaPeru(driver).click();
				mensaje = "Flujo : Seleccion Unidad de familia: Implementos";
				msj = "IngresoFamilia";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.funcionPausa(1500);
			}

			utilidades.Funciones.moverScroll(driver, crearLead.Personal.inputMarcaInteres(driver));
			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,"Hoja1");

			crearLead.Maquinaria.inputMarcaInteres(driver).click();
			crearLead.Maquinaria.elementoMarcaInteres(driver).click();
			utilidades.Funciones.funcionPausa(4000);
			mensaje = "Flujo : Seleccion Producto : Landini";
			msj = "IngresoProducto";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(1500);
			
			switch (paisEjecucion) {
			case "Chile":
								
				break;

			case "Per�":
				
				String[] arregloIngresoLocal = new String[ingresoLocal.length()];
				for (int i = 0; i < ingresoLocal.length(); i++) {
					arregloIngresoLocal[i] = Character.toString(ingresoLocal.charAt(i));
				}
				crearLead.Personal.inputPais(driver).click();
				for (int i = 0; i < ingresoLocal.length(); i++) {

					crearLead.Personal.inputLocal(driver).sendKeys(arregloIngresoLocal[i]);
					Thread.sleep(500);

				}

				mensaje = "Flujo : Ingreso Campo Local : " + ingresoLocal;
				utilidades.Funciones.mensajesLog(mensaje);
				utilidades.Funciones.funcionPausa(2000);
				crearLead.Personal.localSeleccion(driver, ingresoLocal).click();
				mensaje = "Flujo : Seleccion de Local  : " + ingresoLocal;
				msj = "SeleccionLocal";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}
			
			utilidades.Funciones.funcionPausa(1500);
			
			switch (paisEjecucion) {
			case "Chile":

				// Cadena de caracteres aleatoria Para Calle
				String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				String calleAleatorioNumero = utilidades.Funciones.calleAleatorioNumero();

				crearLead.Personal.inputCalle(driver).sendKeys(calleAleatorio + " " + calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio + " " + calleAleatorioNumero;
				msj = "IngresoCalle";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				crearLead.Personal.inputComplementoDireccion(driver).sendKeys("Casa Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			case "Per�":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearLead.Personal.inputDireccion(driver).sendKeys(calleAleatorio );
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.funcionPausa(1500);
				
				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: "+calleAleatorioNumero;
				msj = "IngresoCalle";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.funcionPausa(1500);
				
				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.Funciones.funcionPausa(1500);
				
				crearLead.Personal.inputReferenciaDireccion(driver).sendKeys(calleAleatorio + " Casa Interior");
				mensaje = "Flujo : Campo Referencia Direccion :" + calleAleatorio + " Casa Interior";
				msj = "IngresoComplementoDir";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			default:
				break;
			}

			String regionIngresar = "";
			String regionIngresarAlternativo = "";
			String provinciaIngresar = "";
			String comunaIngresar = "";

			utilidades.Funciones.funcionPausa(1000);
			utilidades.Funciones.moverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));
			utilidades.Funciones.funcionPausa(1000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//div[2]/div/div[2]/div/div/div[3]")));
			utilidades.Funciones.funcionPausa(1000);

			switch (paisEjecucion) {
			case "Chile":

				regionIngresar = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				comunaIngresar = "Buscar Comunas...";
				break;

			case "Per�":

				regionIngresar = "Buscar Departamentos...";
				regionIngresarAlternativo = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				comunaIngresar = "Buscar Distritos...";
				
				break;

			default:
				break;
			}

			String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			crearLead.Personal.inputPais(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {

				crearLead.Personal.inputPais(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);

			}

			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Personal.paisSeleccionLead(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(2000);

			String[] arregloRegionEjecucion = new String[regionEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloRegionEjecucion[i] = Character.toString(regionEjecucion.charAt(i));
			}
			crearLead.Personal.inputRegion(driver).click();
			for (int i = 0; i < regionEjecucion.length(); i++) {

				crearLead.Personal.inputRegion(driver).sendKeys(arregloRegionEjecucion[i]);
				Thread.sleep(500);

			}

			utilidades.Funciones.funcionPausa(2000);
			mensaje = "Flujo : Ingreso Campo Region/Departamento : " + regionEjecucion;
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Personal.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region/Departamento  : " + regionEjecucion;
			msj = "SeleccionRegion";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(1000);
			
			utilidades.Funciones.moverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));
			utilidades.Funciones.funcionPausa(1000);

			String[] arregloProvinciaEjecucion = new String[provinciaEjecucion.length()];
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				arregloProvinciaEjecucion[i] = Character.toString(provinciaEjecucion.charAt(i));
			}
			crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {

				crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(arregloProvinciaEjecucion[i]);
				Thread.sleep(500);

			}

			utilidades.Funciones.funcionPausa(2000);
			mensaje = "Flujo : Ingreso Campo Provincia : " + provinciaEjecucion;
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			crearLead.Personal.provinciaSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion de Provincia  : " + provinciaEjecucion;
			msj = "SeleccionProvincia";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			utilidades.Funciones.funcionPausa(1000);
			utilidades.Funciones.moverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));
			utilidades.Funciones.funcionPausa(1000);

			String[] arregloComunaEjecucion = new String[comunaEjecucion.length()];
			for (int i = 0; i < comunaEjecucion.length(); i++) {
				arregloComunaEjecucion[i] = Character.toString(comunaEjecucion.charAt(i));
			}
			crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {

				utilidades.Funciones.moverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));
				crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(arregloComunaEjecucion[i]);
				Thread.sleep(500);

			}

			utilidades.Funciones.funcionPausa(2000);
			mensaje = "Flujo : Ingreso Campo Comuna/Distrito : " + comunaEjecucion;
			utilidades.Funciones.mensajesLog(mensaje);
			utilidades.Funciones.funcionPausa(2000);
			utilidades.Funciones.moverScroll(driver, crearLead.Personal.comunaSeleccion(driver,comunaIngresar,comunaEjecucion));				
			utilidades.Funciones.funcionPausa(1000);
			crearLead.Personal.comunaSeleccion(driver,comunaIngresar,comunaEjecucion).click();			
			mensaje = "Flujo : Seleccion de Comuna/Distrito : " + comunaEjecucion;
			msj = "SeleccionComuna";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			crearLead.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			utilidades.Funciones.funcionPausa(3000);
			
			try {
				
				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta p�gina.")) {
					
					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta p�gina :  " +crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText()+ "</p>";
					msj = "ErrorPruebaFallida";
					utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append( "\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
					
				}
				
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
				
			}

			utilidades.Funciones.funcionPausa(10000);
			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearLead.Personal.txtNombrePaginaInicio(driver)));
			String comparar = crearLead.Personal.txtNombrePaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creaci�n correcta de nuevo lead personal";
			utilidades.Funciones.mensajesLog(mensaje);
			String nombreCompleto = nombreAleatorio + " " + apellidoPaternoAleatorio;

			if (comparar.contains(nombreCompleto)) {
				mensaje = "Ok : Coincide Nombre Ingresado " + nombreCompleto
						+ " con Nombre desplegado en Creaci�n de Lead " + comparar;
				msj = "OkCoincideNombre";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
				// ValorEncontrado = false;

			} else {
				mensaje = "<p style='color:red;'>" + "Error : No Coincide Nombre Ingresado con Nombre desplegado en Creaci�n de Lead " + "</p>";
				msj = "ErrorNoCoincideNombre";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta, Se Crea Con exito LEAD Maquinaria " + "<p>";
				msj = "OkPruebaCorrecta";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,"Hoja1");
				
				utilidades.Funciones.setExcel(driver, "Nombre Persona Lead Creado", nombreCompleto);				
				utilidades.Funciones.setExcel(driver, "Rut LEAD", rut);
				utilidades.Funciones.setExcel(driver, "Telefono LEAD", numTelefono);
				
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea LEAD Persona de Manera Correcta" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException |ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No se Encuentra el Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecuci�n.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEnEjecucion";
			utilidades.Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			utilidades.Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
