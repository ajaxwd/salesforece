package login;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.ScreenshotException;
import org.testng.Reporter;

import utilidades.ExcelUtils;
import utilidades.Funciones;

public class IniciarSesion {

	private static Logger Log = Logger.getLogger(Log.class.getName());

	public static void Execute(WebDriver driver, String nombreCapturaClase, String Usuario, String Password) throws Exception {

		driver.manage().window().maximize();
		Logger.getRootLogger().setLevel(Level.OFF);
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 0;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String mensaje = "";
		String mensajeError = "";

		// Se verifica si hay sesion anterior abierta
		try {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

			if (login.Login.imgUsuarioPaginaPrincipal(driver).isDisplayed()) {
				mensaje = "Flujo : Se encuentra sesion abierta ";
				msj = "SesionAbierta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(2000);
				login.Login.imgUsuarioPaginaPrincipal(driver).click();

				login.Login.lnkCerrarSesion(driver).click();
				Thread.sleep(2000);
				if (login.Login.inputIngresarUsuario(driver).isDisplayed()) {
					mensaje = "OK : Se cierra sesion anterior ";
					msj = "CierraSesion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					System.out.println(mensaje);
					Thread.sleep(2000);
				}

			}

		} catch (NoSuchElementException | ScreenshotException e) {
			mensaje = "OK : No Encuentra Sesiones Anteriores Abiertas ";
			msj = "OkNoSesionAnterior";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(2000);
		}

//		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(31, 2);
//		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(31, 3);
		
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		System.out.println("Iniciando accion de Login");
		Reporter.log("<br>" +"<p>" + "Iniciando accion de Login"+ "</p>");
		Log.info("Iniciando accion de login");

		// inicia ingreso de datos login
		login.Login.inputIngresarUsuario(driver).clear();
		login.Login.inputIngresarUsuario(driver).sendKeys(utilidades.DatosInicialesYDrivers.Usuario);
		login.Login.inputIngresarContrsena(driver).clear();
		login.Login.inputIngresarContrsena(driver).sendKeys(utilidades.DatosInicialesYDrivers.Password);
		mensaje = "Flujo : Ingreso de datos para 'Login' ";
		msj = "DatosLoguin";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(500);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
		login.Login.btnIniciarSesion(driver).click();

		// se verifica que usuario y contrase�a sea correcto
		try {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			mensajeError = Login.txtErrorIngreso(driver).getText();
			if (mensajeError.contains("Por favor verifique su nombre de usuario y contrase�a. ")) {
				mensaje = "<p style='color:red;'>" + "ERROR : Error de Usuario o Contrase�a" + "</p>";
				msj = "ErrorCredenciales";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(2000);
			} else {
				mensaje = "OK : No Encuentra Errores en datos ingresados para 'Login' ";
				msj = "OkLogin";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(3000);				
			}

		} catch (NoSuchElementException | ScreenshotException e) {
			mensaje = "OK : No Encuentra Errores en datos ingresados para 'Login' ";
			msj = "OkNohayErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(2000);
		}
	}
}





