package utilidades;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;



public class ExcelUtils {

	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell xCell;
	private static XSSFRow xRow;


	// metodo para inicializar la ruta y abrir el archivo excel, se pasan los valores de la ruta y la hoja con la que se trabajara
	public static void setExcelFile(String Path, String SheetName) throws Exception {

		try {
			// abre el archivo exce
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Accede a al hoja deseada
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
		} catch (Exception e) {
			throw (e);
		}
	}


	// metodo para leer datos desde la celda, RowNum y ColNum son los parametros de entrada
	public static String getCellData(int RowNum, int ColNum) throws Exception {
		try {
			xCell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = xCell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			return "";
		}
	}


	//metodo para escribir en el archivo excel Row y ColNum son los parametros

	//public static void setCellData(String Result, int RowNum, int ColNum) throws Exception {
		public static String setCellData(String Result, int RowNum, int ColNum) throws Exception {
		try {
			xRow = ExcelWSheet.getRow(RowNum);
			//xCell = xRow.getCell(ColNum, Row.RETURN_NULL_AND_BLANK);
			 xCell = xRow.getCell(ColNum, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL); 
			if (xCell == null) {
				xCell = xRow.createCell(ColNum);
				xCell.setCellValue(Result);
			} else {
				xCell.setCellValue(Result);
				
			}

			// Variables de la clase Constant.java se envian los valores al archivo
			FileOutputStream fileOut = new FileOutputStream(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData);
			ExcelWBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			throw (e);

		}
		return Result;

	}

}
