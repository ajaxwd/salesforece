package utilidades;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import freemarker.core.ReturnInstruction.Return;
import utilidades.ExcelUtils;
import utilidades.DatosInicialesYDrivers;
import login.IniciarSesion;

public class Funciones {
	
	
	public static String RutaDriverChrome = utilidades.DatosInicialesYDrivers.RutaDriverChrome;
	public WebDriver driver;
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	private static Logger Log = Logger.getLogger(Log.class.getName());

	// metodo para generar cadenas de texto aleatorio de largo indicado en el valor
	// de entrada
	/**
	 * @param cadenaDeTextoAleatorio
	 * @return genera un string de segun el largo indicado como 
	 * numero de entrada,
	 *         el valor retornado corresponde al string con el largo
	 *         indicado @throws
	 */
	public static String cadenaDeTextoAleatorio(int largoCadenaDeTexto) throws Exception {

		//
		int limiteIzquierda = 97; // Letra 'a'
		int limiteDerecha = 122; // letra 'z'
		int largoStringNombre = largoCadenaDeTexto;
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(largoStringNombre);
		for (int i = 0; i < largoStringNombre; i++) {
			int randomLimitedInt = limiteIzquierda + (int) (random.nextFloat() * (limiteDerecha - limiteIzquierda + 1));
			buffer.append((char) randomLimitedInt);
		}
		String textoAleatorio = buffer.toString();
		return textoAleatorio;

	}
	/**
	 * 
	 * @param navegador
	 * @return Se carga el navegador que se utilizara en la prueba
	 * @throws IOException 
	 */
//throws IOException
	public static WebDriver navegador(WebDriver driver, String navegador, String url, String nombreCapturaClase) throws Exception {
		System.err.println("estoy aqui 1 " + navegador + " " + url + " " + RutaDriverChrome);
		switch (navegador) {
			case "firefox":
				funcionPausa(2000);
				Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
				Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
				funcionPausa(4000);
				System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
				driver = new FirefoxDriver();
				String mensaje = "<p>" + "Navegador : " + navegador + "<br>" + "URL " + url + "</p>";
				mensajesLog(mensaje);
				funcionPausa(2000);
				break;
	
			case "chrome":
				funcionPausa(3000);
				Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
				Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
				funcionPausa(4000);
				System.setProperty("webdriver.chrome.driver", RutaDriverChrome);
				driver = new ChromeDriver();
				mensaje = "<p>" + "Navegador: " + navegador + " <br>" + "URL " + url + "</p>";
				mensajesLog(mensaje);
				funcionPausa(2000);
//				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//				driver.get(url);
//				login.IniciarSesion.Execute(driver, nombreCapturaClase);
				break;
	
			case "IE32":
				Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
				Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
				System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
				driver = new InternetExplorerDriver();
				mensaje = "<p>" + "Navegador : " + navegador + "<br>" + "URL " + url + "</p>";
				mensajesLog(mensaje);
				funcionPausa(2000);
				break;
	
			case "IE64":
				Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
				Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
				System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
				driver = new InternetExplorerDriver();
				mensaje = "<p>" + "Navegador : " + navegador + "<br>" + "URL " + url + "</p>";
				mensajesLog(mensaje);
				funcionPausa(2000);
				break;
			}
			return driver;
	}

	/**
	 * 
	 * @param evidencia
	 * @return Carga imagenes en el reporte con la evidencia
	 */
	public static String evidencia(String msj, WebDriver driver, String nombreCapturaClase, int posicionEvidencia){ 
		String posicionEvidenciaString = Integer.toString(posicionEvidencia);
		String nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		funcionPausa(2000);
		String rutaEvidencia = CaptureScreen(driver, nombreCaptura + "_" + msj);
		return rutaEvidencia;
	}

	/**
	 * 
	 * @param mensajesLog
	 * @return genera mensajes en el transcurso de de la ejecucion de la prueba
	 * @throws Exception
	 */
	public static void mensajesLog(String mensaje){
		Log.info(mensaje);
		Reporter.log("<br>" + mensaje);
		System.out.println(mensaje);
	}

	/**
	 * 
	 * @param mensajesLogRuta
	 * @return genera mensajes en el transcurso de de la ejecucion de la prueba, con ruta evidencia
	 * @throws Exception
	 */
	public static void mensajesLogRuta(String mensaje, String rutaEvidencia){
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src="+ rutaEvidencia + ">");
		System.err.println(mensaje);
	}
	
	/**
	 * 
	 * @param mensajesLogError
	 * @return genera mensajes en el transcurso de de la ejecucion de la prueba con errores
	 * @throws Exception
	 */
	public static void mensajesLogError(String mensaje){
		Log.info(mensaje);
		Reporter.log(mensaje);
		System.err.println(mensaje);
	}
	/**
	 * @param numeroMovil
	 * @return genera un numero en forma aleatoria segun el valor minimo y el valor
	 *         maximo ingresado para un telefono celular
	 * @throws Exception
	 */
	public static String numeroMovil() throws Exception {
		String numMovil = "";
		String azar = "";
		// Cadena de numeros aleatoria Para telefono movil
		for (int i = 0; i < 8; i++) {
			azar = numeroAleatorio(1, 8);
			numMovil = numMovil + azar;
		}
		return numMovil;
	}
	
	/**
	 * @param getExcel
	 * @return buscar campo en excel que sera ocupado en el sistema
	 * @throws Exception
	 */
	public static String getExcel(WebDriver driver, String nombre, int posicionExcel) throws Exception{
		int recorreExcell = 0;
		int inicioExcell = 0;
		String ejecucion = "";
		while (recorreExcell == 0) {
			if (nombre.equals(utilidades.ExcelUtils.getCellData(0, inicioExcell))) {
				ejecucion = utilidades.ExcelUtils.getCellData(posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
				+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}	
		return ejecucion;
	} 
	/**
	 * @param setExcel
	 * @return ingresar datos en excel para su comparacion, con datos antes trabajados
	 * @throws Exception
	 */
	public static void setExcel(WebDriver driver, String nombre, String comparar) throws Exception{
		int posicionExcel = 1;
		int recorreExcell = 0;
		int inicioExcell = 0;
		while (recorreExcell == 0) {
			if (nombre.equals(utilidades.ExcelUtils.getCellData(0, inicioExcell))) {
				String setComparar = comparar;
				utilidades.ExcelUtils.setCellData(setComparar, posicionExcel, inicioExcell);
				recorreExcell = 1;
				break;
			}
			inicioExcell = inicioExcell + 1;
			recorreExcell = 0;
			if (inicioExcell > 150) {
				String mensaje = "<p style='color:red;'>"
				+ "Error : No encuentra elemento" + nombre + "en Archivo de Ingreso de datos Favor Revisar" + "</p>";
				mensajesLogError(mensaje);
				driver.quit();
				throw new AssertionError();
			}
		}
	}

	/**
	 * @param moverScroll
	 * @return Mueve el Scroll de pa pagina
	 * @throws Exception
	 */
	public static void moverScroll(WebDriver driver, WebElement elemento){
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		String DOM = "arguments[0].scrollIntoView();"; 
		jse.executeScript(DOM, elemento);
	}
	
	/**
	 * @param calleAleatorioNumero
	 * @return genera un numero de calle aleatorio
	 * @throws Exception
	 */
	public static String calleAleatorioNumero() throws Exception{
		String calleAleatorioNumero = "";
		String azar = "";
		// Cadena de numeros aleatoria Para numero Calle Laboral
		for (int i = 0; i < 2; i++) {
			azar = numeroAleatorio(1, 8);
			calleAleatorioNumero = calleAleatorioNumero + azar;
		}
		return calleAleatorioNumero;
	}
	/**
	 * @param numeroTelefono
	 * @return genera un numero en forma aleatoria segun el valor minimo y el valor
	 *         maximo ingresado para un telefono fijo
	 * @throws Exception
	 */
	public static String numeroTelefono() throws Exception {
		String numTelefono = "";
		String azar = "";
		// Cadena de numeros aleatoria Para telefono movil
		for (int i = 0; i < 8; i++) {
			azar = numeroAleatorio(1, 8);
			numTelefono = numTelefono + azar;
		}
		return numTelefono;
	}

	/**
	 * @param numeroAleatorio
	 * @return genera un numero en forma aleatoria segun el valor minimo y el valor
	 *         maximo ingresado
	 * @throws Exception
	 */
	public static String numeroAleatorio(int numeroMinimo, int numeroMaximo) throws Exception {

		int minimo = numeroMinimo;
		int maximo = numeroMaximo;
		Random randomNum = new Random();
		int numeroAleatorio = minimo + randomNum.nextInt(maximo);
		String numeroAleatorioString = Integer.toString(numeroAleatorio);
		return numeroAleatorioString;
	}

	/**
	 * @param generadorDeRut
	 * @return genera un numero numero de rut con formato xxxxxxxx-x a partir de un
	 *         numero minimo y un numero maximo, requiere que el largo del rut sin
	 *         digito verficador se minimo 7
	 * @throws Exception
	 */
	public static String generadorDeRut(int numeroMinimo, int numeroMaximo) throws Exception {
		// genera un numero de largo 7 caracteres
		int minimo = numeroMinimo;
		int maximo = numeroMaximo;
		Random randomNum = new Random();
		int numeroAleatorio = minimo + randomNum.nextInt(maximo);
		String rutSinDV = Integer.toString(numeroAleatorio);

		String codigo;
		String rutConDV = "";
		int multiplo = 2;
		int cont = 0;

		for (int x = 0; x < rutSinDV.length(); x++) {
			cont = cont + (Integer.parseInt(rutSinDV.substring(rutSinDV.length() - x - 1, rutSinDV.length() - x))
					* multiplo);
			multiplo++;
			if (multiplo == 8) {
				multiplo = 2;
			}
		}

		cont = 11 - (cont % 11);

		if (cont <= 9) {
			codigo = "" + cont;
		} else if (cont == 11) {
			codigo = "0";
		} else {
			codigo = "K";
		}

		if (codigo != null) {
			rutConDV = rutSinDV + "-" + codigo;
		}

		return rutConDV;
	}

	/**
	 * @param funcionNombreAleatorio()
	 * @return Genera Nombre al Azar de entre los casos de la funcion @throws
	 */
	public static String funcionNombreAleatorio() throws Exception {

		Random randomNum = new Random();
		int numeroAleatorio = randomNum.nextInt(30);
		String nombreAleatorio = "";

		switch (numeroAleatorio) {
		case (0):
			nombreAleatorio = "Antonio";
			break;
		case (1):
			nombreAleatorio = "Daniel";
			break;
		case (2):
			nombreAleatorio = "Alexis";
			break;
		case (3):
			nombreAleatorio = "Carlos";
			break;
		case (4):
			nombreAleatorio = "Cristian";
			break;
		case (5):
			nombreAleatorio = "Luis";
			break;
		case (6):
			nombreAleatorio = "Juan";
			break;
		case (7):
			nombreAleatorio = "Marcelo";
			break;
		case (8):
			nombreAleatorio = "Mario";
			break;
		case (9):
			nombreAleatorio = "Claudio";
			break;
		case (10):
			nombreAleatorio = "Esteban";
			break;
		case (11):
			nombreAleatorio = "Isaac";
			break;
		case (12):
			nombreAleatorio = "Paul";
			break;
		case (13):
			nombreAleatorio = "Emilo";
			break;
		case (14):
			nombreAleatorio = "Joaquin";
			break;
		case (15):
			nombreAleatorio = "Miguel";
			break;
		case (16):
			nombreAleatorio = "Vicente";
			break;
		case (17):
			nombreAleatorio = "Jorge";
			break;
		case (18):
			nombreAleatorio = "Nelson";
			break;
		case (19):
			nombreAleatorio = "Julio";
			break;
		case (20):
			nombreAleatorio = "Mateo";
			break;
		case (21):
			nombreAleatorio = "Pablo";
			break;
		case (22):
			nombreAleatorio = "Adrian";
			break;
		case (23):
			nombreAleatorio = "Diego";
			break;
		case (24):
			nombreAleatorio = "David";
			break;
		case (25):
			nombreAleatorio = "Javier";
			break;
		case (26):
			nombreAleatorio = "Raul";
			break;
		case (27):
			nombreAleatorio = "Alvaro";
			break;
		case (28):
			nombreAleatorio = "Marcos";
			break;
		case (29):
			nombreAleatorio = "Sergio";
			break;
		case (30):
			nombreAleatorio = "Lucas";
			break;
		default:
			nombreAleatorio = "Test Nombre";
			break;
		}

		return nombreAleatorio;

	}

	/**
	 * @param funcionApellidoAleatorio()
	 * @return Genera Apellido al Azar de entre los casos de la funcion @throws
	 */
	public static String funcionApellidoAleatorio() throws Exception {

		Random randomNum = new Random();
		int numeroAleatorio = randomNum.nextInt(30);
		String apellidoAleatorio = "";

		switch (numeroAleatorio) {
		case (0):
			apellidoAleatorio = "Parra";
			break;
		case (1):
			apellidoAleatorio = "Salinas";
			break;
		case (2):
			apellidoAleatorio = "Arenas";
			break;
		case (3):
			apellidoAleatorio = "Perez";
			break;
		case (4):
			apellidoAleatorio = "Soto";
			break;
		case (5):
			apellidoAleatorio = "Lorca";
			break;
		case (6):
			apellidoAleatorio = "Illanes";
			break;
		case (7):
			apellidoAleatorio = "Pino";
			break;
		case (8):
			apellidoAleatorio = "Aliaga";
			break;
		case (9):
			apellidoAleatorio = "Jaramillo";
			break;
		case (10):
			apellidoAleatorio = "Cossio";
			break;
		case (11):
			apellidoAleatorio = "Aguirre";
			break;
		case (12):
			apellidoAleatorio = "Albero";
			break;
		case (13):
			apellidoAleatorio = "Arcos";
			break;
		case (14):
			apellidoAleatorio = "Aranda";
			break;
		case (15):
			apellidoAleatorio = "Campos";
			break;
		case (16):
			apellidoAleatorio = "Canales";
			break;
		case (17):
			apellidoAleatorio = "Borja";
			break;
		case (18):
			apellidoAleatorio = "Bustos";
			break;
		case (19):
			apellidoAleatorio = "Bernal";
			break;
		case (20):
			apellidoAleatorio = "Daza";
			break;
		case (21):
			apellidoAleatorio = "Donoso";
			break;
		case (22):
			apellidoAleatorio = "Duque";
			break;
		case (23):
			apellidoAleatorio = "Egea";
			break;
		case (24):
			apellidoAleatorio = "Escobar";
			break;
		case (25):
			apellidoAleatorio = "Exposito";
			break;
		case (26):
			apellidoAleatorio = "Folch";
			break;
		case (27):
			apellidoAleatorio = "Fuentes";
			break;
		case (28):
			apellidoAleatorio = "Figueroa";
			break;
		case (29):
			apellidoAleatorio = "Hierro";
			break;
		case (30):
			apellidoAleatorio = "Zaravia";
			break;
		default:
			apellidoAleatorio = "Test Apellido";
			break;
		}

		return apellidoAleatorio;

	}

	/**
	 * @param cadenaDeTextoAleatorio
	 * @return genera un string de segun el largo indicado como numero de entrada,
	 *         el valor retornado corresponde al string con el largo
	 *         indicado @throws
	 */
	public static String funcionCalleAleatorio() throws Exception {

		Random randomNum = new Random();
		int numeroAleatorio = randomNum.nextInt(10);
		String calleAleatorio = "";

		switch (numeroAleatorio) {
		case (0):
			calleAleatorio = "Av. Macul";
			break;
		case (1):
			calleAleatorio = "Av. Irarrazaval";
			break;
		case (2):
			calleAleatorio = "Av. Pajaritos";
			break;
		case (3):
			calleAleatorio = "Av. Ossa";
			break;
		case (4):
			calleAleatorio = "Av. La Paz";
			break;
		case (5):
			calleAleatorio = "Av. Matta";
			break;
		case (6):
			calleAleatorio = "Pje. Ancud";
			break;
		case (7):
			calleAleatorio = "Pje. Roman Diaz ";
			break;
		case (8):
			calleAleatorio = "Pje. R.Kock";
			break;
		case (9):
			calleAleatorio = "Pje. La Travesia";
			break;
		case (10):
			calleAleatorio = "Pje. La Estera";
			break;
		default:
			calleAleatorio = "Test Calle";
			break;
		}

		return calleAleatorio;

	}

	public static void funcionPausa(int seconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void funcionCapturar_Pantalla_OLD(WebDriver driver) throws IOException {

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-YY hh-mm-ss");
		Date date = new Date();
		String dateTime = dateFormat.format(date);
		File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File dest = new File("Evidencias/" + dateTime + ".png");
		FileUtils.copyFile(scr, dest);

	}

	public void WaitForAelement(String Element, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Element)));

	}

	public static String funcionCapturar_Pantalla_OLD20190909(WebDriver driver, String nombreCaptura) throws IOException {
		// System.err.println("Largo "+nombreCaptura.length());
		// System.err.println("Nombre "+nombreCaptura);
		// nombreCaptura = nombreCaptura.substring(0, 20);

		// String rutaEvidencia = "";
		// DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH-mm-ss");
		// Date date = new Date();
		// String dateTime = dateFormat.format(date);
		// dateTime = dateTime.replaceAll("-","");
		// dateTime = dateTime.replaceAll(" ","_");
		// File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// scr.scalePercent(60);
		// File dest = new File("test-output/Evidencias/" + nombreCaptura +"_" +
		// dateTime + ".png");
		// FileUtils.copyFile(scr, dest);
		// rutaEvidencia = "Evidencias/" + nombreCaptura +"_" + dateTime + ".png";
		// return rutaEvidencia;

		String rutaEvidencia = "";
		DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH-mm-ss");
		Date date = new Date();
		String dateTime = dateFormat.format(date);
		dateTime = dateTime.replaceAll("-", "");
		dateTime = dateTime.replaceAll(" ", "_");
		// File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// String scrBase64 = ((TakesScreenshot)
		// driver).getScreenshotAs(OutputType.BASE64);
		// File scr = OutputType.FILE.convertFromBase64Png(scrBase64);
		// File dest = new File("test-output/Evidencias/" + nombreCaptura +"_" +
		// dateTime + ".png");
		// FileUtils.copyFile(scr, dest);

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("test-output/Evidencias/" + nombreCaptura + "_" + dateTime + ".jpg"));

		rutaEvidencia = "Evidencias/" + nombreCaptura + "_" + dateTime + ".jpg";
		// String filePath = dest.toString();
		return rutaEvidencia;

	}

	public static String funcionSumarMes(int cantidadDeMeses, String paisEjecucion) throws Exception {

		Date fecha = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.MONTH, cantidadDeMeses); // numero de meses a a�adir, o restar en caso de d�as<0

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");

		switch (paisEjecucion) {
		case "Chile":
			sdf = new SimpleDateFormat("dd-MM-YYYY");
			break;

		case "Perú":
			sdf = new SimpleDateFormat("dd/MM/YYYY");
			break;

		default:
			break;
		}

		String fechaIngresar = sdf.format(calendar.getTime()); // Imprime la fecha en la consola
		return fechaIngresar;
	}

	public static String funcionSumarMes(int cantidadDeMeses) throws Exception {

		Date fecha = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.MONTH, cantidadDeMeses); // numero de meses a a�adir, o restar en caso de d�as<0

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");

		// switch (paisEjecucion) {
		// case "Chile":
		// sdf = new SimpleDateFormat("dd-MM-YYYY");
		// break;
		//
		// case "Per�":
		// sdf = new SimpleDateFormat("dd/MM/YYYY");
		// break;
		//
		// default:
		// break;
		// }

		String fechaIngresar = sdf.format(calendar.getTime()); // Imprime la fecha en la consola
		return fechaIngresar;
	}

	public static String funcionFechaHoy() throws Exception {

		Date fecha = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(fecha); // Configuramos la fecha que se recibe

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");

		// switch (paisEjecucion) {
		// case "Chile":
		// sdf = new SimpleDateFormat("dd-MM-YYYY");
		// break;
		//
		// case "Per�":
		// sdf = new SimpleDateFormat("dd/MM/YYYY");
		// break;
		//
		// default:
		// break;
		// }

		String fechaIngresar = sdf.format(calendar.getTime()); // Imprime la fecha en la consola
		return fechaIngresar;
	}

	public static String funcionFechaHoyMinutos() throws Exception {

		Date fecha = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(fecha); // Configuramos la fecha que se recibe

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd'_'HH:mm:ss");

		// switch (paisEjecucion) {
		// case "Chile":
		// sdf = new SimpleDateFormat("dd-MM-YYYY");
		// break;
		//
		// case "Per�":
		// sdf = new SimpleDateFormat("dd/MM/YYYY");
		// break;
		//
		// default:
		// break;
		// }

		String fechaIngresar = sdf.format(calendar.getTime()); // Imprime la fecha en la consola
		return fechaIngresar;
	}
	
	// @Test
	// public static String funcionXML(ITestContext ctx) {
	// String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
	// return suiteName;
	// }

	// @Parameters({"sNumeroEscenarioXml"})
	// @Test
	// public static String parametrizacion(String sNumeroEscenarioParametrizacion)
	// {
	//
	// System.out.println("Test param is: " + sNumeroEscenarioParametrizacion);
	// System.out.println("Test param is: " + sNumeroEscenarioParametrizacion);
	// return sNumeroEscenarioParametrizacion;
	//
	// }

	public static String funcionRestarAnio(int cantidadDeAnios) throws Exception {

		Date fecha = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setLenient(false);

		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		// calendar.add(Calendar.MONTH, cantidadDeMeses); // numero de meses a a�adir, o
		// restar en caso de d�as<0
		calendar.add(Calendar.YEAR, -cantidadDeAnios); // numero de meses a a�adir, o restar en caso de d�as<0

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");

		// switch (paisEjecucion) {
		// case "Chile":
		// sdf = new SimpleDateFormat("dd-MM-YYYY");
		// break;
		//
		// case "Per�":
		// sdf = new SimpleDateFormat("dd/MM/YYYY");
		// break;
		//
		// default:
		// break;
		// }

		String fechaIngresar = sdf.format(calendar.getTime()); // Imprime la fecha en la consola
		return fechaIngresar;
	}

	public static String CaptureScreen(WebDriver driver, String imageName) {
		File sourceFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader = null;
		try {
			fileInputStreamReader = new FileInputStream(sourceFile);
			byte[] bytes = new byte[(int) sourceFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));

			String screenShotDestination = "test-output/Evidencias/" + imageName.trim().replace(" ", "_") + ".png";

			File destination = new File(screenShotDestination);
			FileUtils.copyFile(sourceFile, destination);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return "data:image/png;base64," + encodedBase64;
	}
	
	public static String funcionCapturar_Pantalla(WebDriver driver, String imageName) {
		File sourceFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader = null;
		try {
			fileInputStreamReader = new FileInputStream(sourceFile);
			byte[] bytes = new byte[(int) sourceFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));

			String screenShotDestination = "test-output/Evidencias/" + imageName.trim().replace(" ", "_") + ".png";
//			String screenShotDestination =  imageName.trim().replace(" ", "_") + ".png";

			File destination = new File(screenShotDestination);
			FileUtils.copyFile(sourceFile, destination);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return "data:image/png;base64," + encodedBase64;		
	}
	
	public static void funcionMoverScroll(WebDriver driver, WebElement webElement) throws Exception {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",webElement);
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-200)", "");
	}
	
			
	
	public static int getDigitoDian(String nitParam) throws Exception
    {
        if( nitParam == null || ( nitParam.trim() ).isEmpty() )
        {
            throw new Exception("La cadena del NIT es nula o vac�a.");
        }
         
        nitParam = nitParam.trim();
         
        int indiceRaya = nitParam.indexOf("-");
         
        String nitInterno = indiceRaya > 0 ? nitParam.substring(0, indiceRaya): nitParam;
 
        try
        {
            long longValidacion = Long.parseLong(nitInterno);
        }
        catch(Exception ex)
        {
            throw new Exception("El nit contiene caracteres no num�ricos. Valor recibido: |" +  nitParam + "|");
        }
         
 
        String nitVector[] = nitInterno.split("(?<=.)");
 
        int valorCalculado = 0;
        int aux = nitVector.length -1;
 
        for( int i = 0; i < nitVector.length; i++ )
        {
            switch( i )
            {
                case 0:
                    valorCalculado += 3 * Integer.parseInt(nitVector[aux - 0]);
                    break;
                case 1:
                    valorCalculado += 7 * Integer.parseInt(nitVector[aux - 1]);
                    break;
                case 2:
                    valorCalculado += 13 * Integer.parseInt(nitVector[aux - 2]);
                    break;
                case 3:
                    valorCalculado += 17 * Integer.parseInt(nitVector[aux - 3]);
                    break;
                case 4:
                    valorCalculado += 19 * Integer.parseInt(nitVector[aux - 4]);
                    break;
                case 5:
                    valorCalculado += 23 * Integer.parseInt(nitVector[aux - 5]);
                    break;
                case 6:
                    valorCalculado += 29 * Integer.parseInt(nitVector[aux - 6]);
                    break;
                case 7:
                    valorCalculado += 37 * Integer.parseInt(nitVector[aux - 7]);
                    break;
                case 8:
                    valorCalculado += 41 * Integer.parseInt(nitVector[aux - 8]);
                    break;
                case 9:
                    valorCalculado += 43 * Integer.parseInt(nitVector[aux - 9]);
                    break;
                case 10:
                    valorCalculado += 47 * Integer.parseInt(nitVector[aux - 10]);
                    break;
                case 11:
                    valorCalculado += 53 * Integer.parseInt(nitVector[aux - 11]);
                    break;
                case 12:
                    valorCalculado += 59 * Integer.parseInt(nitVector[aux - 12]);
                    break;
                case 13:
                    valorCalculado += 67 * Integer.parseInt(nitVector[aux - 13]);
                    break;
                case 14:
                    valorCalculado += 71 * Integer.parseInt(nitVector[aux - 14]);
                    break;
            }
        }//for( int i = 0; i < nitVector.length; i++ )
         
        int modulo = valorCalculado % 11;
         
        if( modulo >= 2 )
        {
            modulo = 11 - modulo;
        }
         
        return modulo;
    }
	
	public static int calcularDigito(String nit) {
		int digito = 0, acum = 0, residuo = 0;
		char [] nit_array = nit.toCharArray();
		int [] primos = {3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 39, 67, 71};
		int max = nit_array.length;

		for (int i = 0; i < nit.length(); i++) {
		acum += Integer.parseInt(String.valueOf(nit_array[max - 1])) * primos[i];
		max--;
		}

		residuo = acum % 11;
		if (residuo > 1) {
		digito = 11 - residuo;
		} else {
		digito = residuo;
		}

		return digito;
		}
	
	public static String incrementarString(String numero){
		int numeroInt = Integer.parseInt(numero);
		numeroInt = numeroInt + 1;
		if (numeroInt <= 9){
			String numeroStr= Integer.toString(numeroInt);
			numeroStr = "00" + numeroStr;
			return numeroStr;
		} else if (numeroInt > 9 && numeroInt <= 99) {
			String numeroStr= Integer.toString(numeroInt);
			numeroStr = "0" + numeroStr;
			return numeroStr;
		} else {
			String numeroStr= Integer.toString(numeroInt);
			return numeroStr;
		}
	}

	public static int numeroAstring(String numero){
		int numeroInt = Integer.parseInt(numero);
		return numeroInt;
	}
	
}
