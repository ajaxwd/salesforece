package utilidades;

public class DatosInicialesYDrivers {
		
	//valores posibles para seleccion de browser: firefox, chrome, IE32, IE64
	public static  String Browser = "";
		
	//ruta donde se guarda el exe con el driver para ejecucion
//	public static final String RutaDriverChrome = "./selenium-java-3.14.0/chromedriver.exe";
//	public static final String RutaDriverFirefox = "./selenium-java-3.14.0/geckodriver.exe";
//	public static final String RutaDriverIE32 = "./selenium-java-3.14.0/IEDriverServer32.exe";
//	public static final String RutaDriverIE64 = "./selenium-java-3.14.0/IEDriverServer64.exe";
	
	public static final String RutaDriverChrome = "./Librerias/selenium-java-3.14.0/chromedriver.exe";
	public static final String RutaDriverFirefox = "./Librerias/selenium-java-3.14.0/geckodriver.exe";
	public static final String RutaDriverIE32 = "./Librerias/selenium-java-3.14.0/IEDriverServer32.exe";
	public static final String RutaDriverIE64 = "./Librerias/selenium-java-3.14.0/IEDriverServer64.exe";
	
	//URL a ingresar
	public static  String URL = "";
	
	
	//datos de usuario
	public static  String Usuario = "";
	public static  String Password = "";
	
	
	//Ruta y archivo de ejecucion con datos basicos de configuracion(browser, url, usuario para pruebas en general)	
	public static  String Path_TestData = "./DatosEjecucionSalesForce/";

    //Archivo de ejecucion con datos basicos de configuracion(browser, url, usuario para pruebas en general)	
    public static String Path_TestDataInicial = Path_TestData;
    public static  String File_TestData = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
    
    
}
