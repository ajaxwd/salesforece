package marketingSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class HU_MKT001_16023_Crear_Campana_con_Datos_Obligatorios_Chile {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "HU_MKT001_16023_Crear_Campana_con_Datos_Obligatorios_Chile.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "HU_MKT001_16023_Crear_Campana_con_Datos_Obligatorios_Chile.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);

			// ------experimento------

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();

			// ------fin------
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicaciones(driver)))
					.click();

			mensaje = "Flujo : Click Menu Aplicaciones ";
			msj = "ClickMenuApp";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkMarketingMenuAplicaciones(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click Link Marketing";
			msj = "ClickServicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearCampanas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Campa�as ";
			msj = "ClickBtnCampa�as";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
						// flujo

					driver.findElement(By.xpath(
							"(//h2[contains(text(),'Llamada de Tel�fono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
							.click();
					// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Log.info(mensaje);
					Reporter.log("<br>" + "<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) {
					cierraLlamado = 1;
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnNuevaCampana(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Nueva Campa�a ";
			msj = "ClickBtnNuevaCampa�a";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputNombreCampana(driver)))
					.clear();
			String nombreCampanaCompleto = utilidades.DatosInicialesYDrivers.Usuario;
			String[] nombreCampana = nombreCampanaCompleto.split("@");
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputNombreCampana(driver)))
					.sendKeys("Campa�a_" + nombreCampana[0] + "_" + Funciones.funcionFechaHoyMinutos());
			mensaje = "Flujo : Se ingresa Nombre Campa�a " + "Campa�a_" + nombreCampana[0] + "_"
					+ Funciones.funcionFechaHoyMinutos();
			msj = "InputCampana";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.selectTipoCampa�a(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(3000);

			int recorreExcell = 0;
			int inicioExcell = 0;
			String tipoCampana = "";

			while (recorreExcell == 0) {
				if ("tipoCampana".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					tipoCampana = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'tipoCampana' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(crearCampana.CrearCampana.elementoSelectTipoCampa�a(driver, tipoCampana)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Se Selecciona Tipo Campa�a : " + tipoCampana;
			msj = "SeleccionCampana";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			Funciones.funcionMoverScroll(driver, crearCampana.CrearCampana.inputFechaInicio(driver));

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputFechaInicio(driver))).clear();
			String fechaInicio = Funciones.funcionFechaHoy();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputFechaInicio(driver)))
					.sendKeys(fechaInicio);
			mensaje = "Flujo : Fecha Inicio : " + fechaInicio;
			msj = "InputFechaInicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputFechaFinalizacion(driver)))
					.clear();
			String fechaTermino = Funciones.funcionSumarMes(2);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.inputFechaFinalizacion(driver)))
					.sendKeys(fechaTermino);
			mensaje = "Flujo : Fecha Finalizacion : " + fechaTermino;
			msj = "InputFechaFinalizacion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.selectPais(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(3000);

			recorreExcell = 0;
			inicioExcell = 0;
			String pais = "";

			while (recorreExcell == 0) {
				if ("pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					pais = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'pais' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.elementoSelectPais(driver, pais)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Se Selecciona Pais : " + pais;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCampana.CrearCampana.selectUnidadDeNegocio(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(3000);

			recorreExcell = 0;
			inicioExcell = 0;
			String unidadDeNegocio = "";

			while (recorreExcell == 0) {
				if ("unidadDeNegocio".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					unidadDeNegocio = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'unidadDeNegocio' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(
							crearCampana.CrearCampana.elementoSelectUnidadDeNegocio(driver, unidadDeNegocio)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Se Selecciona Unidad De Negocio : " + unidadDeNegocio;
			msj = "SeleccionUnidadDeNegocio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCaso.CrearCaso.btnGuardarCaso(driver))).click();

			mensaje = "Flujo : Click 'Guardar'";
			msj = "ClickGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			int sw = 0;
			int contarTiempo = 0;
			String textoCuadroVerde = "";

			while (sw == 0) {
				if (contarTiempo > 30) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible" + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					textoCuadroVerde = new WebDriverWait(driver, 20, 100).until(
							ExpectedConditions.visibilityOf(crearCaso.CrearCaso.txtMensajeGuardadoCampana(driver)))
							.getText();
					sw = 1;

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;
				}
			}

			mensaje = "Flujo : Se revisa Guardado Correcto";
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>");
			System.out.println(mensaje);

			if (textoCuadroVerde.contains("Se cre�")) {

				Thread.sleep(2000);

				mensaje = "Ok :" + textoCuadroVerde;
				msj = "OkGuardadoCorrecto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				ValorEncontrado = true;

				Thread.sleep(6000);

			} else {

				mensaje = "<p style='color:red;'>" + "Error : error al guardar " + textoCuadroVerde + "</p>";
				msj = "ErrorGuardado";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crea y Guarda Campa�a" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: No Crea Campa�a" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();

	}

}
