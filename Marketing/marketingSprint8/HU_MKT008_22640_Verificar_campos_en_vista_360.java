package marketingSprint8;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import crearCuenta.Personal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class HU_MKT008_22640_Verificar_campos_en_vista_360 {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "MKT.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = Funciones.numeroAstring(sNumeroEscenario);

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "MKT.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		String Perfil = Funciones.getExcel(driver, "Perfil", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		mensaje = "<p>" + "Usuario test en Ejecucion : " + Perfil + "</p>";
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
			+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
			+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			WebDriverWait waitX = new WebDriverWait(driver, 20);
			waitX.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)));
			paginaPrincipal.MenuPrincipal.btnCuentas(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			//Seleccionar cuenta
			String Cuenta = crearCuenta.Personal.selectCuenta(driver).getText();
			crearCuenta.Personal.selectCuenta(driver).click();
			mensaje = "Flujo : Se selecciona cuenta : " + Cuenta;
			msj = "SelectCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			//Ingresar pestaña relacionado
			crearCuenta.Personal.selectRelacionado(driver).click();
			Thread.sleep(3000);
			mensaje = "Flujo : Se selecciona pestaña relacionado";
			msj = "SelectRelacionado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			//Historial de campaña, validar si tiene campañas
			String texto = crearCuenta.Personal.validarCampania(driver).getText();
			texto = texto.replace("(", "-");
			texto = texto.replace(")", "-");
			texto = texto.replace("-", "");
			int cantidad = Funciones.numeroAstring(texto);
			int contador = 0;
			while(contador == 0){
				String campo1 = "Nombre de la campaña";
				String campo2 = "Fecha de inicio";
				String campo3 = "Tipo";
				String campo4 = "Sub-tipo de campaña";
				
				if(cantidad > 0){
					//TODO
					switch (Perfil) {
						case "Agendador":
							
							String validar1 = crearCuenta.Personal.validarCampoCampania(driver, campo1).getText();
							campo1 = campo1.toUpperCase();
							validar1 = validar1.toUpperCase();
							System.out.println(campo1);
							System.out.println(validar1);
							if(validar1.equals(campo1)){
								//System.out.println("Campo validado");
								mensaje = "Flujo : Campos validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}else{
								System.out.println("Campo no validado");
								mensaje = "Flujo : Campos NO validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}
							
							break;
						case "Analista BI":
							
							validar1 = crearCuenta.Personal.validarCampoCampania(driver, campo1).getText();
							String validar2 = crearCuenta.Personal.validarCampoCampania(driver, campo2).getText();
							String validar3 = crearCuenta.Personal.validarCampoCampania(driver, campo3).getText();
							String validar4 = crearCuenta.Personal.validarCampoCampania(driver, campo4).getText();
							campo1 = campo1.toUpperCase();
							validar1 = validar1.toUpperCase();
							campo2 = campo1.toUpperCase();
							validar2 = validar1.toUpperCase();
							campo3 = campo1.toUpperCase();
							validar3 = validar1.toUpperCase();
							campo4 = campo1.toUpperCase();
							validar4 = validar1.toUpperCase();
							if((validar1.equals(campo1)) & (validar2.equals(campo2)) & (validar3.equals(campo3))
								& (validar4.equals(campo4))){
								//System.out.println("Campos validados");
								mensaje = "Flujo : Campos validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}else{
								//System.out.println("Campos no validados");
								mensaje = "Flujo : Campos NO validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}

						contador = contador + 1;
							break;
						case "Analista":
							validar1 = crearCuenta.Personal.validarCampoCampania(driver, campo1).getText();
							validar2 = crearCuenta.Personal.validarCampoCampania(driver, campo2).getText();
							validar3 = crearCuenta.Personal.validarCampoCampania(driver, campo3).getText();
							validar4 = crearCuenta.Personal.validarCampoCampania(driver, campo4).getText();
							campo1 = campo1.toUpperCase();
							validar1 = validar1.toUpperCase();
							campo2 = campo1.toUpperCase();
							validar2 = validar1.toUpperCase();
							campo3 = campo1.toUpperCase();
							validar3 = validar1.toUpperCase();
							campo4 = campo1.toUpperCase();
							validar4 = validar1.toUpperCase();
							if((validar1.equals(campo1)) & (validar2.equals(campo2)) & (validar3.equals(campo3))
								& (validar4.equals(campo4))){
								//System.out.println("Campos validados");
								mensaje = "Flujo : Campos validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}else{
								//System.out.println("Campos no validados");
								mensaje = "Flujo : Campos NO validados";
								msj = "LinkAgregarCampania";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contador = contador + 1;
							}
							contador = contador + 1;
							break;
					
						default:
							System.out.println("El perfil ingresado no esta validado aun, favor contactar a su automatizador preferido");
							contador = contador + 1;
							break;
					}
				}else if(cantidad == 0){

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					crearCuenta.Personal.linkAgregarCampania(driver));
					Thread.sleep(2000);
					WebDriverWait wait = new WebDriverWait(driver, 20);
					wait.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.linkAgregarCampania(driver)));
					crearCuenta.Personal.linkAgregarCampania(driver).sendKeys(Keys.ENTER);
					Thread.sleep(3000);
					mensaje = "Flujo : Se selecciona link agregar campaña";
					msj = "LinkAgregarCampania";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
	
					Thread.sleep(2000);
	
					crearCuenta.Personal.inputBuscarCampania(driver).click();
					crearCuenta.Personal.inputBuscarCampania(driver).sendKeys("aprobacion2");
					crearCuenta.Personal.selectInputCompania(driver).click();
					mensaje = "Flujo : Se selecciona campaña";
					msj = "selectInputCompania";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
	
					Thread.sleep(2000);
	
					crearCuenta.Personal.btnSiguiente(driver).click();
					mensaje = "Flujo : Se selecciona boton siguiente";
					msj = "btnSiguiente";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
	
					Thread.sleep(2000);
	
					crearCuenta.Personal.btnGuardarCampania(driver).click();
					mensaje = "Flujo : Se selecciona boton Guardar";
					msj = "btnGuardar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Thread.sleep(2000);

					texto = crearCuenta.Personal.validarCampania(driver).getText();
					texto = texto.replace("(", "-");
					texto = texto.replace(")", "-");
					texto = texto.replace("-", "");
					cantidad = Funciones.numeroAstring(texto);

					Thread.sleep(2000);

					if(cantidad == 0){
						mensaje = "Flujo : No se creo la campaña";
						msj = "noCreoCampania";
						Funciones.mensajesLog(mensaje);
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					}else{
						mensaje = "Flujo : Se creo la campaña";
						msj = "seCreoCampania";
						Funciones.mensajesLog(mensaje);
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = utilidades.Funciones.CaptureScreen(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					}
		
				}
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
