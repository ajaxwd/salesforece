package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class LEAD_SP1_00048_P {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	 @Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		mensaje = "<p>"+"Escenario en Ejecucion : " + sNombrePrueba+"</p>";
		Funciones.mensajesLog(mensaje);

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
//			driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String modeloInteres = "";
			String ingresoLocal = "";

			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			ingresoLocal = Funciones.getExcel(driver, "Local", posicionExcel);

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) {

			}
			
			int cierraLlamado = 0;			
			while (cierraLlamado == 0) {	
			Thread.sleep(2000);	
			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
			} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}
			
			Thread.sleep(2000);

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearLead(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Crear Lead ";
			msj = "ClickBtnCrearLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.rdLeadPersona(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Lead Persona";
			msj = "ClickRadioCrearPersona";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.btnSiguienteCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(10000);

			WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
			waitSelectEstadoLead.until(ExpectedConditions.visibilityOf(crearLead.Personal.selectEstadoLead(driver)));
			crearLead.Personal.selectEstadoLead(driver).click();
			Thread.sleep(3000);
			crearLead.Personal.nuevoEstadoLead(driver).click();
			mensaje = "Flujo : Seleccion Estado de Lead :" + "NUEVO";
			msj = "SeleccionEstado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String rut = "";
			String tipoDocumentoIdentidad = "";
			switch (paisEjecucion) {

			case "Chile":

				// Cadena de caracteres aleatoria Para Rut
				rut = utilidades.Funciones.generadorDeRut(9000000, 22000000);
				crearLead.Personal.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
				msj = "IngresoRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			case "Per�":

				waitSelectEstadoLead
				.until(ExpectedConditions.elementToBeClickable(crearLead.Personal.selectDocumentoIdentidad(driver)));
				System.err.println("Aca");
				crearLead.Personal.selectDocumentoIdentidad(driver).sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				tipoDocumentoIdentidad = "DNI";
				Thread.sleep(2000);
				crearLead.Personal.tipoDocumentoIdentidad(driver, tipoDocumentoIdentidad).click();
				mensaje = "Flujo : Seleccion Documento Identidad : " + tipoDocumentoIdentidad;
				mensaje = "Flujo : Seleccion Documento Identidad :" + "DNI";
				msj = "SeleccionDocumentoIdentidad";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				// Cadena de caracteres aleatoria Para DNI
				rut = utilidades.Funciones.generadorDeRut(10000000, 22000000);
				String vectorRut[] = rut.split("-");
				System.err.println("*"+vectorRut[0]+"*");
				System.err.println("*"+vectorRut[1]+"*");
				rut=vectorRut[0];
				crearLead.Personal.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de DNI Aleatorio : " + rut;
				msj = "IngresoDni";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}

			Thread.sleep(3000);
			
			crearLead.Personal.selectTratamiento(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.srTratamiento(driver).click();

			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			crearLead.Personal.selectOrigen(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.webOrigen(driver).click();
			mensaje = "Flujo : Ingreso de Origen : " + "WEB";
			msj = "IngresoOrigen";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			crearLead.Personal.inputNombre(driver).sendKeys(nombreAleatorio);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();

			crearLead.Personal.inputApellidoPaterno(driver).sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputApellidoMaterno(driver).sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			crearLead.Personal.selectPrioridad(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.medioInteresPrioridad(driver).click();
			mensaje = "Flujo : Seleccion Prioridad :" + "Medio Interes";
			msj = "SeleccionPrioridad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Nombre
			String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
			referidoAleatorio = referidoAleatorio + " " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Personal.inputReferidoPor(driver).sendKeys(referidoAleatorio);
			mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
			msj = "IngresoNombreRef";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			correo = correo + correoAzar + "@derco.cl";
			correo = correo.toLowerCase();
			crearLead.Personal.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numMovil = numMovil + azar;
			}
			crearLead.Personal.inputMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			switch (paisEjecucion) {

			case "Chile":
				
				numTelefono = "2" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
				msj = "IngresoTelefono";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			case "Per�":

				numTelefono = "0" + numTelefono;
				crearLead.Personal.inputTelefono(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : "  + numTelefono;
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}
			
			Thread.sleep(4000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputMarcaInteres(driver));

			recorreExcell = 0;
			inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
			"Hoja1");

			String[] arregloMarcaInteres = new String[modeloInteres.length()];
			for (int i = 0; i < modeloInteres.length(); i++) {
				arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
			}

			for (int i = 0; i < modeloInteres.length(); i++) {
				crearLead.Personal.inputMarcaInteres(driver).sendKeys(arregloMarcaInteres[i]);
				Thread.sleep(500);
			}

			Thread.sleep(4000);

			WebElement elementoMarcaInteresPreProd = driver
			.findElement(By.xpath("//div[@title='" + modeloInteres + "']"));
			elementoMarcaInteresPreProd.click();
			mensaje = "Flujo : Seleccion Producto :" + modeloInteres;
			msj = "IngresoProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			Thread.sleep(1500);
			
			switch (paisEjecucion) {

			case "Chile":
								
				break;

			case "Per�":
				
				String[] arregloIngresoLocal = new String[ingresoLocal.length()];
				for (int i = 0; i < ingresoLocal.length(); i++) {
					arregloIngresoLocal[i] = Character.toString(ingresoLocal.charAt(i));
				}
				crearLead.Personal.inputPais(driver).click();
				for (int i = 0; i < ingresoLocal.length(); i++) {
					crearLead.Personal.inputLocal(driver).sendKeys(arregloIngresoLocal[i]);
					Thread.sleep(500);
				}

				mensaje = "Flujo : Ingreso Campo Local : " + ingresoLocal;
				Funciones.mensajesLog(mensaje);

				Thread.sleep(2000);
				crearLead.Personal.localSeleccion(driver, ingresoLocal).click();
				mensaje = "Flujo : Seleccion de Local  : " + ingresoLocal;
				msj = "SeleccionLocal";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}
			
			Thread.sleep(1500);
			
			String calleAleatorio = "";
			String calleAleatorioNumero = "";
			
			switch (paisEjecucion) {

			case "Chile":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearLead.Personal.inputCalle(driver).sendKeys(calleAleatorio + " " + calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio + " " + calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				crearLead.Personal.inputComplementoDireccion(driver).sendKeys("Casa Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			case "Per�":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearLead.Personal.inputDireccion(driver).sendKeys(calleAleatorio );
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);
				
				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: "+calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				Thread.sleep(1500);
				
				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				Thread.sleep(1500);
				
				crearLead.Personal.inputReferenciaDireccion(driver).sendKeys(calleAleatorio + " Casa Interior");
				mensaje = "Flujo : Campo Referencia Direccion :" + calleAleatorio + " Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				
				break;

			default:
				break;
			}

			String regionIngresar = "";
			String regionIngresarAlternativo = "";
			String provinciaIngresar = "";
			String comunaIngresar = "";

			Thread.sleep(1000);

			Funciones.funcionMoverScroll(driver, crearLead.Personal.inputComuna(driver, comunaIngresar));

			switch (paisEjecucion) {
			case "Chile":

				regionIngresar = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				regionIngresarAlternativo = "Buscar Regiones...";
				comunaIngresar = "Buscar Comunas...";
				break;

			case "Per�":

				regionIngresar = "Buscar Departamentos...";
				regionIngresarAlternativo = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				comunaIngresar = "Buscar Distritos...";
				
				break;

			default:
				break;
			}

			String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			crearLead.Personal.inputPais(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				crearLead.Personal.inputPais(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			
			crearLead.Personal.paisSeleccionLead(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);
			
			String[] arregloRegionEjecucion = new String[regionEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloRegionEjecucion[i] = Character.toString(regionEjecucion.charAt(i));
			}
			crearLead.Personal.inputRegion(driver, comunaIngresar, regionIngresarAlternativo).click();
			for (int i = 0; i < regionEjecucion.length(); i++) {
				crearLead.Personal.inputRegion(driver, comunaIngresar, regionIngresarAlternativo).sendKeys(arregloRegionEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
	
			mensaje = "Flujo : Ingreso Campo Region/Departamento : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			
			crearLead.Personal.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region/Departamento  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloProvinciaEjecucion = new String[provinciaEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloProvinciaEjecucion[i] = Character.toString(provinciaEjecucion.charAt(i));
			}
			crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(arregloProvinciaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			
			mensaje = "Flujo : Ingreso Campo Provincia : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
		
			crearLead.Personal.provinciaSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion de Provincia  : " + provinciaEjecucion;
			msj = "SeleccionProvincia";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloComunaEjecucion = new String[comunaEjecucion.length()];
			for (int i = 0; i < comunaEjecucion.length(); i++) {
				arregloComunaEjecucion[i] = Character.toString(comunaEjecucion.charAt(i));
			}
			crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				jse.executeScript("window.scrollBy (0,150)");
				crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(arregloComunaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
	
			mensaje = "Flujo : Ingreso Campo Comuna/Distrito : " + comunaEjecucion;
			Log.info(mensaje);
			Reporter.log("<p>" + "<br>" + mensaje + "</p>");
			System.out.println(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("(//lightning-icon[@class='itemIcon slds-icon slds-icon--x-small slds-m-left--x-small slds-icon-text-default slds-button__icon slds-icon-utility-search slds-icon_container forceIcon']/following::span[contains(@title,'"+comunaEjecucion+"')])[2]")));
			Thread.sleep(1000);
			
			crearLead.Personal.comunaSeleccion(driver,comunaIngresar,comunaEjecucion).click();
			mensaje = "Flujo : Seleccion de Comuna/Distrito : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			crearLead.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			
			Thread.sleep(3000);
			
			try {
				
				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta p�gina.")) {
					
					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta p�gina :  " +crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText()+ "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append( "\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
					
				}
				
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
				
			}

			Thread.sleep(10000);

			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearLead.Personal.txtNombrePaginaInicio(driver)));
			String comparar = crearLead.Personal.txtNombrePaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creacion correcta de nuevo lead personal";
			Funciones.mensajesLog(mensaje);

			if (comparar.contains(nombreAleatorio + " " + apellidoPaternoAleatorio)) {
				mensaje = "Ok : Coincide Nombre Ingresado " + nombreAleatorio + " " + apellidoPaternoAleatorio
				+ " con Nombre desplegado en Cracion de Lead " + comparar;
				msj = "OkCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;

			} else {
				mensaje = "<p style='color:red;'>"
				+ "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Lead " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito LEAD Personal " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
						"Hoja1");
				
				recorreExcell = 0;
				inicioExcell = 0;
				while (recorreExcell == 0) {
					if ("Nombre Persona Lead Creado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						String setNombreLead = nombreAleatorio + " " + apellidoPaternoAleatorio;
						ExcelUtils.setCellData(setNombreLead, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Nombre Persona Lead Creadoo' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				recorreExcell = 0;
				inicioExcell = 0;
				while (recorreExcell == 0) {
					if ("Rut LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						String setRut = rut;
						ExcelUtils.setCellData(setRut, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Rut LEAD' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}
				
				recorreExcell = 0;
				inicioExcell = 0;
				while (recorreExcell == 0) {
					if ("Telefono LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						String setTelefono = numTelefono;
						ExcelUtils.setCellData(setTelefono, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Telefono LEAD' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
						Funciones.mensajesLogError(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}
				

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
				+ "ERROR: Prueba Fallida ; No se crea LAED Personal de Manera Correcta" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException |ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEnEjecucion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
