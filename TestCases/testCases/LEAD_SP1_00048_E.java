package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class LEAD_SP1_00048_E {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}
		String modeloInteres ="";
		modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:	
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

				
		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			// JavascriptExecutor jse = (JavascriptExecutor) driver;
			// jse.executeScript("arguments[0].scrollIntoView();",crearLead.Personal.inputTelefono(driver));
			// ((JavascriptExecutor)driver).executeScript("window.print()");
			// Thread.sleep(999999);

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearLead(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Crear Lead ";
			msj = "ClickBtnCrearLead";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.rdLeadEmpresa(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Lead Empresa";
			msj = "ClickRadioCrearEmpresa";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.btnSiguienteCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(500);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
			waitSelectEstadoLead.until(ExpectedConditions.visibilityOf(crearLead.Personal.selectEstadoLead(driver)));
			crearLead.Personal.selectEstadoLead(driver).click();
			Thread.sleep(2000);
			crearLead.Personal.nuevoEstadoLead(driver).click();
			mensaje = "Flujo : Seleccion Estado de Lead :" + "NUEVO";
			msj = "SeleccionEstadoLead";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Rut
			String rut = utilidades.Funciones.generadorDeRut(49999999, 50000000);
			String rutEmpresa = "20551597939";
			crearLead.Empresa.inputDocumentoIdentidad(driver).sendKeys(rutEmpresa);
			mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rutEmpresa;
			msj = "IngresoRut";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputCompania(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			String nombreEmpresaPlanilla = empresaAleatorio + " Cia Ltda";
			mensaje = "Flujo : Ingreso de Compañia Aleatorio : " + empresaAleatorio;
			msj = "IngresoCompania";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			empresaAleatorio = empresaAleatorio.toLowerCase();
			crearLead.Empresa.inputSitioWeb(driver).sendKeys("www." + empresaAleatorio + ".com");
			mensaje = "Flujo : Ingreso de Sitio Web Compañia Aleatorio : " + "www." + empresaAleatorio + ".com";
			msj = "IngresoSitioWeb";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			crearLead.Empresa.selectPrioridad(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.medioInteresPrioridad(driver).click();
			mensaje = "Flujo : Seleccion Prioridad : " + "Medio Interes";
			msj = "SeleccionPrioridad";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearLead.Empresa.selectOrigen(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.webOrigen(driver).click();
			mensaje = "Flujo : Ingreso de Origen : " + "WEB";
			msj = "IngresoOrigen";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String numEmpleados = "";
			azar = "";
			// Cadena de numeros aleatoria Para Numero Empleados
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numEmpleados = numEmpleados + azar;
			}
			crearLead.Empresa.inputNumeroEmpleados(driver).sendKeys(numEmpleados);
			mensaje = "Flujo : Ingreso Numero Empleados Aleatorio : " + numEmpleados;
			msj = "IngresoNumeroEmplados";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			crearLead.Empresa.selectTratamiento(driver).click();
			Thread.sleep(3000);
			WebDriverWait waitSrTratamiento = new WebDriverWait(driver, 20);
			waitSrTratamiento.until(ExpectedConditions.visibilityOf(crearLead.Empresa.srTratamiento(driver)));
			crearLead.Empresa.srTratamiento(driver).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			crearLead.Empresa.inputNombre(driver).sendKeys(nombreAleatorio);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();

			crearLead.Empresa.inputApellidoPaterno(driver).sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPat";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputApellidoMaterno(driver).sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMat";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Nombre
			String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
			referidoAleatorio = referidoAleatorio + " " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputReferidoPor(driver).sendKeys(referidoAleatorio);
			mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
			msj = "IngresoReferido";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearLead.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numMovil = numMovil + azar;
			}
			crearLead.Empresa.inputMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearLead.Empresa.inputTelefono(driver).sendKeys("0" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "0" + numTelefono;
			msj = "IngresoTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputMarcaInteres(driver));

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,"Hoja1");
			modeloInteres = "";
			modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);

			String[] arregloMarcaInteres = new String[modeloInteres.length()];
			for (int i = 0; i < modeloInteres.length(); i++) {
				arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
			}
			for (int i = 0; i < modeloInteres.length(); i++) {
				crearLead.Empresa.inputMarcaInteres(driver).sendKeys(arregloMarcaInteres[i]);
				Thread.sleep(500);
			}

			Thread.sleep(4000);

			// WebElement elementoMarcaInteresPreProd =
			// driver.findElement(By.xpath("//div[@title='NEW MAZDA6 SDN 2.0 6AT']"));
			WebElement elementoMarcaInteresPreProd = driver.findElement(By.xpath("//div[@title='" + modeloInteres + "']"));
			elementoMarcaInteresPreProd.click();
			// mensaje = "Flujo : Seleccion Producto :" + "NEW MAZDA6 SDN 2.0 6AT";
			mensaje = "Flujo : Seleccion Producto :" + modeloInteres;
			msj = "IngresoProducto";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1500);

			/* crearLead.Empresa.selectMarcaInteres(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.selectMarcaInteres(driver).clear(); 
			
//			String[] arregloMarcaInteres = new String[modeloInteres.length()];
			arregloMarcaInteres = new String[modeloInteres.length()];

			for (int i = 0; i < modeloInteres.length(); i++) {
				arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
			}
			for (int i = 0; i < modeloInteres.length(); i++) {
				crearLead.Empresa.selectMarcaInteres(driver).sendKeys(arregloMarcaInteres[i]);
				Thread.sleep(500);
			}
			
			mensaje = "Flujo : Seleccion Marca Interes :" + modeloInteres;
			msj = "SeleccionMarca";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearLead.Empresa.selectModeloInteres(driver, modeloInteres).click();
			Thread.sleep(2000);
			//crearLead.Empresa.mazdaMX5ModeloInteres(driver).click();
			mensaje = "Flujo : Seleccion Modelo Interes :" + modeloInteres;
			msj = "SeleccionModelo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearLead.Empresa.selectVersion(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.ver206MTVersion(driver).click();
			mensaje = "Flujo : Seleccion Version Interes :" + "20L 6MT";
			msj = "SeleccionVersion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
 
			crearLead.Empresa.inputDescripcionLocal(driver).sendKeys("Concesionario DERCO");
			mensaje = "Flujo : Campo descripcion local :" + "Concesionario DERCO";
			msj = "IngresoConcesionario";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			Thread.sleep(2000);

			/* crearLead.Empresa.inputConcesionario(driver).click();
			crearLead.Empresa.inputConcesionario(driver).sendKeys("DERCO");
			mensaje = "Flujo : Ingreso Campo Concesionario : " + "DERCO";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.inputConcesionario(driver).sendKeys(" ");
			crearLead.Empresa.concesionarioSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Concesionario  : " + "DERCO";
			msj = "SeleccionConcesionario";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			// Cadena de caracteres aleatoria Para Calle
			String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			String calleAleatorioNumero = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				calleAleatorioNumero = calleAleatorioNumero + azar;
			}

			crearLead.Empresa.inputCalle(driver).sendKeys(calleAleatorio + " " + calleAleatorioNumero);
			mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio + " " + calleAleatorioNumero;
			msj = "IngresoCalle";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Empresa.inputComplementoDireccion(driver).sendKeys("Casa Interior");
			mensaje = "Flujo : Campo Complemento Direccion :" + "Casa Interior";
			msj = "IngresoDetalleDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Empresa.inputPais(driver).click();
			crearLead.Empresa.inputPais(driver).sendKeys("PE");
			Thread.sleep(1000);
			crearLead.Empresa.inputPais(driver).sendKeys("RU");
			mensaje = "Flujo : Ingreso Campo Pais : " + "PERU";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputPais(driver).sendKeys(" ");
			crearLead.Empresa.paisSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Pais  : " + "PERU";
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Empresa.inputRegion(driver).click();
			crearLead.Empresa.inputRegion(driver).sendKeys("LI");
			Thread.sleep(1000);
			crearLead.Empresa.inputRegion(driver).sendKeys("MA");
			mensaje = "Flujo : Ingreso Campo Region : " + "LIMA";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputRegion(driver).sendKeys(" ");
			crearLead.Empresa.regionSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Region  : " + "LIMA";
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearLead.Empresa.inputProvincia(driver).click();
			crearLead.Empresa.inputProvincia(driver).sendKeys("LI");
			Thread.sleep(1000);
			crearLead.Empresa.inputProvincia(driver).sendKeys("MA");
			mensaje = "Flujo : Ingreso Campo Provincia : " + "LIMA";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.inputProvincia(driver).sendKeys(" ");
			crearLead.Empresa.ProvinciaSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Comuna : " + "LIMA";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearLead.Empresa.inputComuna(driver).click();
			crearLead.Empresa.inputComuna(driver).sendKeys("LI");
			Thread.sleep(1000);
			crearLead.Empresa.inputComuna(driver).sendKeys("MA");
			mensaje = "Flujo : Ingreso Campo Comuna : " + "LIMA";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputComuna(driver).sendKeys(" ");
			crearLead.Empresa.comunaSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Comuna : " + "LIMA";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Empresa.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(10000);

			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearLead.Personal.txtNombrePaginaInicio(driver)));
			String comparar = crearLead.Empresa.txtNombrePaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creacion correcta de nuevo lead Empresa";
			Funciones.mensajesLog(mensaje);

			if (comparar.contains(nombreAleatorio + " " + apellidoPaternoAleatorio)) {
				mensaje = "Ok : Coincide Nombre Ingresado " + nombreAleatorio + " " + apellidoPaternoAleatorio + " con Nombre desplegado en Cracion de Lead " + comparar;
				msj = "OkCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>" + "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Lead " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito LEAD EMPRESA " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,"Hoja1");
				String setNombreLeadEmpresa = nombreAleatorio + " " + apellidoPaternoAleatorio;
				ExcelUtils.setCellData(setNombreLeadEmpresa, posicionExcel, 1);

				String setNombreEmpresa = nombreEmpresaPlanilla;
				ExcelUtils.setCellData(setNombreEmpresa, posicionExcel, 2);

				String setTelefono = "2" + numTelefono;
				ExcelUtils.setCellData(setTelefono, posicionExcel, 4);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea LAED Personal de Manera Correcta" + "<p>";
				Log.info(mensaje);
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorElementoNoVisible";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ExistenErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
