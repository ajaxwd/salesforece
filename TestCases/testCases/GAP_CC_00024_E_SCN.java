package testCases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class GAP_CC_00024_E_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		mensaje = "<p>" + "Escenario en Ejecucion : " + sNombrePrueba + "</p>";
		Funciones.mensajesLog(mensaje);

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);
		// String currentURL = driver.getCurrentUrl();
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		// String modeloInteres = ExcelUtils.getCellData(posicionExcel, 26);
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String modeloInteres = "";

			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			//fechaNacimiento = Funciones.getExcel(driver, "Fecha de Nacimiento", posicionExcel);
			
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)));
			paginaPrincipal.MenuPrincipal.btnCuentas(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificación')])[1]|(//h2[contains(text(),'Llamada')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			// try {// presiona boton borrar todo para llamados anteriores y no ocultar
			// botones de
			// // flujo
			//
			// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
			//
			// mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados ";
			// Log.info(mensaje);
			// Reporter.log("<p>" + "<br>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			//
			// }
			//
			// try {// presiona boton borrar todo para llamados anteriores y no ocultar
			// botones de
			// // flujo
			//
			// for (int i = 0; i < 5; i++) {
			//
			// driver.findElement(By.xpath(
			// "//*[@id=\"oneHeader\"]/div[2]/span/ul/li[7]/div[1]/div/section[1]/div/button/lightning-primitive-icon"))
			// .click();
			// // paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
			//
			// mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente ";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);
			// }
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			//
			// }

			Thread.sleep(5000);

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.btnCrearCuenta(driver)));
			principalCuenta.MenuPrincipalCuenta.btnCrearCuenta(driver).click();
			mensaje = "Flujo : Click Boton Crear Cuenta ";
			msj = "ClickBtnCrearCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalCuenta.MenuPrincipalCuenta.rdCuentaEmpresa(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Cuenta Empresa";
			msj = "ClickRadioCrearPersona";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalCuenta.MenuPrincipalCuenta.btnSiguienteCrearCuenta(driver).click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(10000);

			// WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
			// waitSelectEstadoLead.until(ExpectedConditions.visibilityOf(crearLead.Personal.selectEstadoLead(driver)));
			// crearLead.Personal.selectEstadoLead(driver).click();
			// Thread.sleep(2000);
			// crearLead.Personal.nuevoEstadoLead(driver).click();
			// mensaje = "Flujo : Seleccion Estado de Lead :" + "NUEVO";
			// msj = "SeleccionEstado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(1500);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearLead.Empresa.inputDocumentoIdentidad(driver))).getText();

			String rut = "";
			//String tipoDocumentoIdentidad = "";
			switch (paisEjecucion) {

			case "Chile":

				// Cadena de caracteres aleatoria Para Rut
				rut = utilidades.Funciones.generadorDeRut(49999999, 50000000);
				crearLead.Empresa.inputDocumentoIdentidad(driver).click();
				crearLead.Empresa.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
				msj = "IngresoRut";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			case "Perú":

				// tipoDocumentoIdentidad = "RUC";
				// crearLead.Empresa.inputDocumentoIdentidad(driver).click();
				// Thread.sleep(2000);
				// crearLead.Personal.tipoDocumentoIdentidad(driver,
				// tipoDocumentoIdentidad).click();
				// mensaje = "Flujo : Seleccion Documento Identidad : " +
				// tipoDocumentoIdentidad;
				// mensaje = "Flujo : Seleccion Documento Identidad :" + "RUC";
				// msj = "SeleccionDocumentoIdentidad";
				// posicionEvidencia = posicionEvidencia + 1;
				// posicionEvidenciaString = Integer.toString(posicionEvidencia);
				// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				// Thread.sleep(1500);
				// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
				// "_" + msj);
				// Log.info(mensaje);
				// Reporter.log(
				// "<p>" + mensaje + "</p>" + "<img src=" +
				// rutaEvidencia + ">");
				// System.out.println(mensaje);

				// Cadena de caracteres aleatoria Para RUC
				rut = utilidades.Funciones.generadorDeRut(500000000, 990000000);
				String aleatorioNum = Funciones.numeroAleatorio(1, 8);				
				String vectorRut[] = rut.split("-");
				System.err.println("*" + vectorRut[0] + "*");
				System.err.println("*" + vectorRut[1] + "*");
				rut = vectorRut[0];
				rut = rut + Integer.parseInt(aleatorioNum);
				if (rut.length()<11){
					aleatorioNum = Funciones.numeroAleatorio(1, 8);
					rut = rut + Integer.parseInt(aleatorioNum);
				}
				crearLead.Empresa.inputDocumentoIdentidad(driver).sendKeys(rut);
				mensaje = "Flujo : Ingreso de RUC Aleatorio Empresa : " + rut;
				msj = "IngresoDni";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}

			String giroEmpresa = Funciones.getExcel(driver, "Giro", posicionExcel);

			crearCuenta.Empresa.inputGiroEmpresa(driver).click();
			Thread.sleep(500);
			crearCuenta.Empresa.inputGiroEmpresa(driver).sendKeys(giroEmpresa);
			mensaje = "Flujo : Ingreso de Giro : "+giroEmpresa;
			msj = "IngresoGiro";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			/* crearCuenta.Empresa.selectClasificacionFlotas(driver).click();
			Thread.sleep(2000);
			crearCuenta.Empresa.clasificacionFlotasSeleccion(driver, "Agrícola").click();

			mensaje = "Flujo : Ingreso de Flota : " + "A";
			msj = "IngresoFlota";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			String empresaAleatorioComparar = empresaAleatorio + " Cia Ltda";
			crearCuenta.Empresa.inputRazonSocial(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			//String nombreEmpresaPlanilla = empresaAleatorio + " Cia Ltda";
			mensaje = "Flujo : Ingreso de Compañia Aleatorio : " + empresaAleatorio + " Cia Ltda";
			msj = "IngresoCompania";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			
			empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			empresaAleatorio = empresaAleatorio.toLowerCase();
			crearCuenta.Empresa.inputSitioWeb(driver).sendKeys("www." + empresaAleatorio + ".com");
			mensaje = "Flujo : Ingreso de Sitio Web Compañia Aleatorio : " + "www." + empresaAleatorio + ".com";
			msj = "IngresoSitioWeb";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			String detalleDeCuenta = ExcelUtils.getCellData(posicionExcel, 31);
			System.err.println("Cuenta " + detalleDeCuenta);
			
//			new WebDriverWait(driver, 20, 100)
//					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputCuentaPrincipal(driver))).click();			
//
//			String[] arregloIngresoCuenta = new String[detalleDeCuenta.length()];
//			for (int i = 0; i < detalleDeCuenta.length(); i++) {
//				arregloIngresoCuenta[i] = Character.toString(detalleDeCuenta.charAt(i));
//			}
//
//			for (int i = 0; i < detalleDeCuenta.length(); i++) {
//				
//				crearCuenta.Empresa.inputCuentaPrincipal(driver).sendKeys(arregloIngresoCuenta[i]);
//				Thread.sleep(500);
//
//			}
//
//			crearCuenta.Empresa.cuentaPrincipalSeleccionElemento(driver, detalleDeCuenta).click();
//
//			mensaje = "Flujo : Ingreso Cuenta :" + detalleDeCuenta;
//			msj = "IngresoCuenta";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);

			Thread.sleep(3000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			driver.findElement(By.xpath("//*[contains(text(),'Nacionalidad')]")));

			Thread.sleep(3000);

			String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			principalCuenta.MenuPrincipalCuenta.inputNacionalidad(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				crearCuenta.Empresa.inputNacionalidad(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Ingreso Campo Nacionalidad : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);

			crearCuenta.Empresa.nacionalidadSeleccion(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Nacionalidad  : " + paisEjecucion;
			msj = "SeleccionNacionalidad";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			String correo = "";
			correo = "contactenos";
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearCuenta.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			switch (paisEjecucion) {

			case "Chile":

				numTelefono = "2" + numTelefono;
				crearCuenta.Empresa.inputTelefonoEmpresa(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
				msj = "IngresoTelefono";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			case "Perú":

				numTelefono = "0" + numTelefono;
				crearCuenta.Empresa.inputTelefonoEmpresa(driver).sendKeys(numTelefono);
				mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + numTelefono;
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				break;

			default:
				break;
			}

			Thread.sleep(4000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			Thread.sleep(1500);

			String calleAleatorio = "";
			String calleAleatorioNumero = "";

			switch (paisEjecucion) {

			case "Chile":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				// crearLead.Personal.inputCalle(driver).sendKeys(calleAleatorio + " " +
				// calleAleatorioNumero);
				// mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio + " " +
				// calleAleatorioNumero;
				// msj = "IngresoCalle";
				// posicionEvidencia = posicionEvidencia + 1;
				// posicionEvidenciaString = Integer.toString(posicionEvidencia);
				// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				// Thread.sleep(1500);
				// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
				// "_" + msj);
				// Log.info(mensaje);
				// Reporter.log(
				// "<p>" + mensaje + "</p>" + "<img src=" +
				// rutaEvidencia + ">");
				// System.out.println(mensaje);

				crearCuenta.Empresa.inputDireccion(driver).sendKeys(calleAleatorio);
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: " + calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearCuenta.Empresa.inputReferenciaDireccion(driver).sendKeys("Sector Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Sector Interior";
				msj = "IngresoReferenciaDir";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			case "Perú":

				// Cadena de caracteres aleatoria Para Calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
				calleAleatorioNumero = "";
				azar = "";
				// Cadena de numeros aleatoria Para numero Calle Laboral
				for (int i = 0; i < 2; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					calleAleatorioNumero = calleAleatorioNumero + azar;
				}

				crearCuenta.Empresa.inputDireccion(driver).sendKeys(calleAleatorio);
				mensaje = "Flujo: Ingreso Campo Direccion Aleatorio: " + calleAleatorio;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputNumeroDireccion(driver).sendKeys(calleAleatorioNumero);
				mensaje = "Flujo: Ingreso Campo Numero Aleatorio: " + calleAleatorioNumero;
				msj = "IngresoCalle";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearLead.Personal.inputDeptoInterior(driver).sendKeys("Interior");
				mensaje = "Flujo : Campo Depto/Interior :" + "Casa Interior";
				msj = "IngresoComplementoDir";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1500);

				crearCuenta.Empresa.inputReferenciaDireccion(driver).sendKeys("Sector Interior");
				mensaje = "Flujo : Campo Complemento Direccion :" + "Sector Interior";
				msj = "IngresoReferenciaDir";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				break;

			default:
				break;
			}

			String regionIngresar = "";
			String regionIngresarAlternativo = "";
			String provinciaIngresar = "";
			String comunaIngresar = "";

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);
			// jse.executeScript("arguments[0].scrollIntoView();",
			// driver.findElement(By.xpath("//div[2]/div/div[2]/div/div[3]")));
			// jse.executeScript("window.scrollBy (0,600)");
			Thread.sleep(1000);

			switch (paisEjecucion) {
			case "Chile":

				regionIngresar = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				regionIngresarAlternativo = "Buscar Regiones...";
				comunaIngresar = "Buscar Comunas...";
				break;

			case "Perú":

				regionIngresar = "Buscar Departamentos...";
				regionIngresarAlternativo = "Buscar Regiones...";
				provinciaIngresar = "Buscar Provincias...";
				comunaIngresar = "Buscar Distritos...";

				break;

			default:
				break;
			}

			for (int i = 0; i < paisEjecucion.length(); i++) {
				arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
			}
			principalCuenta.MenuPrincipalCuenta.inputPais(driver).click();
			for (int i = 0; i < paisEjecucion.length(); i++) {
				principalCuenta.MenuPrincipalCuenta.inputPais(driver).sendKeys(arregloPaisEjecucion[i]);
				Thread.sleep(500);
			}

			// crearLead.Personal.inputPais(driver).click();
			// crearLead.Personal.inputPais(driver).sendKeys("CHI");
			// Thread.sleep(1000);
			// crearLead.Personal.inputPais(driver).sendKeys("LE");
			// mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);
			// crearLead.Personal.inputPais(driver).sendKeys(" ");
			int largo = crearLead.Personal.paisSelecciones(driver, paisEjecucion).size();
			crearLead.Personal.paisSelecciones(driver, paisEjecucion).get(largo - 1).click();
			// mensaje = "Flujo : Seleccion de Pais : " + "CHILE";
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo));
			Thread.sleep(1000);

			String[] arregloRegionEjecucion = new String[regionEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloRegionEjecucion[i] = Character.toString(regionEjecucion.charAt(i));
			}
			crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).click();
			for (int i = 0; i < regionEjecucion.length(); i++) {
				crearLead.Personal.inputRegion(driver, regionIngresar, regionIngresarAlternativo).sendKeys(arregloRegionEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			// crearLead.Personal.inputRegion(driver, regionIngresar).click();
			// crearLead.Personal.inputRegion(driver, regionIngresar).sendKeys("SANT");
			// Thread.sleep(1000);
			// crearLead.Personal.inputRegion(driver, regionIngresar).sendKeys("IAGO");
			// mensaje = "Flujo : Ingreso Campo Region : " + "SANTIAGO";
			mensaje = "Flujo : Ingreso Campo Region/Departamento : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			// crearLead.Personal.inputRegion(driver, regionIngresar).sendKeys(" ");
			crearLead.Personal.regionSeleccion(driver, regionEjecucion).click();
			// mensaje = "Flujo : Seleccion de Region : " + "SANTIAGO";
			mensaje = "Flujo : Seleccion de Region/Departamento  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloProvinciaEjecucion = new String[provinciaEjecucion.length()];
			for (int i = 0; i < regionEjecucion.length(); i++) {
				arregloProvinciaEjecucion[i] = Character.toString(provinciaEjecucion.charAt(i));
			}
			crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(arregloProvinciaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			// crearLead.Personal.inputProvincia(driver, provinciaIngresar).click();
			// crearLead.Personal.inputProvincia(driver,
			// provinciaIngresar).sendKeys("SANT");
			// Thread.sleep(1000);
			// crearLead.Personal.inputProvincia(driver,
			// provinciaIngresar).sendKeys("IAGO");
			// mensaje = "Flujo : Ingreso Campo Provincia : " + "SANTIAGO";
			mensaje = "Flujo : Ingreso Campo Provincia : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			// crearLead.Personal.inputProvincia(driver, provinciaIngresar).sendKeys(" ");
			crearLead.Personal.provinciaSeleccion(driver, provinciaEjecucion).click();
			// mensaje = "Flujo : Seleccion de Provincia : " + "SANTIAGO";
			mensaje = "Flujo : Seleccion de Provincia  : " + provinciaEjecucion;
			msj = "SeleccionProvincia";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			jse.executeScript("arguments[0].scrollIntoView();", crearLead.Personal.inputComuna(driver, comunaIngresar));
			Thread.sleep(1000);

			String[] arregloComunaEjecucion = new String[comunaEjecucion.length()];
			for (int i = 0; i < comunaEjecucion.length(); i++) {
				arregloComunaEjecucion[i] = Character.toString(comunaEjecucion.charAt(i));
			}
			crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			for (int i = 0; i < provinciaEjecucion.length(); i++) {
				jse.executeScript("window.scrollBy (0,150)");
				crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(arregloComunaEjecucion[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);
			// crearLead.Personal.inputComuna(driver, comunaIngresar).click();
			// crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys("SANT");
			// Thread.sleep(1000);
			// crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys("IAGO");
			// mensaje = "Flujo : Ingreso Campo Comuna : " + "SANTIAGO";
			mensaje = "Flujo : Ingreso Campo Comuna/Distrito : " + comunaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			// crearLead.Personal.inputComuna(driver, comunaIngresar).sendKeys(" ");
			Thread.sleep(1000);
			// jse.executeScript("arguments[0].scrollIntoView();",
			// crearLead.Personal.comunaSeleccion(driver, comunaEjecucion));
			jse.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(
			"(//lightning-icon[@class='itemIcon slds-icon slds-icon--x-small slds-m-left--x-small slds-icon-text-default slds-button__icon slds-icon-utility-search slds-icon_container forceIcon']/following::span[contains(@title,'"
			+ comunaEjecucion + "')])[2]")));
			// lightning-icon[@class='itemIcon slds-icon slds-icon--x-small
			// slds-m-left--x-small slds-icon-text-default slds-button__icon
			// slds-icon-utility-search slds-icon_container
			// forceIcon']/following::span[contains(@title,'LIMA')]
			Thread.sleep(1000);
			crearLead.Personal.comunaSeleccion(driver, comunaIngresar, comunaEjecucion).click();
			// mensaje = "Flujo : Seleccion de Comuna : " + "SANTIAGO";
			mensaje = "Flujo : Seleccion de Comuna/Distrito : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearLead.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {

				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta página.")) {

					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta página :  "
					+ crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText() + "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(10000);

			// String textoNotificacionesVerde = "";
			// String textoNotificacionesVerdeDos = "";
			// String textoNotificacionesVerdePequeno = "";
			// String textoNotificacionesVerdePequeno1 = "";
			// String textoNotificacionesVerdePequeno2 = "";
			// String textoNotificacionesVerdePequeno3 = "";
			// String textoNotificacionesVerdePequeno4 = "";
			// String textoNotificacionesVerdePequeno5 = "";
			// String verificaGuardado = "";

			// try {
			// textoNotificacionesVerde =
			// MenuPrincipal.textoNotificacionesVerde(driver).getText();
			// System.err.println("textoNotificacionesVerde "+textoNotificacionesVerde);
			// verificaGuardado = textoNotificacionesVerde;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// try {
			// textoNotificacionesVerdeDos =
			// MenuPrincipal.textoNotificacionesVerdeDos(driver).getAttribute("class");
			// System.err.println("textoNotificacionesVerdeDos
			// "+textoNotificacionesVerdeDos);
			// verificaGuardado = textoNotificacionesVerdeDos;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// try {
			// textoNotificacionesVerdePequeno =
			// driver.findElement(By.xpath("//section/div/div/div/div//following::*[contains(text(),'Se
			// cre�')]")).getText();
			// System.err.println("textoNotificacionesVerdePequeno
			// "+textoNotificacionesVerdePequeno);
			// Log.info("textoNotificacionesVerdePequeno "+textoNotificacionesVerdePequeno);
			// Reporter.log("textoNotificacionesVerdePequeno
			// "+textoNotificacionesVerdePequeno);
			// verificaGuardado = textoNotificacionesVerdePequeno;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// try {
			// textoNotificacionesVerdePequeno1=
			// driver.findElement(By.xpath("//*[contains(text(),'success')]//following::span[@class='toastMessage
			// slds-text-heading--small forceActionsText']")).getText();
			// System.err.println("textoNotificacionesVerdePequeno1
			// "+textoNotificacionesVerdePequeno1);
			// verificaGuardado = textoNotificacionesVerdePequeno1;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			//
			// try {
			// int num =
			// driver.findElements(By.xpath("//*[contains(text(),'success')]/following::span")).size();
			// //System.err.println("textoNotificacionesVerdePequeno2
			// "+textoNotificacionesVerdePequeno2);
			// System.err.println("textoNotificacionesVerdePequeno2 cant "+num);
			// List<WebElement> allFormChildElements =
			// driver.findElements(By.xpath("//*[contains(text(),'success')]//following::span"));
			// int a = 1;
			// Log.info("HTMLSIZE CLASS :"+num);
			// Reporter.log("HTMLSIZE CLASS :"+num);
			// for (WebElement item : allFormChildElements){
			// a = a + 1;
			// System.err.println(a);
			// System.err.println("HTML :"+a+" "+item.getAttribute("class"));
			// Log.info("HTML :"+a+" "+item.getAttribute("class"));
			// Reporter.log("HTML :"+a+" "+item.getAttribute("class"));
			//// System.err.println("HTML :"+item.getAttribute("outerHTML"));
			//// System.err.println("HTML :"+item.getAttribute("class"));
			//// System.err.println("HTML :"+item.getText());
			// if (item.getAttribute("outerHTML").contains("Se cre� Cuenta personal .")) {
			// System.err.println("outerHTML Cuenta:"+a+" "+item.getAttribute("outerHTML"));
			// Log.info("outerHTML Cuenta:"+a+" "+item.getAttribute("outerHTML"));
			// Reporter.log("outerHTML Cuenta:"+a+" "+item.getAttribute("outerHTML"));
			// }
			// if (item.getAttribute("outerHTML").contains("success")) {
			// System.err.println("outerHTML success:"+a+"
			// "+item.getAttribute("outerHTML"));
			// Log.info("outerHTML success:"+a+" "+item.getAttribute("outerHTML"));
			// Reporter.log("outerHTML success:"+a+" "+item.getAttribute("outerHTML"));
			// }
			// }
			// verificaGuardado = textoNotificacionesVerdePequeno2;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }

			// try {
			// int num =
			// driver.findElements(By.xpath("//section/div/div/div//following::span[@data-aura-class='forceActionsText']")).size();
			// System.err.println("textoNotificacionesVerdePequeno2
			// "+textoNotificacionesVerdePequeno2);
			// System.err.println("textoNotificacionesVerdePequeno3 cant "+num);
			// List<WebElement> allFormChildElements =
			// driver.findElements(By.xpath("//section/div/div/div//following::span[@data-aura-class='forceActionsText']"));
			// int a = 1;
			// (//span[contains(@class,'toastMessage slds-text-heading--small
			// forceActionsText')])[1]

			// String msjeTest = "<p>"+"HTMLSIZE :"+num+"</p>";
			// String msjeTest2 = "textoNotificacionesVerdePequeno3 cant "+num;
			// Log.info(msjeTest);
			// Reporter.log(msjeTest);
			//
			//
			// for (WebElement item : allFormChildElements){
			// msjeTest2 = msjeTest2 + item.getText()+"\\n";
			// //getAttribute("text")+"\\n";
			// msjeTest ="<p>"+"HTML :"+a+" *"+msjeTest2+"*"+"</p>";
			// System.err.println(a);
			// System.err.println(msjeTest);
			// Log.info(msjeTest);
			//
			// a = a + 1;
			// System.err.println("HTML :"+item.getAttribute("outerHTML"));
			// System.err.println("HTML :"+item.getAttribute("class"));
			// System.err.println("HTML :"+item.getText());
			// if (item.getAttribute("outerHTML").contains("Se cre� Cuenta personal .")) {
			// System.err.println("HTML :"+item.getAttribute("outerHTML"));
			// Log.info("HTML :"+a+" "+item.getAttribute("outerHTML"));
			// Reporter.log("HTML :"+a+" "+item.getAttribute("outerHTML"));
			// }
			// if (item.getAttribute("outerHTML").contains("success")) {
			// System.err.println("outerHTML.contains success
			// "+a+item.getAttribute("outerHTML"));
			// Log.info("outerHTML.contains success "+a+item.getAttribute("outerHTML"));
			// Reporter.log("outerHTML.contains success "+a+item.getAttribute("outerHTML"));
			// }
			// }
			//
			//
			//
			// String ruta = "C:\\SalesForce\\nombreArchivo - copia.txt";
			// File archivo = new File(ruta);
			// BufferedWriter bw = null;
			// if(archivo.exists()) {
			// try {
			// bw = new BufferedWriter(new FileWriter(archivo));
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// try {
			// bw.write("");
			// bw.write(msjeTest2);
			// bw.close();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			// System.err.println("No Existe el Archivo de Texto 'nombreArchivo.txt' para
			// crear Nombre de Archivo PDF ");
			// }
			// verificaGuardado = textoNotificacionesVerdePequeno3;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// try {
			// textoNotificacionesVerdePequeno3 =
			// driver.findElement(By.xpath("//*[contains(text(),'Se cre�')]")).getText();
			// System.err.println("textoNotificacionesVerdePequeno3
			// *"+textoNotificacionesVerdePequeno3+"*");
			// verificaGuardado = textoNotificacionesVerdePequeno3;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }

			// try {
			// textoNotificacionesVerdePequeno4 =
			// driver.findElement(By.xpath("//*[contains(text(),'success')]//following::span")).getText();
			// System.err.println("textoNotificacionesVerdePequeno4
			// "+textoNotificacionesVerdePequeno4);
			// verificaGuardado = textoNotificacionesVerdePequeno4;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }

			//
			// try {
			// int num =
			// driver.findElements(By.xpath("//section/div/div/div/div/div//following::span[@data-aura-class='forceActionsText']")).size();
			// //System.err.println("textoNotificacionesVerdePequeno2
			// "+textoNotificacionesVerdePequeno2);
			// System.err.println("textoNotificacionesVerdePequeno4 cant "+num);
			// List<WebElement> allFormChildElements =
			// driver.findElements(By.xpath("//section/div/div/div/div/div//following::span[@data-aura-class='forceActionsText']"));
			// int a = 1;
			//
			// String msjeTest = "<p>"+"HTMLSIZE :"+num+"</p>";
			// String msjeTest2 = "textoNotificacionesVerdePequeno4 cant "+num;
			// Log.info(msjeTest);
			// Reporter.log(msjeTest);
			// for (WebElement item : allFormChildElements){
			// msjeTest2 = msjeTest2 + item.getAttribute("text")+"\\n";
			// msjeTest ="<p>"+"HTML :"+a+" *"+msjeTest2+"*"+"</p>";
			// System.err.println(a);
			// System.err.println(msjeTest);
			// Log.info(msjeTest);
			//
			//
			// a = a + 1;
			// System.err.println("HTML :"+item.getAttribute("outerHTML"));
			// System.err.println("HTML :"+item.getAttribute("class"));
			// System.err.println("HTML :"+item.getText());
			// if (item.getAttribute("outerHTML").contains("Se cre� Cuenta personal .")) {
			// System.err.println("HTML :"+item.getAttribute("outerHTML"));
			// Log.info("HTML :"+a+" "+item.getAttribute("outerHTML"));
			// Reporter.log("HTML :"+a+" "+item.getAttribute("outerHTML"));
			// }
			// if (item.getAttribute("outerHTML").contains("success")) {
			// System.err.println("outerHTML.contains success
			// "+a+item.getAttribute("outerHTML"));
			// Log.info("outerHTML.contains success "+a+item.getAttribute("outerHTML"));
			// Reporter.log("outerHTML.contains success "+a+item.getAttribute("outerHTML"));
			// }
			// }
			//
			//
			// String ruta = "C:\\SalesForce\\nombreArchivo - copia (2).txt";
			// File archivo = new File(ruta);
			// BufferedWriter bw = null;
			// if(archivo.exists()) {
			// try {
			// bw = new BufferedWriter(new FileWriter(archivo));
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// try {
			// bw.write("");
			// bw.write(msjeTest2);
			// bw.close();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			// System.err.println("No Existe el Archivo de Texto 'nombreArchivo.txt' para
			// crear Nombre de Archivo PDF ");
			// }
			//
			//
			// verificaGuardado = textoNotificacionesVerdePequeno3;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			//
			//
			// try {
			// textoNotificacionesVerdePequeno5 =
			// driver.findElement(By.xpath("//*[contains(text(),'Se cre�')]")).getText();
			// System.err.println("textoNotificacionesVerdePequeno5
			// "+textoNotificacionesVerdePequeno5);
			// verificaGuardado = textoNotificacionesVerdePequeno5;
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			//
			//

			mensaje = "Flujo : Se revisa creacion correcta de nueva Cuenta Empresa";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(10000);

			try {

				String textoError = driver.findElement(By.xpath("//span[@class='genericError uiOutputText']")).getText();
				String contenidoError = driver.findElement(By.xpath(
				"//span[@class='genericError uiOutputText'][contains(text(),'"+textoError+"')]//following::li")).getText();
				System.err.println("Cont Error " + contenidoError);
				if (textoError.contains("Revise")) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Creacion " + contenidoError + "<p>";
					msj = "ErrorExistenCreacion";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					driver.close();
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (NoSuchElementException | ScreenshotException e1) { }

			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.txtNombrePaginaInicioCuenta(driver)));
			//String compararNombre = crearCuenta.Empresa.txtNombrePaginaInicioCuenta(driver).getAttribute("title");
			String compararNombre = crearCuenta.Empresa.txtNombrePaginaInicioCuenta(driver).getText();
			String compararRut = crearCuenta.Empresa.txtRutPaginaInicioCuenta(driver).getText();
			String compararTelefono = crearCuenta.Empresa.txtTelefonoPaginaInicioCuenta(driver).getText();

			System.err.println("*" + compararNombre + "*");
			System.err.println("*" + empresaAleatorioComparar + "*");
			System.err.println("*" + compararRut + "*");
			System.err.println("*" + rut + "*");
			System.err.println("*" + compararTelefono + "*");
			System.err.println("*" + numTelefono + "*");

			if (compararNombre.contains(empresaAleatorioComparar) && compararRut.contains(rut) && compararTelefono.contains(numTelefono)) {
				mensaje = "Ok : Guardado exitoso";
				msj = "OkExito";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
				// ValorEncontrado = false;

			} else {
				mensaje = "<p style='color:red;'>" + "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Cuenta " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito CUENTA Empresa " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,"Hoja1");

				recorreExcell = 0;
				inicioExcell = 0;
				String setNombreLead = "";
				while (recorreExcell == 0) {
					if ("Nombre Empresa Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setNombreLead = compararNombre;
						ExcelUtils.setCellData(setNombreLead, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Nombre Persona Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				recorreExcell = 0;
				inicioExcell = 0;
				while (recorreExcell == 0) {
					if ("Nombre Persona Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						ExcelUtils.setCellData("Cuenta Empresa", posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Nombre Persona Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}
				
				
				recorreExcell = 0;
				inicioExcell = 0;
				String setRut = "";
				while (recorreExcell == 0) {
					if ("Rut Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setRut = rut;
						ExcelUtils.setCellData(setRut, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Rut Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				recorreExcell = 0;
				inicioExcell = 0;
				String setTelefono = "";
				while (recorreExcell == 0) {
					if ("Telefono Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						setTelefono = numTelefono;
						ExcelUtils.setCellData(setTelefono, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'Telefono Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				String ruta = "C:\\SalesForce\\listadoCreacionCuentasVolumen.txt";
				File archivo = new File(ruta);
				FileWriter fw = null;
				BufferedWriter bw = null;
				// flag true, indica adjuntar informaci�n al archivo.

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
				Date date = new Date();
				String dateTime = dateFormat.format(date);
				String data = setRut + "  " + setNombreLead + "  " + setTelefono + "  " + dateTime + "\r\n";
				// String data = setRut + " " + setNombreLead+ " " + setTelefono + " "+
				// dateTime;
				if (archivo.exists()) {
					try {
						// bw = new BufferedWriter(new FileWriter(archivo));
						fw = new FileWriter(archivo.getAbsoluteFile(), true);
						bw = new BufferedWriter(fw);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						bw.write(data);
						// bw.newLine();
						bw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.err.println("No Existe el Archivo de Texto 'listadoCreacionCuentasVolumen.txt'");
				}

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea CUENTA Empresa de Manera Correcta" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEnEjecucion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
