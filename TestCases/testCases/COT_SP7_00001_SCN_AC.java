package testCases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.thoughtworks.selenium.webdriven.commands.Refresh;
import login.IniciarSesion;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00001_SCN_AC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = Funciones.numeroAstring(sNumeroEscenario);
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		if (sNumeroEscenario == "0011" || sNumeroEscenario == "0012") {
			mensaje = "Escenario en Ejecucion : " + sNombrePrueba;
			Funciones.mensajesLog(mensaje);
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			utilidades.Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		IniciarSesion.Execute(driver, nombreCapturaClase);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		String nombreCotizacion = ExcelUtils.getCellData(posicionExcel, 1);
		String nombreCotizacionEmpresa = ExcelUtils.getCellData(posicionExcel, 2);
		String busquedaOportunidad = "";
		String formaDePago = "";
		System.err.println("Nombre Cuenta " + nombreCotizacion);
		System.err.println("Nombre Empresa " + nombreCotizacionEmpresa);
		
		formaDePago = Funciones.getExcel(driver, "Forma de Pago", posicionExcel);
		busquedaOportunidad = Funciones.getExcel(driver, "Oportunidad", posicionExcel);
		System.out.println(formaDePago);
		System.out.println(busquedaOportunidad);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {
					WebElement registro = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnNuevaCotizacion(driver)))
			.click();
			mensaje = "Flujo : Click Boton 'Nueva Cotizacion' en Oportunidades ";
			msj = "ClickBtnNvaCotizacions";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver)))
			.click();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(formaDePago);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Forma de Pago' ";
			msj = "IngresoFormaPago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + formaDePago + "']")))
			.click();
			mensaje = "Flujo : Se Selecciona 'Forma de Pago' : " + formaDePago;
			msj = "SeleccionFormaPago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH-mm-ss");
			Date date = new Date();
			String dateTime = dateFormat.format(date);
			dateTime = dateTime.replaceAll("-", "");
			dateTime = dateTime.replaceAll(" ", "_");
			if (nombreCotizacion.equals("Cuenta Empresa")) {
				nombreCotizacion = nombreCotizacionEmpresa;
			}
			nombreCotizacion = nombreCotizacion.replaceAll(" ", "");
			nombreCotizacion = "COT_" + nombreCotizacion + "_" + dateTime;

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.inputNombreCotizacionSCN(driver))).click();
			principalOportunidades.PrincipalOportunidades.inputNombreCotizacionSCN(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputNombreCotizacionSCN(driver).sendKeys(nombreCotizacion);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputNombreCotizacionSCN(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Nombre de la Cotizacion' ";
			msj = "IngresoNombreCotizacion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnGuardarCotizacion(driver)))
			.click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar' en Cotizacion";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			try {
				String textoNotificacionesVerdePequeno = MenuPrincipal.textoNotificacionesVerdePequeno(driver)
				.getText();
				System.err.println("textoNotificacionesVerdePequeno " + textoNotificacionesVerdePequeno);
			} catch (TimeoutException | NoSuchElementException e) {

			}

			String nombreCotizacionTextoVerde = new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.lnkCotizacionCreada(driver)))
			.getText();

			System.err.println("nombreCotizacionTextoVerde " + nombreCotizacionTextoVerde);
			nombreCotizacionTextoVerde = nombreCotizacionTextoVerde.replaceAll("Se creó Cotización ", "");
			//nombreCotizacionTextoVerde = nombreCotizacionTextoVerde.replaceAll("\\.", "");

			System.err.println("nombreCotizacionTextoVerde " + nombreCotizacionTextoVerde);

			mensaje = "Flujo : Verifica Cotizacion Recien Creada ";
			Funciones.mensajesLog(mensaje);
			//System.out.println(nombreCotizacion);
			nombreCotizacionTextoVerde = nombreCotizacionTextoVerde.replaceAll("Se creó Cotización ", "");
			nombreCotizacionTextoVerde = nombreCotizacionTextoVerde.replaceAll("\\.", "");
			//System.out.println(nombreCotizacionTextoVerde);

			Thread.sleep(3000);
			String numeroCotizacion = "";
			if (nombreCotizacion.equals(nombreCotizacionTextoVerde)) {

				Thread.sleep(5000);

				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
				principalOportunidades.PrincipalOportunidades.inputBuscar(driver)))
				.sendKeys(nombreCotizacion);

				mensaje = "Flujo : Buscar 'Cotizacion Creada'";
				msj = "btnCotizaciones";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				new WebDriverWait(driver, 20, 100)
				.until(ExpectedConditions.elementToBeClickable(
				principalOportunidades.PrincipalOportunidades.inputBuscar(driver)))
				.sendKeys(" " + Keys.ENTER);

				Thread.sleep(5000);
				String revisaNombre = "";
				posicion = 1;
				sw = 0;
				while (sw == 0) {

					if (posicion == 50) {

						mensaje = "<p style='color:red;'>" + "Error : Mensaje desconocido o no existe" + "</p>";
						msj = "ErroresMensajesEncontrados";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.quit();
						throw new AssertionError();
					}

					try {

						new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOf(principalOportunidades.PrincipalOportunidades
						.btnCotizacioneCreada(driver, nombreCotizacion))).click();

						Thread.sleep(5000);

						mensaje = "Flujo : Click Link 'Cotizacion'Creada' ";
						msj = "btnCotizacioneCreada";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						sw = 1;

					} catch (NoSuchElementException | ScreenshotException e) {
						System.err.println("Iteracion " + posicion + " " + nombreCotizacion);
						sw = 0;
						posicion = posicion + 1;
						driver.navigate().refresh();
						Thread.sleep(3000);

					}
					
					Thread.sleep(5000);
					
					try {
						revisaNombre = principalOportunidades.PrincipalOportunidades.txtNombreCotizacionPaginaPrincipal(driver).getText();
						System.out.println("revisaNombre " + revisaNombre);
						sw = 1;
					} catch (NoSuchElementException | ScreenshotException e) {
						System.err.println("No encontro nombre " + posicion);
						sw = 0;
						posicion = posicion + 1;
						driver.navigate().refresh();
						Thread.sleep(3000);
					}

					if (revisaNombre.equals(nombreCotizacion)) {

						Thread.sleep(1000);
						mensaje = "Flujo : Se Encuentra Cotizacion y Revisa Numero";
						msj = "revisionNumeroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						numeroCotizacion = principalOportunidades.PrincipalOportunidades.txtNumeroCotizacion(driver)
								.getText();
						System.err.println("Numero Cotizacion " + numeroCotizacion);
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				}

				mensaje = "OK : Cotizacion " + nombreCotizacion + " Existe Con Numero : " + numeroCotizacion;
				msj = "RevisionCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(1000);
				ValorEncontrado = true;

			} else {
				mensaje = "Error : Cotizacion No Existe ";
				msj = "RevisionCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(1000);
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Cotizacion  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
						"Hoja2");
				ExcelUtils.setCellData(nombreCotizacion, posicionExcel, 20);
				ExcelUtils.setCellData(numeroCotizacion, posicionExcel, 19);
			
				
				String setRut = "";
				setRut = Funciones.getExcel(driver, "Rut Cuenta Creada", posicionExcel);

				String setNombreOportunidad = "";
				setNombreOportunidad = Funciones.getExcel(driver, "Nombre Oportunidad", posicionExcel);

				String setNombreCuentaEmpresa = "";
				setNombreCuentaEmpresa = Funciones.getExcel(driver, "Nombre Empresa Cuenta Creada", posicionExcel);
				
				String setNombreCuentaPersona = "";
				setNombreCuentaPersona = Funciones.getExcel(driver, "Nombre Persona Cuenta Creada", posicionExcel);

				String setNombreCuenta = "";
				if (setNombreCuentaPersona.equals("Cuenta Empresa")) {
					setNombreCuenta = setNombreCuentaEmpresa;
				} else {
					setNombreCuenta = setNombreCuentaPersona;
				}

				String ruta = "C:\\SalesForce\\listadoCreacionCotizacionesVolumen.txt";
				File archivo = new File(ruta);
				FileWriter fw = null;
				BufferedWriter bw = null;
				// flag true, indica adjuntar información al archivo.

				DateFormat dateFormatTxt = new SimpleDateFormat("dd-MM-YYYY");
				Date dateTxt = new Date();
				String dateTimeTxt = dateFormatTxt.format(dateTxt);
				String data = numeroCotizacion + "  " + setRut + "  " + setNombreCuenta + "  " + setNombreOportunidad
						+ "  " + dateTimeTxt + "\r\n";
				if (archivo.exists()) {
					try {
						// bw = new BufferedWriter(new FileWriter(archivo));
						fw = new FileWriter(archivo.getAbsoluteFile(), true);
						bw = new BufferedWriter(fw);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						bw.write(data);
						// bw.newLine();
						bw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.err.println("No Existe el Archivo de Texto 'listadoCreacionCotizacionesVolumen.txt'");
				}

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Cotizacion Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
