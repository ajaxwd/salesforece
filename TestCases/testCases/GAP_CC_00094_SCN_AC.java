package testCases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class GAP_CC_00094_SCN_AC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = Funciones.numeroAstring(sNumeroEscenario);
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		String busquedaLead = ExcelUtils.getCellData(posicionExcel, 1);
		String busquedaEmpresa = ExcelUtils.getCellData(posicionExcel, 2);
		String razonSocial = "";
		String modeloInteres = "";
		String tipoDeVenta = ExcelUtils.getCellData(posicionExcel, 30);
		String detalleDeCuenta = ExcelUtils.getCellData(posicionExcel, 31);

		modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
		
		if (busquedaLead.equals("Cuenta Empresa")) {
			razonSocial = busquedaEmpresa;
		} else {
			razonSocial = busquedaLead;
		}

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";

			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Oportunidades ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]|//button[@class='slds-button slds-button slds-button_icon slds-button--icon-container slds-notification__close slds-button_icon-bare']//lightning-primitive-icon"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearOportunidad(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearOportunidad(driver).click();
			mensaje = "Flujo : Click Boton Crear Oportunidad ";
			msj = "ClickBtnCrearOportunidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			try {
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				WebElement btnPopUpAdicional = driver.findElement(By.xpath("//span[contains(text(),'Siguiente')]"));
				btnPopUpAdicional.click();
			} catch (NoSuchElementException | ScreenshotException e) {
			}

			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			String titulo = crearOportunidades.Oportunidades.labelTituloNuevaOPortunidad(driver).getText();

			DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH-mm-ss");
			Date date = new Date();
			String dateTime = dateFormat.format(date);
			dateTime = dateTime.replaceAll("-", "");
			dateTime = dateTime.replaceAll(" ", "_");

			String nombreOportunidadCompleto = "OP " + razonSocial + " " + dateTime;

			WebDriverWait waitNombre = new WebDriverWait(driver, 50);
			waitNombre.until(
			ExpectedConditions.visibilityOf(crearOportunidades.Oportunidades.inputNombreOportunidad(driver)));
			crearOportunidades.Oportunidades.inputNombreOportunidad(driver).sendKeys(nombreOportunidadCompleto);
			String nombreOp = crearOportunidades.Oportunidades.inputNombreOportunidad(driver).getText();
			mensaje = "Flujo : Ingreso de Nombre Oportunidad : " + nombreOportunidadCompleto;
			msj = "IngresoNombreOportunidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearOportunidades.Oportunidades.inputNombreRazon(driver).click();
			crearOportunidades.Oportunidades.inputNombreRazon(driver).sendKeys(razonSocial);
			mensaje = "Flujo : Ingreso Campo Nombre/Razon Social : " + razonSocial;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + razonSocial + "']/mark")))
			.click();
			mensaje = "Flujo : Seleccion Campo Nombre/Razon Social : " + razonSocial;
			msj = "SeleccionDeNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearOportunidades.Oportunidades.inputFechaCierre(driver)
			.sendKeys(Funciones.funcionSumarMes(1, paisEjecucion));
			mensaje = "Flujo : Ingreso de Fecha de Cierre : " + Funciones.funcionSumarMes(1, paisEjecucion);
			msj = "IngresoFechaCierrre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			if (titulo.contains("Usados")) {

				Thread.sleep(2000);
				crearOportunidades.Oportunidades.selectEtapa(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.detencion(driver).click();
				mensaje = "Flujo : Seleccion de Etapa : " + "Deteccion";
				msj = "SeleccionDeEtapa";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(3000);

				crearOportunidades.Oportunidades.selectDivisaOportunidad(driver).click();
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String divisa = "";
				divisa = Funciones.getExcel(driver, "Divisa", posicionExcel);

				crearOportunidades.Oportunidades.divisa(driver, divisa).click();
				mensaje = "Flujo : Ingreso de Divisa de la Oportunidad : " + divisa;
				msj = "IngresoDivisa";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				crearOportunidades.Oportunidades.selectPrioridad(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.altoInteres(driver).click();
				mensaje = "Flujo : Ingreso de Origen : " + "Medio Interes";
				msj = "MedioDeInteres";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				crearOportunidades.Oportunidades.selectOrigen(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.web(driver).click();
				mensaje = "Flujo : Ingreso de Origen : " + "Web";
				msj = "IngresoOrigen";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} else {

				crearOportunidades.Oportunidades.selectPrioridad(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.altoInteres(driver).click();
				mensaje = "Flujo : Ingreso de Origen : " + "Medio Interes";
				msj = "MedioDeInteres";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				crearOportunidades.Oportunidades.selectEtapa(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.detencion(driver).click();
				mensaje = "Flujo : Seleccion de Etapa : " + "Deteccion";
				msj = "SeleccionarEtapa";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				crearOportunidades.Oportunidades.selectDivisaOportunidad(driver).click();
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String divisa = "";
				divisa = Funciones.getExcel(driver, "Divisa", posicionExcel);

				crearOportunidades.Oportunidades.divisa(driver, divisa).click();
				mensaje = "Flujo : Ingreso de Divisa de la Oportunidad : " + divisa;
				msj = "IngresarLaDivisa";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				crearOportunidades.Oportunidades.selectOrigen(driver).click();
				Thread.sleep(2000);
				crearOportunidades.Oportunidades.web(driver).click();
				mensaje = "Flujo : Ingreso de Origen : " + "Web";
				msj = "IngresoOrigen";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			}

			crearOportunidades.Oportunidades.inputReferido(driver).sendKeys("Test Referido");
			mensaje = "Flujo : Ingreso Referido : " + "Test referido";
			msj = "IngresoReferido";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);
			int posicionVenta = 0;
			switch (tipoDeVenta) {

			case ("Leasing"):
				posicionVenta = 1;
				break;
			case ("Para"):
				posicionVenta = 2;
				break;
			case ("Y"):
				posicionVenta = 3;
				break;
			case ("Normal"):
				posicionVenta = 0;
				break;
			case (""):
				posicionVenta = 0;
				break;
			default:
				posicionVenta = 0;
				break;
			}

			if (posicionVenta != 0) {
				
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				crearOportunidades.Oportunidades.inputReferido(driver));

				WebElement buscarCuenta = driver.findElement(By.xpath("(//div[@class='inputWrapper slds-grid slds-grid_vertical-align-center slds-p-right_x-small']//following::input[@title='Buscar Cuentas'])[" + posicionVenta + "]"));
								
				String[] arregloIngresoCuenta = new String[detalleDeCuenta.length()];
				for (int i = 0; i < detalleDeCuenta.length(); i++) {
					arregloIngresoCuenta[i] = Character.toString(detalleDeCuenta.charAt(i));
				}

				for (int i = 0; i < detalleDeCuenta.length(); i++) {
					buscarCuenta.sendKeys(arregloIngresoCuenta[i]);
					Thread.sleep(500);
				}

				mensaje = "Flujo : Ingreso Cuenta :" + detalleDeCuenta;
				msj = "IngresoCuenta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				int sw = 0;
				while (sw == 0) {

					try {
						int ok_size = driver.findElements(By.xpath("//div[@title='" + detalleDeCuenta + "']")).size();
						driver.findElements(By.xpath("//div[@title='" + detalleDeCuenta + "']")).get(ok_size - 1)
						.click();
						sw = 1;

					} catch (NoSuchElementException | ScreenshotException e) { sw = 0; }

				}
				Thread.sleep(2000);
			}

			if (titulo.contains("Usados")) {

				recorreExcell = 0;
				inicioExcell = 0;
				String vin = "";
				vin = Funciones.getExcel(driver, "VIN", posicionExcel);

				if (vin != "") {
					crearOportunidades.Oportunidades.inputVinOportunidad(driver).sendKeys(vin);
					mensaje = "Flujo : Ingreso VIN : " + vin;
					msj = "IngresoVin";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String cilindrada = "";
				cilindrada = Funciones.getExcel(driver, "Cilindrada", posicionExcel);

				if (cilindrada != "") {
					crearOportunidades.Oportunidades.inputCilindradaOportunidad(driver).sendKeys(cilindrada);
					mensaje = "Flujo : Ingreso Cilindrada : " + cilindrada;
					msj = "IngresoVin";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String marcaUsado = "";
				marcaUsado = Funciones.getExcel(driver, "MarcaUsado", posicionExcel);
			
				if (marcaUsado != "") {
					crearOportunidades.Oportunidades.inputMarcaOportunidad(driver).sendKeys(marcaUsado);
					mensaje = "Flujo : Ingreso Marca : " + marcaUsado;
					msj = "IngresoMarcaUsado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String kilometraje = "";
				kilometraje = Funciones.getExcel(driver, "Kilometraje", posicionExcel);

				if (kilometraje != "") {
					crearOportunidades.Oportunidades.inputKilometrajeOportunidad(driver).sendKeys(kilometraje);
					mensaje = "Flujo : Ingreso Kilometraje : " + kilometraje;
					msj = "IngresoKilometraje";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String local = "";
				local = Funciones.getExcel(driver, "LocalUsados", posicionExcel);

				System.err.println("Local " + local);
				if (local != "") {
					crearOportunidades.Oportunidades.inputLocalOportunidad(driver).sendKeys(local);
					mensaje = "Flujo : Ingreso Local : " + local;
					msj = "IngresoLocal";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String combustible = "";
				combustible = Funciones.getExcel(driver, "Combustible", posicionExcel);

				if (combustible != "") {
					crearOportunidades.Oportunidades.inputCombustibleOportunidad(driver).sendKeys(combustible);
					mensaje = "Flujo : Ingreso Combustible : " + combustible;
					msj = "IngresoCombustible";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String patente = "";
				patente = Funciones.getExcel(driver, "Patente", posicionExcel);
				
				if (patente != "") {
					crearOportunidades.Oportunidades.inputPatenteOportunidad(driver).sendKeys(patente);
					mensaje = "Flujo : Ingreso Patente : " + patente;
					msj = "IngresoPatente";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String reserva = "";
				reserva = Funciones.getExcel(driver, "Reserva", posicionExcel);
			
				if (reserva != "") {
					crearOportunidades.Oportunidades.inputReservaOportunidad(driver).sendKeys(reserva);
					mensaje = "Flujo : Ingreso Reserva : " + reserva;
					msj = "IngresoReserva";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String precio = "";
				precio = Funciones.getExcel(driver, "Precio", posicionExcel);
			
				if (precio != "") {
					crearOportunidades.Oportunidades.inputPrecioOportunidad(driver).sendKeys(precio);
					mensaje = "Flujo : Ingreso Precio : " + precio;
					msj = "IngresoPrecio";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String color = "";
				color = Funciones.getExcel(driver, "Color", posicionExcel);
			
				if (color != "") {
					crearOportunidades.Oportunidades.inputColorOportunidad(driver).sendKeys(color);
					mensaje = "Flujo : Ingreso Color : " + color;
					msj = "IngresoColor";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String Anio = "";
				Anio = Funciones.getExcel(driver, "Anio", posicionExcel);
			
				if (Anio != "") {
					crearOportunidades.Oportunidades.inputAnioOportunidad(driver).sendKeys(Anio);
					mensaje = "Flujo : Ingreso Año : " + Anio;
					msj = "IngresoAno";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String transmision = "";
				transmision = Funciones.getExcel(driver, "Transmision", posicionExcel);
			
				if (transmision != "") {
					crearOportunidades.Oportunidades.selectTransmisionOportunidad(driver).click();
					Thread.sleep(2000);
					crearOportunidades.Oportunidades.divisa(driver, transmision).click();
					mensaje = "Flujo : Ingreso de Transmision : " + transmision;
					msj = "IngresoTransmision";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String modelo = "";
				modelo = Funciones.getExcel(driver, "Modelo", posicionExcel);
			
				if (modelo != "") {
					crearOportunidades.Oportunidades.inputModeloOportunidad(driver).sendKeys(modelo);
					mensaje = "Flujo : Ingreso Modelo : " + modelo;
					msj = "IngresoModelo";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

				recorreExcell = 0;
				inicioExcell = 0;
				String version = "";
				version = Funciones.getExcel(driver, "Version", posicionExcel);
		
				if (version != "") {
					crearOportunidades.Oportunidades.inputVersionOportunidad(driver).sendKeys(version);
					mensaje = "Flujo : Ingreso Version : " + version;
					msj = "IngresoVersion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

			} else {

				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("arguments[0].scrollIntoView();",
				crearOportunidades.Oportunidades.inputBuscarProductos(driver));
				WebElement selectMarcaInteresPreProd = crearOportunidades.Oportunidades.inputBuscarProductos(driver);
				System.err.println("ACA marca");
				String[] arregloMarcaInteres = new String[modeloInteres.length()];
				for (int i = 0; i < modeloInteres.length(); i++) {
					arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
				}

				for (int i = 0; i < modeloInteres.length(); i++) {

					selectMarcaInteresPreProd.sendKeys(arregloMarcaInteres[i]);
					Thread.sleep(1000);

				}
				System.err.println("ACA 1");
				Thread.sleep(2000);

				WebElement elementoMarcaInteresPreProd = driver	.findElement(By.xpath("//div[@title='" + modeloInteres + "']"));
				System.err.println("ACA 2");
				elementoMarcaInteresPreProd.click();
				System.err.println("ACA 3");
				mensaje = "Flujo : Seleccion Producto :" + modeloInteres;
				msj = "IngresoProducto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(2000);

			}

			crearOportunidades.Oportunidades.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar Oportunidad";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			try {

				String txtMensajeErrorEnPagina = crearLead.Personal.txtMensajeErrorEnPagina(driver).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta página.")) {

					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta página :  "
					+ crearLead.Personal.txtContenidoMensajeErrorEnPagina(driver).getText() + "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(1500);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogError(mensaje);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());

				}

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

			}

			Thread.sleep(10000);

			WebDriverWait waitCompararOp = new WebDriverWait(driver, 20);
			waitCompararOp.until(
			ExpectedConditions.visibilityOf(crearOportunidades.Oportunidades.txtNombreOpPaginaInicio(driver)));
			String compararOp = crearOportunidades.Oportunidades.txtNombreOpPaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creacion correcta de nueva oportunidad";
			Funciones.mensajesLog(mensaje);

			if (compararOp.contains(nombreOp)) {
				mensaje = "Ok : Coincide con Nombre de Oportunidad Ingresado "
				+ crearOportunidades.Oportunidades.txtNombreOpPaginaInicio(driver).getText()
				+ " con Nombre de Oportunidad desplegado en Creacion de Oportunidad " + compararOp;
				msj = "OkCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLog(mensaje);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
				+ "Error : No Coincide con de Nombre de Oportunidad Ingresado con Nombre de Oportunidad en Creacin de Oportunidad "
				+ "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLog(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito Oportunidad " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
				"Hoja2");
				ExcelUtils.setCellData(nombreOportunidadCompleto, posicionExcel, 5);

				recorreExcell = 0;
				inicioExcell = 0;
				String oportunidad = "";
				while (recorreExcell == 0) {
					if ("Oportunidad".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						oportunidad = nombreOportunidadCompleto;
						ExcelUtils.setCellData(oportunidad, posicionExcel, inicioExcell);
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
						+ "Error : No encuentra elemento 'Oportunidad' en Archivo de Ingreso de datos Favor Revisar"
						+ "</p>";
						Funciones.mensajesLogError(mensaje);
						driver.quit();
						throw new AssertionError();
					}
				}

				String setRut = "";
				setRut = Funciones.getExcel(driver, "Rut Cuenta Creada", posicionExcel);

				String setTelefono = "";
				setTelefono = Funciones.getExcel(driver, "Telefono Cuenta Creada", posicionExcel);

				String ruta = "C:\\SalesForce\\listadoCreacionOportunidadesVolumen.txt";
				File archivo = new File(ruta);
				FileWriter fw = null;
				BufferedWriter bw = null;
				// flag true, indica adjuntar información al archivo.

				DateFormat dateFormatTxt = new SimpleDateFormat("dd-MM-YYYY");
				Date dateTxt = new Date();
				String dateTimeTxt = dateFormatTxt.format(dateTxt);
				String data = setRut + "  " + nombreOportunidadCompleto + "  " + setTelefono + "  " + dateTimeTxt
						+ "\r\n";
				if (archivo.exists()) {
					try {
						fw = new FileWriter(archivo.getAbsoluteFile(), true);
						bw = new BufferedWriter(fw);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						bw.write(data);
						bw.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.err.println("No Existe el Archivo de Texto 'listadoCreacionOportunidadesVolumen.txt'");
				}

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea Oportunidad de Manera Correcta"
				+ "<p>";
				msj = "OkPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErrorSeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
