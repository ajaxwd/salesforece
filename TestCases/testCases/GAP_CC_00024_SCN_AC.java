package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class GAP_CC_00024_SCN_AC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		for (int i = 0; i < 25; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {

				posicionExcel = i;
				break;

			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		//String busquedaLead = ExcelUtils.getCellData(posicionExcel, 1);
		//String busquedaEmpresa = ExcelUtils.getCellData(posicionExcel, 2);
		String rut = ExcelUtils.getCellData(posicionExcel, 2);
		String comuna = ExcelUtils.getCellData(posicionExcel, 3);
		String telefono = ExcelUtils.getCellData(posicionExcel, 4);
		String buscarCuenta = "";
		String compararCuenta = "";
		String compararInicial = "";
		String comparaFinal = "";

//		if (busquedaEmpresa == "") {
//			buscarCuenta = busquedaLead;
//			compararCuenta = "persona";
//			compararInicial = rut;
//		} else {
//			buscarCuenta = busquedaEmpresa;
//			compararCuenta = "empresa";
//			compararInicial = telefono;
//		}

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";

			while (recorreExcell == 0) {
				if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
					Funciones.mensajesLog(mensaje);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
					+ "Error : No encuentra elemento 'Pais' en Archivo de Ingreso de datos Favor Revisar"
					+ "</p>";
					Funciones.mensajesLog(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)));
			paginaPrincipal.MenuPrincipal.btnCuentas(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo

					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			Thread.sleep(5000);

			int posicion = 1;
			int sw = 0;
			String compara = "";
			while (sw == 0) {
				Thread.sleep(1000);
				try {
					WebElement registro = new WebDriverWait(driver, 10, 50).until(
					ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[" + posicion + "]/th/span/a")));
					compara = registro.getText();

					if (buscarCuenta.equals(compara)) {

						mensaje = "Flujo : Se Selecciona Registro ";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						registro.click();
						sw = 1;
					} else {
						posicion = posicion + 1;
						sw = 0;
					}

				} catch (NoSuchElementException e) {
					posicion = posicion + 1;
					sw = 0;
				}
			}

			Thread.sleep(8000);

			if (compararCuenta == "empresa") {
				comparaFinal = principalCuenta.MenuPrincipalCuenta.txtTelefonoEmpresa(driver).getText();
			} else {
				comparaFinal = principalCuenta.MenuPrincipalCuenta.txtRutPersona(driver).getText();
			}

			Thread.sleep(3000);

			mensaje = "Flujo : Se revisa si los datos son correcto";
			msj = "RevisionRut";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(3000);

			System.err.println("compararInicial" + compararInicial);
			System.err.println("comparaFinal" + comparaFinal);

			if (compararInicial.equals(comparaFinal)) {

				new WebDriverWait(driver, 20, 100)
				.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnModificarCuenta(driver)));
				crearCuenta.Personal.btnModificarCuenta(driver).click();
				mensaje = "Flujo : Click Modificar Cuentas ";
				msj = "ClickBtnModificarCuentas";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(5000);

				switch (paisEjecucion) {

				case "Chile":

					if (compararCuenta == "persona") {

						crearCuenta.Personal.inputNacionalidadModificarCuenta(driver).click();
						crearCuenta.Personal.inputNacionalidadModificarCuenta(driver).sendKeys("CHI");
						Thread.sleep(1000);
						crearCuenta.Personal.inputNacionalidadModificarCuenta(driver).sendKeys("LE");
						mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
						Funciones.mensajesLog(mensaje);

						Thread.sleep(2000);

						String nacionalidadModificada = "Chile";
						crearCuenta.Personal.selectElementoNacionalidadModificarCuenta(driver, nacionalidadModificada)
						.click();
						mensaje = "Flujo : Seleccion de Pais  : " + "CHILE";
						msj = "SeleccionPais";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						Select selectGenero = new Select(
						crearCuenta.Personal.selectNacionalidadModificarCuenta(driver));
						selectGenero.selectByIndex(1);

						mensaje = "Flujo : Ingreso de Genero : " + "Masculino";
						msj = "IngresoGeneroModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						crearCuenta.Personal.inputFechaNacimientoModificar(driver).clear();

						crearCuenta.Personal.inputFechaNacimientoModificar(driver).sendKeys("18/08/1978");
						mensaje = "Flujo : Ingreso de Fecha Nacimiento : " + "18/08/1978";
						msj = "IngresoFachaNacimientoModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						crearCuenta.Personal.selectEstadoCivilModificar(driver).click();
						Thread.sleep(2000);
						Select selectEstadoCivil = new Select(crearCuenta.Personal.selectEstadoCivilModificar(driver));
						selectEstadoCivil.selectByVisibleText("Soltero");
						mensaje = "Flujo : Seleccion de Estado Civil : " + "Soltero";
						msj = "IngresoEstadoCivilModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(3000);

						JavascriptExecutor jse = (JavascriptExecutor) driver;
						jse.executeScript("arguments[0].scrollIntoView();",
						crearCuenta.Personal.inputPaisModificar(driver));

						crearCuenta.Personal.inputPaisModificar(driver).click();
						crearCuenta.Personal.inputPaisModificar(driver).sendKeys("CHI");
						Thread.sleep(1000);
						crearCuenta.Personal.inputPaisModificar(driver).sendKeys("LE");
						mensaje = "Flujo : Ingreso Campo Pais Modificar : " + "CHILE";
						Funciones.mensajesLog(mensaje);

						Thread.sleep(3000);

						crearCuenta.Personal.inputPaisModificar(driver).sendKeys(" ");
						crearCuenta.Personal.paisElementoModificar(driver).click();
						mensaje = "Flujo : Seleccion Campo Pais Modificar : " + "CHILE";
						msj = "SeleccionPaisModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(3000);

						crearCuenta.Personal.inputRegionModificar(driver).click();
						crearCuenta.Personal.inputRegionModificar(driver).sendKeys("R");
						Thread.sleep(1000);
						crearCuenta.Personal.inputRegionModificar(driver).sendKeys("M");
						mensaje = "Flujo : Ingreso Campo Region Modificar : " + "SANTIAGO";
						Funciones.mensajesLog(mensaje);

						Thread.sleep(3000);

						crearCuenta.Personal.inputRegionModificar(driver).sendKeys("");
						crearCuenta.Personal.regionElementoModificar(driver).click();
						mensaje = "Flujo : Seleccion Campo Region Modificar : " + "SANTIAGO";
						msj = "SeleccionRegionModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(3000);

						crearCuenta.Personal.inputCiudadModificar(driver).click();
						crearCuenta.Personal.inputCiudadModificar(driver).sendKeys("SANT");
						Thread.sleep(1000);
						crearCuenta.Personal.inputCiudadModificar(driver).sendKeys("IAGO");
						mensaje = "Flujo : Ingreso Campo Ciudad Modificar : " + "SANTIAGO";
						Funciones.mensajesLog(mensaje);

						Thread.sleep(3000);

						crearCuenta.Personal.inputCiudadModificar(driver).sendKeys(" ");
						crearCuenta.Personal.comunaElementoModificar(driver).click();
						mensaje = "Flujo : Seleccion Campo Ciudad Modificar : " + "SANTIAGO";
						msj = "SeleccionCiudadModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(3000);

						crearCuenta.Personal.inputComunaModificar(driver).click();
						Thread.sleep(1000);
						crearCuenta.Personal.inputComunaModificar(driver).sendKeys(comuna);
						mensaje = "Flujo : Ingreso Campo Comuna Modificar : " + comuna;
						Funciones.mensajesLog(mensaje);

						Thread.sleep(3000);

						crearCuenta.Personal.inputComunaModificar(driver).sendKeys(" ");
						crearCuenta.Personal.comunaElementoModificar(driver).click();
						mensaje = "Flujo : Seleccion Campo Comuna Modificar : " + "SANTIAGO";
						msj = "SeleccionComunaModificar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						sw = 0;
						int cont = 1;
						while (sw == 0) {

							try {

								WebElement numeroDireccionModificar = driver
								.findElement(By.xpath("//div[8]/lightning-input[2]/div/input"));
								numeroDireccionModificar.click();
								numeroDireccionModificar.clear();
								numeroDireccionModificar.sendKeys("1234");
								sw = 1;

							} catch (NoSuchElementException | ScreenshotException e) {
								System.err.println(cont);
								cont = cont + 1;
								sw = 0;
							}

						}

						Thread.sleep(3000);

						mensaje = "Flujo: Ingreso Campo Calle Aleatorio: ";
						msj = "IngresoCalle";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(1500);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					} else {

					}

					break;

				case "Per�":

					if (compararCuenta == "persona") {

//						Select selectGenero = new Select(
//								crearCuenta.Personal.selectNacionalidadModificarCuenta(driver));
//						selectGenero.selectByIndex(1);
//
//						mensaje = "Flujo : Ingreso de Genero : " + "Masculino";
//						msj = "IngresoGeneroModificar";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);
//
//						Thread.sleep(2000);
//
//						crearCuenta.Personal.inputFechaNacimientoModificar(driver).clear();
//
//						crearCuenta.Personal.inputFechaNacimientoModificar(driver).sendKeys("18/08/1978");
//						mensaje = "Flujo : Ingreso de Fecha Nacimiento : " + "18/08/1978";
//						msj = "IngresoFachaNacimientoModificar";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);
//
//						Thread.sleep(2000);
//
//						crearCuenta.Personal.selectEstadoCivilModificar(driver).click();
//						Thread.sleep(2000);
//						Select selectEstadoCivil = new Select(crearCuenta.Personal.selectEstadoCivilModificar(driver));
//						selectEstadoCivil.selectByVisibleText("Soltero");
//						mensaje = "Flujo : Seleccion de Estado Civil : " + "Soltero";
//						msj = "IngresoEstadoCivilModificar";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);
//
//						Thread.sleep(3000);
//
//						JavascriptExecutor jse = (JavascriptExecutor) driver;
//						jse.executeScript("arguments[0].scrollIntoView();",
//								crearCuenta.Personal.inputFechaNacimientoModificar(driver));
//
//						String[] arregloPaisEjecucion = new String[paisEjecucion.length()];
//						for (int i = 0; i < paisEjecucion.length(); i++) {
//							arregloPaisEjecucion[i] = Character.toString(paisEjecucion.charAt(i));
//						}
//						crearCuenta.Personal.inputNacionalidadModificarCuenta(driver).click();
//						for (int i = 0; i < paisEjecucion.length(); i++) {
//
//							crearCuenta.Personal.inputNacionalidadModificarCuenta(driver)
//									.sendKeys(arregloPaisEjecucion[i]);
//							Thread.sleep(500);
//
//						}
//						mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
//						Log.info(mensaje);
//						Reporter.log("<p>" + "<br>" + mensaje + "</p>");
//						System.out.println(mensaje);
//
//						Thread.sleep(2000);
//
//						crearCuenta.Personal.selectElementoNacionalidadModificarCuenta(driver, paisEjecucion).click();
//						mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
//						msj = "SeleccionPais";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						Thread.sleep(1500);
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);

					} else {

					}

					break;

				default:
					break;
				}

				Thread.sleep(3000);
				WebElement grabarModificar = driver.findElement(By.xpath("//button[contains(text(),'Guardar')]"));
				grabarModificar.click();
				Thread.sleep(3000);

				mensaje = "Ok : Coincide numero telefono/rut " + comparaFinal + "  con telefono/rut" + comparaFinal
				+ " desplegado en Cracion de Lead ";
				msj = "OkCoincideRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
				+ "Error : No Coincide telefono/rut de Cuenta con telefono/rut desplegado en Cracion de Lead "
				+ "</p>";
				msj = "ErrorNoCoincideRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se verifica creacion de  Cuenta" + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida ; No se crea Cuenta de Manera Correcta"
				+ "<p>";
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				driver.close();
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			//driver.close();
			msj = "ErrorNoEncuentraElemento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErrorSeEncontraronErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
//Fin del for
