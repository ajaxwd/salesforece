package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import utilidades.Funciones;

public class PrimeraSoloPruebaTestNG {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";

	@Test
	public void main() throws Exception {

		// Funciones Func_exec = new Funciones();
		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 0;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		String nombreS = "Mr Robot";

		// Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
		// Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
		Thread.sleep(4000);
		System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
		driver = new ChromeDriver();
		mensaje = "Navegador : Chrome" + "<br>" + "URL " + "http://demoregistro.sqamexico.com/";
		System.out.println(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		Log.info(mensaje);
		Thread.sleep(2000);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://demoregistro.sqamexico.com/");
		driver.manage().window().maximize(); // maximizo mi browser

		// #3 Manipulo mis objetos
		// A) Forma de manipular un objeto, usando WebElement
		WebElement nombre = driver.findElement(By.id("txt_name"));
		nombre.sendKeys(nombreS);
		mensaje = "Flujo : Se Ingresa Nombre " + nombreS;
		msj = "IngresaNombre";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);

		// B) Forma de manipular un objeto, usando Webdriver directo
		driver.findElement(By.name("txt_user")).sendKeys("root");
		mensaje = "Flujo : Se Ingresa Contraseña ";
		msj = "IngresaContrasena";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// C) Manipular un Combobox
		Select combo = new Select(driver.findElement(By.id("cmb_pais")));
		combo.selectByValue("Mexico");
		mensaje = "Flujo : Se Ingresa Pais " + "Mexico";
		msj = "SelectPais";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		combo = new Select(driver.findElement(By.id("cmb_ciudad")));
		combo.selectByValue("Jalisco");
		mensaje = "Flujo : Se Ingresa Ciudad " + "Jalisco";
		msj = "SelectCuidad";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// Manipulando un checkbox
		WebElement checkbox_correr = driver.findElement(By.id("chk_hobbie-3"));
		if (!checkbox_correr.isSelected()) // valido que NO este seleccionado para seleccionarlo
			checkbox_correr.click();
		mensaje = "Flujo : Se Clickea check Correr ";
		msj = "ChkCorrer";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);

		// Doy click en el boton submit
		WebElement boton = driver.findElement(By.id("btnsubmit"));
		boton.click();
		Thread.sleep(2000);
		mensaje = "Flujo : Click Enviar";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);
		Thread.sleep(2000);

		// #4 Agrego un punto de verificación, para validar que el registro fue dado de
		// alta
		WebElement confirmacion = driver.findElement(By.id("registrationok"));

		if (!confirmacion.getText().contains("Perfecto! el registro fue dado de alta correctamente")) {
			mensaje = "Hubo un error";
			msj = "Error";
			// log.severe("Hubo un error en el registro");
			Log.info(mensaje);
			System.err.println(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			errorBuffer.append("\n" + mensaje + "\n");
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
Thread.sleep(2000);
rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			return;
		} else {
			msj = "OK";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
Thread.sleep(2000);
rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			ValorEncontrado = true;
			mensaje = "OK: Registro completado! ";
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
		}

		try {
			Assert.assertTrue(ValorEncontrado);
			mensaje = "OK: Prueba Correcta";
			Reporter.log("<p style='color:blue;'>" + mensaje + "</p>");
			Log.info(mensaje);
			System.out.println(mensaje);

		} catch (AssertionError e) {
			mensaje = "ERROR: Prueba Fallida";
			Log.info(mensaje);
			System.err.println(mensaje);
			Reporter.log("<p style='color:red;'>" + mensaje + "</p>");
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

		}

		if (errorBuffer.length() > 0) {
			mensaje = "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias";
			Log.info(mensaje);
			System.err.println(mensaje);
			Reporter.log("<p style='color:red;'>" + mensaje + "</p>");
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		// Duermo el proceso para ver las modificaciones
		Thread.sleep(10000);

		// #5 Cierro el browser
		driver.close();

	}
}
