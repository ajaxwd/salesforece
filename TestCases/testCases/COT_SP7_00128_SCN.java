package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00128_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = ExcelUtils.getCellData(posicionExcel, 12);
		String busquedaOportunidad = ExcelUtils.getCellData(posicionExcel, 5);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int esperarBtnPagarContado = 0;
		int contadorEspera = 0;

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificación')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver))).click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[2]/span/a")));
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro" + nombreCotizacion;
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(10000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			driver.findElement(By.xpath("//*[contains(text(),'Pago contado')]")));
			//Total Descuento Retail
			while (esperarBtnPagarContado == 0) {

				try {
					principalCotizaciones.PrincipalCotizaciones.btnPagoContado(driver).sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se presiona Boton 'Pago contado' ";
					msj = "ClickBtnSPagoContado";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					esperarBtnPagarContado = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					esperarBtnPagarContado = 0;
					contadorEspera = contadorEspera + 1;
					if (contadorEspera == 25) {
						mensaje = "<p style='color:red;'>"+ "ERROR: Prueba Fallida ; No Existe Boton Pago Contado o deshabilitado" + "<p>";
						msj = "ErrorNoExistenDescuentos";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				}
			}

			Thread.sleep(1000);

			int esperarMensajeSimulacion = 0;
			while (esperarMensajeSimulacion == 0) {
				if (driver.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']")).size() > 0) {
					esperarMensajeSimulacion = 0;
				} else {
					esperarMensajeSimulacion = 1;
					System.err.println("Carga Scn");
				}
			}

			Thread.sleep(1000);

			String textoMensajeExitoso = "";
			int largo = 0;
			int esperarMensaje = 0;
			int contadorQuiebre = 0;
			String textoRevisar = "Vacio";
			String textoRevisarDetalle = "Vacio";
			String compararTexto = "";
			while (esperarMensaje == 0) {

				if (contadorQuiebre == 15) {

					ValorEncontrado = false;
					mensaje = "<p style='color:red;'>" + "Error: NO aparece mensaje de 'Estado de Envio de Evaluacion'" + "<p>";
					Funciones.mensajesLog(mensaje);
					driver.close();
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}

				largo = driver.findElements(By.xpath(
				"//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']|//span[@class='toastMessage forceActionsText']"))
				.size();
				
				System.err.println("Largo " + largo);

				if (largo == 0) {

					esperarMensaje = 0;
					contadorQuiebre = contadorQuiebre + 1;
					System.err.println("contadorQuiebre " + contadorQuiebre);

				} else {
					try {
						textoMensajeExitoso = principalCotizaciones.PrincipalCotizaciones.txtMensajeEnvioEvaluacion(driver).getText();
						System.err.println("textoMensajeExitoso " + textoMensajeExitoso);
					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
						textoMensajeExitoso = "Vacio";
						break;
					}
				}

				try {
					textoRevisarDetalle = principalCotizaciones.PrincipalCotizaciones
					.txtMensajeEnvioEvaluacionDetalle(driver).getText();
					System.err.println("textoRevisarDetalle " + textoRevisarDetalle);
					String[] bpArray = textoRevisarDetalle.split("BP: ");
					System.err.println("0" + bpArray[0]);
					System.err.println("1" + bpArray[1]);
					textoRevisar = bpArray[1];
					compararTexto = textoRevisar;

				} catch (Exception e) { System.err.println("textoRevisarDetalle error"); }

				if (textoMensajeExitoso.equals("Vacio")) {

				} else {

					System.err.println("textoRevisar *"+textoRevisar+"*");
					
					if (textoMensajeExitoso.contains("Pago contado exitoso.")) {

						ValorEncontrado = true;
						mensaje = "OK : Existe mensaje '" + textoRevisarDetalle + "'";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						
						int recorreExcell = 0;
						int inicioExcell = 0;
						int salir = 0;
						boolean continuar = false;

							while (continuar == false) {

								System.err.println("compararTexto Loop*" + compararTexto + "*");
								
								if (compararTexto.contains("[a-zA-Z]+") == false) {

									if (salir == 15) {

										ValorEncontrado = false;
										mensaje = "<p style='color:red;'>" + "Error: NO aparece mensaje de 'Estado de Envio de Evaluacion'" + "<p>";
										Funciones.mensajesLog(mensaje);
										driver.close();
										errorBuffer.append("\n" + mensaje + "\n");
										throw new AssertionError(errorBuffer.toString());
									}

									compararTexto = compararTexto.replaceAll(" ", "");
									compararTexto = compararTexto.replaceAll("yPagocontadoexitoso.", "");
									compararTexto = compararTexto.replaceAll(" y Pago contado exitoso.", "");
									compararTexto = compararTexto.replaceAll("cerrar", "");
									compararTexto = compararTexto.replace("yPagocontadoexitoso.", "");
									compararTexto = compararTexto.replace(" y Pago contado exitoso.", "");
									compararTexto = compararTexto.replace("cerrar", "");
									compararTexto = compararTexto.replace(" ", "");
									continuar = true;
									System.err.println("compararTexto Correcto*" + compararTexto + "*");

								} else {
									continuar = false;
									salir = salir + 1;
								}
								
							}

						mensaje = "OK : 'Pago contado' Exitoso BP " + compararTexto;
						msj = "OkPagoExitoso";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						esperarBtnPagarContado = 1;
						ValorEncontrado = true;
						sw = 1;

						Thread.sleep(3000);

						utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
						utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
						ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

						recorreExcell = 0;
						inicioExcell = 0;
						System.err.println("BP " + compararTexto);
						while (recorreExcell == 0) {
							if ("BP".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								ExcelUtils.setCellData(compararTexto, posicionExcel, inicioExcell);
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							if (inicioExcell > 150) {
								mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento 'BP' en Archivo de Ingreso de datos Favor Revisar"
								+ "</p>";
								Funciones.mensajesLog(mensaje);
								driver.quit();
								throw new AssertionError();
							}
						}

						ValorEncontrado = true;
						break;
				}else {

					if ((textoRevisar.contains("error")) | (textoRevisar.contains("Error"))) {

						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; " 
						+ textoRevisarDetalle + "<p>";
						msj = "ErrorNoExisteBoton";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());

					} else {

						ValorEncontrado = false;
						mensaje = "<p style='color:red;'>" + "Error: " + textoRevisarDetalle + "<p>";
						Funciones.mensajesLog(mensaje);
						driver.close();
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}		
				}		
			}

			}

			Thread.sleep(3000);

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + 
				"OK: Prueba Correcta ; Existen Datos en 'Resumen Simulacion' " + "<p>";
				msj = "OkIngresaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Campos Vacios " + "<p>";
				msj = "ErrorNoCreaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			Funciones.mensajesLog(mensaje);
			driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
