package testCases;

//import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import com.thoughtworks.selenium.webdriven.commands.Click;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
//import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import principalOportunidades.PrincipalOportunidades;
//import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00016_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		int posicionExcel = 1;

		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = "";
		String busquedaOportunidad = "";
		// String listaPrecio = ExcelUtils.getCellData(1, 7);
		// String productoABuscar = ExcelUtils.getCellData(1, 8);
		// String nombreProductoABuscar = ExcelUtils.getCellData(1, 9);
		// String cantidadProducto = ExcelUtils.getCellData(1, 10);
		String modeloInteres = "";
		String marcaInteres = "";
		String codigoModeloInteres = "";
		String accesorioInteres = "";
		String Plan = "";

		nombreCotizacion = Funciones.getExcel(driver, "Nombre Cotizacion Creada", posicionExcel);
		busquedaOportunidad = Funciones.getExcel(driver, "Nombre Oportunidad", posicionExcel);
		modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
		marcaInteres = Funciones.getExcel(driver, "Marca", posicionExcel);
		codigoModeloInteres = Funciones.getExcel(driver, "Codigo Modelo", posicionExcel);
		accesorioInteres = Funciones.getExcel(driver, "Accesorio", posicionExcel);
		Plan = Funciones.getExcel(driver, "Plan", posicionExcel);

		// String currentURL = driver.getCurrentUrl();

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		String productoSeleccionado = "";
		String textoNombreVerificar = "";

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id='brandBand_1']/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;
					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver))).click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {
					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[2]/span/a")));
					System.out.println("1 " + registroCotizacion.getText());
					System.out.println("2 " + nombreCotizacion);
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;
					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(5000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", 
			driver.findElement(By.xpath(
			"(//li[contains(@class,'rlql-relatedListQuickLink')]//div[contains(@class,'slds-media__body slds-m-left--x-small')])[1]")));
			Thread.sleep(5000);

			sw = 0;
			int contadorEspera = 0;
			while (sw == 0) {

				try {
					principalCotizaciones.PrincipalCotizaciones.btnAgregarProductos(driver).sendKeys(Keys.ENTER);
					sw = 1;
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contadorEspera = contadorEspera + 1;
					if (contadorEspera == 30) {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Elemento Agregar Producto "+ "<p>";
						msj = "ErrorNoExistenPagos";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				}
			}

			mensaje = "Flujo : Click en Boton 'Agregar Productos' en Cotizacion";
			msj = "ClickBtnAddProductos";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);
			String usado = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
			principalOportunidades.PrincipalOportunidades.inputBusquedaMarcaPreprod(driver))).getAttribute("value");
			Thread.sleep(1000);

			String textoAccesorioIngresado = "";
			String textoModeloIngresado = "";

			if (usado.equals("B2B")) {
				System.err.println("B2B");
				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
				principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver))).click();
				principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver).clear();
				Thread.sleep(500);

				String[] arregloCodigoModeloInteres = new String[codigoModeloInteres.length()];
				for (int i = 0; i < codigoModeloInteres.length(); i++) {
					arregloCodigoModeloInteres[i] = Character.toString(codigoModeloInteres.charAt(i));
				}

				for (int i = 0; i < codigoModeloInteres.length(); i++) {
					principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver)
					.sendKeys(arregloCodigoModeloInteres[i]);
					Thread.sleep(500);
				}

				mensaje = "Flujo : Se Ingresa 'Busqueda Modelo' ";
				Funciones.mensajesLog(mensaje);

				Thread.sleep(5000);

				WebElement modeloIngresoPreprod = driver.findElement(
				By.xpath("//span[@class='slds-listbox__option-text slds-listbox__option-text_entity']"));
				textoModeloIngresado = modeloIngresoPreprod.getText();
				modeloIngresoPreprod.click();
				mensaje = "Flujo : Se Selecciona Modelo " + modeloInteres;
				msj = "SeleccionModelo";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(2000);

				String precio = Funciones.getExcel(driver, "Precio", posicionExcel);

				if (precio != "") {
					principalOportunidades.PrincipalOportunidades.inputPrecioOportunidad(driver).sendKeys(precio);
					mensaje = "Flujo : Ingreso Precio : " + precio;
					msj = "IngresoPrecio";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);

			} else {

				System.err.println(" Input Marca " + principalOportunidades.PrincipalOportunidades
				.inputBusquedaMarcaPreprod(driver).getAttribute("disabled"));
				System.err.println(" Valor Marca " + usado);
				String tipoUser = principalOportunidades.PrincipalOportunidades
				.inputBusquedaMarcaPreprod(driver).getAttribute("disabled");

				try {

					if (tipoUser.equals("true")) {

						mensaje = "Flujo : Verificar Usuario 'MonoMarca' ";
						Funciones.mensajesLog(mensaje);

						if (usado.equals(marcaInteres)) {
							mensaje = "Flujo : Marca Asignada a Usuario es Correcta";
							msj = "ClickBtnAddProductos";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						} else {

							mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida  Marca Asignada a Usuario No Corresponde" + "<p>";
							msj = "PruebaErronea";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
							driver.quit();
							throw new AssertionError(errorBuffer.toString());
						}
					}
					
					if (tipoUser.equals("null")) {

						mensaje = "Flujo : Verificar Usuario 'MultiMarca' ";
						Funciones.mensajesLog(mensaje);

						if (usado.equals("")) {

							mensaje = "Flujo : Marca Se puede Ingresar Usuario Multimarca Correcto";
							msj = "ClickBtnAddProductos";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						} else {

							mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida  Marca ya Asignada a Usuario No Corresponde a Multimarca" + "<p>";
							msj = "PruebaErronea";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
							driver.quit();
							throw new AssertionError(errorBuffer.toString());
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

				try {
										
					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
					principalOportunidades.PrincipalOportunidades.inputBusquedaMarcaPreprod(driver))).click();

					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
					principalOportunidades.PrincipalOportunidades.inputBusquedaMarcaPreprod(driver))).click();
					principalOportunidades.PrincipalOportunidades.inputBusquedaMarcaPreprod(driver).clear();
					Thread.sleep(500);

					String[] arregloMarcaInteres = new String[marcaInteres.length()];
					for (int i = 0; i < marcaInteres.length(); i++) {
						arregloMarcaInteres[i] = Character.toString(marcaInteres.charAt(i));
					}
					for (int i = 0; i < marcaInteres.length(); i++) {
						principalOportunidades.PrincipalOportunidades.inputBusquedaMarcaPreprod(driver).sendKeys(arregloMarcaInteres[i]);
						Thread.sleep(500);
					}

					Thread.sleep(500);
					mensaje = "Flujo : Se Ingresa 'Marca Producto' ";
					Funciones.mensajesLog(mensaje);

					Thread.sleep(3000);
					WebElement marca = driver.findElement(By.xpath(
					"//*[@id='listbox-option-unique-id-01']//span//span[contains(text(),'" + marcaInteres + "')]"));
					marca.click();
				
					mensaje = "Flujo : Se Selecciona Marca ";
					msj = "SeleccionProducto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					String marcaIngresoPreprod = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					// "//*[@id=\"brandBand_1\"]/div/div[1]/div[5]/div[1]/div/div[3]/div[2]/div[1]/div/div[3]/div[1]/div[1]/div/div[2]/div[1]/div[1]/div/div/div/div[1]/span/span/span")))
					"//div[@class='slds-pill-container slds-show']//span[@class='slds-pill__label']"))).getText();
					marcaIngresoPreprod.getClass();

				} catch (Exception e) {
					// TODO: handle exception
				}

				Thread.sleep(5000);
				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
				principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver))).click();
				principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver).clear();
				Thread.sleep(500);
			
				String[] arregloCodigoModeloInteres = new String[codigoModeloInteres.length()];
				for (int i = 0; i < codigoModeloInteres.length(); i++) {
					arregloCodigoModeloInteres[i] = Character.toString(codigoModeloInteres.charAt(i));
				}
				for (int i = 0; i < codigoModeloInteres.length(); i++) {
					principalOportunidades.PrincipalOportunidades.inputBusquedaModeloPreprod(driver)
					.sendKeys(arregloCodigoModeloInteres[i]);
					Thread.sleep(500);
				}

				mensaje = "Flujo : Se Ingresa 'Busqueda Modelo' ";
				Funciones.mensajesLog(mensaje);

				Thread.sleep(5000);

				WebElement modeloIngresoPreprod = driver.findElement(By.xpath("//span[contains(text(),'" + modeloInteres + "')]"));
				textoModeloIngresado = modeloIngresoPreprod.getText();
				modeloIngresoPreprod.click();
				mensaje = "Flujo : Se Selecciona Modelo " + modeloInteres;
				msj = "SeleccionModelo";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				if (accesorioInteres != "") {

					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
					principalOportunidades.PrincipalOportunidades.inputBusquedaAccesorioPreprod(driver))).click();
					principalOportunidades.PrincipalOportunidades.inputBusquedaAccesorioPreprod(driver).clear();
					Thread.sleep(500);

					String[] arregloAccesorioInteres = new String[accesorioInteres.length()];
					for (int i = 0; i < accesorioInteres.length(); i++) {
						arregloAccesorioInteres[i] = Character.toString(accesorioInteres.charAt(i));
					}
					for (int i = 0; i < accesorioInteres.length(); i++) {
						principalOportunidades.PrincipalOportunidades.inputBusquedaAccesorioPreprod(driver)
						.sendKeys(arregloAccesorioInteres[i]);
						Thread.sleep(500);
					}

					Thread.sleep(500);
					mensaje = "Flujo : Se Ingresa 'Busqueda Accesorio' ";
					Funciones.mensajesLog(mensaje);

					Thread.sleep(3000);

					WebElement accesorioPreprod = driver.findElement(By.xpath("//span[contains(text(),'" + accesorioInteres + "')]"));
					textoAccesorioIngresado = accesorioPreprod.getText();
					accesorioPreprod.click();
					mensaje = "Flujo : Se Selecciona Accesorio ";
					msj = "SeleccionProducto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Funciones.funcionPausa(2000);

					//Seleccionar pestaña Cotización Zoura
					principalOportunidades.PrincipalOportunidades.pestanaZoura(driver).click();
					mensaje = "Flujo: Se seleccionar pestaña de Cotización Zouora";
					msj= "pestanaZuora";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Funciones.funcionPausa(2000);

					//Seleccionar input Subcripción
					principalOportunidades.PrincipalOportunidades.inputSeleccionSubcripcion(driver).click();
					Funciones.funcionPausa(2000);
					String registro = PrincipalOportunidades.selectSubcripcion(driver).getText();
					PrincipalOportunidades.selectSubcripcion(driver).click();
					mensaje = "Flujo: Se seleccionar input Subcripción: " + registro;
					msj= "selectSubcripcion";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Funciones.funcionPausa(2000);

					//Seleccionar producto de cotizción zuora
					PrincipalOportunidades.inputProductoSubcripcion(driver).click();
					String registro2 = PrincipalOportunidades.optionProducto(driver).getText();
					PrincipalOportunidades.optionProducto(driver).click();
					mensaje = "Flujo: Se seleccionar input producto: " + registro2;
					msj= "optionProducto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					Funciones.funcionPausa(2000);

					//Seleccionar Plan
					PrincipalOportunidades.inputPlan(driver).click();
					String registro3 = PrincipalOportunidades.selectPlan(driver, Plan).getText();
					PrincipalOportunidades.selectPlan(driver, Plan).click();
					mensaje = "Flujo: Se seleccionar input Plan: " + registro3;
					msj= "selectPlan";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				} else {
					mensaje = "Flujo: No se agrego ningun Accesorio ";
					Funciones.mensajesLog(mensaje);
				}
			}

			Thread.sleep(6000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.btnGuardarProducto(driver))).click();
			mensaje = "Flujo : Click Boton 'Guardar' en Agregar Productos ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(6000);

			int esperarMensajeSimulacion = 0;
			while (esperarMensajeSimulacion == 0) {
				if (driver.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']")).size() > 0) {
					esperarMensajeSimulacion = 0;
					try {
						driver.findElement(By.xpath("//*[@class='msg ltngDeveloperError'][@data-aura-class='ltngDeveloperError']")).getText();
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida " + "<p>";
						msj = "PruebaErronea";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
						driver.quit();
						throw new AssertionError(errorBuffer.toString());
					} catch (Exception e) { }
				} else { esperarMensajeSimulacion = 1; }
			}

			try {

				String textoCuadroRojoError = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
				principalOportunidades.PrincipalOportunidades.txtError(driver))).getText();
				if (textoCuadroRojoError.equals("Error")) {

					String detalleTtextoCuadroRojoError = new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.visibilityOf(principalOportunidades.PrincipalOportunidades.detalleTxtError(driver))).getText();
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; " + detalleTtextoCuadroRojoError + "<p>";
					msj = "PruebaErronea";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(5000);

			for (int i = 1; i < 5; i++) {

				productoSeleccionado = driver.findElement(By.xpath(
				"(//span[contains(text(),'Productos de la')]//following::tbody/tr/th[" + i + "]/div/a)[1]")).getText();
				//"(//span[@title='Productos de la cotización']//following::tbody/tr/th[" + i + "]/div/a)[1]"
				if (productoSeleccionado.equals(textoModeloIngresado) || productoSeleccionado.equals(textoAccesorioIngresado)) {
					mensaje = "Ok : Coincide Producto Seleccionado : " + productoSeleccionado
					+ " con Producto desplegado en Productos de la Cotizacion : " + textoNombreVerificar;
					msj = "ValidaProducto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = true;
					break;
				} else {
					mensaje = "<p style='color:red;'>" + "Error : No Coincide Producto Seleccionado con Producto desplegado en Productos de la Cotizacion " + "</p>";
					msj = "ValidaProducto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
				}
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Ingreso de Productos a Cotizacion  "+ "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Ingreso de Productos Fallido" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
