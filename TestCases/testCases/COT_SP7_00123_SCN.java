package testCases;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00123_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String busquedaLead = ExcelUtils.getCellData(posicionExcel, 1);
		String busquedaEmpresa = ExcelUtils.getCellData(posicionExcel, 2);
		String rut = ExcelUtils.getCellData(posicionExcel, 3);
		String telefono = ExcelUtils.getCellData(posicionExcel, 4);
		String buscarCuenta = "";
		String compararCuenta = "";
		String compararInicial = "";
		String comparaFinal = "";
		String numeroBP = Funciones.getExcel(driver, "BP", posicionExcel);
		String tipoDeVenta = ExcelUtils.getCellData(posicionExcel, 30);
		String detalleDeCuenta = ExcelUtils.getCellData(posicionExcel, 31);
		String rutBuscarRegistro = rut;
		String rutBuscarRegistroAlternativo = "";
		String nombreCuentaPersona = "";
		String nombreCuentaEmpresa = "";

		rutBuscarRegistro = Funciones.getExcel(driver, "Rut Venta Destinatario", posicionExcel);
		rutBuscarRegistroAlternativo = Funciones.getExcel(driver, "Rut Cuenta Aletrnativa", posicionExcel);
		nombreCuentaPersona = Funciones.getExcel(driver, "Nombre Persona Cuenta Creada", posicionExcel);
		nombreCuentaEmpresa = Funciones.getExcel(driver, "Nombre Empresa Cuenta Creada", posicionExcel);

		switch (tipoDeVenta) {

		case ("Leasing"):
			if (nombreCuentaPersona != "Cuenta Empresa") {
				buscarCuenta = detalleDeCuenta;
				// buscarCuenta = nombreCuentaEmpresa;
				compararCuenta = "persona";
				// compararInicial = rut;
				compararInicial = "registroparayleasing";
				rutBuscarRegistro = rutBuscarRegistroAlternativo;
			} else {
				buscarCuenta = detalleDeCuenta;
				// buscarCuenta = nombreCuentaEmpresa;
				compararCuenta = "empresa";
				// compararInicial = telefono;
				compararInicial = "registroparayleasing";
				rutBuscarRegistro = rutBuscarRegistroAlternativo;
			}
			break;
		case ("Para"):
			if (nombreCuentaPersona != "Cuenta Empresa") {
				buscarCuenta = busquedaLead;
				compararCuenta = "persona";
				compararInicial = rut;
				rutBuscarRegistro = rut;
			} else {
				buscarCuenta = busquedaEmpresa;
				compararCuenta = "empresa";
				compararInicial = telefono;
				rutBuscarRegistro = rut;
			}
			break;
		case ("Y"):
			if (nombreCuentaPersona != "Cuenta Empresa") {
				buscarCuenta = nombreCuentaPersona;
				compararCuenta = "persona";
				// compararInicial = rut;
				compararInicial = "registroparayleasing";
				rutBuscarRegistro = rut;
				// rutBuscarRegistro = rutBuscarRegistroAlternativo;
			} else {
				buscarCuenta = nombreCuentaEmpresa;
				compararCuenta = "empresa";
				// compararInicial = telefono;
				compararInicial = "registroparayleasing";
				rutBuscarRegistro = rut;
				// rutBuscarRegistro = rutBuscarRegistroAlternativo;
			}
			break;
		case ("Normal"):
			if (nombreCuentaPersona != "Cuenta Empresa") {
				buscarCuenta = busquedaLead;
				compararCuenta = "persona";
				compararInicial = rut;
				rutBuscarRegistro = rut;
			} else {
				buscarCuenta = busquedaEmpresa;
				compararCuenta = "empresa";
				compararInicial = telefono;
				rutBuscarRegistro = rut;
			}
			break;
		case (""):
			System.err.println("nombreCuentaPersona *" + nombreCuentaPersona + "*");
			System.err.println("buscarCuenta " + buscarCuenta);
			System.err.println("busquedaEmpresa " + busquedaEmpresa);
			if (nombreCuentaPersona.equals("Cuenta Empresa")) {

				buscarCuenta = busquedaEmpresa;
				compararCuenta = "empresa";
				compararInicial = telefono;
				rutBuscarRegistro = rut;
				System.err.println("compararCuenta " + compararCuenta);

			} else {

				buscarCuenta = busquedaLead;
				compararCuenta = "persona";
				compararInicial = rut;
				rutBuscarRegistro = rut;
				System.err.println("compararCuenta " + compararCuenta);
			}
			break;
		default:
			if (nombreCuentaPersona != "Cuenta Empresa") {
				buscarCuenta = busquedaLead;
				compararCuenta = "persona";
				compararInicial = rut;
				rutBuscarRegistro = rut;
			} else {
				buscarCuenta = busquedaEmpresa;
				compararCuenta = "empresa";
				compararInicial = telefono;
				rutBuscarRegistro = rut;
			}
			break;
		}
		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)));
			paginaPrincipal.MenuPrincipal.btnCuentas(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificación')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}

			Thread.sleep(2000);

			WebElement buscaCta = principalLead.PrincipalLead.inputBuscarPaginaLead(driver);
			buscaCta.sendKeys(" ");
			buscaCta.sendKeys(rutBuscarRegistro);
			Thread.sleep(3000);
			buscaCta.sendKeys(Keys.ENTER);
			Thread.sleep(2000);

			mensaje = "Flujo : Se busca registro " + buscarCuenta;
			msj = "ClickElementoencontrado";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			int posicion = 1;
			int sw = 0;
			String compara = "";
			while (sw == 0) {
				try {
					// WebElement registro = new WebDriverWait(driver, 10, 50).until(
					// ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[" + posicion +
					// "]/th/span/a")));
					// compara = registro.getText();

					WebElement registro = new WebDriverWait(driver, 10, 50).until(ExpectedConditions.presenceOfElementLocated(
					By.xpath("(//a[contains(text(),'" + buscarCuenta + "')])[" + posicion + "]")));
					compara = registro.getText();

					if (buscarCuenta.equals(compara)) {

						mensaje = "Flujo : Se Selecciona Registro ";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						registro.click();
						Thread.sleep(3000);
						sw = 1;
					} else {
						posicion = posicion + 1;
						sw = 0;
					}

				} catch (NoSuchElementException e) {
					posicion = posicion + 1;
					sw = 0;
				}
			}

			Thread.sleep(5000);

			if (compararCuenta == "empresa") {
				// WebElement comparaResultado =
				// driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[1]/div[1]/header/div[2]/ul/li[1]/p[2]/span"));
				// comparaFinal = comparaResultado.getText();
				comparaFinal = principalCuenta.MenuPrincipalCuenta.txtTelefonoEmpresa(driver).getText();

			} else {
				// WebElement comparaResultado =
				// driver.findElement(By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[3]/div/div/div[1]/div[1]/header/div[2]/ul/li[1]/p[2]/span"));
				// comparaFinal = comparaResultado.getText();
				comparaFinal = principalCuenta.MenuPrincipalCuenta.txtRutPersona(driver).getText();
			}

			Thread.sleep(3000);

			mensaje = "Flujo : Se revisa si los datos son correcto";
			msj = "RevisionRut";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);
			System.err.println("Rut o telefono Inicial " + compararInicial);
			System.err.println("Rut o telefono Final " + comparaFinal);

			if (compararInicial.equals(comparaFinal) || compararInicial.equals("registroparayleasing")) {

				// if (compararCuenta == "persona") {

				try {

					mensaje = "Flujo : Se revisa si los datos 'BP ' coinciden";
					msj = "RevisionBP";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					Thread.sleep(3000);

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//span[@class='test-id__field-label'][contains(text(),'Business Partner')]")));
					Thread.sleep(4000);
					// ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)", "");

					List<WebElement> numeroBpCuentaLista = driver.findElements(By.xpath(
					"//span[contains(text(),'Business Partner')]//following::span[@class='uiOutputText']"));
					System.err.println("Largo " + numeroBpCuentaLista.size());

					// String[] bpArray;
					// String aux = "";
					for (WebElement item : numeroBpCuentaLista) {

						// System.err.println("BP Rescate *" + bpArray[0]+"*");
						System.err.println("BP Rescate *" + numeroBP + "*");
						System.err.println("BP Final *" + item.getText() + "*");
						// aux = item.getText();
						// bpArray=aux.split("c");
						// System.err.println("0" + bpArray[0]);
						// System.err.println("1" + bpArray[1]);
						// WebElement numeroBpCuenta =
						// driver.findElement(By.xpath("//span[contains(text(),'" + numeroBP + "')]"));

						// if (numeroBpCuenta.getText().contains(numeroBP)) {
						// if (numeroBP.contains(item.getText())) {
						if (numeroBP.equals(item.getText())) {
							// if (item.getText().contains(bpArray[0])) {
							mensaje = "Ok : Numero BP Correcto ";
							msj = "OkCoincideBP";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							ValorEncontrado = true;
							break;
						} else {
							//
							// mensaje = "<p style='color:red;'>" + "Error : Numero BP Incorrecto " +
							// "</p>";
							// msj = "ErrorNoCoincideBP";
							// posicionEvidencia = posicionEvidencia + 1;
							// posicionEvidenciaString = Integer.toString(posicionEvidencia);
							// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
							// "_" + msj);
							// Log.info(mensaje);
							// Reporter.log("<p>" + mensaje + "</p>" + "<img
							// src="
							// + rutaEvidencia + ">");
							// System.err.println(mensaje);
							// errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
						}
					}
				} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra BP o No esta Visible, " + "</p>";
					driver.close();
					msj = "ErrorNoEncuentraBP";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					// driver.close();
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
				// } else {
				//
				// }
			} else {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida ; Rut o Telefono no corresponde" + "<p>";
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				driver.close();
				errorBuffer.append("\n" + mensaje + "\n");
			}

			Thread.sleep(3000);

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; BP " + numeroBP + " corresponde para cuenta " + buscarCuenta + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida ; BP no corresponde" + "<p>";
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				driver.close();
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorPruebaFallida";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			driver.close();
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErrorSeEncontraronErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
