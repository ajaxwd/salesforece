package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import utilidades.Funciones;

public class testPruebasDemoSegunda {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosDemo.xlsx";

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 0;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		

		// Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
		// Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
		Thread.sleep(4000);
		System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
		driver = new ChromeDriver();
		mensaje = "Navegador : Chrome" + "<br>" + "URL " + "https://the-internet.herokuapp.com/";
		System.out.println(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		Log.info(mensaje);
		Thread.sleep(2000);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://the-internet.herokuapp.com/");
		driver.manage().window().maximize(); // maximizo mi browser
		
		
		// #3 Manipulo mis objetos
		// A) Forma de manipular un objeto, usando WebElement
		
				
		mensaje = "Flujo : Pinchar Link";
		msj = "ClickLink";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);
		
		Runtime.getRuntime().exec("C:\\SalesForce\\prueba.exe");
				
		WebElement basicAuth = driver.findElement(By.xpath("//a[@href='/basic_auth']"));
		basicAuth.click();
		
		mensaje = "Flujo : Autenticacion";
		msj = "Autenticacion";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);
		
		Thread.sleep(2000);
		
		driver.navigate().back();
				
		WebElement frames = driver.findElement(By.xpath("//a[@href='/frames']"));
		frames.click();
		
		mensaje = "Flujo : frames";
		msj = "frames";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);
		
		Thread.sleep(2000);
		
		driver.navigate().back();
				
		WebElement geolocation = driver.findElement(By.xpath("//a[@href='/geolocation']"));
		geolocation.click();
		
		mensaje = "Flujo : geolocation";
		msj = "geolocation";
		posicionEvidencia = posicionEvidencia + 1;
		posicionEvidenciaString = Integer.toString(posicionEvidencia);
		nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
		Thread.sleep(2000);
		rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
		System.out.println(mensaje);
		
		Thread.sleep(2000);
		
		driver.navigate().back();
		
		// #5 Cierro el browser
		driver.close();

	}

}
