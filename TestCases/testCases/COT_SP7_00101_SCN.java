package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
//import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00101_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		if (sNumeroEscenario == "0011" || sNumeroEscenario == "0012") {
			mensaje = "Escenario en Ejecucion : " + sNombrePrueba;
			Funciones.mensajesLog(mensaje);
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = ExcelUtils.getCellData(posicionExcel, 12);
		String busquedaOportunidad = ExcelUtils.getCellData(posicionExcel, 5);
		String tipoDescuento = ExcelUtils.getCellData(posicionExcel, 17);
		String cantidadDescuento = ExcelUtils.getCellData(posicionExcel, 18);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver))).click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[2]/span/a")));
					//registroCotizacion.click();
					String dato = registroCotizacion.getText();
					System.out.println(dato);						
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;
					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(4000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("window.scrollBy (0,500)");
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			driver.findElement(By.xpath("(//h2/a/span)[1]")));
			js.executeScript("arguments[0].scrollIntoView();",driver.findElement(By.xpath("//span[contains(text(),'Relacionado')]")));
			Thread.sleep(1000);
			
			posicion = 1;
			sw = 0;
			int validarElemento = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				try {

					validarElemento = 0;
					while (validarElemento == 0) {
						
						if (contarTiempo > 15) {
							mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "+ "</p>";
							msj = "ElementoNoEncontrado";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}

						try {
//							// int ok_size =
//							// driver.findElements(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[2]/article[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]/div[1]"))
//							int ok_size = driver.findElements(By.xpath(
//									"//div[@id='brandBand_1']//div[2]//article[1]//div[1]//div[1]//div[1]//ul[1]//li[1]//a[1]//div[1]"))
//									.size();
//							// driver.findElements(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[2]/article[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]/div[1]"))
//							driver.findElements(By.xpath(
//									"//div[@id='brandBand_1']//div[2]//article[1]//div[1]//div[1]//div[1]//ul[1]//li[1]//a[1]//div[1]"))
//									.get(ok_size - 1).click();
//							WebElement nuevoDcto = driver.findElement(By.xpath("//div[@id='brandBand_1']//div[2]//article[1]//div[1]//div[1]//div[1]//ul[1]//li[1]//a[1]//div[1]"));
//							nuevoDcto.click();
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
							principalOportunidades.PrincipalOportunidades.btnGuardarAgregarDcto(driver))).click();
							validarElemento = 1;
						} catch (NoSuchElementException | ScreenshotException e) {
							validarElemento = 0;
							contarTiempo = contarTiempo + 1;
						}
					}
					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
					principalOportunidades.PrincipalOportunidades.btnNuevoDescuento(driver))).sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se Presiona Boton 'Nuevo Descuento' ";
					msj = "ClickBtnNuevoDcto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					sw = 1;
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver))).click();
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).clear();
			// principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(tipoDescuento);
			// Thread.sleep(500);
			// principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys("
			// ");
			String[] arregloIngresoDescuento = new String[tipoDescuento.length()];
			for (int i = 0; i < tipoDescuento.length(); i++) {
				arregloIngresoDescuento[i] = Character.toString(tipoDescuento.charAt(i));
			}
			for (int i = 0; i < tipoDescuento.length(); i++) {
				principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(arregloIngresoDescuento[i]);
				Thread.sleep(500);
			}

			mensaje = "Flujo : Se Ingresa 'Tipo De Descuento' ";
			msj = "TipoDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);
			String obtenerTextoDescuentoABuscar = new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + tipoDescuento + "']"))).getText();

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + tipoDescuento + "']"))).click();
			mensaje = "Flujo : Se Selecciona 'Descuento' " + tipoDescuento;
			msj = "SeleccionDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver))).click();
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).sendKeys(cantidadDescuento);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Cantidad De Descuento' " + cantidadDescuento + "%";
			msj = "CantidadDeDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.visibilityOf(principalOportunidades.PrincipalOportunidades.btnGuardarAgregarDescuento(driver))).click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar Descuento' ";
			msj = "ClickBtnGuardarDcto";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);
			
			try {
				String txtMensajeErrorEnPagina = driver.findElement(By.xpath("//span[@class='genericError uiOutputText']")).getText();
				if (txtMensajeErrorEnPagina.equals("Revise los errores de esta página.")) {
					mensaje = "<p style='color:red;'>" + "Error : Revise los errores de esta página :  "
					+ driver.findElement(By.xpath("//ul[@class='errorsList']//li")).getText() + "</p>";
					msj = "ErrorPruebaFallida";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					driver.close();
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(1000);
			mensaje = "Flujo : Se Comprueba existencia de ingresos ";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(1000);

			posicion = 1;
			sw = 0;
			String txtDescuentoCotizacionValidar = "";
			try {
				WebElement descuentoCotizacionValidar = driver.findElement(By.xpath("//span[@title='" + tipoDescuento + "']"));
				txtDescuentoCotizacionValidar = descuentoCotizacionValidar.getText();
				if (txtDescuentoCotizacionValidar.contains(obtenerTextoDescuentoABuscar)) {

					ValorEncontrado = true;
					mensaje = "OK : Se Comprueba existencia de descuentos creados ";
					msj = "OkExistenDescuentos";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					sw = 1;
				} else {
					posicion = posicion + 1;
					sw = 0;
				}
			} catch (NoSuchElementException | ScreenshotException e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Existen descuentos creados" + "<p>";
				msj = "ErrorNoExistenDescuentos";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
				sw = 1;
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; se ingresan descuento " + txtDescuentoCotizacionValidar + "<p>";
				msj = "OkIngresaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea descuentos" + "<p>";
				msj = "ErrorNoCreaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			Funciones.mensajesLog(mensaje);
			driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}

}
