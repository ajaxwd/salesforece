package testCases;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00134_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {

				posicionExcel = i;

				break;

			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = ExcelUtils.getCellData(posicionExcel, 12);
		String busquedaOportunidad = ExcelUtils.getCellData(posicionExcel, 5);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int esperarBtnEvaluarAmicar = 0;
		int contadorEspera = 0;

		Thread.sleep(30000);

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
						// flujo

					driver.findElement(By.xpath(
							"(//h2[contains(text(),'Llamada de Tel�fono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
							.click();
					// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);

					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) {
					cierraLlamado = 1;
				}
			}

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver)))
					.click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/td[2]/span/a")));
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(10000);

			// JavascriptExecutor js = (JavascriptExecutor) driver;
			// js.executeScript("window.scrollBy (0,500)");

			JavascriptExecutor js = (JavascriptExecutor) driver;
			Thread.sleep(1000);
			js.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//a[@title='Creada']")));
			Thread.sleep(1000);

			while (esperarBtnEvaluarAmicar == 0) {

				try {
					principalCotizaciones.PrincipalCotizaciones.btnEvaluarAmicar(driver).sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se presiona Boton 'Evaluar Amicar' ";
					msj = "ClickBtnSEvaluarAmicar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(10000);

					try {

						principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText();

						if (principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText().equals(
								"Ya ha hecho Pago Contado, para realizar una cotizaci�n con Cr�dito debe clonar la cotizaci�n")) {

							mensaje = "<p style='color:red;'>"
									+ "Error : Fin de ejecucion.......se encontraron errores, "
									+ principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText()
									+ "</p>";
							msj = "SeEncuentranErrores";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							driver.quit();
							throw new AssertionError(errorBuffer.toString());
						}

					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

						mensaje = "Flujo : No Hay Dificultades Para Evaluar Amicar";
						Log.info(mensaje);
						Reporter.log("<p>" + "<br>" + mensaje + "</p>");
						System.out.println(mensaje);

					}

					Thread.sleep(30000);

					try {

						int recorreExcell = 0;
						int inicioExcell = 0;
						String tipoCuenta = "";
						while (recorreExcell == 0) {
							if ("Nombre Persona Cuenta Creada".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								tipoCuenta = ExcelUtils.getCellData(posicionExcel, inicioExcell);
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							if (inicioExcell > 150) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No encuentra elemento 'Nombre Persona Cuenta Creada' en Archivo de Ingreso de datos Favor Revisar"
										+ "</p>";
								Log.info(mensaje);
								Reporter.log(mensaje);
								System.err.println(mensaje);
								driver.quit();
								throw new AssertionError();
							}
						}

						recorreExcell = 0;
						inicioExcell = 0;
						String paisEjecucion = "";
						while (recorreExcell == 0) {
							if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
								mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
								// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
								System.out.println(mensaje);
								Reporter.log(mensaje);
								Log.info(mensaje);
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							if (inicioExcell > 150) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar"
										+ "</p>";
								Log.info(mensaje);
								Reporter.log(mensaje);
								System.err.println(mensaje);
								driver.quit();
								throw new AssertionError();
							}
						}

						if (!tipoCuenta.equals("Cuenta Empresa")) {

							recorreExcell = 0;
							inicioExcell = 0;
							paisEjecucion = "";
							while (recorreExcell == 0) {
								if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
									paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
									// mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
									// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
									System.out.println(mensaje);
									Reporter.log(mensaje);
									Log.info(mensaje);
									recorreExcell = 1;
									break;
								}
								inicioExcell = inicioExcell + 1;
								recorreExcell = 0;
								if (inicioExcell > 150) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar"
											+ "</p>";
									Log.info(mensaje);
									Reporter.log(mensaje);
									System.err.println(mensaje);
									driver.quit();
									throw new AssertionError();
								}
							}

							System.err.println("CUENTA PERSONA");
							System.err.println("Pais Ejecucion : " + paisEjecucion);

							principalCotizaciones.PrincipalCotizaciones.btnInfoClientes(driver).sendKeys(Keys.ENTER);
							mensaje = "Flujo : Se presiona Boton 'Informacion Cliente' ";
							msj = "ClickBtnInfoCliente";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(3000);

							recorreExcell = 0;
							inicioExcell = 0;
							String identificadorFyi = "";
							while (recorreExcell == 0) {
								if ("Codigo F&I".equals(ExcelUtils.getCellData(0, inicioExcell))) {
									identificadorFyi = ExcelUtils.getCellData(posicionExcel, inicioExcell);
									recorreExcell = 1;
									break;
								}
								inicioExcell = inicioExcell + 1;
								recorreExcell = 0;
								if (inicioExcell > 150) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No encuentra 'Codigo F&I' en Archivo de Ingreso de datos Favor Revisar"
											+ "</p>";
									Log.info(mensaje);
									Reporter.log(mensaje);
									System.err.println(mensaje);
									driver.quit();
									throw new AssertionError();
								}
							}
							Select selectFyi = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectCodigoFyi(driver));
							// selectFyi.selectByValue(identificadorFyi);
							selectFyi.selectByVisibleText(identificadorFyi);
							mensaje = "Flujo : Se Selecciona F&I: " + identificadorFyi;
							msj = "SelectFYI";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							
							System.err.println("Pais Ejecucion : " + paisEjecucion);
							if (paisEjecucion.equals("Per�")) {
								
								Select selectRegion = new Select(
										principalCotizaciones.PrincipalCotizaciones.selectRegionEvaluarAmicarAlternativo(driver));
								// selectRegion.selectByValue("13-REGION METROPOLITANA");

								java.util.List<WebElement> opcionesRegion = selectRegion.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesRegion.size() - 1;
									opcionesRegion = selectRegion.getOptions();
								}
								int indiceRegionSeleccionar = 0;
								while (indiceRegionSeleccionar <= 0) {
									Random randomNumRegion = new Random();
									indiceRegionSeleccionar = minimo + randomNumRegion.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectRegion.selectByIndex(indiceRegionSeleccionar);
								String valorRegionSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectRegionEvaluarAmicarAlternativo(driver).getAttribute("value");
								System.err.println("Region "+valorRegionSeleccionado);
								
								mensaje = "Flujo : Se Selecciona Departamento: " + valorRegionSeleccionado;
								msj = "SelectDepartamento";
								 posicionEvidencia = posicionEvidencia + 1;
								 posicionEvidenciaString = Integer.toString(posicionEvidencia);
								 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								 Thread.sleep(1000);
								 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
								 "_" + msj);
								 Log.info(mensaje);
								 Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								 + rutaEvidencia + ">");
								 System.out.println(mensaje);
								
								 Thread.sleep(5000);
								 
							} else {

								Select selectRegion = new Select(
										principalCotizaciones.PrincipalCotizaciones.selectRegionEvaluarAmicar(driver));
								// selectRegion.selectByValue("13-REGION METROPOLITANA");

								java.util.List<WebElement> opcionesRegion = selectRegion.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesRegion.size() - 1;
									opcionesRegion = selectRegion.getOptions();
								}
								int indiceRegionSeleccionar = 0;
								while (indiceRegionSeleccionar <= 0) {
									Random randomNumRegion = new Random();
									indiceRegionSeleccionar = minimo + randomNumRegion.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectRegion.selectByIndex(indiceRegionSeleccionar);
								String valorRegionSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectRegionEvaluarAmicar(driver).getAttribute("value");
								System.err.println("Region "+valorRegionSeleccionado);

								mensaje = "Flujo : Se Selecciona Region: " + valorRegionSeleccionado;
								msj = "SelectRegion";
								 posicionEvidencia = posicionEvidencia + 1;
								 posicionEvidenciaString = Integer.toString(posicionEvidencia);
								 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								 Thread.sleep(1000);
								 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
								 "_" + msj);
								 Log.info(mensaje);
								 Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								 + rutaEvidencia + ">");
								 System.out.println(mensaje);
								
								 Thread.sleep(5000);
							}

							Thread.sleep(5000);
							
							if (paisEjecucion.equals("Per�")) {							 
								
								Select selectComuna = new Select(principalCotizaciones.PrincipalCotizaciones
										.selectComunaEvaluarAmicarAlternativo(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesComuna = selectComuna.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesComuna.size() - 1;
									opcionesComuna = selectComuna.getOptions();
								}
								int indiceComunaSeleccionar = 0;
								while (indiceComunaSeleccionar <= 0) {
									Random randomNumComuna = new Random();
									indiceComunaSeleccionar = minimo + randomNumComuna.nextInt(maximo);
								}
								Thread.sleep(3000);
								
								// selecciona el valor del indice y lo guarda en la variable
								selectComuna.selectByIndex(indiceComunaSeleccionar);
								String valorComunaSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectComunaEvaluarAmicarAlternativo(driver).getAttribute("value");
								System.err.println("Comuna "+valorComunaSeleccionado);

								mensaje = "Flujo : Se Selecciona Provincia: " + valorComunaSeleccionado;
								msj = "SelectProvincia";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							} else {

								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(5000);

								Select selectComuna = new Select(
										principalCotizaciones.PrincipalCotizaciones.selectComunaEvaluarAmicar(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesComuna = selectComuna.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesComuna.size() - 1;
									opcionesComuna = selectComuna.getOptions();
								}
								int indiceComunaSeleccionar = 0;
								while (indiceComunaSeleccionar <= 0) {
									Random randomNumComuna = new Random();
									indiceComunaSeleccionar = minimo + randomNumComuna.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectComuna.selectByIndex(indiceComunaSeleccionar);
								String valorComunaSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectComunaEvaluarAmicar(driver).getAttribute("value");
								System.err.println("Comuna "+valorComunaSeleccionado);

								mensaje = "Flujo : Se Selecciona Comuna: " + valorComunaSeleccionado;
								msj = "SelectComuna";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							}

							Thread.sleep(5000);
							
							// posicionEvidencia = posicionEvidencia + 1;
							// posicionEvidenciaString = Integer.toString(posicionEvidencia);
							// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							// Thread.sleep(1000);
							// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
							// "_" + msj);
							// Log.info(mensaje);
							// Reporter.log("<p>" + mensaje + "</p>" + "<img
							// src="
							// + rutaEvidencia + ">");
							// System.out.println(mensaje);
							//
							// Thread.sleep(5000);
							//
							// Select selectComuna = new Select(
							// principalCotizaciones.PrincipalCotizaciones.selectComunaEvaluarAmicar(driver));
							// // selectComuna.selectByValue("13101-SANTIAGO");
							//
							// java.util.List<WebElement> opcionesComuna = selectComuna.getOptions();
							//
							// minimo = 1;
							// maximo = 0;
							// while (maximo < 1) {// para evitar que sea menor a cero y arroje error
							// maximo = opcionesComuna.size() - 1;
							// opcionesComuna = selectComuna.getOptions();
							// }
							// int indiceComunaSeleccionar = 0;
							// while (indiceComunaSeleccionar <= 0) {
							// Random randomNumComuna = new Random();
							// indiceComunaSeleccionar = minimo + randomNumComuna.nextInt(maximo);
							// }
							// Thread.sleep(1000);
							//
							// // selecciona el valor del indice y lo guarda en la variable
							// selectComuna.selectByIndex(indiceComunaSeleccionar);
							// String valorComunaSeleccionado = principalCotizaciones.PrincipalCotizaciones
							// .selectComunaEvaluarAmicar(driver).getAttribute("value");

							// System.err.println("Pais Ejecucion : " + paisEjecucion);
							// if (paisEjecucion.equals("Per�")) {
							// mensaje = "Flujo : Se Selecciona Provincia: " + valorRegionSeleccionado;
							// msj = "SelectProvincia";
							// } else {
							// mensaje = "Flujo : Se Selecciona Comuna: " + valorComunaSeleccionado;
							// msj = "SelectComuna";
							// }

							// posicionEvidencia = posicionEvidencia + 1;
							// posicionEvidenciaString = Integer.toString(posicionEvidencia);
							// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							// Thread.sleep(1000);
							// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
							// "_" + msj);
							// Log.info(mensaje);
							// Reporter.log("<p>" + mensaje + "</p>" + "<img
							// src="
							// + rutaEvidencia + ">");
							// System.out.println(mensaje);
							//
							// Thread.sleep(2000);

							
							if (paisEjecucion.equals("Per�")) {	

								Thread.sleep(5000);

								Select selectDistrito = new Select(principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesDistrito = selectDistrito.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesDistrito.size() - 1;
									opcionesDistrito = selectDistrito.getOptions();
								}
								int indiceDistritoSeleccionar = 0;
								while (indiceDistritoSeleccionar <= 0) {
									Random randomNumDistrito = new Random();
									indiceDistritoSeleccionar = minimo + randomNumDistrito.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectDistrito.selectByIndex(indiceDistritoSeleccionar);
								String valorDistritoSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver).getAttribute("value");
								System.err.println("Distrito "+valorDistritoSeleccionado);

								mensaje = "Flujo : Se Selecciona Distrito: " + valorDistritoSeleccionado;
								msj = "SelectDistrito";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							}
							
							if (paisEjecucion.equals("Per�")) {	

								Thread.sleep(5000);

								Select selectDistrito = new Select(principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesDistrito = selectDistrito.getOptions();

								int minimo = 1;
								int maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesDistrito.size() - 1;
									opcionesDistrito = selectDistrito.getOptions();
								}
								int indiceDistritoSeleccionar = 0;
								while (indiceDistritoSeleccionar <= 0) {
									Random randomNumDistrito = new Random();
									indiceDistritoSeleccionar = minimo + randomNumDistrito.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectDistrito.selectByIndex(indiceDistritoSeleccionar);
								String valorDistritoSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver).getAttribute("value");
								System.err.println("Distrito "+valorDistritoSeleccionado);

								mensaje = "Flujo : Se Selecciona Distrito: " + valorDistritoSeleccionado;
								msj = "SelectDistrito";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							}


							Select selectNacionalidad = new Select(principalCotizaciones.PrincipalCotizaciones
									.selectNacionalidadEvaluarAmicar(driver));
							
							if (paisEjecucion.equals("Per�")) {	
								selectNacionalidad.selectByValue("PERU-PERU");
								mensaje = "Flujo : Se Selecciona Nacionalidad: PERU ";
							}else {
								selectNacionalidad.selectByValue("CHILE-CHILE");
								mensaje = "Flujo : Se Selecciona Nacionalidad: CHILE ";	
							}
							msj = "SelectNacionalidad";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectGenero = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectGeneroEvaluarAmicar(driver));
							selectGenero.selectByValue("M-MASCULINO");
							mensaje = "Flujo : Se Selecciona Genero: MASCULINO ";
							msj = "selectGenero";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectEstadoCivil = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectEstadoCivilEvaluarAmicar(driver));
							selectEstadoCivil.selectByValue("SO-SOLTERO");
							mensaje = "Flujo : Se Selecciona Estado Civil: SOLTERO ";
							msj = "selectEstadoCivil";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectRegimenMatrimonial = new Select(principalCotizaciones.PrincipalCotizaciones
									.selectRegimenMatrimonialEvaluarAmicar(driver));
							selectRegimenMatrimonial.selectByValue("NA-NO APLICA");
							mensaje = "Flujo : Se Selecciona Regimen Matrimonial: NO APLICA ";
							msj = "selectRegimenMatrimonial";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("arguments[0].scrollIntoView();",
									principalCotizaciones.PrincipalCotizaciones.selectEstudiosEvaluarAmicar(driver));

							Thread.sleep(2000);

							Select selectEstudiosEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectEstudiosEvaluarAmicar(driver));
							selectEstudiosEvaluarAmicar.selectByValue("SU-SUPERIORES");
							mensaje = "Flujo : Se Selecciona Nivel de Estudios: SUPERIORES ";
							msj = "selectEstudiosEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectTipoResidenciaEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectTipoResidenciaEvaluarAmicar(driver));
							selectTipoResidenciaEvaluarAmicar.selectByValue("5-VIVE CON PADRES");
							mensaje = "Flujo : Se Selecciona Tipo Residencia: VIVE CON PADRES ";
							msj = "selectTipoResidenciaEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectTipoTrabajadorEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectTipoTrabajadorEvaluarAmicar(driver));
							selectTipoTrabajadorEvaluarAmicar.selectByValue("DE-DEPENDIENTE");
							mensaje = "Flujo : Se Selecciona Tipo Trabajador: DEPENDIENTE ";
							msj = "selectTipoTrabajadorEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectTipoActividadEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectTipoActividadEvaluarAmicar(driver));
							selectTipoActividadEvaluarAmicar.selectByValue("27-Profesional");
							mensaje = "Flujo : Se Selecciona Tipo Actividad: Profesional ";
							msj = "selectTipoActividadEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							principalCotizaciones.PrincipalCotizaciones.inputCargoEvaluarAmicar(driver).click();
							principalCotizaciones.PrincipalCotizaciones.inputCargoEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputCargoEvaluarAmicar(driver)
									.sendKeys("Ingeniero");
							mensaje = "Flujo : Se ingresa Cargo: Ingeniero ";
							msj = "IngresoCargo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							principalCotizaciones.PrincipalCotizaciones.inputFechaIngresoEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputFechaIngresoEvaluarAmicar(driver)
									.sendKeys("10/10/2010");
							mensaje = "Flujo : Se ingresa Fecha Ingreso: 10/10/2010 ";
							msj = "FechaIngreso";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							// Cadena de caracteres aleatoria Para Crear Nombre Empresa
							String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
							empresaAleatorio = empresaAleatorio + " & "
									+ utilidades.Funciones.funcionApellidoAleatorio() + " Cia Ltda";
							principalCotizaciones.PrincipalCotizaciones.inputNombreEmpleadorEvaluarAmicar(driver)
									.click();
							principalCotizaciones.PrincipalCotizaciones.inputNombreEmpleadorEvaluarAmicar(driver)
									.clear();
							principalCotizaciones.PrincipalCotizaciones.inputNombreEmpleadorEvaluarAmicar(driver)
									.sendKeys(empresaAleatorio);
							mensaje = "Flujo : Se ingresa Nombre Empleador: " + empresaAleatorio;
							msj = "NombreEmpleador";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							// Cadena de caracteres aleatoria Para Calle
							String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
							String calleAleatorioNumero = "";
							String azar = "";
							// Cadena de numeros aleatoria Para numero Calle Laboral
							for (int i = 0; i < 2; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								calleAleatorioNumero = calleAleatorioNumero + azar;
							}
							principalCotizaciones.PrincipalCotizaciones.inputDireccionEmpleadorEvaluarAmicar(driver)
									.click();
							principalCotizaciones.PrincipalCotizaciones.inputDireccionEmpleadorEvaluarAmicar(driver)
									.clear();
							principalCotizaciones.PrincipalCotizaciones.inputDireccionEmpleadorEvaluarAmicar(driver)
									.sendKeys(calleAleatorio + " " + calleAleatorioNumero);
							mensaje = "Flujo : Se ingresa Direccion Empresa:  " + calleAleatorio + " "
									+ calleAleatorioNumero;
							msj = "IngresoDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectRubroEmpleadorEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectRubroEmpleadorEvaluarAmicar(driver));
							selectRubroEmpleadorEvaluarAmicar.selectByValue("8-SERVICIOS");
							mensaje = "Flujo : Se Selecciona Rubro: SERVICIOS ";
							msj = "selectRubroEmpleadorEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectRegionEmpleadorEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectRegionEmpleadorEvaluarAmicar(driver));
							// selectRegionEmpleadorEvaluarAmicar.selectByValue("13-REGION METROPOLITANA");

							java.util.List<WebElement> opcionesRegionEmpleador = selectRegionEmpleadorEvaluarAmicar
									.getOptions();

							int minimo = 1;
							int maximo = 0;
							while (maximo < 1) {// para evitar que sea menor a cero y arroje error
								maximo = opcionesRegionEmpleador.size() - 1;
								opcionesRegionEmpleador = selectRegionEmpleadorEvaluarAmicar.getOptions();
							}
							int indiceRegionEmpleadorSeleccionar = 0;
							while (indiceRegionEmpleadorSeleccionar <= 0) {
								Random randomNumRegionEmpleador = new Random();
								indiceRegionEmpleadorSeleccionar = minimo + randomNumRegionEmpleador.nextInt(maximo);
							}
							Thread.sleep(1000);

							// selecciona el valor del indice y lo guarda en la variable
							selectRegionEmpleadorEvaluarAmicar.selectByIndex(indiceRegionEmpleadorSeleccionar);
							String valorRegionEmpleadorSeleccionado = principalCotizaciones.PrincipalCotizaciones
									.selectRegionEmpleadorEvaluarAmicar(driver).getAttribute("value");

							mensaje = "Flujo : Se Selecciona Region Empleador: " + valorRegionEmpleadorSeleccionado;
							msj = "selectRegionEmpleadorEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectComunaEmpleadorEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectComunaEmpleadorEvaluarAmicar(driver));
							// selectComunaEmpleadorEvaluarAmicar.selectByValue("13101-SANTIAGO");

							java.util.List<WebElement> opcionesComunaEmpleador = selectComunaEmpleadorEvaluarAmicar
									.getOptions();

							minimo = 1;
							maximo = 0;
							while (maximo < 1) {// para evitar que sea menor a cero y arroje error
								maximo = opcionesComunaEmpleador.size() - 1;
								opcionesComunaEmpleador = selectComunaEmpleadorEvaluarAmicar.getOptions();
							}
							int indiceComunaEmpleadorSeleccionar = 0;
							while (indiceComunaEmpleadorSeleccionar <= 0) {
								Random randomNumComunaEmpleador = new Random();
								indiceComunaEmpleadorSeleccionar = minimo + randomNumComunaEmpleador.nextInt(maximo);
							}
							Thread.sleep(1000);

							// selecciona el valor del indice y lo guarda en la variable
							selectComunaEmpleadorEvaluarAmicar.selectByIndex(indiceComunaEmpleadorSeleccionar);
							String valorComunaEmpleadorSeleccionado = principalCotizaciones.PrincipalCotizaciones
									.selectComunaEmpleadorEvaluarAmicar(driver).getAttribute("value");

							mensaje = "Flujo : Se Selecciona Comuna Empleador: " + valorComunaEmpleadorSeleccionado;
							msj = "selectComunaEmpleadorEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);
							
							if (paisEjecucion.equals("Per�")) {	

								Thread.sleep(5000);

								Select selectDistrito = new Select(principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEmpleadorEvaluarAmicar(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesDistrito = selectDistrito.getOptions();

								minimo = 1;
								maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesDistrito.size() - 1;
									opcionesDistrito = selectDistrito.getOptions();
								}
								int indiceDistritoSeleccionar = 0;
								while (indiceDistritoSeleccionar <= 0) {
									Random randomNumDistrito = new Random();
									indiceDistritoSeleccionar = minimo + randomNumDistrito.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectDistrito.selectByIndex(indiceDistritoSeleccionar);
								String valorDistritoSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEmpleadorEvaluarAmicar(driver).getAttribute("value");
								System.err.println("Distrito "+valorDistritoSeleccionado);

								mensaje = "Flujo : Se Selecciona Distrito Empleador: " + valorDistritoSeleccionado;
								msj = "SelectDistrito";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							}


							String numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.click();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.clear();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono Empresa:  " + "2" + numTelefono;
							msj = "IngresoTelefonoEmpleador";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).click();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono Empresa Alternativo:  " + "2" + numTelefono;
							msj = "IngresoTelefonoEmpleadorAlternativo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);
							jse.executeScript("arguments[0].scrollIntoView();",
									principalCotizaciones.PrincipalCotizaciones.btnIngresosyEgresos(driver));
							Thread.sleep(2000);

							principalCotizaciones.PrincipalCotizaciones.btnIngresosyEgresos(driver).click();
							mensaje = "Flujo : Se presiona Boton 'Ingresos Y Egresos' ";
							msj = "ClickBtnIngresosyEgresos";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectTipoContratoEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones
											.selectTipoContratoEvaluarAmicar(driver));
							selectTipoContratoEvaluarAmicar.selectByValue("IN-INDEFINIDO");
							mensaje = "Flujo : Se Selecciona Tipo Contrato: INDEFINIDO ";
							msj = "selectTipoContratoEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectTipoRentaEvaluarAmicar = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectTipoRentaEvaluarAmicar(driver));
							selectTipoRentaEvaluarAmicar.selectByValue("FI-FIJA");
							mensaje = "Flujo : Se Selecciona Tipo Renta: FIJA ";
							msj = "selectTipoRentaEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							String diaDePago = "";
							azar = "";
							// Cadena de numeros aleatoria Para diaDePago
							azar = utilidades.Funciones.numeroAleatorio(1, 30);
							diaDePago = azar;

							principalCotizaciones.PrincipalCotizaciones.inputDiaDePagoEvaluarAmicar(driver).click();
							principalCotizaciones.PrincipalCotizaciones.inputDiaDePagoEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputDiaDePagoEvaluarAmicar(driver)
									.sendKeys(diaDePago);
							mensaje = "Flujo : Se ingresa Dia De Pago:  " + diaDePago;
							msj = "inputDiaDePagoEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							if (paisEjecucion.equals("Per�")) {
								
								String sueldoLiquido = "";
								azar = "";
								// Cadena de numeros aleatoria Para sueldoLiquido
								azar = utilidades.Funciones.numeroAleatorio(900000, 1500000);
								sueldoLiquido = azar;

								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver).click();
								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver).clear();

								int borraContenido = 0;
								while (borraContenido == 0) {
									if (principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
											.getAttribute("value").equals("")) {
										borraContenido = 1;
									} else {
										principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
												.click();
										principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
												.sendKeys(Keys.BACK_SPACE);
									}
								}

								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
										.sendKeys(sueldoLiquido);
								mensaje = "Flujo : Se ingresa Sueldo Bruto:  " + sueldoLiquido;
								msj = "inputSueldoLiquidoEvaluarAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								
								sueldoLiquido = "";
								azar = "";
								// Cadena de numeros aleatoria Para sueldoLiquido
								azar = utilidades.Funciones.numeroAleatorio(900000, 1500000);
								sueldoLiquido = azar;

								principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver).click();
								principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver).clear();

								borraContenido = 0;
								while (borraContenido == 0) {
									if (principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver)
											.getAttribute("value").equals("")) {
										borraContenido = 1;
									} else {
										principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver)
												.click();
										principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver)
												.sendKeys(Keys.BACK_SPACE);
									}
								}

								principalCotizaciones.PrincipalCotizaciones.inputSueldoNetoEvaluarAmicar(driver)
										.sendKeys(sueldoLiquido);
								mensaje = "Flujo : Se ingresa Sueldo Neto:  " + sueldoLiquido;
								msj = "inputSueldoNetoEvaluarAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

								String arriendo = "";
								azar = "";
								// Cadena de numeros aleatoria Para Arriendo
								azar = utilidades.Funciones.numeroAleatorio(1, 30000);
								arriendo = azar;

								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.click();
								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.clear();

								borraContenido = 0;
								while (borraContenido == 0) {
									if (principalCotizaciones.PrincipalCotizaciones
											.inputArriendoDividendoEvaluarAmicar(driver).getAttribute("value").equals("")) {
										borraContenido = 1;
									} else {
										principalCotizaciones.PrincipalCotizaciones
												.inputArriendoDividendoEvaluarAmicar(driver).click();
										principalCotizaciones.PrincipalCotizaciones
												.inputArriendoDividendoEvaluarAmicar(driver).sendKeys(Keys.BACK_SPACE);
									}
								}

								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.sendKeys(arriendo);
								mensaje = "Flujo : Se ingresa Hipoteca:  " + arriendo;
								msj = "inputArriendoDividendoEvaluarAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								
							} else {
								String sueldoLiquido = "";
								azar = "";
								// Cadena de numeros aleatoria Para sueldoLiquido
								azar = utilidades.Funciones.numeroAleatorio(900000, 1500000);
								sueldoLiquido = azar;

								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver).click();
								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver).clear();

								int borraContenido = 0;
								while (borraContenido == 0) {
									if (principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
											.getAttribute("value").equals("")) {
										borraContenido = 1;
									} else {
										principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
												.click();
										principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
												.sendKeys(Keys.BACK_SPACE);
									}
								}

								principalCotizaciones.PrincipalCotizaciones.inputSueldoLiquidoEvaluarAmicar(driver)
										.sendKeys(sueldoLiquido);
								mensaje = "Flujo : Se ingresa Sueldo liquido:  " + sueldoLiquido;
								msj = "inputSueldoLiquidoEvaluarAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

								String arriendo = "";
								azar = "";
								// Cadena de numeros aleatoria Para Arriendo
								azar = utilidades.Funciones.numeroAleatorio(1, 30000);
								arriendo = azar;

								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.click();
								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.clear();

								borraContenido = 0;
								while (borraContenido == 0) {
									if (principalCotizaciones.PrincipalCotizaciones
											.inputArriendoDividendoEvaluarAmicar(driver).getAttribute("value").equals("")) {
										borraContenido = 1;
									} else {
										principalCotizaciones.PrincipalCotizaciones
												.inputArriendoDividendoEvaluarAmicar(driver).click();
										principalCotizaciones.PrincipalCotizaciones
												.inputArriendoDividendoEvaluarAmicar(driver).sendKeys(Keys.BACK_SPACE);
									}
								}

								principalCotizaciones.PrincipalCotizaciones.inputArriendoDividendoEvaluarAmicar(driver)
										.sendKeys(arriendo);
								mensaje = "Flujo : Se ingresa Arriendo:  " + arriendo;
								msj = "inputArriendoDividendoEvaluarAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

							}
							
							Thread.sleep(2000);
							jse.executeScript("arguments[0].scrollIntoView();",
									principalCotizaciones.PrincipalCotizaciones.btnIngresosyEgresos(driver));
							Thread.sleep(2000);

							principalCotizaciones.PrincipalCotizaciones.btnInformacionComercial(driver).click();
							mensaje = "Flujo : Se presiona Boton 'Informacion Comercial' ";
							msj = "ClickBtnInformacionComercial";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							jse.executeScript("arguments[0].scrollIntoView();",
									principalCotizaciones.PrincipalCotizaciones.inputNombreReferido(driver));

							// Cadena de caracteres aleatoria Para Nombre
							String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
							referidoAleatorio = referidoAleatorio + " "
									+ utilidades.Funciones.funcionApellidoAleatorio();
							principalCotizaciones.PrincipalCotizaciones.inputNombreReferido(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputNombreReferido(driver)
									.sendKeys(referidoAleatorio);
							mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
							msj = "IngresoReferido";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							Thread.sleep(1000);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							// Cadena de caracteres aleatoria Para Calle
							String calleAleatorioReferido = utilidades.Funciones.funcionCalleAleatorio();
							String calleAleatorioNumeroReferido = "";
							azar = "";
							// Cadena de numeros aleatoria Para numero Calle Laboral
							for (int i = 0; i < 2; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								calleAleatorioNumeroReferido = calleAleatorioNumeroReferido + azar;
							}

							principalCotizaciones.PrincipalCotizaciones.inputDireccionReferido(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputDireccionReferido(driver)
									.sendKeys(calleAleatorioReferido + " " + calleAleatorioNumeroReferido);
							mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorioReferido + " "
									+ calleAleatorioNumeroReferido;
							msj = "IngresoCalleReferido";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							principalCotizaciones.PrincipalCotizaciones.inputTipoRelacionReferido(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputTipoRelacionReferido(driver)
									.sendKeys("FAMILIAR");
							mensaje = "Flujo: Ingreso Tipo Relacion Referido: FAMILIAR";
							msj = "IngresoRelacionReferido";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones.inputTelefonoReferido(driver).click();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoReferido(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoReferido(driver)
									.sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono de Referido:  " + "2" + numTelefono;
							msj = "inputTelefonoReferido";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							recorreExcell = 0;
							inicioExcell = 0;
							paisEjecucion = "";
							while (recorreExcell == 0) {
								if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
									paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
									// mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
									// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
									System.out.println(mensaje);
									Reporter.log(mensaje);
									Log.info(mensaje);
									recorreExcell = 1;
									break;
								}
								inicioExcell = inicioExcell + 1;
								recorreExcell = 0;
								if (inicioExcell > 150) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar"
											+ "</p>";
									Log.info(mensaje);
									Reporter.log(mensaje);
									System.err.println(mensaje);
									driver.quit();
									throw new AssertionError();
								}
							}

							System.err.println("CUENTA EMPRESA");
							System.err.println("Pais Ejecucion : " + paisEjecucion);

							principalCotizaciones.PrincipalCotizaciones.btnInfoClientes(driver).sendKeys(Keys.ENTER);
							mensaje = "Flujo : Se presiona Boton 'Informacion Cliente' ";
							msj = "ClickBtnInfoCliente";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(3000);

							recorreExcell = 0;
							inicioExcell = 0;
							String identificadorFyi = "";
							while (recorreExcell == 0) {
								if ("Codigo F&I".equals(ExcelUtils.getCellData(0, inicioExcell))) {
									identificadorFyi = ExcelUtils.getCellData(posicionExcel, inicioExcell);
									recorreExcell = 1;
									break;
								}
								inicioExcell = inicioExcell + 1;
								recorreExcell = 0;
								if (inicioExcell > 150) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No encuentra 'Codigo F&I' en Archivo de Ingreso de datos Favor Revisar"
											+ "</p>";
									Log.info(mensaje);
									Reporter.log(mensaje);
									System.err.println(mensaje);
									driver.quit();
									throw new AssertionError();
								}
							}
							Select selectFyi = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectCodigoFyi(driver));
							// selectFyi.selectByValue(identificadorFyi);
							selectFyi.selectByVisibleText(identificadorFyi);
							mensaje = "Flujo : Se Selecciona F&I: " + identificadorFyi;
							msj = "SelectFYI";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);
							int cantidadDeAnios = 10;
							String inputFechaConstitucion = utilidades.Funciones.funcionRestarAnio(cantidadDeAnios);
							principalCotizaciones.PrincipalCotizaciones.inputFechaConstitucionEvaluarAmicar(driver)
									.clear();
							Thread.sleep(500);
							principalCotizaciones.PrincipalCotizaciones.inputFechaConstitucionEvaluarAmicar(driver)
									.sendKeys(inputFechaConstitucion);
							mensaje = "Flujo : Se ingresa Fecha Constitucion:  " + inputFechaConstitucion;
							msj = "inputFechaConstitucionEvaluarAmicar";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectRubro = new Select(
									principalCotizaciones.PrincipalCotizaciones.selectRubroEvaluarAmicar(driver));

							java.util.List<WebElement> opcionesRubro = selectRubro.getOptions();

							int minimo = 1;
							int maximo = 0;
							while (maximo < 1) {// para evitar que sea menor a cero y arroje error
								maximo = opcionesRubro.size() - 1;
								opcionesRubro = selectRubro.getOptions();
							}
							int indiceRubroSeleccionar = 0;
							while (indiceRubroSeleccionar <= 0) {
								Random randomNumRubro = new Random();
								indiceRubroSeleccionar = minimo + randomNumRubro.nextInt(maximo);
							}
							Thread.sleep(1000);

							// selecciona el valor del indice y lo guarda en la variable
							selectRubro.selectByIndex(indiceRubroSeleccionar);
							String valorRubroSeleccionado = principalCotizaciones.PrincipalCotizaciones
									.selectRubroEvaluarAmicar(driver).getAttribute("value");

							mensaje = "Flujo : Se Selecciona Rubro: " + valorRubroSeleccionado;
							msj = "SelectRubro";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							recorreExcell = 0;
							inicioExcell = 0;
							String giroEmpresa = "";
							while (recorreExcell == 0) {
								if ("Giro".equals(ExcelUtils.getCellData(0, inicioExcell))) {
									giroEmpresa = ExcelUtils.getCellData(posicionExcel, inicioExcell);
									recorreExcell = 1;
									break;
								}
								inicioExcell = inicioExcell + 1;
								recorreExcell = 0;
								if (inicioExcell > 150) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No encuentra elemento 'Giro' en Archivo de Ingreso de datos Favor Revisar"
											+ "</p>";
									Log.info(mensaje);
									Reporter.log(mensaje);
									System.err.println(mensaje);
									driver.quit();
									throw new AssertionError();
								}
							}

							principalCotizaciones.PrincipalCotizaciones.inputRubroEvaluarAmicar(driver).click();
							Thread.sleep(500);
							principalCotizaciones.PrincipalCotizaciones.inputRubroEvaluarAmicar(driver).clear();
							Thread.sleep(500);
							principalCotizaciones.PrincipalCotizaciones.inputRubroEvaluarAmicar(driver)
									.sendKeys(giroEmpresa);
							;
							mensaje = "Flujo : Ingreso de Rubro : " + giroEmpresa;
							msj = "IngresoRubro";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							String cantidadDeEmpleados = utilidades.Funciones.numeroAleatorio(15, 300);
							principalCotizaciones.PrincipalCotizaciones.inputNumeroTrabajadoresEvaluarAmicar(driver)
									.click();
							Thread.sleep(500);
							principalCotizaciones.PrincipalCotizaciones.inputNumeroTrabajadoresEvaluarAmicar(driver)
									.clear();
							Thread.sleep(500);
							principalCotizaciones.PrincipalCotizaciones.inputNumeroTrabajadoresEvaluarAmicar(driver)
									.sendKeys(cantidadDeEmpleados);

							mensaje = "Flujo : Numero Trabajadores : " + cantidadDeEmpleados;
							msj = "IngresoCantidadDeEmpleados";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							Select selectRegion = new Select(principalCotizaciones.PrincipalCotizaciones
									.selectRegionEmpresaEvaluarAmicar(driver));
							// selectRegion.selectByValue("13-REGION METROPOLITANA");

							java.util.List<WebElement> opcionesRegion = selectRegion.getOptions();

							minimo = 1;
							maximo = 0;
							while (maximo < 1) {// para evitar que sea menor a cero y arroje error
								maximo = opcionesRegion.size() - 1;
								opcionesRegion = selectRegion.getOptions();
							}
							int indiceRegionSeleccionar = 0;
							while (indiceRegionSeleccionar <= 0) {
								Random randomNumRegion = new Random();
								indiceRegionSeleccionar = minimo + randomNumRegion.nextInt(maximo);
							}
							Thread.sleep(1000);

							// selecciona el valor del indice y lo guarda en la variable
							selectRegion.selectByIndex(indiceRegionSeleccionar);
							String valorRegionSeleccionado = principalCotizaciones.PrincipalCotizaciones
									.selectRegionEmpresaEvaluarAmicar(driver).getAttribute("value");

							System.err.println("Pais Ejecucion : " + paisEjecucion);
							if (paisEjecucion.equals("Per�")) {
								mensaje = "Flujo : Se Selecciona Departamento: " + valorRegionSeleccionado;
								msj = "SelectDepartamento";
							} else {
								mensaje = "Flujo : Se Selecciona Region: " + valorRegionSeleccionado;
								msj = "SelectRegion";
							}

							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(6000);

							Select selectComuna = new Select(principalCotizaciones.PrincipalCotizaciones
									.selectComunaEmpresaEvaluarAmicar(driver));
							// selectComuna.selectByValue("13101-SANTIAGO");

							java.util.List<WebElement> opcionesComuna = selectComuna.getOptions();

							minimo = 1;
							maximo = 0;
							while (maximo < 1) {// para evitar que sea menor a cero y arroje error
								maximo = opcionesComuna.size() - 1;
								opcionesComuna = selectComuna.getOptions();
							}
							int indiceComunaSeleccionar = 0;
							while (indiceComunaSeleccionar <= 0) {
								Random randomNumComuna = new Random();
								indiceComunaSeleccionar = minimo + randomNumComuna.nextInt(maximo);
							}
							Thread.sleep(1000);

							// selecciona el valor del indice y lo guarda en la variable
							selectComuna.selectByIndex(indiceComunaSeleccionar);
							String valorComunaSeleccionado = principalCotizaciones.PrincipalCotizaciones
									.selectComunaEmpresaEvaluarAmicar(driver).getAttribute("value");

							System.err.println("Pais Ejecucion : " + paisEjecucion);
							if (paisEjecucion.equals("Per�")) {
								mensaje = "Flujo : Se Selecciona Provincia: " + valorRegionSeleccionado;
								msj = "SelectProvincia";
							} else {
								mensaje = "Flujo : Se Selecciona Comuna: " + valorComunaSeleccionado;
								msj = "SelectComuna";
							}

							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							System.err.println("Pais Ejecucion : " + paisEjecucion);
							if (paisEjecucion.equals("Per�")) {

								Thread.sleep(5000);

								Select selectDistrito = new Select(principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver));
								// selectComuna.selectByValue("13101-SANTIAGO");

								java.util.List<WebElement> opcionesDistrito = selectDistrito.getOptions();

								minimo = 1;
								maximo = 0;
								while (maximo < 1) {// para evitar que sea menor a cero y arroje error
									maximo = opcionesDistrito.size() - 1;
									opcionesDistrito = selectComuna.getOptions();
								}
								int indiceDistritoSeleccionar = 0;
								while (indiceDistritoSeleccionar <= 0) {
									Random randomNumDistrito = new Random();
									indiceDistritoSeleccionar = minimo + randomNumDistrito.nextInt(maximo);
								}
								Thread.sleep(1000);

								// selecciona el valor del indice y lo guarda en la variable
								selectDistrito.selectByIndex(indiceDistritoSeleccionar);
								String valorDistritoSeleccionado = principalCotizaciones.PrincipalCotizaciones
										.selectDistritoEvaluarAmicar(driver).getAttribute("value");

								mensaje = "Flujo : Se Selecciona Distrito: " + valorDistritoSeleccionado;
								msj = "SelectDistrito";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(2000);

							}

							String numTelefono = "";
							String azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones.inputTelefonoComercialEvaluarAmicar(driver)
									.click();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoComercialEvaluarAmicar(driver)
									.clear();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoComercialEvaluarAmicar(driver)
									.sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono Comercial:  " + "2" + numTelefono;
							msj = "IngresoTelefonoComercial";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoComercialAlternativoEvaluarAmicar(driver).click();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoComercialAlternativoEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoComercialAlternativoEvaluarAmicar(driver).sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono Comercial 2 :  " + "2" + numTelefono;
							msj = "IngresoTelefonoComercial2";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.click();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.clear();
							principalCotizaciones.PrincipalCotizaciones.inputTelefonoEmpleadorEvaluarAmicar(driver)
									.sendKeys("2" + numTelefono);
							mensaje = "Flujo : Se ingresa Telefono Contacto:  " + "2" + numTelefono;
							msj = "IngresoTelefonoContacto";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).click();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).clear();
							principalCotizaciones.PrincipalCotizaciones
									.inputTelefonoAlternativoEmpleadorEvaluarAmicar(driver).sendKeys("9" + numTelefono);
							mensaje = "Flujo : Se ingresa Celular Contacto:  " + "9" + numTelefono;
							msj = "IngresoTelefonoCelularContacto";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							// Cadena de caracteres aleatoria Para Nombre
							String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
							referidoAleatorio = referidoAleatorio + " "
									+ utilidades.Funciones.funcionApellidoAleatorio();
							principalCotizaciones.PrincipalCotizaciones.inputNombreContacto(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputNombreContacto(driver)
									.sendKeys(referidoAleatorio);
							mensaje = "Flujo : Ingreso de Contacto Aleatorio : " + referidoAleatorio;
							msj = "IngresoContacto";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							Thread.sleep(1000);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(2000);

							// Cadena de caracteres aleatoria Para Nombre
							String cargoContacto = "Tester";
							principalCotizaciones.PrincipalCotizaciones.inputCargoContacto(driver).clear();
							principalCotizaciones.PrincipalCotizaciones.inputCargoContacto(driver)
									.sendKeys(cargoContacto);
							mensaje = "Flujo : Ingreso de Cargo Contacto : " + cargoContacto;
							msj = "IngresoCargo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							Thread.sleep(1000);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(5000);

						principalCotizaciones.PrincipalCotizaciones.btnGuardarEvaluarAmicar(driver).click();
						mensaje = "Flujo : Se presiona Boton 'Guardar evaluar Amicar' ";
						msj = "ClickBtnGuardarEvaluarAmicar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(1000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						// if (MenuPrincipal.textoNotificacionesVerdeDos(driver).getText()
						String msjTextoNotificacion = MenuPrincipal.textoNotificacionesEvaluar(driver).getText();
						System.err.println("MEnsaje " + msjTextoNotificacion);
						if ((MenuPrincipal.textoNotificacionesEvaluar(driver).getText()
								.contains("El registro se ha guardado exitosamente"))
								|| (MenuPrincipal.textoNotificacionesVerdeDos(driver).getText()
										.contains("El registro se ha guardado exitosamente"))) {

							ValorEncontrado = true;
							mensaje = "OK : Aparece mensaje �El registro se ha guardado exitosamente�";
							Log.info(mensaje);
							Reporter.log("<br>" + "<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							int esperarMensajeSimulacion = 0;
							while (esperarMensajeSimulacion == 0) {
								if (driver
										.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']"))
										.size() > 0) {
									esperarMensajeSimulacion = 0;
								} else {
									esperarMensajeSimulacion = 1;
								}
							}

							Thread.sleep(2000);

							// System.err.println("ACA");
							Thread.sleep(30000);

							// Thread.sleep(99999999);

							principalCotizaciones.PrincipalCotizaciones.btnEnviarEvaluacion(driver).click();
							mensaje = "Flujo : Se presiona Boton 'Enviar Evaluacion' ";
							msj = "ClickBtnEnviarEvaluacion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							esperarMensajeSimulacion = 0;
							while (esperarMensajeSimulacion == 0) {
								if (driver
										.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']"))
										.size() > 0) {
									esperarMensajeSimulacion = 0;
								} else {
									esperarMensajeSimulacion = 1;
								}
							}

							Thread.sleep(1000);

							// try {

							// String textoRevisarError = new WebDriverWait(driver, 20, 100)
							// .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
							// "/html/body/div[5]/div[1]/section/div[1]/div/div/div/div/div/span")))
							// .getText();
							// System.err.println("Texto :
							// "+driver.findElement(By.xpath("(.//*[normalize-space(text()) and
							// normalize-space(.)='History'])[2]/following::div[22]")).getText());
							// System.err.println("CSS Texto :
							// "+driver.findElement(By.cssSelector("div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage")).getText());
							// String textoRevisarError = driver
							// .findElement(By.xpath(
							// "/html/body/div[5]/div[1]/section/div[1]/div/div/div/div/div/span"))
							// .getText();
							//
							// if (textoRevisarError
							// .contains("Debe volver a simular, existe diferencia por un monto de:")) {
							// mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ;" +
							// textoRevisarError
							// + "<p>";
							// msj = "ErrorNoExisteBoton";
							// posicionEvidencia = posicionEvidencia + 1;
							// posicionEvidenciaString = Integer.toString(posicionEvidencia);
							// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
							// nombreCaptura + "_" + msj);
							// Log.info(mensaje);
							// Reporter.log("<p>" + mensaje + "</p>"
							// + "<img src=" + rutaEvidencia + ">");
							// System.err.println(mensaje);
							// // driver.close();
							// ValorEncontrado = false;
							// errorBuffer.append("\n" + mensaje + "\n");
							// throw new AssertionError(errorBuffer.toString());
							// }
							//
							// } catch (ElementNotVisibleException | NoSuchElementException |
							// ScreenshotException e) {
							//
							// }
							int largo = 0;
							// int largo2 = 0;
							// String largo2Text = "";
							// int largo3 = 0;
							// String largo3Text = "";
							int esperarMensaje = 0;
							int contadorQuiebre = 0;
							String textoRevisar = "Vacio";
							String textoRevisarDetalle = "Vacio";
							while (esperarMensaje == 0) {

								if (contadorQuiebre == 15) {

									ValorEncontrado = false;
									mensaje = "<p style='color:red;'>"
											+ "Error: NO aparece mensaje de 'Estado de Envio de Evaluacion'" + "<p>";
									Log.info(mensaje);
									Reporter.log("<br>" + mensaje);
									System.out.println(mensaje);
									esperarBtnEvaluarAmicar = 0;
									driver.close();
									errorBuffer.append("\n" + mensaje + "\n");
									throw new AssertionError(errorBuffer.toString());

								}

								// largo =
								// driver.findElements(By.cssSelector("div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage")).size();
								largo = principalCotizaciones.PrincipalCotizaciones
										.txtMensajeEnvioEvaluacionColeccion(driver).size();
								System.err.println("Largo " + largo);
								System.err.println("Largo texto " + principalCotizaciones.PrincipalCotizaciones
										.txtMensajeEnvioEvaluacion(driver).getText());
								//
								//
								// largo2 = driver.findElements(By.xpath("(.//*[normalize-space(text()) and
								// normalize-space(.)='History'])[2]/following::div[22]")).size();
								// largo2Text = driver.findElement(By.xpath("(.//*[normalize-space(text()) and
								// normalize-space(.)='History'])[2]/following::div[22]")).getText();
								// System.err.println("Largo2 "+largo2);
								// System.err.println("Largo2 texto "+largo2Text);
								//
								// largo3 =
								// driver.findElements(By.cssSelector("span.toastMessage.forceActionsText")).size();
								// largo3Text =
								// driver.findElement(By.cssSelector("span.toastMessage.forceActionsText")).getText();
								// System.err.println("Largo3 "+largo3);
								// System.err.println("Largo3 texto "+largo3Text);

								if (largo == 0) {

									esperarMensaje = 0;
									contadorQuiebre = contadorQuiebre + 1;
									System.err.println("contadorQuiebre " + contadorQuiebre);

								} else {
									try {
										// textoRevisar =
										// driver.findElement(By.cssSelector("div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage")).getText();
										textoRevisar = principalCotizaciones.PrincipalCotizaciones
												.txtMensajeEnvioEvaluacion(driver).getText();
										System.err.println("textoRevisar " + textoRevisar);
										// break;

									} catch (ElementNotVisibleException | NoSuchElementException
											| ScreenshotException e) {
										textoRevisar = "Vacio";
										break;
									}
									// esperarMensaje = 0;
									// contadorQuiebre = contadorQuiebre +1;
									// System.err.println(textoRevisar + " "+contadorQuiebre);
									// try {
									// textoRevisar = driver.findElement(By.xpath(
									// "(.//*[normalize-space(text()) and
									// normalize-space(.)='History'])[2]/following::div[22]"))
									// .getText();
									// System.err.println("textoRevisar 2 " + textoRevisar);
									//// break;
									//
									// } catch (ElementNotVisibleException | NoSuchElementException
									// | ScreenshotException k) {
									// textoRevisar = "Vacio";
									//// esperarMensaje = 0;
									//// contadorQuiebre = contadorQuiebre + 1;
									// System.err.println("3 " + textoRevisar + " " + contadorQuiebre);
									// break;
									// }
									// }

								}

								try {
									textoRevisarDetalle = principalCotizaciones.PrincipalCotizaciones
											.txtMensajeEnvioEvaluacionDetalle(driver).getText();
									System.err.println("textoRevisarDetalle " + textoRevisarDetalle);
								} catch (Exception e) {
									System.err.println("textoRevisarDetalle error");
								}

								if (textoRevisar.equals("Vacio")) {

								} else {

									if (textoRevisar.contains("�xito!")) {

										ValorEncontrado = true;
										// mensaje = "OK : Se aparece mensaje �El envio a evaluar se realiz�
										// exitosamente�";
										mensaje = "OK : Existe mensaje '" + textoRevisarDetalle + "'";
										// Log.info(mensaje);
										// Reporter.log("<br>"+"<p>" + mensaje + "</p>");
										// System.out.println(mensaje);
										posicionEvidencia = posicionEvidencia + 1;
										posicionEvidenciaString = Integer.toString(posicionEvidencia);
										nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
										rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
												nombreCaptura + "_" + msj);
										Log.info(mensaje);
										Reporter.log("<p>" + mensaje + "</p>"
												+ "<img src=" + rutaEvidencia + ">");
										System.out.println(mensaje);

										esperarBtnEvaluarAmicar = 1;
										ValorEncontrado = true;
										break;

									} else {

										if ((textoRevisar.contains("error")) | (textoRevisar.contains("Error"))) {

											mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; "
													+ textoRevisarDetalle + "<p>";
											msj = "ErrorNoExisteBoton";
											posicionEvidencia = posicionEvidencia + 1;
											posicionEvidenciaString = Integer.toString(posicionEvidencia);
											nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
											rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
													nombreCaptura + "_" + msj);
											Log.info(mensaje);
											Reporter.log("<p>" + mensaje + "</p>"
													+ "<img src=" + rutaEvidencia
													+ ">");
											System.err.println(mensaje);
											driver.close();
											ValorEncontrado = false;
											errorBuffer.append("\n" + mensaje + "\n");
											throw new AssertionError(errorBuffer.toString());

										} else {

											ValorEncontrado = false;
											mensaje = "<p style='color:red;'>" + "Error: " + textoRevisarDetalle
													+ "<p>";
											Log.info(mensaje);
											Reporter.log("<br>" + mensaje);
											System.out.println(mensaje);
											esperarBtnEvaluarAmicar = 0;
											driver.close();
											errorBuffer.append("\n" + mensaje + "\n");
											throw new AssertionError(errorBuffer.toString());

										}
									}
									// {
									// ValorEncontrado = false;
									// mensaje = "<p style='color:red;'>"
									// + "Error: NO aparece mensaje �El registro se ha enviado exitosamente� " +
									// "<p>";
									// Log.info(mensaje);
									// Reporter.log("<br>" + mensaje);
									// System.out.println(mensaje);
									// esperarBtnEvaluarAmicar = 0;
									// driver.close();
									// errorBuffer.append("\n" + mensaje + "\n");
									//
									// }

								}
							}
						} else {

							mensaje = "<p style='color:red;'>"
									+ "Error: NO aparece mensaje �El registro se ha guardado exitosamente� , "
									+ msjTextoNotificacion + "<p>";
							Log.info(mensaje);
							Reporter.log("<br>" + "<p>" + mensaje + "</p>");
							System.out.println(mensaje);
							ValorEncontrado = false;
							driver.close();
							errorBuffer.append("\n" + mensaje + "\n");
							throw new AssertionError(errorBuffer.toString());
						}

						Thread.sleep(2000);

					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
						
						System.err.println("Error "+e.getMessage());
						
						mensaje = "<p style='color:red;'>"
								+ "ERROR: Prueba Fallida ; No encuentra elemento de formulario 'Evaluar Amicar' o esta deshabilitado"
								+ "<p>";
						msj = "ErrorNoExisteBoton";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());

					}

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					esperarBtnEvaluarAmicar = 0;
					contadorEspera = contadorEspera + 1;
					if (contadorEspera == 15) {
						mensaje = "<p style='color:red;'>"
								+ "ERROR: Prueba Fallida ; No Existe Boton 'Evaluar Amicar' o deshabilitado" + "<p>";
						msj = "ErrorNoExisteBoton";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());

					}
				}

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
						+ "OK: Prueba Correcta ; Se Envia 'Evaluar Amicar de manera correcta' " + "<p>";
				msj = "OkIngresaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
						+ "ERROR: Prueba Fallida ; NO Se Envia 'Evaluar Amicar de manera correcta' " + "<p>";
				msj = "ErrorNoCreaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorNoEncuentraElemento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());

		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();

	}
}
