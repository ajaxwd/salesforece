package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00103_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	
	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		// Funciones Func_exec = new Funciones();
		
		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {

				posicionExcel = i;

				break;

			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = ExcelUtils.getCellData(1, 11);
		String busquedaOportunidad = ExcelUtils.getCellData(1, 4);
		String tipoDescuento = ExcelUtils.getCellData(1, 24);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		String precioConDescuento = "";

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			Thread.sleep(3000);

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver)))
					.click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/td[2]/span/a")));
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(3000);

						JavascriptExecutor js = (JavascriptExecutor) driver;
						js.executeScript("window.scrollBy (0,300)");

						Thread.sleep(5000);

						int swDetalles = 0;
						while (swDetalles == 0) {

							try {

								int ok_size = driver.findElements(By.xpath("//*[@title='Detalles']")).size();
								driver.findElements(By.xpath("//*[@title='Detalles']")).get(ok_size - 1)
										.sendKeys(Keys.ENTER);
								swDetalles = 1;

							} catch (NoSuchElementException | ScreenshotException e) {
								swDetalles = 0;
							}

						}

						mensaje = "Flujo : Se Presiona Detalles";
						msj = "SeleccionDeDetalles";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(2000);

						String precioSinDescuento = new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOf(
										principalCotizaciones.PrincipalCotizaciones.inputPrecioListaNeto(driver)))
								.getText();
						
						int recorreExcell = 0;
						int inicioExcell = 0;
						String paisEjecucion = "";

						while (recorreExcell == 0) {
							if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
								mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
								// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
								System.out.println(mensaje);
								Reporter.log(mensaje);
								Log.info(mensaje);
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							if (inicioExcell > 150) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No encuentra elemento 'Pais' en Archivo de Ingreso de datos Favor Revisar"
										+ "</p>";
								Log.info(mensaje);
								Reporter.log(mensaje);
								System.err.println(mensaje);
								driver.quit();
								throw new AssertionError();
							}
						}

						if (paisEjecucion.equals("Per�")) {
							
							
							precioConDescuento = "1000";

							
						} else {

							precioSinDescuento = precioSinDescuento.replace(".", "");
							precioSinDescuento = precioSinDescuento.replace("CLP ", "");
							int precio = Integer.parseInt(precioSinDescuento);
							int precioDescuento = (precio * 1) / 100;
							precioDescuento = Math.round(precioDescuento);
							precioConDescuento = Integer.toString(precioDescuento);

							mensaje = "Flujo : Se Obtiene el 1% del Neto Para Aplicar Descuento";
							Log.info(mensaje);
							Reporter.log("<br>" + mensaje);
							System.out.println(mensaje);
						}
						
						

						Thread.sleep(2000);

						int swRelacionado = 0;
						while (swRelacionado == 0) {

							try {

								int ok_size = driver.findElements(By.xpath("//*[@title='Relacionado']")).size();
								driver.findElements(By.xpath("//*[@title='Relacionado']")).get(ok_size - 1)
										.sendKeys(Keys.ENTER);
								swRelacionado = 1;

							} catch (NoSuchElementException | ScreenshotException e) {
								swRelacionado = 0;
							}

						}

						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(4000);

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy (0,300)");

			Thread.sleep(5000);

			int swNuevo = 0;
			while (swNuevo == 0) {

				try {

					// int ok_size = driver.findElements(By.xpath("(//a[@title='Nuevo'])")).size();
					// System.out.println("Cuenta "+ok_size);
					// driver.findElements(By.xpath("(//a[@title='Nuevo'])[5]")).get(ok_size - 1)
					// .sendKeys(Keys.ENTER);
					WebElement btnNuevo = driver.findElement(By.xpath("(//a[@title='Nuevo'])[2]"));
					btnNuevo.sendKeys(Keys.ENTER);
					mensaje = "Flujo : Se Presiona Boton 'Nuevo Descuento' ";
					msj = "ClickBtnNuevoDcto";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
					swNuevo = 1;

				} catch (NoSuchElementException | ScreenshotException e) {
					swNuevo = 0;
				}

			}

			// posicion = 1;
			// sw = 0;
			// int validarElemento = 0;
			// while (sw == 0) {
			//
			// try {
			//
			// validarElemento = 0;
			// while (validarElemento == 0) {
			//
			// try {
			// // int ok_size =
			// //
			// driver.findElements(By.xpath("/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[2]/article[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/a[1]/div[1]")).size();
			// driver.findElement(By.xpath("//*[@title='Nuevo']")).sendKeys(Keys.ENTER);
			// validarElemento = 1;
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			// validarElemento = 0;
			// }
			//
			// }
			//
			// // new WebDriverWait(driver, 20, 100)
			// // .until(ExpectedConditions
			// //
			// .visibilityOf(principalOportunidades.PrincipalOportunidades.btnNuevoDescuento(driver)))
			// // .sendKeys(Keys.RETURN);
			// mensaje = "Flujo : Se Presiona Boton 'Nuevo Descuento' ";
			// msj = "ClickBtnNuevoDcto";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			// sw = 1;
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			// sw = 0;
			// }
			// }

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(tipoDescuento);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Tipo De Descuento' ";
			msj = "TipoDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// mark[contains(text(),'Descuento')]
			Thread.sleep(1000);
			// String obtenerTextoDescuentoABuscar =
			// driver.findElement(By.xpath("//div[@title='" + tipoDescuento +
			// "']")).getText();
			String obtenerTextoDescuentoABuscar = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]/div[1]"))
					.getText();

			// WebElement descuento = driver.findElement(By.xpath("//div[@title='" +
			// tipoDescuento + "']"));
			WebElement descuento = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/ul[1]/li[1]/a[1]/div[2]"));

			descuento.click();

			// Thread.sleep(1000);
			// String obtenerTextoDescuentoABuscar = new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" +
			// tipoDescuento + "']")))
			// .getText();
			//
			// Thread.sleep(1000);
			// new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" +
			// tipoDescuento + "']")))
			// .click();
			mensaje = "Flujo : Se Selecciona 'Descuento' " + tipoDescuento;
			msj = "SeleccionDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputMontoDescuento(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputMontoDescuento(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputMontoDescuento(driver).sendKeys(precioConDescuento);
			Thread.sleep(500);
			// principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).sendKeys("
			// ");
			mensaje = "Flujo : Se Ingresa 'Monto De Descuento' " + precioConDescuento;
			msj = "MontoDeDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.visibilityOf(principalOportunidades.PrincipalOportunidades.btnGuardarAgregarDescuento(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar Descuento' ";
			msj = "ClickBtnGuardarDcto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			mensaje = "Flujo : Se Comprueba existencia de ingresos ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(7000);

			posicion = 1;
			sw = 0;
			String txtDescuentoCotizacionValidar = "";
			while (sw == 0) {
				try {

					txtDescuentoCotizacionValidar = driver.findElement(
							By.xpath("//div[2]/article/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr[" + posicion
									+ "]/td/span"))
							.getText();
					System.err.println(txtDescuentoCotizacionValidar);
					System.err.println(obtenerTextoDescuentoABuscar);
					// int swRelacionado = 0;
					// while (swRelacionado == 0) {
					//
					// try {

					// int ok_size = driver.findElements(By.xpath("//span[@title='" + tipoDescuento
					// + "']")).size();
					// txtDescuentoCotizacionValidar = driver.findElement(By.xpath("//span[@title='"
					// + tipoDescuento + "']")).getText();
					// System.err.println(txtDescuentoCotizacionValidar);
					// swRelacionado = 1;
					//
					// } catch (NoSuchElementException | ScreenshotException e) {
					// swRelacionado = 0;
					// }
					//
					// }

					// WebElement descuentoCotizacionValidar = driver
					// .findElement(By.xpath("//span[@title='" + tipoDescuento + "']"));
					// txtDescuentoCotizacionValidar = descuentoCotizacionValidar.getText();

					if (txtDescuentoCotizacionValidar.equals(obtenerTextoDescuentoABuscar)) {

						ValorEncontrado = true;
						mensaje = "OK : Se Comprueba existencia de descuentos creados ";
						msj = "OkExistenDescuentos";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						sw = 1;

					} else {
						posicion = posicion + 1;
						sw = 0;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Existen descuentos creados"
							+ "<p>";
					msj = "ErrorNoExistenDescuentos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					sw = 1;
					ValorEncontrado = false;
				}
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; se ingresan descuento "
						+ txtDescuentoCotizacionValidar + "<p>";
				msj = "OkIngresaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se crea descuentos" + "<p>";
				msj = "ErrorNoCreaDescuento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.err.println(mensaje);
			// driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());

		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
