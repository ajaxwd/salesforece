package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00064_SCN_AC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = Funciones.numeroAstring(sNumeroEscenario);
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "CuentasClientes.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja2");
		String nombreCotizacion = "";
		String busquedaOportunidad = "";
		String tipoVehiculo = "";
		String tipoCredito = "";
		String pieSimulacionAmicar = "";
		String cuotasSimulacionAmicar = "";

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			String paisEjecucion = "";
			String moneda = "";

			nombreCotizacion = Funciones.getExcel(driver, "Cotizacion", posicionExcel);
			busquedaOportunidad = Funciones.getExcel(driver, "Oportunidad", posicionExcel);
			tipoVehiculo = Funciones.getExcel(driver, "Tipo de Vehiculo", posicionExcel);
			tipoCredito = Funciones.getExcel(driver, "Tipo de Credito Amicar", posicionExcel);
			pieSimulacionAmicar = Funciones.getExcel(driver, "Pie", posicionExcel);
			cuotasSimulacionAmicar = Funciones.getExcel(driver, "Cuotas", posicionExcel);
			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			moneda = Funciones.getExcel(driver, "Moneda", posicionExcel);

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
					driver.findElement(By.xpath(
					"(//h2[contains(text(),'Llamada de Teléfono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
					.click();
					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Funciones.mensajesLog(mensaje);
					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) { cierraLlamado = 1; }
			}
			
			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(3000);

			int esperarBtnSimularAmicar = 0;
			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver)))
			.click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[2]/span/a")));
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(1000);
						sw = 0;
						break;
					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;
				}
			}

			Thread.sleep(4000);

			while (esperarBtnSimularAmicar == 0) {

				try {
					Thread.sleep(20000);
					new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
					.visibilityOf(principalCotizaciones.PrincipalCotizaciones.btnSimularAmicar(driver)))
					.sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se presiona Boton 'Simular Amicar' ";
					msj = "ClickBtnSimularAmicar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					esperarBtnSimularAmicar = 1;

					Thread.sleep(3000);

					try {
						String contenidoError = MenuPrincipal.textoNotificacionesVerde(driver).getText();
						String errorMsj = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
						try {
							String textoNotificacionesVerdePequeno = MenuPrincipal
							.textoNotificacionesVerdePequeno(driver).getText();
							System.err.println("textoNotificacionesVerdePequeno " + textoNotificacionesVerdePequeno);
						} catch (TimeoutException | NoSuchElementException e) {

						}

						if (errorMsj.contains("Error")) {

							mensaje = "<p style='color:red;'>"
							+ "Error : Fin de ejecucion.......se encontraron errores, " + contenidoError
							+ "</p>";
							msj = "SeEncuentranErrores";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							driver.quit();
							throw new AssertionError(errorBuffer.toString());
						}
					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

					}

					try {

						principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText();

						if (principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText()
							.equals("Debe agregar producto a la cotización antes de simular.")) {
							mensaje = "<p style='color:red;'>"
							+ "Error : Fin de ejecucion.......se encontraron errores, "
							+ principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver).getText()
							+ "</p>";
							msj = "SeEncuentranErrores";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							driver.quit();
							throw new AssertionError(errorBuffer.toString());
						}

					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

					}

				} catch (NoSuchElementException | ScreenshotException e) { esperarBtnSimularAmicar = 0; }
			}

			Thread.sleep(7000);

			switch (paisEjecucion) {

			case "Chile":

				break;

			case "Per�":

				new WebDriverWait(driver, 30, 100).until(ExpectedConditions
				.elementToBeClickable(principalCotizaciones.PrincipalCotizaciones.selectTipoMoneda(driver)));
				Select selectMoneda = new Select(principalCotizaciones.PrincipalCotizaciones.selectTipoMoneda(driver));
				selectMoneda.selectByVisibleText(moneda);
				mensaje = "Flujo : Se Seleciona tipo de Moneda " + moneda;
				msj = "SeleccionVehiculo";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				Thread.sleep(3000);

				break;

			default:
				break;
			}

			new WebDriverWait(driver, 30, 100).until(ExpectedConditions
			.elementToBeClickable(principalCotizaciones.PrincipalCotizaciones.selectTipoVehiculo(driver)));
			Select selectVehiculo = new Select(principalCotizaciones.PrincipalCotizaciones.selectTipoVehiculo(driver));
			selectVehiculo.selectByVisibleText(tipoVehiculo);
			mensaje = "Flujo : Se Seleciona tipo de Vehiculo " + tipoVehiculo;
			msj = "SeleccionVehiculo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(3000);
			try {

				int recorreExcell = 0;
				int inicioExcell = 0;
				String marcaUsado = "";
				marcaUsado = Funciones.getExcel(driver, "MarcaUsado", posicionExcel);
			
				if (marcaUsado != "") {
					new WebDriverWait(driver, 30, 100).until(ExpectedConditions.elementToBeClickable(
					principalCotizaciones.PrincipalCotizaciones.selectMarcaVehiculo(driver)));
					Select selectMarcaVehiculo = new Select(
					principalCotizaciones.PrincipalCotizaciones.selectMarcaVehiculo(driver));
					selectMarcaVehiculo.selectByVisibleText(marcaUsado);
					mensaje = "Flujo : Se Seleciona Marca de Vehiculo " + marcaUsado;
					msj = "SeleccionMarcaVehiculo";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}

				Thread.sleep(5000);
			} catch (NoSuchElementException | ScreenshotException e) {

			}

			Thread.sleep(3000);

			try {

				int recorreExcell = 0;
				int inicioExcell = 0;
				String modelo = "";
				modelo = Funciones.getExcel(driver, "Modelo", posicionExcel);
		
				if (modelo != "") {
					new WebDriverWait(driver, 30, 100).until(ExpectedConditions.elementToBeClickable(
					principalCotizaciones.PrincipalCotizaciones.selectModeloVehiculo(driver)));
					Select selectModeloVehiculo = new Select(
					principalCotizaciones.PrincipalCotizaciones.selectModeloVehiculo(driver));
					selectModeloVehiculo.selectByVisibleText(modelo);
					mensaje = "Flujo : Se Seleciona Modelo de Vehiculo " + modelo;
					msj = "SeleccionModeloVehiculo";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				}
				Thread.sleep(2000);
			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.visibilityOf(principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver)))
			.click();
			principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver).clear();
			principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver).sendKeys(pieSimulacionAmicar);
			mensaje = "Flujo : Ingreso de Pie en Simulacion " + pieSimulacionAmicar;
			msj = "IngresoPie";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 30, 100).until(ExpectedConditions.elementToBeClickable(principalCotizaciones.PrincipalCotizaciones.selectTipoCreditoAmicar(driver)));
			Select selectCredito = new Select(
			principalCotizaciones.PrincipalCotizaciones.selectTipoCreditoAmicar(driver));
			selectCredito.selectByVisibleText(tipoCredito);
			mensaje = "Flujo : Se Seleciona tipo de Credito Amicar " + tipoCredito;
			msj = "SeleccionTipoCredito";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);
			
			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.visibilityOf(principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver)))
			.click();
			principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver).clear();
			principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver)
			.sendKeys(cuotasSimulacionAmicar);

			mensaje = "Flujo : Ingreso de Coutas en Simulacion " + cuotasSimulacionAmicar;
			msj = "IngresoCuotas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			Thread.sleep(5000);
			
			try {
				principalCotizaciones.PrincipalCotizaciones.btnBonoAmicar(driver).click();
				mensaje = "Flujo : Se presiona boton 'Bono Amicar'";
				msj = "ClickBtnBonoAmicar";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (NoSuchElementException | ScreenshotException e) {

			}

			Thread.sleep(3000);

			try {

				String textoError = driver.findElement(By.xpath(
				"//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']"))
				.getText();
				String contenidoError = driver.findElement(By.xpath("//span[@class='toastMessage forceActionsText']"))
				.getText();
				System.err.println("Cont Error " + contenidoError);
				if (textoError.contains("Error")) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
					+ contenidoError + "<p>";
					msj = "ErrorNoExistenPagos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					driver.close();
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (NoSuchElementException | ScreenshotException e1) {

			}

			Thread.sleep(20000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions
			.elementToBeClickable(principalCotizaciones.PrincipalCotizaciones.btnSimular(driver)))
			.click();
			mensaje = "Flujo : Se presiona boton 'Simular'";
			msj = "ClickBtnSimular";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String textoNotificacion = "";
			int esperarMensajeSimulacion = 0;
			while (esperarMensajeSimulacion == 0) {
				if (driver.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']")).size() > 0) {
					esperarMensajeSimulacion = 0;
				} else {
					mensaje = "Flujo : Mensaje 'Simulacion Amicar'";
					msj = "MsjSimulacionAmicar";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					esperarMensajeSimulacion = 1;
				}
			}

			int mensajeTextoVF = 0;
			while (mensajeTextoVF == 0) {
				
				try {
					textoNotificacion = MenuPrincipal.textoNotificacionesVerde(driver).getText();
					System.err.println("textoNotificacionesVerde " + textoNotificacion);
					if (textoNotificacion.contains("exitosa.")) {
						mensajeTextoVF = 1;
						break;
					}
					if (textoNotificacion.contains("Error")) {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
						+ textoNotificacion + "<p>";
						msj = "ErrorNoExistenPagos";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						mensajeTextoVF = 1;
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				} catch (TimeoutException | NoSuchElementException e) {

				}

				try {
				textoNotificacion = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
				System.err.println("textoNotificacionesVerdeDos " + textoNotificacion);
				System.err.println("textoNotificacionesVerde " + textoNotificacion);
				if (textoNotificacion.contains("exitosa.")) {
					mensajeTextoVF = 1;
					break;
				}
				if (textoNotificacion.contains("Error")) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
					+ textoNotificacion + "<p>";
					msj = "ErrorNoExistenPagos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					mensajeTextoVF = 1;
					driver.close();
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
				} catch (TimeoutException | NoSuchElementException e) {

				}
				try {
					textoNotificacion = MenuPrincipal.textoNotificacionesVerdePequeno(driver)
					.getText();
					System.err.println("textoNotificacionesVerdePequeno " + textoNotificacion);
					System.err.println("textoNotificacionesVerde " + textoNotificacion);
					if (textoNotificacion.contains("exitosa.")) {
						mensajeTextoVF = 1;
						break;
					}
					if (textoNotificacion.contains("Error")) {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
						+ textoNotificacion + "<p>";
						msj = "ErrorNoExistenPagos";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						mensajeTextoVF = 1;
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				} catch (TimeoutException | NoSuchElementException e) { }
			}
			
			if (textoNotificacion.contains("Simulación exitosa.")) {
				
			}else {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
				+ "<p>";
				msj = "ErrorNoExistenPagos";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				driver.close();
				ValorEncontrado = false;
				errorBuffer.append("\n" + mensaje + "\n");
				throw new AssertionError(errorBuffer.toString());
			}
			
			try {

				String textoError = driver.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']"))
				.getText();
				String contenidoError = driver.findElement(By.xpath(
				"(//div[@class='toastTitle slds-text-heading--small'])[1]/following::span[@class='toastMessage forceActionsText'][1]"))
				.getText();
				System.err.println("Cont Error " + contenidoError);
				if (textoError.contains("Error")) {
					mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Error Mensaje de Pago : "
					+ contenidoError + "<p>";
					msj = "ErrorNoExistenPagos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					driver.close();
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					throw new AssertionError(errorBuffer.toString());
				}
			} catch (NoSuchElementException | ScreenshotException e1) { sw = 0; }

			int esperarPantallaPrincipal = 0;
			while (esperarPantallaPrincipal == 0) {
				try {
					
					Thread.sleep(2000);

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath(
					"//span[contains(text(),'Relacionado')]//following::span[contains(text(),'Detalles')]")));
					Thread.sleep(2000);
					((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-200)", "");

					Thread.sleep(4000);

					mensaje = "Flujo : Se presiona boton 'Detalles' para verificar creacion de simulacion";
					msj = "ClickBtnDetalles";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					WebElement clickDetalles = driver.findElement(By.xpath(
					"(//*[contains(text(),'Relacionado')]//following::span[@class='title']//preceding::a[@title='Detalles'])[2]"));
					clickDetalles.sendKeys(Keys.ENTER);
					esperarPantallaPrincipal = 1;
				} catch (NoSuchElementException | ScreenshotException e) { esperarPantallaPrincipal = 0; }
			}

			Thread.sleep(7000);
			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
			.visibilityOf(principalCotizaciones.PrincipalCotizaciones.txtVerificarCotizacion(driver)));
			Thread.sleep(2000);
			String verificaCotizacion = principalCotizaciones.PrincipalCotizaciones.txtVerificarCotizacion(driver)
			.getText();

			if (verificaCotizacion.contains("Creada")) {
				mensaje = "Ok : Cotizacion Creada ";
				msj = "OkCotizacionCreada";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;

			} else {
				mensaje = "ERROR : Cotizacion no Creada ";
				msj = "ErrorCotizacionNoCreada";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Simulacion " + "<p>";
				msj = "OkCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Se Crea Simulacion" + "<p>";
				msj = "ErrorNoCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
