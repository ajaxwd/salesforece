package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00101 {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		// Funciones Func_exec = new Funciones();

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */

		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "COT_SP7_00101.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String formaDePago = ExcelUtils.getCellData(1, 0);
		String listaPrecio = ExcelUtils.getCellData(1, 1);
		String productoABuscar = ExcelUtils.getCellData(1, 2);
		String tipoDescuento = ExcelUtils.getCellData(1, 3);
		String cantidadDescuento = ExcelUtils.getCellData(1, 4);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).click();
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOPortunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(4000);

			principalOportunidades.PrincipalOportunidades.linkPrimerRegistroDesplegado(driver).click();
			mensaje = "Flujo : Click Primer Registro Desplegado en Grilla";
			msj = "ClickPrimerRegistroGrilla";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(principalOportunidades.PrincipalOportunidades.btnNuevaCotizacion(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Nueva Cotizacion' ";
			msj = "ClickBtnNvaCotizacion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			String nombreCotizacion = new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.txtNombreCotizacion(driver)))
					.getText();

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(formaDePago);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Forma de Pago' ";
			msj = "IngresoFormadePago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + formaDePago + "']")))
					.click();
			mensaje = "Flujo : Se Selecciona 'Forma de Pago' " + formaDePago;
			msj = "SeleccionFormaPago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnGuardarCotizacion(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton Guardar Cotizacion";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(4000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Cotizaciones' para revisar";
			msj = "ClickBtnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			mensaje = "Flujo : Entra a loop para buscar cotizacion ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			int posicion = 1;
			int contador = 0;
			int sw = 0;
			while (sw == 0) {
				try {
					WebElement contar = driver.findElement(By.xpath(
							"/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr["
									+ posicion + "]/td[2]/span[1]/a[1]"));
					contar.getText();
					posicion = posicion + 1;
					contador = contador + 1;
					sw = 0;

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 1;
				}
			}
			sw = 0;
			posicion = contador;

			while (sw == 0) {
				try {
					WebElement registro = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.elementToBeClickable(By.xpath(
									"/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr["
											+ posicion + "]/td[2]/span[1]/a[1]")));

					if (registro.getText().contains(nombreCotizacion)) {
						registro.click();
						mensaje = "OK : Se Encuentra Cotizacion " + nombreCotizacion;
						msj = "EncuentraCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						sw = 1;

					} else {
						sw = 0;
						posicion = posicion - 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					mensaje = "<p style='color:red;'>" + "ERROR : Cotizacion no Encontrada " + "</p>";
					msj = "ErrorNoEncuentraCotizacion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					sw = 1;
					errorBuffer.append("\n" + mensaje + "\n");
					// driver.quit();
					throw new AssertionError(errorBuffer.toString());

				}
			}

			Thread.sleep(3000);

			int esperarBtnAgregarProductos = 0;

			while (esperarBtnAgregarProductos == 0) {

				Thread.sleep(3000);
				try {
					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.elementToBeClickable(
									principalOportunidades.PrincipalOportunidades.btnAgregarProductos(driver)))
							.sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se Presiona Boton 'Agregar Productos' cotizaciones";
					msj = "ClickBtnAgregarProductos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
					esperarBtnAgregarProductos = 1;

				} catch (NoSuchElementException | ScreenshotException e) {
					esperarBtnAgregarProductos = 0;
				}

			}

			Thread.sleep(2000);

			try {

				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
						principalOportunidades.PrincipalOportunidades.selectListaDePrecio(driver))).click();
				mensaje = "Flujo : Se Presiona Select Lista de Precios";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

				Thread.sleep(2000);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='" + listaPrecio + "']")))
						.click();
				mensaje = "Flujo : Se Selecciona 'Lista de Precios' " + listaPrecio;
				msj = "SeleccionListaDePrecios";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(2000);

				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
						principalOportunidades.PrincipalOportunidades.btnGuardarSeleccionarListaDePrecio(driver)))
						.click();
				mensaje = "Flujo : Se Presiona Guardar Lista de Precios";
				msj = "ClickBtnGuardar";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {
				mensaje = "Flujo : No desplego seleccion de listas, solo tiene una lista";
				msj = "NoHayListas";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).sendKeys(productoABuscar);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Busqueda Producto' ";
			msj = "IngresoBusquedaProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);
			String obtenerTextoProductoABuscar = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + productoABuscar + "']")))
					.getText();
			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + productoABuscar + "']")))
					.click();
			mensaje = "Flujo : Se Selecciona 'Producto' " + productoABuscar;
			msj = "SeleccionProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.visibilityOf(principalOportunidades.PrincipalOportunidades.btnSiguienteAgregarProducto(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Siguiente' ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(principalOportunidades.PrincipalOportunidades.inputCantidadProducto(driver)))
					.click();

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.inputCantidadProductoIngreso(driver)))
					// .sendKeys(Keys.NUMPAD1);
					.sendKeys("1");

			mensaje = "Flujo : Se Ingresa Cantidad de Productos";
			msj = "SeleccionCantidadProductos";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.visibilityOf(principalOportunidades.PrincipalOportunidades.btnGuardarAgregarProducto(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar' ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(principalOportunidades.PrincipalOportunidades.btnNuevoDescuento(driver)))
					.sendKeys(Keys.RETURN);
			mensaje = "Flujo : Se Presiona Boton 'Nuevo Descuento' ";
			msj = "ClickBtnNuevoDcto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(tipoDescuento);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputTipoDescuento(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Tipo De Descuento' ";
			msj = "TipoDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);
			String obtenerTextoDescuentoABuscar = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + tipoDescuento + "']")))
					.getText();

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + tipoDescuento + "']")))
					.click();
			mensaje = "Flujo : Se Selecciona 'Descuento' " + tipoDescuento;
			msj = "SeleccionDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
					principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver))).click();
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).sendKeys(cantidadDescuento);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputPorcentajeDescuento(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Cantidad De Descuento' " + cantidadDescuento + "%";
			msj = "CantidadDeDescuento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.visibilityOf(principalOportunidades.PrincipalOportunidades.btnGuardarAgregarDescuento(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar Descuento' ";
			msj = "ClickBtnGuardarDcto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			mensaje = "Flujo : Se Comprueba existencia de ingresos ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(1000);

			posicion = 1;
			sw = 0;
			String txtDescuentoCotizacionValidar = "";
			try {
				WebElement descuentoCotizacionValidar = driver
						.findElement(By.xpath("//span[@title='" + tipoDescuento + "']"));
				txtDescuentoCotizacionValidar = descuentoCotizacionValidar.getText();
				if (txtDescuentoCotizacionValidar.contains(obtenerTextoDescuentoABuscar)) {
					if (principalOportunidades.PrincipalOportunidades.txtProductoCotizacionValidar(driver).getText()
							.contains(obtenerTextoProductoABuscar)) {
						ValorEncontrado = true;
						mensaje = "OK : Se Comprueba existencia de Productos y descuentos creados ";
						msj = "OkExistenProductos";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						sw = 1;
					}
				} else {
					posicion = posicion + 1;
					sw = 0;
				}
			} catch (NoSuchElementException | ScreenshotException e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Existen Productos y descuentos creados"
						+ "<p>";
				msj = "ErrorNoExistenProductos";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
				sw = 1;
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se crea Cotizacion, se ingresan productos "
						+ obtenerTextoProductoABuscar + " y descuentos " + txtDescuentoCotizacionValidar + "<p>";
				msj = "OkCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
						+ "ERROR: Prueba Fallida ; No se crea Cotizacion, productos o descuentos" + "<p>";
				msj = "ErrorNoCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.err.println(mensaje);
			driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());

		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
