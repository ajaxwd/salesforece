package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00064 {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		// Funciones Func_exec = new Funciones();
		int posicionExcel = 1;

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "COT_SP7_00064.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String tipoVehiculo = ExcelUtils.getCellData(1, 0);
		String tipoCredito = ExcelUtils.getCellData(1, 1);
		String pieSimulacionAmicar = ExcelUtils.getCellData(1, 2);
		String cuotasSimulacionAmicar = ExcelUtils.getCellData(1, 3);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(paginaPrincipal.MenuPrincipal.btnCotizaciones(driver))).click();
			mensaje = "Flujo : Click Boton Cotizaciones ";
			msj = "ClickBtnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			int posicion = 1;
			int sw = 0;
			String textoSubTotal = "";
			// int intSubTotal = 0;
			int esperarBtnSimularAmicar = 0;
			while (sw == 0) {

				try {
					WebElement subTotal = new WebDriverWait(driver, 10, 50)
					.until(ExpectedConditions.elementToBeClickable(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[6]/span/span[1]")));
					textoSubTotal = subTotal.getText();
					textoSubTotal = textoSubTotal.replace("CLP ", "");
					textoSubTotal = textoSubTotal.replace(".", "");
					textoSubTotal = textoSubTotal.replace("USD ", "");
					textoSubTotal = textoSubTotal.replace("(", "");
					textoSubTotal = textoSubTotal.replace(")", "");
					textoSubTotal = textoSubTotal.replace(" ", "");
					textoSubTotal = textoSubTotal.replace(",", "");
					// intSubTotal = Integer.parseInt(textoSubTotal);
					long longSubTotal = new Long(textoSubTotal);

					if (longSubTotal > 0) {

						WebElement registro = new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.elementToBeClickable(By.xpath(
						"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
						+ posicion + "]/th/span/a")));
						registro.click();

						while (esperarBtnSimularAmicar == 0) {

							try {
								new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOf(principalCotizaciones.PrincipalCotizaciones.btnSimularAmicar(driver)))
								.sendKeys(Keys.RETURN);
								mensaje = "Flujo : Se presiona Boton 'Simular Amicar' ";
								msj = "ClickBtnSimularAmicar";
								posicionEvidencia = posicionEvidencia + 1;
								rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								esperarBtnSimularAmicar = 1;
							} catch (NoSuchElementException | ScreenshotException e) { esperarBtnSimularAmicar = 0; }
						}

						Thread.sleep(3000);

						new WebDriverWait(driver, 30, 100).until(ExpectedConditions.elementToBeClickable(
						principalCotizaciones.PrincipalCotizaciones.selectTipoVehiculo(driver)));
						Select selectVehiculo = new Select(
						principalCotizaciones.PrincipalCotizaciones.selectTipoVehiculo(driver));
						selectVehiculo.selectByVisibleText(tipoVehiculo);
						mensaje = "Flujo : Se Seleciona tipo de Vehiculo " + tipoVehiculo;
						msj = "SeleccionVehiculo";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(3000);

						new WebDriverWait(driver, 30, 100).until(ExpectedConditions.elementToBeClickable(
						principalCotizaciones.PrincipalCotizaciones.selectTipoCreditoAmicar(driver)));
						Select selectCredito = new Select(
						principalCotizaciones.PrincipalCotizaciones.selectTipoCreditoAmicar(driver));
						selectCredito.selectByVisibleText(tipoCredito);
						mensaje = "Flujo : Se Seleciona tipo de Credito Amicar " + tipoCredito;
						msj = "SeleccionTipoCredito";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
						principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver))).click();
						principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver).clear();
						principalCotizaciones.PrincipalCotizaciones.inputPieSimularAmicar(driver).sendKeys(pieSimulacionAmicar);
						mensaje = "Flujo : Ingreso de Pie en Simulacion " + pieSimulacionAmicar;
						msj = "IngresoPie";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
						principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver))).click();
						principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver).clear();
						principalCotizaciones.PrincipalCotizaciones.inputCuotasSimularAmicar(driver).sendKeys(cuotasSimulacionAmicar);
						mensaje = "Flujo : Ingreso de Coutas en Simulacion " + cuotasSimulacionAmicar;
						msj = "IngresoCuotas";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(2000);

						principalCotizaciones.PrincipalCotizaciones.btnBonoAmicar(driver).click();
						mensaje = "Flujo : Se presiona boton 'Bono Amicar'";
						msj = "ClickBtnBonoAmicar";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						Thread.sleep(6000);
						new WebDriverWait(driver, 20, 100).until(ExpectedConditions
						.visibilityOf(principalCotizaciones.PrincipalCotizaciones.btnSimular(driver))).click();
						mensaje = "Flujo : Se presiona boton 'Simular'";
						msj = "ClickBtnSimular";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						Thread.sleep(30000);

						int esperarPantallaPrincipal = 0;
						while (esperarPantallaPrincipal == 0) {

							try {
								new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
								principalCotizaciones.PrincipalCotizaciones.btnSimularAmicar(driver)));
								// .sendKeys(Keys.RETURN);
								new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
								principalCotizaciones.PrincipalCotizaciones.btnDetallesCotizacion(driver)));

								principalCotizaciones.PrincipalCotizaciones.btnDetallesCotizacion(driver).click();
								mensaje = "Flujo : Se presiona boton 'Detalles' para verificar creacion de simulacion";
								msj = "ClickBtnDetalles";
								posicionEvidencia = posicionEvidencia + 1;
								rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								esperarPantallaPrincipal = 1;
							} catch (NoSuchElementException | ScreenshotException e) { esperarPantallaPrincipal = 0; }
						}

						Thread.sleep(2000);
						new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
						principalCotizaciones.PrincipalCotizaciones.txtVerificarCotizacion(driver)));
						Thread.sleep(2000);
						String verificaCotizacion = principalCotizaciones.PrincipalCotizaciones.txtVerificarCotizacion(driver).getText();
						System.err.println("Verificar " + verificaCotizacion);

						if (verificaCotizacion.contains("Creada")) {
							mensaje = "Ok : Cotizacion Creada ";
							msj = "OkCotizacionCreada";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							ValorEncontrado = true;
							sw = 1;
						} else {
							mensaje = "ERROR : Cotizacion no Creada ";
							msj = "ErrorCotizacionNoCreada";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
							sw = 1;
						}
					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					mensaje = "<p style='color:red;'>"+ "Error no encontro elemento al Recorrer grilla de cotizaciones " + "<p>";
					msj = "NoHayElementoEnGrilla";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Cotizacion " + "<p>";
				msj = "OkCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Se Crea Cotizacion" + "<p>";
				msj = "ErrorNoCreaCotizacion";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoVisible";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "RevisarErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
