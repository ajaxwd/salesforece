package testCases;

import java.io.BufferedInputStream;
import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.itextpdf.text.pdf.PdfDocument;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;
import java.net.*;
import java.io.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLConnection;


public class COT_SP7_00023_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {
				posicionExcel = i;
				break;
			}
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String nombreCotizacion = ExcelUtils.getCellData(posicionExcel, 12);
		String busquedaOportunidad = ExcelUtils.getCellData(posicionExcel, 5);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int esperarBtnImprimirPdf = 0;
		int esperarBtnGuardarCotizacion = 0;
		int contadorEspera = 0;

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(3000);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement registro = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/th/span/a")));
					if (registro.getText().equals(busquedaOportunidad)) {
						registro.click();
						mensaje = "Flujo : Se Encuentra Oportunidad y Selecciona Registro";
						msj = "SeleccionRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnCotizaciones(driver)))
					.click();
			mensaje = "Flujo : Click Link 'Cotizaciones' en Oportunidad";
			msj = "btnCotizaciones";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			posicion = 1;
			sw = 0;
			while (sw == 0) {

				try {

					WebElement registroCotizacion = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@id=\"brandBand_1\"]/div/div[1]/div[4]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
											+ posicion + "]/td[2]/span/a")));
					if (registroCotizacion.getText().equals(nombreCotizacion)) {
						registroCotizacion.click();
						mensaje = "Flujo : Se Encuentra Cotizacion y Selecciona Registro";
						msj = "SeleccionRegistroCotizacion";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						Thread.sleep(1000);
						sw = 0;
						break;

					} else {
						sw = 0;
						posicion = posicion + 1;
					}

				} catch (NoSuchElementException | ScreenshotException e) {
					sw = 0;
					posicion = posicion + 1;

				}

			}

			Thread.sleep(3000);

			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy (0,300)");

			while (esperarBtnImprimirPdf == 0) {

				try {
					principalCotizaciones.PrincipalCotizaciones.btnCrearPdf(driver).sendKeys(Keys.RETURN);
					mensaje = "Flujo : Se presiona Boton 'Imprimir PDF' ";
					msj = "ClickBtnImprimirPdf";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
					esperarBtnImprimirPdf = 1;

					Thread.sleep(1000);

//					int esperarMensajeSimulacion = 0;
//					while (esperarMensajeSimulacion == 0) {
//
//						if (driver.findElements(By.xpath("//h2[contains(text(),'Cotizaci�n')]")).size() < 1) {
//
//							esperarMensajeSimulacion = 0;
//
//						} else {
//							mensaje = "Flujo : 'Documento PDF Abierto'";
//							msj = "MsjSimulacionAmicar";
//							posicionEvidencia = posicionEvidencia + 1;
//							posicionEvidenciaString = Integer.toString(posicionEvidencia);
//							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//							Thread.sleep(2000);
//							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//							Log.info(mensaje);
//							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//									+ rutaEvidencia + ">");
//							System.out.println(mensaje);
//
//							Thread.sleep(2000);
//
//							// String documentoPdfLink =
//							// driver.findElement(By.xpath("//*[@id='plugin']")).getAttribute("src");
//							// System.err.println("Link PDF : "+documentoPdfLink);
//
//							String documentoPdfLink = driver
//									.findElement(By
//											.xpath("//div[@class='slds-modal__content slds-p-around--medium']//iframe"))
//									.getAttribute("src");
//							System.err.println("Link PDF : " + documentoPdfLink);
//							
//							documentoPdfLink = documentoPdfLink.replaceAll("&version=imprimible", "");
//							
//							System.err.println("Link PDF MOD : *" + documentoPdfLink+"*");
//							
//							 String inputLine = "";
							 
//							 URL url1 = new URL(documentoPdfLink);
//
//								    byte[] ba1 = new byte[1024];
//								    int baLength;
//								    FileOutputStream fos1 = new FileOutputStream("download.pdf");
//							try
//							{
//							    PDDocument pddDocument = PDDocument.load(new URL(documentoPdfLink));
//							    PDFTextStripper textStripper = new PDFTextStripper();
//							    String doc = textStripper.getText(pddDocument);
//							    pddDocument.close();
//							    System.out.println(doc);
//	---------------------------------------							
//								 URL oracle = new URL(documentoPdfLink);
//							        BufferedReader in = new BufferedReader(
//							        new InputStreamReader(oracle.openStream()));
//
//							       
//							        while ((inputLine = in.readLine()) != null)
//							            System.out.println(inputLine);
//							        in.close();
//---------------------------------------		
							        
//								// Contacting the URL
//							      System.out.print("Connecting to " + url1.toString() + " ... ");
//							      URLConnection urlConn = url1.openConnection();
//
//							      // Checking whether the URL contains a PDF
//							      
//							   //   if (!urlConn.getContentType().equalsIgnoreCase("application/pdf")) {
//							   	  if (!urlConn.getContentType().equalsIgnoreCase("application/x-google-chrome-pdf")) {
//							          System.out.println("FAILED.\n[Sorry. This is not a PDF.]");
//							      } else {
//							        try {
//
//							          // Read the PDF from the URL and save to a local file
//							          InputStream is1 = url1.openStream();
//							          while ((baLength = is1.read(ba1)) != -1) {
//							              fos1.write(ba1, 0, baLength);
//							          }
//							          fos1.flush();
//							          fos1.close();
//							          is1.close();
//
//							          // Load the PDF document and display its page count
//							          System.out.print("DONE.\nProcessing the PDF ... ");
//							          PdfDocument doc = new PdfDocument();
//							          try {
//							        	PDDocument.load("download.pdf");
//							            System.out.println("DONE.\nNumber of pages in the PDF is " + doc.getPageSize());
//							            doc.close();
//							          } catch (Exception e) {
//							            System.out.println("FAILED.\n[" + e.getMessage() + "]");
//							          }
//
//							        } catch (ConnectException ce) {
//							          System.out.println("FAILED.\n[" + ce.getMessage() + "]\n");
//							        }
//							      }
//								
//								ChromeOptions options = new ChromeOptions();
//								options.addExtensions(new File("/path/to/extension.crx"));
//								options.addExtensions(new File("C:\\Program Files (x86)\\Google\\Chrome\\Application\\75.0.3770.142\\resources.pak"));
//								DesiredCapabilities capabilities = new DesiredCapabilities();
//								capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//								ChromeDriver driver = new ChromeDriver(capabilities);
//								
//								driver.switchTo().frame(1);     
//								String tagdata = driver.findElement(By.id("plugin")).getAttribute("outerHTML");     
//								System.out.println(tagdata);
//								
//								URL TestURL = new URL(documentoPdfLink);
//								BufferedInputStream TestFile = new BufferedInputStream(TestURL.openStream());
////								PDDocument doc = PDDocument.load(TestFile);
//								PDFParser TestPDF = new PDFParser(TestFile);							
//								TestPDF.parse();
//								TestFile.close();
//								String TestText = new PDFTextStripper().getText(TestPDF.getPDDocument());
//								
//								System.err.println("Contenido PDF : " + TestText);
//								System.err.println("Contenido PDF OUTER : " + tagdata);
//								
//							}
//							catch (Exception e)
//							{
//							    System.out.println(e.getMessage());
//							}
//							
							

//							URL TestURL = new URL(documentoPdfLink);
//							BufferedInputStream TestFile = new BufferedInputStream(TestURL.openStream());
////							PDDocument doc = PDDocument.load(TestFile);
//							PDFParser TestPDF = new PDFParser(TestFile);							
//							TestPDF.parse();
//							TestFile.close();
//							String TestText = new PDFTextStripper().getText(TestPDF.getPDDocument());
//							
//
//							Assert.assertTrue(TestText.contains("Open the setting.xml, you can see it is like this"));

//							System.err.println("Contenido PDF : " + inputLine);
//
//							//
//							// esperarMensajeSimulacion = 1;
//							// esperarBtnImprimirPdf = 1;
//						}

						try {

							String textoAccion = principalCotizaciones.PrincipalCotizaciones.txtCuadroDialogo(driver)
									.getText();

							if (textoAccion.contains("Debe volver a simular, existe diferencia por un monto de:")) {

								mensaje = "<p style='color:red;'>" + "Fin de ejecucion....... " + textoAccion
										+ " No se Genera PDF de manera correcta, revise datos de entrada" + "</p>";
								msj = "noRealizaPago";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);
								ValorEncontrado = false;
								driver.quit();
								throw new AssertionError(errorBuffer.toString());
							}

						} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

						}
						String contenido = "";
						while (esperarBtnGuardarCotizacion == 0) {

							try {

								Thread.sleep(4000);

								principalCotizaciones.PrincipalCotizaciones.btnGuardarCotizacion(driver)
										.sendKeys(Keys.RETURN);
								mensaje = "Flujo : Se presiona Boton 'Guardar Cotizacion' ";
								msj = "ClickBtnGuardarCotizacion";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(5000);

								// try {
								// String textoNotificacionesVerde =
								// MenuPrincipal.textoNotificacionesVerde(driver).getText();
								// System.err.println("textoNotificacionesVerde " + textoNotificacionesVerde);
								//
								// } catch (TimeoutException | NoSuchElementException e1) {
								//
								// }
								//
								// try {
								// String textoNotificacionesVerdeDos =
								// MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
								// System.err.println("textoNotificacionesVerdeDos " +
								// textoNotificacionesVerdeDos);
								// } catch (TimeoutException | NoSuchElementException e1) {
								//
								// }
								//
								// try {
								// String textoNotificacionesVerdePequeno =
								// MenuPrincipal.textoNotificacionesVerdePequeno(driver)
								// .getText();
								// System.err.println("textoNotificacionesVerdePequeno " +
								// textoNotificacionesVerdePequeno);
								// } catch (TimeoutException | NoSuchElementException e1) {
								//
								// }
								//
								// System.err.println("Texto Mensaje PDF :
								// "+principalCotizaciones.PrincipalCotizaciones.txtConfirmaGuardarCotizacion(driver)
								// .getText());

								// if
								// (principalCotizaciones.PrincipalCotizaciones.txtConfirmaGuardarCotizacion(driver)
								// .getText().contains("Se guard� la cotizaci�n.")
								// ||
								// principalCotizaciones.PrincipalCotizaciones.txtConfirmaGuardarCotizacion(driver)
								// .getText().contains(
								// "Cotizaci�n ya enviada a SAP, favor revisar documentos adjuntos."))
								// {

								if (MenuPrincipal.textoNotificacionesVerde(driver).getText()
										.contains("Se guard� la cotizaci�n.")
										|| MenuPrincipal.textoNotificacionesVerde(driver).getText()
												.contains("ya enviada a SAP")) {
									String textoNotificacionesVerde = MenuPrincipal.textoNotificacionesVerde(driver)
											.getText();
									System.err.println("textoNotificacionesVerde " + textoNotificacionesVerde);
									esperarBtnGuardarCotizacion = 1;
									esperarBtnImprimirPdf = 1;
									
									ValorEncontrado = true;									
									contenido = MenuPrincipal.textoNotificacionesVerde(driver).getText();											
									break;

								} else {

									mensaje = "<p style='color:red;'>"
											+ "Fin de ejecucion....... No se Genera PDF de manera correcta, revise datos de entrada"
											+ "</p>";
									msj = "noRealizaPago";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.out.println(mensaje);
									ValorEncontrado = false;
									driver.quit();
									throw new AssertionError(errorBuffer.toString());
								}

							} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e1) {
								
								if (contenido.contains("ya enviada a SAP")) {
									esperarBtnGuardarCotizacion = 1;
									esperarBtnImprimirPdf = 1;
								} else {
									esperarBtnGuardarCotizacion = 0;
									esperarBtnImprimirPdf = 0;
								}
								
								contadorEspera = contadorEspera + 1;
								if (contadorEspera == 5) {
									mensaje = "<p style='color:red;'>"
											+ "Error: Prueba Fallida ; No Existe Boton Guardar Cotizacion o deshabilitado"
											+ "<p>";
									msj = "ErrorNoExisteBoton";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.err.println(mensaje);
									driver.close();
									ValorEncontrado = false;
									errorBuffer.append(e1.getMessage() + "\n" + mensaje + "\n");
									throw new AssertionError(errorBuffer.toString());

								}
							}
						}

//					}
			

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					esperarBtnImprimirPdf = 0;
					contadorEspera = contadorEspera + 1;
					if (contadorEspera == 15) {
						mensaje = "<p style='color:red;'>"
								+ "Error: Prueba Fallida ; No Existe Boton Imprimir Pdf o deshabilitado" + "<p>";
						msj = "ErrorNoExisteBoton";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());

					}
				}

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se guarda cotizacion" + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida ; No se guarda cotizacion" + "<p>";
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				driver.close();
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.err.println(mensaje);
			driver.close();
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());

		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "SeEncuentranErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
