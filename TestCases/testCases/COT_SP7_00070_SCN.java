package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00070_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";

	@Parameters({ "sNumeroEscenarioXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		
		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {						
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {		
				posicionExcel = i;
				break;
			} 
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + Browser + "<br>" + "URL " + URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String busquedaOportunidad = ExcelUtils.getCellData(posicionExcel, 5);
		String modificarVentaPara = ExcelUtils.getCellData(posicionExcel, 24);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Oportunidades ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);
			
			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
			paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
			mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
			Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			principalOportunidades.PrincipalOportunidades.selectSeleccionVistaLista(driver).click();
			mensaje = "Flujo : Click Seleccion Vista de Listas ";
			msj = "ClickBtnVistasListas";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalOportunidades.PrincipalOportunidades.elementoVistosRecientemente(driver).click();
			mensaje = "Flujo : Click Todas Las Oportunidades ";
			msj = "ClickBtnTodasLasOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			mensaje = "Flujo : Entra a loop para buscar registro ";
			Funciones.mensajesLog(mensaje);

			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				try {

					WebElement etapaOportunidad = driver.findElement(By.xpath(
					"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
					+ posicion + "]/td[3]/span/span"));
					if (etapaOportunidad.getText().contains("Detección")
							|| etapaOportunidad.getText().contains("Propuesta")) {

						WebElement registro = new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
						"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
						+ posicion + "]/th/span/a")));

						if (registro.getText().equals(busquedaOportunidad)) {
							registro.click();
							mensaje = "Flujo : Se Encuentra y Selecciona Registro";
							msj = "SeleccionRegistro";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(5000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.linkDetalles(driver))).click();
							mensaje = "Flujo : Se presiona link 'Detalles' ";
							msj = "ClickLinkDetalles";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(3000);

							JavascriptExecutor js = (JavascriptExecutor) driver;
							js.executeScript("window.scrollBy (0,700)");

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.lapizModificarVentaPara(driver))).click();
							mensaje = "Flujo : Se presiona lapiz 'Modificar Venta para' ";
							msj = "ClickLapizVentaPara";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(2000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.inputModificarVentaPara(driver))).sendKeys(modificarVentaPara);
							mensaje = "Flujo : Se ingresa Nombre 'Modificar Venta para': " + modificarVentaPara;
							msj = "IngresoNombreVentaPara";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(2000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
							.elementToBeClickable(By.xpath("//div[@title = '" + modificarVentaPara + "']"))).click();
							mensaje = "Flujo : Se Selecciona Nombre 'Modificar Venta para': " + modificarVentaPara;
							msj = "SeleccionNombreVentaPara";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(2000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.btnGuardarModificarVentaPara(driver))).click();
							mensaje = "Flujo : Se presiona Guardar 'Modificar Venta para' ";
							msj = "ClickLapizVentaPara";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

							Thread.sleep(6000);
							mensaje = "Flujo : Se revisa Modificacion correcta de Venta Para";
							Funciones.mensajesLog(mensaje);
							driver.navigate().refresh();
							Thread.sleep(6000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.linkDetalles(driver))).click();
							mensaje = "Flujo : Se presiona link 'Detalles' ";
							msj = "ClickLinkDetalles";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							
							Thread.sleep(2000);
							js.executeScript("window.scrollBy (0,700)");
							Thread.sleep(5000);
							
							String comparaAlt = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.imgCheckModificarVentaPara(driver))).getAttribute("alt");	
							
							String comparaClass = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
							principalOportunidades.PrincipalOportunidades.imgCheckModificarVentaPara(driver))).getAttribute("class");	
							
							if (comparaClass.equals(" checked") && comparaAlt.equals("True")) {

								mensaje = "Ok : Se realiza Modificacion 'Venta Para' ";
								msj = "OkModificacionCorrecta";
								posicionEvidencia = posicionEvidencia + 1;
								rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								ValorEncontrado = true;	
								sw = 1;

							} else {

								mensaje = "ERROR : No se realiza Modificacion 'Venta Para' ";
								msj = "ErrorModificacionIncorrecta";
								posicionEvidencia = posicionEvidencia + 1;
								rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								errorBuffer.append("\n" + mensaje + "\n");
								ValorEncontrado = false;
								driver.quit();
								throw new AssertionError(errorBuffer.toString());
							}
						} else {
							sw = 0;
							posicion = posicion + 1;
							// break;
						}
					} else {
						sw = 0;
						posicion = posicion + 1;
					}
				} catch (NoSuchElementException | ScreenshotException e) {
					mensaje = "<p style='color:red;'>" + "Error no encontro elemento " + "<p>";
					msj = "NoHayElemento";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					//driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Realiza Modificacion para 'Cuenta Para ' " + modificarVentaPara +"<p>";
				msj = "OkModifcaCuentaPara";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No Se Realiza Modificacion para 'Cuenta Para '" + "<p>";
				msj = "ErrorNoModifcaCuentaPara";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoVisible";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>" + "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "RevisarErrores";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
