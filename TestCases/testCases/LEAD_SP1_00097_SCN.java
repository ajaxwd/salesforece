package testCases;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

//@Test(groups = {"Dependency"}) 
public class LEAD_SP1_00097_SCN {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";
	public static String sNumeroEscenario = "";
	
	@Parameters({"sNumeroEscenarioXml"})
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
	}
		
	//@Test
	@Test(groups = {"Dependency"}) 
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();

		int posicionExcel = 1;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		for (int i = 0; i < 17; i++) {						
			if (sNumeroEscenario.equals(ExcelUtils.getCellData(i, 0))) {				
				posicionExcel = i;
				break;
			} 
		}

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */

		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);
		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "Datos_Scenarios.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		//String busquedaLead = ExcelUtils.getCellData(posicionExcel, 1);
		//String busquedaEmpresa = ExcelUtils.getCellData(posicionExcel, 2);
		//String buscarTelefono = ExcelUtils.getCellData(posicionExcel, 4);
		//String currentURL = driver.getCurrentUrl();
		String buscarCuenta = "";	

		

		Thread.sleep(15000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {
			
			int recorreExcell = 0;
			int inicioExcell = 0;
			String paisEjecucion = "";
			
			while (recorreExcell == 0) {
				if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
					// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
					System.out.println(mensaje);
					Reporter.log(mensaje);
					Log.info(mensaje);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Pais' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}


			recorreExcell = 0;
			inicioExcell = 0;
			String busquedaLead = "";
			while (recorreExcell == 0) {
				if ("Nombre Persona Lead Creado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					busquedaLead = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Nombre Persona Lead Creado' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}
			
			recorreExcell = 0;
			inicioExcell = 0;
			String busquedaEmpresa = "";
			while (recorreExcell == 0) {
				if ("Nombre Lead Empresa Creado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					busquedaEmpresa = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Nombre Lead Empresa Creado' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String buscarRut = "";
			System.out.println(buscarRut);
			while (recorreExcell == 0) {
				if ("Rut LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					buscarRut = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Nombre Lead Creado' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}
			
			recorreExcell = 0;
			inicioExcell = 0;
			String buscarTelefono = "";
			while (recorreExcell == 0) {
				if ("Telefono LEAD".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					buscarTelefono = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Telefono LEAD' en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}
			
			
			if (busquedaEmpresa == "") {
				buscarCuenta = busquedaLead;
			} else {
				buscarCuenta = busquedaEmpresa;
			}
			
			
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(4000);

//			if (currentURL.contains("preprod.lightning")) {

				mensaje = "Flujo : Proceso busqueda lead ";
				Log.info(mensaje);
				Reporter.log("<br>" + "<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				WebElement buscaLead = principalLead.PrincipalLead.inputBuscarPaginaLead(driver);
				buscaLead.sendKeys(" ");
				buscaLead.sendKeys(buscarCuenta);
				Thread.sleep(3000);
				buscaLead.sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				
				mensaje = "Flujo : Click Elemento Encontrado " + buscarCuenta;
				msj = "ClickElementoencontrado";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src="
						+ rutaEvidencia + ">");
				System.out.println(mensaje);

//			} else {
//
//				principalLead.PrincipalLead.selectSeleccionVistaLista(driver).click();
//				mensaje = "Flujo : Click Seleccion Vista de Listas ";
//				msj = "ClickBtnVistas";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				Thread.sleep(1500);
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);
//				Thread.sleep(2000);
//
//				principalLead.PrincipalLead.elementoTodosLosCandidatosAbiertos(driver).click();
//				mensaje = "Flujo : Selecciona elemento Todos Los Leads Abiertos ";
//				msj = "SeleccionTodosLosCandidatos";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				Thread.sleep(1500);
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);
//
//				Thread.sleep(3000);
//
//				mensaje = "Flujo : Proceso busqueda lead ";
//				Log.info(mensaje);
//				Reporter.log("<br>" + mensaje);
//				System.out.println(mensaje);
//			}

			Thread.sleep(3000);
			
						
//			WebElement nombre = driver.findElement(By.xpath(
//					"/html[1]/body[1]/div[5]/div[1]/section[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/nav[1]/ol[1]/li[1]/span[1]"));
//			
			WebElement	nombre;
			try {

				nombre = driver.findElement(By.xpath(
						"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/th/span/a"));

			
			} catch (org.openqa.selenium.NoSuchSessionException e) {

				mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
				driver.close();
				msj = "ErrorElementoNoVisible";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				ValorEncontrado = false;
				errorBuffer.append("\n" + mensaje + "\n");
				throw new AssertionError(errorBuffer.toString());

			}

						
			int posicion = 1;
			int sw = 0;
			while (sw == 0) {

				if (busquedaEmpresa == "") {
//					nombre = new WebDriverWait(driver, 10, 50).until(ExpectedConditions.elementToBeClickable(By.xpath(
//							"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//									+ posicion + "]/th/span/a")));
					nombre = new WebDriverWait(driver, 10, 50).until(ExpectedConditions.elementToBeClickable(By.xpath(
							"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["+ posicion + "]/th/span/a")));
									

				} else {
					nombre = new WebDriverWait(driver, 10, 50).until(ExpectedConditions.elementToBeClickable(By.xpath(
							"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
									+ posicion + "]/td[3]/span/span")));
				}

				if (nombre.getText().equals(busquedaLead)) {
					Thread.sleep(3000);
					
					mensaje = "Flujo : Nombre Registro Coincide ";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);
		
//					WebElement estado = new WebDriverWait(driver, 10, 50)
//							.until(ExpectedConditions.elementToBeClickable(By.xpath(
//									"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/td[8]/span/span")));


//					WebElement estado = new WebDriverWait(driver, 10, 50)
//							.until(ExpectedConditions.elementToBeClickable(By.xpath(
//									"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr/td[1]/span/div/span")));

//					if (currentURL.contains("preprod.lightning")) {

//						estado = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(By
//								.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//										+ posicion + "]/td[8]/span/span")));
						
//						estado = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(By
//								.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//										+ posicion + "]/td[7]/span/span")));
						
					WebElement	estado = new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(By
								.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["+posicion+"]/td[8]/span/span")));
						
//					} else {
//
//						estado = new WebDriverWait(driver, 10, 50).until(ExpectedConditions.elementToBeClickable(By
//								.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//										+ posicion + "]/td[8]/span/span")));
//					}

					if (estado.getText().contains("Nuevo")) {
						Thread.sleep(3000);
						
						mensaje = "Flujo : Estado Registro 'Nuevo' ";
						Log.info(mensaje);
						Reporter.log("<br>"+"<p>" + mensaje + "</p>");
						System.out.println(mensaje);
						
//						WebElement telefonoCompara = new WebDriverWait(driver, 20, 100)
//								.until(ExpectedConditions.elementToBeClickable(By.xpath(
//										"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//												+ posicion + "]/td[5]/span/span")));
						
						WebElement telefonoCompara = new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.elementToBeClickable(By.xpath(
										"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["+posicion+"]/td[5]/span/span")));
						
//						if (currentURL.contains("preprod.lightning")) {

//							telefonoCompara = new WebDriverWait(driver, 20, 100)
//									.until(ExpectedConditions.elementToBeClickable(By.xpath(
//											"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//													+ posicion + "]/td[4]/span/span")));
//
//						} else {
//
//							telefonoCompara = new WebDriverWait(driver, 20, 100)
//									.until(ExpectedConditions.elementToBeClickable(By.xpath(
//											"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//													+ posicion + "]/td[6]/span/span")));
//						}

														
						if (telefonoCompara.getText().contains(buscarTelefono)) {

							mensaje = "Flujo : Telefono Registro Coincide ";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);
							
//							WebElement registro = new WebDriverWait(driver, 20, 100)
//									.until(ExpectedConditions.elementToBeClickable(By.xpath(
//											"//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["
//													+ posicion + "]/th/span/a")));
							
							WebElement registro = new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.elementToBeClickable(By.xpath(
											"//*[@id=\"brandBand_1\"]/div/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr["+posicion+"]/th/span/a")));

							registro.click();
							mensaje = "Flujo : Se Selecciona Registro ";
							msj = "SeleccionRegistro";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1500);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="+ rutaEvidencia + ">");
							System.out.println(mensaje);
							Thread.sleep(10000);

							try {
								
								new WebDriverWait(driver, 30, 100).until(ExpectedConditions
										.elementToBeClickable(principalLead.PrincipalLead.btnEstadoCalificado(driver)))
										.click();
								mensaje = "Flujo : Se presiona Boton 'Calificar' en menu superior ";
								msj = "ClickBtnCalificar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(3000);

								new WebDriverWait(driver, 20, 100)
										.until(ExpectedConditions.elementToBeClickable(
												principalLead.PrincipalLead.btnCalificarLeadMenuSuperior(driver)))
										.click();
								mensaje = "Flujo : Se presiona Boton 'Marcar como Estado de lead actual' en menu superior ";
								msj = "ClickBtnMarcar";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(3000);

								new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
										principalLead.PrincipalLead.btnConvertirMenuSuperior(driver))).click();

								mensaje = "Flujo : Se presiona Boton 'Convertir' en menu superior ";
								msj = "ClickBtnConvertir";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
								System.out.println(mensaje);

								mensaje = "Flujo : Se Califica Lead ";
								Log.info(mensaje);
								Reporter.log("<br>" + mensaje);
								System.out.println(mensaje);

							} catch (NoSuchElementException | ScreenshotException e) {

								new WebDriverWait(driver, 20, 100)
										.until(ExpectedConditions.elementToBeClickable(
												principalLead.PrincipalLead.btnDosAccionesMenuSuperior(driver)))
										.click();
								// principalLead.PrincipalLead.btnDosAccionesMenuSuperior(driver).click();
								mensaje = "Flujo : Se presiona Boton 'Dos Acciones Mas' en menu superior ";
								msj = "ClickBtnDosAccionesMas";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
								System.out.println(mensaje);

								Thread.sleep(3000);
								new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
										principalLead.PrincipalLead.btnConvertirDosAccionesMenuSuperior(driver)))
										.click();
								// principalLead.PrincipalLead.btnConvertirDosAccionesMenuSuperior(driver).click();

								mensaje = "Flujo : Se presiona Boton 'Convertir' en menu superior ";
								msj = "ClickBtnConvertir";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
								System.out.println(mensaje);
							}

							Thread.sleep(3000);

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.elementToBeClickable(
											principalLead.PrincipalLead.chkNoCrearOportinidadTrasConvertir(driver)))
									.click();
							mensaje = "Flujo : Se tickea checkbox 'No crear una oportunidad tras la conversi�n' ";
							msj = "ClickChkNoCrearOportunidad";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1500);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(5000);

							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
									principalLead.PrincipalLead.btnConvertirPopUpConvertir(driver))).click();
							mensaje = "Flujo : Se presiona Boton 'Convertir' en Pop Up 'Convertir' ";
							msj = "ClickBtnConvertirPopUp";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(1500);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="	+ rutaEvidencia + ">");
							System.out.println(mensaje);
							Thread.sleep(3000);

							try {
								Thread.sleep(5000);
								String comprobar = "";

//								if (currentURL.contains("preprod.lightning")) {

									WebElement comprobarObjeto = driver.findElement(By.xpath(
											"/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div[1]/div[1]/span"));
									comprobar = comprobarObjeto.getText();

//								} else {
//									comprobar = principalLead.PrincipalLead.txtVerificarConvertirPopUp(driver)
//											.getText();
//								}

								if (comprobar.contains("Se ha convertido su lead")) {
									mensaje = "OK : Registro Se encuentra en Estado 'Convertido' ";
									msj = "OkregistroConvertido";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(1500);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"+ "<img src=" + rutaEvidencia + ">");
									System.out.println(mensaje);
									new WebDriverWait(driver, 20, 100).until(ExpectedConditions
											.elementToBeClickable(principalLead.PrincipalLead.btnIrALeads(driver)))
											.click();
									ValorEncontrado = true;
									sw = 1;
								} else {
									mensaje = "Error: Registro No Se encuentra en Estado 'Convertido'  ";
									msj = "ErrorRegistroNoCovertido";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(1500);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.err.println(mensaje);
									errorBuffer.append("\n" + mensaje + "\n");
									ValorEncontrado = false;
									sw = 1;
								}
							} catch (NoSuchElementException | ScreenshotException e) {
								mensaje = mensaje + "<p style='color:red;'>"
										+ " Error : No encuentra Elemento de estado en pop up Convertir" + "<p>";
								msj = "ErrorNoExisteElementoPopUp";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(1500);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								errorBuffer.append("\n" + mensaje + "\n");
								driver.quit();
								throw new AssertionError(errorBuffer.toString());
							}

						} else {
							sw = 0;
							posicion = posicion + 1;
						}
					} else {

						mensaje = "Error: Registro No Se encuentra en Estado 'Nuevo'  ";
						msj = "ErrorRegistroNoNuevo";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(1500);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
						sw = 1;
					}

				}
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Convierte Lead " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se Convierte Lead" + "<p>";
				msj = "ErrorPruebaIncorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(1500);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			driver.close();
			msj = "ErrorElementoNoVisible";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();

	}

}
