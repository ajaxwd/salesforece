package testCases;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class COT_SP7_00001 {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "DatosBasicosYConfiguracionGeneralDeEjecucion.xlsx";

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		// Funciones Func_exec = new Funciones();

		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");

		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		utilidades.DatosInicialesYDrivers.File_TestData = "COT_SP7_00001.xlsx";
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String formaDePago = ExcelUtils.getCellData(1, 0);
		String listaPrecio = ExcelUtils.getCellData(1, 1);
		String productoABuscar = ExcelUtils.getCellData(1, 2);
		String cantidadProducto = ExcelUtils.getCellData(1, 3);

		Thread.sleep(5000);

		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		try {

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnOportunidades(driver)));
			paginaPrincipal.MenuPrincipal.btnOportunidades(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton 'Oportunidades' ";
			msj = "ClickBtnOportunidades";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			principalOportunidades.PrincipalOportunidades.linkPrimerRegistroDesplegado(driver).click();
			mensaje = "Flujo : Seleccion del Primer Registro de Oportunidad desplegado ";
			msj = "SeleccionPrimerRegistro";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnNuevaCotizacion(driver)))
					.click();
			mensaje = "Flujo : Click Boton 'Nueva Cotizacion' en Oportunidades ";
			msj = "ClickBtnNvaCotizacions";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).clear();
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(formaDePago);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputFormasDePago(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Forma de Pago' ";
			msj = "IngresoFormaPago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + formaDePago + "']")))
					.click();
			mensaje = "Flujo : Se Selecciona 'Forma de Pago' : " + formaDePago;
			msj = "SeleccionFormaPago";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.btnGuardarCotizacion(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Boton 'Guardar' en Cotizacion";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.lnkCotizacionCreada(driver)))
					.click();
			mensaje = "Flujo : Click en Link Cotizacion Recien Creada ";
			msj = "ClickLinkCotizacionCreada";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(principalOportunidades.PrincipalOportunidades.btnAgregarProductos(driver)))
					.sendKeys(Keys.RETURN);
			mensaje = "Flujo : Click en Boton 'Agregar Productos' en Cotizacion";
			msj = "ClickBtnAddProductos";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {

				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
						principalOportunidades.PrincipalOportunidades.selectListaDePrecio(driver))).click();
				mensaje = "Flujo : Se Presiona Select Lista de Precios";
				msj = "ListaPrecios";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(2000);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='" + listaPrecio + "']")))
						.click();
				mensaje = "Flujo : Se Selecciona 'Lista de Precios' : " + listaPrecio;
				msj = "SeleccionListaPrecios";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(2000);

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
				mensaje = "Flujo : No desplego seleccion de listas, solo tiene una lista";
				msj = "NoHayListas";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(
							principalOportunidades.PrincipalOportunidades.btnGuardarSeleccionarListaDePrecio(driver)))
					.click();
			mensaje = "Flujo : Se Presiona Guardar Lista de Precios";
			msj = "ClickBtnGuardarListas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions
					.elementToBeClickable(principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver)))
					.click();
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).clear();
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).sendKeys(productoABuscar);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputBusquedaProducto(driver).sendKeys(" ");
			mensaje = "Flujo : Se Ingresa 'Busqueda Producto' ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(1000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@title='" + productoABuscar + "']")))
					.click();
			mensaje = "Flujo : Se Selecciona 'Producto' : " + productoABuscar;
			msj = "SeleccionProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
					principalOportunidades.PrincipalOportunidades.btnSiguienteAgregarProducto(driver))).click();
			mensaje = "Flujo : Se Presiona Siguiente en Agregar Productos";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);

			String productoSeleccionado = driver.findElement(By.xpath(
					"//html[1]/body[1]/div[5]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/th[1]/span[1]/a[1]"))
					.getText();
			System.out.println("Flujo : Producto Seleccionado : " + productoSeleccionado);

			new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
					principalOportunidades.PrincipalOportunidades.btnInputCantidadProducto(driver))).click();
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputCantidadProducto(driver).sendKeys(cantidadProducto);
			Thread.sleep(500);
			principalOportunidades.PrincipalOportunidades.inputCantidadProducto(driver).sendKeys(" ");
			mensaje = "Flujo : CLick en Boton 'Cantidad' del Producto ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			mensaje = "Flujo : Se Ingresa 'Cantidad' del Producto ";
			msj = "CantidadProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			principalOportunidades.PrincipalOportunidades.btnGuardarProductosCotizacion(driver).click();
			Thread.sleep(5000);

			int consulta = 0;
			String productoCotizacion = "";
			while (consulta == 0) {
				try {
					new WebDriverWait(driver, 20, 100).until(ExpectedConditions.elementToBeClickable(
							principalOportunidades.PrincipalOportunidades.txtValidarProducto(driver)));
					productoCotizacion = principalOportunidades.PrincipalOportunidades.txtValidarProducto(driver)
							.getText();
				} catch (NoSuchElementException | ScreenshotException e) {
					consulta = 1;
				}
			}

			System.out.println("Flujo : Producto Ingresado : " + productoCotizacion);

			if (productoSeleccionado.contains(productoCotizacion)) {
				mensaje = "Ok : Coincide Producto Seleccionado : " + productoSeleccionado
						+ " con Producto desplegado en Productos de la Cotizacion : " + productoCotizacion;
				msj = "ValidaProducto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
						+ "Error : No Coincide Producto Seleccionado con Producto desplegado en Productos de la Cotizacion "
						+ "</p>";
				msj = "ValidaProducto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Cotizacin  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Cotizacion Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}
}
