package ssttSprint2;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H11_13050_Validar_fecha_de_vencimiento_de_la_actividad_con_validez_48_horas_Perf�l_Asesor_de_SSTT_debe_tener_una_OT_facturada {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H11_13050_Validar_fecha_de_vencimiento_de_la_actividad_con_validez_48_horas_Perf�l_Asesor_de_SSTT_debe_tener_una_OT_facturada.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H11_13050_Validar_fecha_de_vencimiento_de_la_actividad_con_validez_48_horas_Perf�l_Asesor_de_SSTT_debe_tener_una_OT_facturada.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		//String rutCuenta = ExcelUtils.getCellData(1, 5);
		String numeroOrden = ExcelUtils.getCellData(1, 6);
		String fechaDeVencimiento = ExcelUtils.getCellData(1, 8);
		String tarea48Horas = ExcelUtils.getCellData(1, 9);
		// String nombreDetalle = ExcelUtils.getCellData(1, 8);
		// String apellidoPaterno = ExcelUtils.getCellData(1, 9);
		// String apellidoMaterno = ExcelUtils.getCellData(1, 10);
		// String generoDetalle = ExcelUtils.getCellData(1, 11);
		// String fechaNacimientoDetalle = ExcelUtils.getCellData(1, 12);
		// String nacionalidadDetalle = ExcelUtils.getCellData(1, 13);
		// String correoDetalle = ExcelUtils.getCellData(1, 14);
		// String movilDetalle = ExcelUtils.getCellData(1, 15);
		// String telefonoDetalle = ExcelUtils.getCellData(1, 16);
		// String paisDetalle = ExcelUtils.getCellData(1, 17);
		// String calleDetalle = ExcelUtils.getCellData(1, 18);
		// String regionDetalle = ExcelUtils.getCellData(1, 19);
		// String numeroDetalle = ExcelUtils.getCellData(1, 20);
		// String provinciaDetalle = ExcelUtils.getCellData(1, 21);
		// String deptoDetalle = ExcelUtils.getCellData(1, 22);
		// String comunaDetalle = ExcelUtils.getCellData(1, 23);
		// String formaPagoDetalle = ExcelUtils.getCellData(1, 24);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

//			new WebDriverWait(driver, 20, 100)
//					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicaciones(driver)))
//					.click();
//
//			mensaje = "Flujo : Click Menu Aplicaciones ";
//			msj = "ClickMenuApp";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);
//
//			Thread.sleep(3000);
//
//			new WebDriverWait(driver, 20, 100)
//					.until(ExpectedConditions
//							.visibilityOf(paginaPrincipal.MenuPrincipal.linkSSTTMenuAplicaciones(driver)))
//					.sendKeys(Keys.ENTER);
//
//			mensaje = "Flujo : Click Link Servicio Tecnico ";
//			msj = "ClickSSTT";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);
//
//			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnordenesdetrabajo(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Ordenes de Trabajo ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarOrdenPrincipal(driver)))
					.sendKeys(numeroOrden);
			mensaje = "Flujo : Se ingresa elemento a buscar " + numeroOrden;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarOrdenPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			mensaje = "Flujo : Resultado Busqueda ";
			msj = "ResultadoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, numeroOrden)))
							.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + numeroOrden;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, numeroOrden)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}
			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H11_13050_Validar_fecha_de_vencimiento_de_la_actividad_con_validez_48_horas_Perf�l_Asesor_de_SSTT_debe_tener_una_OT_facturada.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Label OT".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			recorreExcell = 0;
			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;
			while (recorreExcell == 0) {

				if ("Label OT".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLabel = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								principalCuenta.MenuPrincipalCuenta.labelARevisarDetalles(driver, nombreLabel));
						principalCuenta.MenuPrincipalCuenta.labelARevisarDetalles(driver, nombreLabel).getText();
						mensaje = "OK : Label esperado '" + nombreLabel + "' Existe en 'Detalle'";
						msj = nombreLabel + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLabel + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLabel
								+ "' NO Existe en 'Detalle'" + "</p>";
						
						System.err.println(mensaje);
						
						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}

				} else {

					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}			

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION :" + "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.err.println(mensajeResumen);

			if (contadorIncorrectos > 0) {
				mensaje = "<p style='color:red;'>" + "Error : " + contadorIncorrectos
						+ " Titulos de campo NO coinciden con los esperados en 'Detalle'" + "</p>";
				msj = "ErrorNoExisteElemento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				// driver.quit();
				// throw new AssertionError(errorBuffer.toString());
				ValorEncontrado = false;

			} else {

				mensaje = "<p style='color:blue;'>" + " Todos los Titulos coinciden con los esperados en 'Detalle'"
						+ "</p>";
				Log.info(mensaje);
				Reporter.log(mensaje);
				System.out.println(mensaje);

				try {					
				
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						principalCuenta.MenuPrincipalCuenta.txtTareaSeguimiento48Horas(driver, tarea48Horas));

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions
								.visibilityOf(principalCuenta.MenuPrincipalCuenta.txtTareaSeguimiento48Horas(driver, tarea48Horas)))
						.sendKeys(Keys.ENTER);
				mensaje = "Flujo : Click Link '"+tarea48Horas+"'";
				msj = "ClickLinkSeguimiento";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(5000);

				try {

					principalCuenta.MenuPrincipalCuenta.labelFechaVencimiento(driver).getText();
					mensaje = "OK : Existe Label 'Fecha de vencimiento'";
					msj = "ExisteElemento";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					mensaje = "Flujo : Se Verifica 'Fecha de vencimiento' ";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					
					if (fechaDeVencimiento
							.equals(principalCuenta.MenuPrincipalCuenta.txtFechaVencimiento(driver).getText())) {

						mensaje = "OK : Coincide 'Fecha de vencimiento' esperada " + fechaDeVencimiento
								+ " con 'Fecha de vencimiento' encontrada "
								+ principalCuenta.MenuPrincipalCuenta.txtFechaVencimiento(driver).getText();
						msj = "ExisteElemento";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						ValorEncontrado = true;

					} else {

						mensaje = "<p style='color:red;'>" + "Error : No existe Label o Campo 'Fecha de vencimiento'"
								+ "</p>";
						msj = "ErrorNoExisteElemento";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
					}

				} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

					mensaje = "<p style='color:red;'>" + "Error : No existe Label o Campo 'Fecha de vencimiento'"
							+ "</p>";
					msj = "ErrorNoExisteElemento";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					ValorEncontrado = false;
				}
				
				} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

					mensaje = "<p style='color:red;'>" + "Error : No Existe Link '"+tarea48Horas+"'"
							+ "</p>";
					msj = "ErrorNoExisteElemento";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					ValorEncontrado = false;
				}


				Thread.sleep(3000);

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
						+ "OK: Prueba Correcta ; campos verificados y Coincide Fecha De Vencimiento" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
