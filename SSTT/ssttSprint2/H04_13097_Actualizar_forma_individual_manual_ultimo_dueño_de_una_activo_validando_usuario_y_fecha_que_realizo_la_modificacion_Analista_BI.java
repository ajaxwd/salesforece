package ssttSprint2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H04_13097_Actualizar_forma_individual_manual_ultimo_due�o_de_una_activo_validando_usuario_y_fecha_que_realizo_la_modificacion_Analista_BI {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H04_13097_Actualizar_forma_individual_manual_ultimo_due�o_de_una_activo_validando_usuario_y_fecha_que_realizo_la_modificacion_Analista_BI.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H04_13097_Actualizar_forma_individual_manual_ultimo_due�o_de_una_activo_validando_usuario_y_fecha_que_realizo_la_modificacion_Analista_BI.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			mensaje = "Flujo : Resultado Busqueda ";
			msj = "ResultadoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//span[@title='Asignaciones']")));

			Thread.sleep(3000);
			
			String textoActivosUltimoAno = new WebDriverWait(driver, 20, 100).until(
					ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkActivosCantidad(driver)))
					.getText();
			System.err.println("*"+textoActivosUltimoAno);
			if (textoActivosUltimoAno.contains("(0)")) {

				mensaje = "<p style='color:red;'>" + "Error: No Existen Activos " + "<p>";
				msj = "ErrorNOHayActivos";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;

			} else {

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.xpath("(//a//following::span[contains(@title,'Activos')])[1]")));
				
				((JavascriptExecutor) driver).executeScript("window.scrollBy (0,-200)");
				
				Thread.sleep(5000);
				new WebDriverWait(driver, 20, 100).until(
						ExpectedConditions.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkActivoRelacionado(driver)))
						.click();
				
				//principalCuenta.MenuPrincipalCuenta.linkActivoRelacionadoVariante(driver).click();
				// new WebDriverWait(driver, 20, 100).until(
				// ExpectedConditions.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkActivoRelacionado(driver)))
				// .click();
				mensaje = "Flujo : Click Boton Activos ";
				msj = "ClickBtnActivos";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(3000);

				int recorreExcell = 0;
				int inicioExcell = 0;
				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "H04_13097_Actualizar_forma_individual_manual_ultimo_due�o_de_una_activo_validando_usuario_y_fecha_que_realizo_la_modificacion_Analista_BI.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
						"Hoja1");
				while (recorreExcell == 0) {
					if ("Nombre Activo a Buscar".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError(errorBuffer.toString());
					}
				}

				String nombreActivo = ExcelUtils.getCellData(1, inicioExcell);
				
//				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
//						principalCuenta.MenuPrincipalCuenta.linkActivoRelacionadoSeleccion(driver, nombreActivo)))
//						.click();
				
				new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOf(
						principalCuenta.MenuPrincipalCuenta.linkSeleccionActivo(driver, nombreActivo)))
						.click();
				
				mensaje = "Flujo : Click Link Activo a Verificar ";
				msj = "ClickLinkActivo";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(1000);

				recorreExcell = 0;
				inicioExcell = 0;
				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "H04_13097_Actualizar_forma_individual_manual_ultimo_due�o_de_una_activo_validando_usuario_y_fecha_que_realizo_la_modificacion_Analista_BI.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
						"Hoja1");
				while (recorreExcell == 0) {
					if ("Nuevo Propietario".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
					if (inicioExcell > 150) {
						mensaje = "<p style='color:red;'>"
								+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
						Log.info(mensaje);
						Reporter.log(mensaje);
						System.err.println(mensaje);
						driver.quit();
						throw new AssertionError(errorBuffer.toString());
					}
				}

				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView(true);",principalCuenta.MenuPrincipalCuenta.btnExpandirInformacionPropietario(driver));
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,250)", "");
				// new WebDriverWait(driver, 20, 100).until(
				// ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.btnExpandirInformacionPropietario(driver))).sendKeys(Keys.ENTER);
				//
				// mensaje = "Flujo : Click Expandir Informacion Propietario ";
				// msj = "ClickExpandirInfo";
				// posicionEvidencia = posicionEvidencia + 1;
				// posicionEvidenciaString = Integer.toString(posicionEvidencia);
				// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				// Thread.sleep(2000);
				// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
				// "_" + msj);
				// Log.info(mensaje);
				// Reporter.log(
				// "<p>" + mensaje + "</p>" + "<img src=" +
				// rutaEvidencia + ">");
				// System.out.println(mensaje);
				//
				// Thread.sleep(3000);

				String propietarioActual = principalCuenta.MenuPrincipalCuenta
						.txtPropietarioCuentaInformacion(driver, nombreCuenta).getText();
				String nuevoPropietario = ExcelUtils.getCellData(1, inicioExcell);

				if (nuevoPropietario.equals(propietarioActual)) {

					mensaje = "<p style='color:red;'>" + "Error : Kilometraje Incorrecto; 'Kilometraje esperado': "
							+ nuevoPropietario + " ;'Kilometraje encontrado' " + propietarioActual + "</p>";
					msj = "ValidacionIncorrecta";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");

				} else {

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(
									principalCuenta.MenuPrincipalCuenta.btnModificarCuentanPropietario(driver)))
							.click();
					mensaje = "Flujo : Click Modificar Cuenta Informacion Propietario ";
					msj = "ClickModificarInfo";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(
									principalCuenta.MenuPrincipalCuenta.btnBorrarPropietarioCuentaInformacion(driver)))
							.click();
					mensaje = "Flujo : Click Borrar Cuenta Informacion Propietario ";
					msj = "ClickBorrarInfo";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					String[] arregloNuevoPropietario = new String[nuevoPropietario.length()];
					for (int i = 0; i < nuevoPropietario.length(); i++) {
						arregloNuevoPropietario[i] = Character.toString(nuevoPropietario.charAt(i));
					}

					for (int i = 0; i < nuevoPropietario.length(); i++) {

						principalCuenta.MenuPrincipalCuenta.inputBuscarCuentaInformacion(driver)
								.sendKeys(arregloNuevoPropietario[i]);
						Thread.sleep(500);

					}

					mensaje = "Flujo : Ingreso Nuevo Propietario ";
					msj = "IngresoNuevoPropietario";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta
									.seleccionElementoBuscarCuentaInformacion(driver, nuevoPropietario)))
							.click();
					mensaje = "Flujo : Selecciona  Cuenta Informacion Propietario ";
					msj = "SeleccionaNuevaCuenta";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
							driver.findElement(By.xpath(
									"//div[@class='slds-form slds-form_stacked slds-is-editing']//div[@class='slds-form-element slds-form-element_readonly slds-grow slds-hint-parent override--slds-form-element']//span[contains(text(),'Local de Venta')]")));

					Date date = new Date();
					DateFormat hourFormat = new SimpleDateFormat("HH");
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

					String fechaModificacionInput = dateFormat.format(date);

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(
									principalCuenta.MenuPrincipalCuenta.inputFechaEditarCuentaInformacion(driver)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"), fechaModificacionInput);
					mensaje = "Flujo : Se ingresa Fecha Modificacion " + fechaModificacionInput;
					msj = "InputFechaModificacion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					String horaModificacionInput = hourFormat.format(date);
					int horaModificacionInt = Integer.parseInt(horaModificacionInput);
					horaModificacionInt = horaModificacionInt - 1;
					horaModificacionInput = Integer.toString(horaModificacionInt);

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(
									principalCuenta.MenuPrincipalCuenta.inputHoraEditarCuentaInformacion(driver)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"), horaModificacionInput + ":00");

					mensaje = "Flujo : Se ingresa Hora Modificacion  " + horaModificacionInput + ":00";
					msj = "InputHoraModificacion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOf(
									principalCuenta.MenuPrincipalCuenta.btnGuardarEditarCuentaInformacion(driver)))
							.click();
					mensaje = "Flujo : Click Guardar Cuenta Informacion Propietario ";
					msj = "ClickGuardarNuevaCuenta";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(6000);

					mensaje = "Se verifica cambio de 'Propietario'";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					String nombreCuentaNueva = "";

					try {

						principalCuenta.MenuPrincipalCuenta.btnCerrarPopUpModificarPropirtario(driver)
								.getAttribute("title");

						String mensajeError = principalCuenta.MenuPrincipalCuenta
								.txtCerrarPopUpModificarPropirtar(driver).getText();
						System.err.println("*" + mensajeError);

						if (mensajeError.contains("Revise los siguientes errores")) {
							mensaje = "<p style='color:red;'>" + "Error en datos de ingreso '" + mensajeError + "'<p>";
							msj = "NoCambiadeCuentaelActivo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}

						principalCuenta.MenuPrincipalCuenta.btnCerrarPopUpModificarPropirtario(driver).click();
						mensaje = "<p style='color:red;'>" + "Error al guardar registro '" + mensajeError + "'<p>";
						msj = "ErrorAlGuardar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						driver.close();
						throw new AssertionError(errorBuffer.toString());

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								driver.findElement(By.xpath(
										"(.//*[normalize-space(text()) and normalize-space(.)='Precio'])[1]/following::span[3]")));
						nombreCuentaNueva = principalCuenta.MenuPrincipalCuenta.txtModificarPropietario(driver)
								.getText();

					}

					Thread.sleep(3000);

					if (nombreCuentaNueva.equals(nombreCuenta)) {

						mensaje = "<p style='color:red;'>" + "Activo NO Cambia de Propietario, mantiene "
								+ nombreCuentaNueva + "<p>";
						msj = "NoCambiadeCuentaelActivo";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;

					} else {

						mensaje = "OK: Activo Cambia de Propietario a " + nombreCuentaNueva;
						msj = "CambiadeCuentaelActivo";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						ValorEncontrado = true;

					}

				}
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
						+ "OK: Prueba Correcta ; Se Modifica Propietario y Registra Fecha y Hora de Modificacion"
						+ "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
						+ "Error: No Modifica Propietario o NO Registra Fecha y Hora de Modificacion" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
