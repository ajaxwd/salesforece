package ssttSprint2;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			 //driver = new ChromeDriver();
			 ChromeOptions options = new ChromeOptions();
			 options.addArguments("--headless");
			 driver = new ChromeDriver(options);
			 driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;
		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
			.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			mensaje = "Flujo : Resultado Busqueda ";
			msj = "ResultadoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
					+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
					.until(ExpectedConditions.visibilityOf(
					paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
					.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;
				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;
				}
			}

			Thread.sleep(8000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
			.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Nombre Activo a Buscar".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			String nombreActivo = ExcelUtils.getCellData(1, inicioExcell);
			
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(principalCuenta.MenuPrincipalCuenta
					.linkActivoRelacionadoSeleccionVariante(driver, nombreActivo)))
			.click();
			
			mensaje = "Flujo : Click Link Activo a Verificar ";
			msj = "ClickLinkActivo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(4000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
					driver.findElement(By.xpath("//*[contains(text(),'Kilómetraje')]")));
			String kilometrajeApp = "";
			String kilometrosEsperados = "";
			recorreExcell = 0;
			inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Kilometraje SAP".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			try {
				kilometrajeApp = principalCuenta.MenuPrincipalCuenta.kilometrajeActivoDetalle(driver).getText();
				kilometrosEsperados = ExcelUtils.getCellData(1, inicioExcell);

			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
				kilometrajeApp = "";
			}

			String fechaActualizacionKilometrajeApp = "";
			String fechaActualizacionKilometrosEsperados = "";
			recorreExcell = 0;
			inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();",
					driver.findElement(By.xpath("//*[contains(text(),'Detalle del Activo')]")));

			while (recorreExcell == 0) {
				if ("Fecha Actualizacion Kilometraje".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			try {
				fechaActualizacionKilometrajeApp = principalCuenta.MenuPrincipalCuenta
				.fechaActualizacionKilometrajeActivoDetalle(driver).getText();
				fechaActualizacionKilometrosEsperados = ExcelUtils.getCellData(1, inicioExcell);

			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
				fechaActualizacionKilometrajeApp = "";
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String creadoPorKilometrajeApp = "";
			String creadoPorKilometrosEsperados = "";
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Creado Por Kilometros".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}
			
			try {

				creadoPorKilometrosEsperados = ExcelUtils.getCellData(1, inicioExcell);
				
				java.util.Iterator<WebElement> i = principalCuenta.MenuPrincipalCuenta
						.linksCreadoyActualizadoPorKilometrajeActivoDetalle(driver).iterator();
				while (i.hasNext()) {
					WebElement creadoPorKilometrajeAppWebElement = i.next();
					creadoPorKilometrajeApp = creadoPorKilometrajeAppWebElement.getText();
					if (creadoPorKilometrajeApp.equals(ExcelUtils.getCellData(1, inicioExcell))) {
						break;
					} else {
						creadoPorKilometrajeApp = "";
					}
				}
			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
				creadoPorKilometrajeApp = "";
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String actualizadoPorKilometrajeApp = "";
			String actualizadoPorKilometrosEsperados = "";
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H01_13197_Actualizacion_de_Kilometraje_en_el_Activo_desde_nueva_OT_creada_en_SAP.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Actualizado Por Kilometros".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;					
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError(errorBuffer.toString());
				}
			}
			
			try {

				actualizadoPorKilometrosEsperados = ExcelUtils.getCellData(1, inicioExcell);
				
				java.util.Iterator<WebElement> i = principalCuenta.MenuPrincipalCuenta
						.linksCreadoyActualizadoPorKilometrajeActivoDetalle(driver).iterator();
				while (i.hasNext()) {
					WebElement actualizadoPorKilometrajeAppWebElement = i.next();
					actualizadoPorKilometrajeApp = actualizadoPorKilometrajeAppWebElement.getText();
					if (actualizadoPorKilometrajeApp.equals(ExcelUtils.getCellData(1, inicioExcell))) {
						break;
					} else {
						actualizadoPorKilometrajeApp = "";
					}
				}
			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
				actualizadoPorKilometrajeApp = "";
			}
			Thread.sleep(2000);

			System.err.println("kilometrosEsperados : " + kilometrosEsperados + "  kilometrajeApp : " + kilometrajeApp);

			if (kilometrosEsperados.equals(kilometrajeApp)) {

				mensaje = "OK : Se Valida de manera correcta 'Kilometraje' esperado: " + kilometrosEsperados
				+ " con 'Kilometraje encontrado' " + kilometrajeApp;
				msj = "ValidacionCorrectaKilometraje";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				System.err.println("fechaActualizacionKilometrosEsperados : " + fechaActualizacionKilometrosEsperados
				+ "  fechaActualizacionKilometrajeApp : " + fechaActualizacionKilometrajeApp);

				if (fechaActualizacionKilometrosEsperados.equals(fechaActualizacionKilometrajeApp)) {

					mensaje = "OK : Se Valida de manera correcta 'Fecha Actualizacion Kilometraje' esperado: "
					+ fechaActualizacionKilometrosEsperados
					+ " con 'Fecha Actualizacion Kilometraje encontrado' " + fechaActualizacionKilometrajeApp;
					msj = "ValidacionCorrectaFechaKilometraje";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					System.err.println("creadoPorKilometrosEsperados : " + creadoPorKilometrosEsperados
					+ "  creadoPorKilometrajeApp : " + creadoPorKilometrajeApp);

					if (creadoPorKilometrosEsperados.equals(creadoPorKilometrajeApp)) {

						mensaje = "OK : Se Valida de manera correcta 'Creado Por' esperado': "
						+ creadoPorKilometrosEsperados + " con 'Creado Por encontrado' "
						+ creadoPorKilometrajeApp;
						msj = "ValidacionCorrectaCreadoPorKilometraje";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						System.err.println("actualizadoPorKilometrosEsperados : " + actualizadoPorKilometrosEsperados
						+ "  actualizadoPorKilometrajeApp : " + actualizadoPorKilometrajeApp);

						if (actualizadoPorKilometrosEsperados.equals(actualizadoPorKilometrajeApp)) {

							mensaje = "OK : Se Valida de manera correcta 'Actualizado Por' esperado': "
							+ actualizadoPorKilometrosEsperados + " con 'Actualizado Por' encontrado "
							+ actualizadoPorKilometrajeApp;
							msj = "ValidacionCorrectaActualizadooPorKilometraje";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							ValorEncontrado = true;
						} else {
							mensaje = "<p style='color:red;'>"
							+ "Error : Dato Incorrecto; 'Actualizado por' esperado': '"
							+ actualizadoPorKilometrosEsperados + "' ;'Actualizado Por encontrado' '"
							+ actualizadoPorKilometrajeApp + "'</p>";
							msj = "ValidacionIncorrectaActualizadoPor";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
						}
					} else {
						mensaje = "<p style='color:red;'>" + "Error : Dato Incorrecto; 'Creado por esperado': '"
						+ creadoPorKilometrosEsperados + "' ;'Creado Por encontrado' '"
						+ creadoPorKilometrajeApp + "'</p>";
						msj = "ValidacionIncorrectaCreadoPor";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
					}
				} else {
					mensaje = "<p style='color:red;'>"
					+ "Error : Kilometraje Incorrecto; 'Fecha Actualizacion Kilometraje esperado': '"
					+ fechaActualizacionKilometrosEsperados
					+ "' ;'Fecha Actualizacion Kilometraje encontrado' '" + fechaActualizacionKilometrajeApp
					+ "'</p>";
					msj = "ValidacionIncorrectaFechaActualizacion";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
				}
			} else {
				mensaje = "<p style='color:red;'>" + "Error : Kilometraje Incorrecto; 'Kilometraje esperado': '"
				+ kilometrosEsperados + "' ;'Kilometraje encontrado' '" + kilometrajeApp + "'</p>";
				msj = "ValidacionIncorrectaKIlometraje";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = false;
				errorBuffer.append("\n" + mensaje + "\n");
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Coinciden Kilometrajes" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida No coinciden kilometrajes" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
