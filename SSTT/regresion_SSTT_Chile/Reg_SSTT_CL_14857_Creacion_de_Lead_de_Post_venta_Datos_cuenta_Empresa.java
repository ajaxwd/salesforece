package regresion_SSTT_Chile;


import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class Reg_SSTT_CL_14857_Creacion_de_Lead_de_Post_venta_Datos_cuenta_Empresa {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "Reg_SSTT_CL_14857_Creacion_de_Lead_de_Post_venta_Datos_cuenta_Empresa.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "Reg_SSTT_CL_14857_Creacion_de_Lead_de_Post_venta_Datos_cuenta_Empresa.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);  
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		mensaje = "Usuario test en Ejecucion : " + Usuario;
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			// String modeloInteres = ExcelUtils.getCellData(posicionExcel, 26);

			int recorreExcell = 0;
			int inicioExcell = 0;
			int posicionExcel = 1;
//			String paisEjecucion = "";
//			String regionEjecucion = "";
//			String provinciaEjecucion = "";
//			String comunaEjecucion = "";
			String modeloInteres = "";

//			while (recorreExcell == 0) {
//				if ("Pais".equals(ExcelUtils.getCellData(0, inicioExcell))) {
//					paisEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
//					// mensaje = "<p>" + "Usuario pertenece a " + paisEjecucion + "</p>";
//					// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
//					System.out.println(mensaje);
//					Reporter.log(mensaje);
//					Log.info(mensaje);
//					recorreExcell = 1;
//					break;
//				}
//				inicioExcell = inicioExcell + 1;
//				recorreExcell = 0;
//				if (inicioExcell > 150) {
//					mensaje = "<p style='color:red;'>"
//							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
//					Log.info(mensaje);
//					Reporter.log(mensaje);
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError();
//				}
//			}
//
//			recorreExcell = 0;
//			inicioExcell = 0;
//			while (recorreExcell == 0) {
//				if ("Region".equals(ExcelUtils.getCellData(0, inicioExcell))) {
//					regionEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
//					recorreExcell = 1;
//					break;
//				}
//				inicioExcell = inicioExcell + 1;
//				recorreExcell = 0;
//				if (inicioExcell > 150) {
//					mensaje = "<p style='color:red;'>"
//							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
//					Log.info(mensaje);
//					Reporter.log(mensaje);
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError();
//				}
//			}
//
//			recorreExcell = 0;
//			inicioExcell = 0;
//			while (recorreExcell == 0) {
//				if ("Provincia".equals(ExcelUtils.getCellData(0, inicioExcell))) {
//					provinciaEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
//					recorreExcell = 1;
//					break;
//				}
//				inicioExcell = inicioExcell + 1;
//				recorreExcell = 0;
//				if (inicioExcell > 150) {
//					mensaje = "<p style='color:red;'>"
//							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
//					Log.info(mensaje);
//					Reporter.log(mensaje);
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError();
//				}
//			}
//
//			recorreExcell = 0;
//			inicioExcell = 0;
//			while (recorreExcell == 0) {
//				if ("Comuna".equals(ExcelUtils.getCellData(0, inicioExcell))) {
//					comunaEjecucion = ExcelUtils.getCellData(posicionExcel, inicioExcell);
//					recorreExcell = 1;
//					break;
//				}
//				inicioExcell = inicioExcell + 1;
//				recorreExcell = 0;
//				if (inicioExcell > 150) {
//					mensaje = "<p style='color:red;'>"
//							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
//					Log.info(mensaje);
//					Reporter.log(mensaje);
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError();
//				}
//			}

			recorreExcell = 0;
			inicioExcell = 0;
			while (recorreExcell == 0) {
				if ("Marca de Interes".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					modeloInteres = ExcelUtils.getCellData(posicionExcel, inicioExcell);
					System.err.println("Modelo " + modeloInteres);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento en Archivo de Ingreso de datos Favor Revisar" + "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnLead(driver)));
			paginaPrincipal.MenuPrincipal.btnLead(driver).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Leads ";
			msj = "ClickBtnLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(4000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
						// flujo

					driver.findElement(By.xpath(
							"(//h2[contains(text(),'Llamada de Tel�fono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
							.click();
					// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Log.info(mensaje);
					Reporter.log("<br>" + "<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) {
					cierraLlamado = 1;
				}
			}

			// try {// presiona boton borrar todo para llamados anteriores y no ocultar
			// botones de
			// // flujo
			//
			// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
			//
			// mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados ";
			// Log.info(mensaje);
			// Reporter.log("<p>" + "<br>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			//
			// }
			//
			// try {// presiona boton borrar todo para llamados anteriores y no ocultar
			// botones de
			// // flujo
			//
			// for (int i = 0; i < 5; i++) {
			//
			// driver.findElement(By.xpath(
			// "//*[@id=\"oneHeader\"]/div[2]/span/ul/li[7]/div[1]/div/section[1]/div/button/lightning-primitive-icon"))
			// .click();
			// // paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
			//
			// mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente ";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);
			// }
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			//
			// }

			WebDriverWait waitNuevo = new WebDriverWait(driver, 20);
			waitNuevo.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearLead(driver)));
			paginaPrincipal.MenuPrincipal.btnCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Crear Lead ";
			msj = "ClickBtnCrearLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.rdLeadEmpresa(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Lead Empresa";
			msj = "ClickRadioCrearEmpresa";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			paginaPrincipal.MenuPrincipal.btnSiguienteCrearLead(driver).click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(500);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";

			mensaje = "Flujo : LLenado de datos ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(3000);

			WebDriverWait waitSelectEstadoLead = new WebDriverWait(driver, 20);
			waitSelectEstadoLead.until(ExpectedConditions.visibilityOf(crearLead.Personal.selectEstadoLead(driver)));
			crearLead.Personal.selectEstadoLead(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.nuevoEstadoLead(driver).click();
			mensaje = "Flujo : Seleccion Estado de Lead :" + "NUEVO";
			msj = "SeleccionEstadoLead";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Rut
			String rut = utilidades.Funciones.generadorDeRut(49999999, 50000000);
			crearLead.Empresa.inputDocumentoIdentidad(driver).sendKeys(rut);
			mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
			msj = "IngresoRut";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(1000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputCompania(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			//String nombreEmpresaPlanilla = empresaAleatorio + " Cia Ltda";
			mensaje = "Flujo : Ingreso de Compa�ia Aleatorio : " + empresaAleatorio;
			msj = "IngresoCompania";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(1000);

			empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			empresaAleatorio = empresaAleatorio.toLowerCase();
			crearLead.Empresa.inputSitioWeb(driver).sendKeys("www." + empresaAleatorio + ".com");
			mensaje = "Flujo : Ingreso de Sitio Web Compa�ia Aleatorio : " + "www." + empresaAleatorio + ".com";
			msj = "IngresoSitioWeb";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(4000);

			crearLead.Empresa.selectPrioridad(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.medioInteresPrioridad(driver).click();
			mensaje = "Flujo : Seleccion Prioridad :" + "Medio Interes";
			msj = "SeleccionPrioridad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.selectOrigen(driver).click();
			Thread.sleep(2000);
			crearLead.Empresa.webOrigen(driver).click();
			mensaje = "Flujo : Ingreso de Origen : " + "WEB";
			msj = "IngresoOrigen";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(2000);

			String numEmpleados = "";
			azar = "";
			// Cadena de numeros aleatoria Para Numero Empleados
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numEmpleados = numEmpleados + azar;
			}
			crearLead.Empresa.inputNumeroEmpleados(driver).sendKeys(numEmpleados);
			mensaje = "Flujo : Ingreso Numero Empleados Aleatorio : " + numEmpleados;
			msj = "IngresoNumeroEmplados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			Thread.sleep(1500);

			crearLead.Empresa.selectTratamiento(driver).click();
			Thread.sleep(3000);
			WebDriverWait waitSrTratamiento = new WebDriverWait(driver, 20);
			waitSrTratamiento.until(ExpectedConditions.visibilityOf(crearLead.Empresa.srTratamiento(driver)));
			crearLead.Empresa.srTratamiento(driver).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			crearLead.Empresa.inputNombre(driver).sendKeys(nombreAleatorio);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();

			crearLead.Empresa.inputApellidoPaterno(driver).sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputApellidoMaterno(driver).sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMat";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(1500);

			// Cadena de caracteres aleatoria Para Nombre
			String referidoAleatorio = utilidades.Funciones.funcionNombreAleatorio();
			referidoAleatorio = referidoAleatorio + " " + utilidades.Funciones.funcionApellidoAleatorio();
			crearLead.Empresa.inputReferidoPor(driver).sendKeys(referidoAleatorio);
			mensaje = "Flujo : Ingreso de Referido Aleatorio : " + referidoAleatorio;
			msj = "IngresoReferido";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearLead.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numMovil = numMovil + azar;
			}
			crearLead.Empresa.inputMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearLead.Empresa.inputTelefono(driver).sendKeys("2" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngresoTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// crearLead.Empresa.selectMarcaInteres(driver).click();
			// Thread.sleep(2000);
			// crearLead.Empresa.mazdaMarcaInteres(driver).click();
			// mensaje = "Flujo : Seleccion Marca Interes :" + "Mazda";
			// msj = "SeleccionMarca";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearLead.Empresa.selectModeloInteres(driver).click();
			// Thread.sleep(2000);
			// crearLead.Empresa.mazdaMX5ModeloInteres(driver).click();
			// mensaje = "Flujo : Seleccion Modelo Interes :" + "Mazda MX 5 ";
			// msj = "SeleccionModelo";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearLead.Empresa.selectVersion(driver).click();
			// Thread.sleep(2000);
			// crearLead.Empresa.ver206MTVersion(driver).click();
			// mensaje = "Flujo : Seleccion Version Interes :" + "20L 6MT";
			// msj = "SeleccionVersion";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// mensaje = "<p style='color:green;'>" + "WARNING : Campo LOCAL deshabilitado
			// sin funcionalidad" + "</p>";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);

			Thread.sleep(2000);

			String[] arregloMarcaInteres = new String[modeloInteres.length()];
			for (int i = 0; i < modeloInteres.length(); i++) {
				arregloMarcaInteres[i] = Character.toString(modeloInteres.charAt(i));
			}

			for (int i = 0; i < modeloInteres.length(); i++) {

				crearLead.Empresa.inputMarcaInteres(driver).sendKeys(arregloMarcaInteres[i]);
				Thread.sleep(500);

			}

			Thread.sleep(4000);

			// WebElement elementoMarcaInteresPreProd =
			// driver.findElement(By.xpath("//div[@title='NEW MAZDA6 SDN 2.0 6AT']"));
			WebElement elementoMarcaInteresPreProd = driver
					.findElement(By.xpath("//div[@title='" + modeloInteres + "']"));
			elementoMarcaInteresPreProd.click();
			// mensaje = "Flujo : Seleccion Producto :" + "NEW MAZDA6 SDN 2.0 6AT";
			mensaje = "Flujo : Seleccion Producto :" + modeloInteres;
			msj = "IngresoProducto";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(1500);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// Thread.sleep(1500);
			//
			// crearLead.Empresa.inputDescripcionLocal(driver).sendKeys("Concesionario
			// DERCO");
			// mensaje = "Flujo : Campo descripcion local :" + "Concesionario DERCO";
			// msj = "IngresoConcesionario";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);

			// Thread.sleep(2000);
			//
			// crearLead.Empresa.inputConcesionario(driver).click();
			// crearLead.Empresa.inputConcesionario(driver).sendKeys("DERCO");
			// mensaje = "Flujo : Ingreso Campo Concesionario : " + "DERCO";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearLead.Empresa.inputConcesionario(driver).sendKeys(" ");
			// crearLead.Empresa.concesionarioSeleccion(driver).click();
			// mensaje = "Flujo : Seleccion de Concesionario : " + "DERCO";
			// msj = "SeleccionConcesionario";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);

			// Cadena de caracteres aleatoria Para Calle
			String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			String calleAleatorioNumero = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				calleAleatorioNumero = calleAleatorioNumero + azar;
			}

			crearLead.Empresa.inputCalle(driver).sendKeys(calleAleatorio);
			mensaje = "Flujo: Ingreso Campo Calle Aleatorio: " + calleAleatorio;
			msj = "IngresoCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.inputNumeroCalle(driver).sendKeys(calleAleatorioNumero);
			mensaje = "Flujo: Ingreso Campo Numero Aleatorio: " + calleAleatorioNumero;
			msj = "IngresoNumero";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.inputDptoOficinaDireccion(driver).sendKeys("Dpto-Of");
			mensaje = "Flujo : Campo Dpto-Oficina : " + "Dpto-Of";
			msj = "IngresoDptoOficinaDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearLead.Empresa.inputComplementoDireccion(driver).sendKeys("Casa Interior");
			mensaje = "Flujo : Campo Complemento Direccion :" + "Casa Interior";
			msj = "IngresoDetalleDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearLead.Empresa.inputPais(driver).click();
			crearLead.Empresa.inputPais(driver).sendKeys("CHI");
			Thread.sleep(1000);
			crearLead.Empresa.inputPais(driver).sendKeys("LE");
			mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputPais(driver).sendKeys(" ");
			crearLead.Empresa.paisSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Pais  : " + "CHILE";
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearLead.Empresa.inputRegion(driver).click();
			crearLead.Empresa.inputRegion(driver).sendKeys("SANT");
			Thread.sleep(1000);
			crearLead.Empresa.inputRegion(driver).sendKeys("IAGO");
			mensaje = "Flujo : Ingreso Campo Region : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputRegion(driver).sendKeys(" ");
			crearLead.Empresa.regionSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Region  : " + "SANTIAGO";
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearLead.Empresa.inputProvincia(driver).click();
			crearLead.Empresa.inputProvincia(driver).sendKeys("SANT");
			Thread.sleep(1000);
			crearLead.Empresa.inputProvincia(driver).sendKeys("IAGO");
			mensaje = "Flujo : Ingreso Campo Comuna : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputProvincia(driver).sendKeys(" ");
			crearLead.Empresa.comunaSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Comuna : " + "SANTIAGO";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearLead.Empresa.inputComuna(driver).click();
			crearLead.Empresa.inputComuna(driver).sendKeys("SANT");
			Thread.sleep(1000);
			crearLead.Empresa.inputComuna(driver).sendKeys("IAGO");
			mensaje = "Flujo : Ingreso Campo Comuna : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearLead.Empresa.inputComuna(driver).sendKeys(" ");
			crearLead.Empresa.comunaSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Comuna : " + "SANTIAGO";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearLead.Empresa.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(10000);

			WebDriverWait waitComparar = new WebDriverWait(driver, 20);
			waitComparar.until(ExpectedConditions.visibilityOf(crearLead.Personal.txtNombrePaginaInicio(driver)));
			String comparar = crearLead.Empresa.txtNombrePaginaInicio(driver).getText();

			mensaje = "Flujo : Se revisa creacion correcta de nuevo lead Empresa";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			if (comparar.contains(nombreAleatorio + " " + apellidoPaternoAleatorio)) {
				mensaje = "Ok : Coincide Nombre Ingresado " + nombreAleatorio + " " + apellidoPaternoAleatorio
						+ " con Nombre desplegado en Cracion de Lead " + comparar;
				msj = "OkCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>"
						+ "Error : No Coincide Nombre Ingresado con Nombre desplegado en Cracion de Lead " + "</p>";
				msj = "ErrorNoCoincideNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se Crea Con exito LEAD EMPRESA " + "<p>";
				msj = "OkPruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
						+ "ERROR: Prueba Fallida ; No se crea LAED Personal de Manera Correcta" + "<p>";
				Log.info(mensaje);
				msj = "ErrorPruebaFallida";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ErrorElementoNoVisible";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ExistenErrores";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();

	}

}
