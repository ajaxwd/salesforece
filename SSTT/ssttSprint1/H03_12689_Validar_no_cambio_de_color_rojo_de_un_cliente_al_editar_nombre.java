package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H03_12689_Validar_no_cambio_de_color_rojo_de_un_cliente_al_editar_nombre {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H03_12689_Validar_no_cambio_de_color_rojo_de_un_cliente_al_editar_nombre.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		String nombreCuentaFinal = "";
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H03_12689_Validar_no_cambio_de_color_rojo_de_un_cliente_al_editar_nombre.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			int swTiempoEspera = 0;
			int contarTiempoEspera = 0;

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);
					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
			msj = "ClickElementoencontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			String textoRedFlag;

			try {

				// String bandera =
				// driver.findElement(By.xpath("//img[@src='/img/samples/flag_red.gif']")).getText();
				// if (bandera.equals("")) {

				String bandera = driver.findElement(By.xpath("//img[@src='/img/samples/flag_red.gif']"))
						.getAttribute("alt");

				if (bandera.equals("Se deben actualizar los datos!")) {

					System.err.println("Texto ALT en Bndera '" + bandera + "'");

					// }
					//
					// if (driver.findElement(By.xpath("//img[@src='/img/samples/flag_red.gif']"))
					// != null) {
					textoRedFlag = new WebDriverWait(driver, 20, 100)
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//div[@class='slds-form-element__static slds-truncate']//span//img[@border='0']")))
							.getText();
					// textoRedFlag = new WebDriverWait(driver, 20,
					// 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='/img/samples/flag_red.gif']"))).getText();
					if (textoRedFlag
							.equals("Alerta, no se han modificado los datos de contacto en los �ltimos 6 meses")) {

						mensaje = "OK : Existe Bandera Roja y contiene mensaje : Alerta, no se han modificado los datos de contacto en los �ltimos 6 meses ";
						msj = "OkExisteBanderaRoja";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						ValorEncontrado = true;

					}

				} else {

					try {

						bandera = driver.findElement(By.xpath("//img[@src='/img/samples/flag_green.gif']"))
								.getAttribute("alt");

						if (bandera.equals("Datos OK!")) {
							System.err.println("Texto ALT en Bandera '" + bandera + "'");

							mensaje = "<p style='color:red;'>"
									+ "Error : No Existe Bandera Roja el registro tiene Bandera Verde" + "</p>";
							msj = "ErrorNoExisteBanderaRoja";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							ValorEncontrado = false;

							// WebElement greenFlag = driver
							// .findElement(By.xpath("//img[@src='/img/samples/flag_green.gif']"));
							// greenFlag.getText();
							// new WebDriverWait(driver, 20,
							// 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='/img/samples/flag_green.gif']"))).getText();

						} else {

							mensaje = "<p style='color:red;'>" + "Error : Existe Bandera Verde " + "</p>";
							msj = "ErrorExisteBanderaVerde";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							ValorEncontrado = false;

						}
						// } catch (Exception e) {
					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
						mensaje = "<p style='color:red;'>" + "Error : No Existe Bandera Roja o Verde " + "</p>";
						msj = "ErrorNoExisteBanderaVerdeoRoja";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}
					ValorEncontrado = false;
				}

			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

				mensaje = "<p style='color:red;'>" + "Error : No Existe Bandera Roja " + "</p>";
				msj = "ErrorNoExisteBanderaRoja";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = false;
			}

			if (ValorEncontrado == true) {

				System.err.println("ValorEncontrado '" + ValorEncontrado + "'");

				Thread.sleep(5000);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnModificarCuenta(driver)));
				crearCuenta.Personal.btnModificarCuenta(driver).click();
				mensaje = "Flujo : Click Modificar Cuentas ";
				msj = "ClickBtnModificarCuentas";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(5000);

				String nombreAntiguo = crearCuenta.Personal.inputNombreModificar(driver).getAttribute("value");

				String apellidoPaternoAntiguo = crearCuenta.Personal.inputApellidoPaternoModificar(driver)
						.getAttribute("value");

				Thread.sleep(2000);
				mensaje = "Flujo : Nombre Antiguo : " + nombreAntiguo;
				msj = "NombreAModificar";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				String nombreAleatorio = nombreAntiguo;
				while (nombreAleatorio.equals(nombreAntiguo)) {
					// Cadena de caracteres aleatoria Para Nombre
					nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();

				}

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNombreModificar(driver)))
						.sendKeys(Keys.chord(Keys.CONTROL, "a"), nombreAleatorio);
				Thread.sleep(2000);
				mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
				msj = "IngresoNombre";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(2000);

				nombreCuentaFinal = nombreAleatorio + " " + apellidoPaternoAntiguo;

				WebElement grabarModificar = driver.findElement(By.xpath("//button[contains(text(),'Guardar')]"));
				grabarModificar.click();

				sw = 0;
				contarTiempo = 0;
				String textoCuadroVerde = "";
				String textoCuadroVerdeDos = "";
				while (sw == 0) {
					if (contarTiempo > 30) {
						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible" + "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}
					try {
						textoCuadroVerde = new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerde(driver)))
								.getText();
						sw = 1;

					} catch (NoSuchElementException | ScreenshotException e) {
						sw = 0;
						contarTiempo = contarTiempo + 1;
					}
				}

				mensaje = "Flujo : Se revisa Edicion correcta de cuenta Persona";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				if (textoCuadroVerde.equals("Registro guardado en SAP.")) {

					Thread.sleep(5000);

					mensaje = "Ok :" + textoCuadroVerde;
					msj = "OkguardadoASap";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					Thread.sleep(2000);

					swTiempoEspera = 0;
					contarTiempoEspera = 0;
					while (swTiempoEspera == 0) {

						try {
							textoCuadroVerdeDos = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
							swTiempoEspera = 1;
						} catch (TimeoutException | NoSuchElementException e) {
							contarTiempoEspera = contarTiempoEspera + 1;
							swTiempoEspera = 0;
						}

						if (contarTiempoEspera > 30) {
							mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
									+ "</p>";
							msj = "ElementoNoEncontrado";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}

						if (textoCuadroVerdeDos.contains("Guardado")) {

							mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" + "<p>";
							msj = "GuardadoSalesForce";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							swTiempoEspera = 0;
							contarTiempoEspera = 0;
							while (swTiempoEspera == 0) {

								try {

									textoCuadroVerdeDos = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
									swTiempoEspera = 1;

								} catch (TimeoutException | NoSuchElementException e) {
									contarTiempoEspera = contarTiempoEspera + 1;
									swTiempoEspera = 0;
								}

								if (contarTiempoEspera > 15) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No Encuentra Elemento o No esta Visible En Sales Force "
											+ "</p>";
									msj = "ElementoNoEncontrado";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(2000);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.err.println(mensaje);
									ValorEncontrado = false;
									driver.close();
									errorBuffer.append("\n" + mensaje + "\n");
									throw new AssertionError(errorBuffer.toString());
								}
							}

							if (textoCuadroVerdeDos.contains("Registro guardado en Salesforce.")) {

								mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" + "<p>";
								msj = "GuardadoSalesForce";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								ValorEncontrado = true;

							} else {

								mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force " + "</p>";
								msj = "ErrorIngresoASap";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								errorBuffer.append("\n" + mensaje + "\n");
								ValorEncontrado = false;
							}
						}
					}
				} else {
					if (textoCuadroVerde.equals("Business Partner no existe, no se realizaran cambios en SAP")) {

						Thread.sleep(3000);

						mensaje = "Warning : " + textoCuadroVerde;
						msj = "WarningGuardadoASap";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						swTiempoEspera = 0;
						contarTiempoEspera = 0;
						while (swTiempoEspera == 0) {

							try {

								textoCuadroVerdeDos = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
								swTiempoEspera = 1;

							} catch (TimeoutException | NoSuchElementException e) {
								contarTiempoEspera = contarTiempoEspera + 1;
								swTiempoEspera = 0;
							}

							if (contarTiempoEspera > 15) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No Encuentra Elemento o No esta Visible En Sales Force " + "</p>";
								msj = "ElementoNoEncontrado";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(2000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								ValorEncontrado = false;
								driver.close();
								errorBuffer.append("\n" + mensaje + "\n");
								throw new AssertionError(errorBuffer.toString());
							}
						}

						if (textoCuadroVerdeDos.contains("Registro guardado en Salesforce.")) {

							mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" + "<p>";
							msj = "GuardadoSalesForce";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							ValorEncontrado = true;

						} else {

							mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force " + "</p>";
							msj = "ErrorIngresoASap";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;

						}

					} else {
						mensaje = "<p style='color:red;'>" + "Error : No Interactua en SAP " + "</p>";
						msj = "ErrorIngresoASap";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
					}
				}

				ValorEncontrado = true;

				mensaje = "Flujo : Se verifican Banderas despues de editar";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
						.sendKeys(Keys.ENTER);
				mensaje = "Flujo : Click Boton Cuentas ";
				msj = "ClickBtnCuentas";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(3000);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions
								.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
						.sendKeys(rutCuenta);
				mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
				msj = "InputElementoBusqueda";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions
								.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
						.sendKeys(Keys.ENTER);

				Thread.sleep(5000);

				swTiempoEspera = 0;
				contarTiempoEspera = 0;

				sw = 0;
				contarTiempo = 0;
				while (sw == 0) {
					if (contarTiempo > 15) {
						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
								+ nombreCuenta + "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}

					nombreCuenta = nombreCuentaFinal;

					try {
						Thread.sleep(500);
						new WebDriverWait(driver, 40, 100).until(ExpectedConditions.visibilityOf(
								paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
								.click();
						sw = 1;

					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
						sw = 0;
						contarTiempo = contarTiempo + 1;

					}
				}

				mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
				msj = "ClickElementoencontrado";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(5000);

				textoRedFlag = "";

				try {
					//
					// if (driver.findElement(By.xpath("//img[@src='/img/samples/flag_red.gif']"))
					// != null) {
					String bandera = driver.findElement(By.xpath("//img[@src='/img/samples/flag_red.gif']"))
							.getAttribute("alt");
					if (bandera.equals("Se deben actualizar los datos!")) {

						textoRedFlag = new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
										"//div[@class='slds-form-element__static slds-truncate']//span//img[@border='0']")))
								.getText();
						// textoRedFlag = new WebDriverWait(driver, 20,
						// 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='/img/samples/flag_red.gif']"))).getText();
						if (textoRedFlag
								.equals("Alerta, no se han modificado los datos de contacto en los �ltimos 6 meses")) {

							mensaje = "OK : Se mantiene Bandera Roja y contiene mensaje : Alerta, no se han modificado los datos de contacto en los �ltimos 6 meses ";
							msj = "OkExisteBanderaRoja";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							ValorEncontrado = true;
						}
					} else {

						try {

							bandera = driver.findElement(By.xpath("//img[@src='/img/samples/flag_green.gif']"))
									.getAttribute("alt");

							if (bandera.equals("Datos OK!")) {
								System.err.println("Texto ALT en Bandera '" + bandera + "'");

								mensaje = "<p style='color:red;'>"
										+ "Error : No Existe Bandera Roja el registro tiene Bandera Verde" + "</p>";
								msj = "ErrorNoExisteBanderaRoja";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);
								ValorEncontrado = false;

								// WebElement greenFlag = driver
								// .findElement(By.xpath("//img[@src='/img/samples/flag_green.gif']"));
								// greenFlag.getText();
								// new WebDriverWait(driver, 20,
								// 100).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='/img/samples/flag_green.gif']"))).getText();

							} else {

								mensaje = "<p style='color:red;'>" + "Error : Existe Bandera Verde " + "</p>";
								msj = "ErrorExisteBanderaVerde";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);
								ValorEncontrado = false;

							}
							// } catch (Exception e) {
						} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
							mensaje = "<p style='color:red;'>" + "Error : No Existe Bandera Roja o Verde " + "</p>";
							msj = "ErrorNoExisteBanderaVerdeoRoja";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}
						ValorEncontrado = false;
					}

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

					mensaje = "<p style='color:red;'>" + "Error : No Existe Bandera Roja " + "</p>";
					msj = "ErrorNoExisteBanderaRoja";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
					ValorEncontrado = false;
				}
			} else {
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Edita Nombre y Mantiene Bandera Roja"
						+ "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: No encuentra Bandera, No Edita Cuenta o No Mantiene Bandera Roja" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
