package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);
		
		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
					.click();
			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(10000);

			int contadorDeErroresTotal = 0;
			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			recorreExcell = 0;
			String nombreLink = "";
			while (recorreExcell == 0) {

				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLink = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink));
						principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink).getText();
						mensaje = "OK : Link esperado '" + nombreLink + "' Existe en 'Relacionado'";
						msj = nombreLink + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Link esperado '" + nombreLink
								+ "' NO Existe en 'Relacionado'" + "</p>";
						System.err.println(mensaje);

						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}

				} else {

					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			// Campa�a se toma elemento Manual

			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver));
				principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver).getText();
				mensaje = "OK : Link esperado 'Campa�a' Existe en 'Relacionado'";
				msj = "CampanaExiste";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				contadorCorrectos = contadorCorrectos + 1;
				correctos = correctos + "<p style='color:blue;'>" + "- " + "Campa�a" + "</p>";
				System.out.println(correctos);
				ValorEncontrado = true;

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

				mensaje = "<p style='color:red;'>" + "Error : Link esperado 'Campa�a' NO Existe en 'Relacionado'"
						+ "</p>";
				System.err.println(mensaje);
				contadorDeErroresTotal = contadorDeErroresTotal + 1;
				erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
				contadorIncorrectos = contadorIncorrectos + 1;
			}

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'RELACIONADO VISTA 360 CLIENTE':" + "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.err.println(mensajeResumen);

			Thread.sleep(2000);
			
			if (contadorIncorrectos > 0) {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
						+ " 'Label' " + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
				
			} else {

				Thread.sleep(2000);
				
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						driver.findElement(By.xpath("//span[@title='Seguros']")));
				
				
				String textoOrdenesUltimoAno = new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions
								.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkOrdenesUltimoAno(driver)))
						.getText();

				if (textoOrdenesUltimoAno.contains("(0)")) {

					mensaje = "<p style='color:red;'>" + "Error: No Existen Ordenes en el Ultimo A�o " + "<p>";
					msj = "ErrorNOHayOT";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;

				} else {

					mensajeResumen = "";
					contadorCorrectos = 0;
					contadorIncorrectos = 0;
					nombreLabel = "";
					correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
					erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
					contadorCorrectos = 0;
					contadorIncorrectos = 0;
					recorreExcell = 0;
					inicioExcell = 0;

					utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
					utilidades.DatosInicialesYDrivers.File_TestData = "H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT.xlsx";
					ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
							"Hoja1");
					while (recorreExcell == 0) {
						if ("Label Orden 360".equals(ExcelUtils.getCellData(0, inicioExcell))) {
							recorreExcell = 1;
							break;
						}

						if (inicioExcell > 100) {
							mensaje = "<p style='color:red;'>"
									+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
							msj = "DatoNoEncontradoEnEntradaDeDatos";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}

						inicioExcell = inicioExcell + 1;
						recorreExcell = 0;
					}

					recorreExcell = 0;
					nombreLink = "";
					while (recorreExcell == 0) {

						if ("Label Orden 360".equals(ExcelUtils.getCellData(0, inicioExcell))) {
							try {
								nombreLink = ExcelUtils.getCellData(1, inicioExcell);
								principalCuenta.MenuPrincipalCuenta.labelOrdenesUltimoAnoVista360(driver, nombreLink)
										.getText();
								mensaje = "OK : Label esperado '" + nombreLink
										+ "' Existe en 'Ordenes del Ultimo A�o 360'";
								msj = nombreLink + "Existe";
								msj = msj.replace(" ", "");
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(2000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);

								contadorCorrectos = contadorCorrectos + 1;
								correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
								System.out.println(correctos);
								ValorEncontrado = true;

							} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

								mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLink
										+ " NO  Existe en 'Ordenes del Ultimo A�o 360'" + "</p>";
								System.err.println(mensaje);

								erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
								contadorIncorrectos = contadorIncorrectos + 1;
							}

						} else {

							recorreExcell = 1;
							break;
						}
						inicioExcell = inicioExcell + 1;
						recorreExcell = 0;
						if (inicioExcell > 100) {
							mensaje = "<p style='color:red;'>"
									+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
							msj = "DatoNoEncontradoEnEntradaDeDatos";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							driver.close();
							throw new AssertionError(errorBuffer.toString());
						}
					}

					Thread.sleep(2000);

					mensajeResumen = "<p><b>" + "RESUMEN REVISION LABEL VISTA 360 'OREDENES DEL ULTIMO A�O':"
							+ "</p></b>";
					Log.info(mensajeResumen);
					Reporter.log(mensajeResumen);
					System.out.println(mensajeResumen);

					mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
							+ "</p></b>";
					Log.info(mensajeResumen);
					Reporter.log(mensajeResumen);
					System.out.println(mensajeResumen);

					mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
					Log.info(mensajeResumen);
					Reporter.log(mensajeResumen);
					System.out.println(mensajeResumen);

					mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
							+ "</p></b>";
					Log.info(mensajeResumen);
					Reporter.log(mensajeResumen);
					System.out.println(mensajeResumen);

					mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
					Log.info(mensajeResumen);
					Reporter.log(mensajeResumen);
					System.err.println(mensajeResumen);

					if (contadorIncorrectos > 0) {

						mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra "
								+ contadorIncorrectos + " 'Label' " + "</p>";
						msj = "PruebaErronea";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
					} else {

						mensaje = "Flujo : Click link Primera Orden Encontrada  ";
						msj = "ClickLinkPrimeraOrden";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOf(
										principalCuenta.MenuPrincipalCuenta.primerElementoOrdenUltimoAno(driver)))
								.click();

						Thread.sleep(5000);

						mensajeResumen = "";
						contadorCorrectos = 0;
						contadorIncorrectos = 0;
						nombreLabel = "";
						correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
						erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
						contadorCorrectos = 0;
						contadorIncorrectos = 0;
						recorreExcell = 0;
						inicioExcell = 0;

						utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
						utilidades.DatosInicialesYDrivers.File_TestData = "H08_12720_Validar_la_no_modificacion_de_datos_de_OT_cuenta_PP_Perf�l_Asesor_de_SSTT.xlsx";
						ExcelUtils.setExcelFile(
								DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
						while (recorreExcell == 0) {
							if ("Label Orden Principal".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								recorreExcell = 1;
								break;
							}
							
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							
							if (inicioExcell > 100) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
								msj = "DatoNoEncontradoEnEntradaDeDatos";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(2000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								ValorEncontrado = false;
								errorBuffer.append("\n" + mensaje + "\n");
								driver.close();
								throw new AssertionError(errorBuffer.toString());
							}
							
						}

						recorreExcell = 0;
						nombreLink = "";
						while (recorreExcell == 0) {
							Thread.sleep(500);
							if ("Label Orden Principal".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								try {
									nombreLink = ExcelUtils.getCellData(1, inicioExcell);
									((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
											principalCuenta.MenuPrincipalCuenta.linkRevisionPrincipalActivo(driver,
													nombreLink));
									principalCuenta.MenuPrincipalCuenta.linkRevisionPrincipalActivo(driver, nombreLink)
											.getText();
									mensaje = "OK : Label esperado '" + nombreLink
											+ "' Existe en 'Ordenes de Trabajo Principal'";
									msj = nombreLink + "Existe";
									msj = msj.replace(" ", "");
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(2000);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.out.println(mensaje);

									contadorCorrectos = contadorCorrectos + 1;
									correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
									System.out.println(correctos);
									ValorEncontrado = true;

								} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

									mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLink
											+ " NO  Existe en 'Ordenes de Trabajo Principal'" + "</p>";
									System.err.println(mensaje);

									erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
									contadorIncorrectos = contadorIncorrectos + 1;
								}

							} else {

								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
							if (inicioExcell > 100) {
								mensaje = "<p style='color:red;'>"
										+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
								msj = "DatoNoEncontradoEnEntradaDeDatos";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(2000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								ValorEncontrado = false;
								errorBuffer.append("\n" + mensaje + "\n");
								driver.close();
								throw new AssertionError(errorBuffer.toString());
							}
						}

						mensajeResumen = "<p><b>" + "RESUMEN REVISION LABEL 'OREDENES DE TRABAJO PRINCIPAL':"
								+ "</p></b>";
						Log.info(mensajeResumen);
						Reporter.log(mensajeResumen);
						System.out.println(mensajeResumen);

						mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
								+ "</p></b>";
						Log.info(mensajeResumen);
						Reporter.log(mensajeResumen);
						System.out.println(mensajeResumen);

						mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
						Log.info(mensajeResumen);
						Reporter.log(mensajeResumen);
						System.out.println(mensajeResumen);

						mensajeResumen = "<p><b>" + contadorIncorrectos
								+ " Titulos de campo NO coinciden con los esperados" + "</p></b>";
						Log.info(mensajeResumen);
						Reporter.log(mensajeResumen);
						System.out.println(mensajeResumen);

						mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
						Log.info(mensajeResumen);
						Reporter.log(mensajeResumen);
						System.err.println(mensajeResumen);

						if (contadorIncorrectos > 0) {

							mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra "
									+ contadorIncorrectos + " 'Label' " + "</p>";
							msj = "PruebaErronea";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;

						}else {
							
							driver.navigate().refresh();
							
							Thread.sleep(5000);
							
							try {
								
								System.err.println(principalCuenta.MenuPrincipalCuenta.btnModificarOrdenDeTrabajo(driver).getText());
								mensaje = "<p style='color:red;'>" + "Error: Esta desplegado boton 'Modificar' " + "</p>";
								msj = "PruebaErronea";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								errorBuffer.append("\n" + mensaje + "\n");
								ValorEncontrado = false;
								
								ValorEncontrado = false;
							} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
								
								mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; No esta desplegado boton 'Modificar' " + "<p>";
								msj = "PruebaCorrecta";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log(
										"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
								System.out.println(mensaje);
								
								ValorEncontrado = true;
							}
							
						}

					}

				}
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; campos verificados" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}


}
