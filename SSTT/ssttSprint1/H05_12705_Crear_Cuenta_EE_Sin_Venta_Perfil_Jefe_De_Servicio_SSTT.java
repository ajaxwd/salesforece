package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H05_12705_Crear_Cuenta_EE_Sin_Venta_Perfil_Jefe_De_Servicio_SSTT {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H05_12705_Crear_Cuenta_EE_Sin_Venta_Perfil_Jefe_De_Servicio_SSTT.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H05_12705_Crear_Cuenta_EE_Sin_Venta_Perfil_Jefe_De_Servicio_SSTT.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String cuentaPrincipal = ExcelUtils.getCellData(1, 4);

		mensaje = "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver))).sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearCuentas(driver)))
					.click();
			mensaje = "Flujo : Click Boton Crear Cuentas ";
			msj = "ClickBtnCrearCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.rdCuentasEmpresas(driver)))
					.click();
			paginaPrincipal.MenuPrincipal.rdCuentasEmpresas(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Cuenta Empresa";
			msj = "ClickBtnRadioCtaEmpresa";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(
					ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnSiguienteCrearCuenta(driver)))
					.click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";

			mensaje = "Flujo : LLenado de datos ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			// Cadena de caracteres aleatoria Para Rut
			String rut = utilidades.Funciones.generadorDeRut(49999999, 50000000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputDocumentoIdentidad(driver)))
					.sendKeys(rut);
			mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
			msj = "IngresoRut";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputGiro(driver)))
					.sendKeys("Testing QA");
			mensaje = "Flujo : Ingreso de Giro : " + "Testing QA";
			msj = "IngresoGiro";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectTratamiento(driver))).click();
			// Thread.sleep(2000);
			// new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.visibilityOf(crearCuenta.Personal.rolSR(driver))).click();
			// mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			// msj = "IngresoTratamiento";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.selectClasificacionFlotas(driver)))
					.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.clasificacionFlotasSeleccion(driver, "A")))
					.click();
			mensaje = "Flujo : Ingreso de Flota : " + "A";
			msj = "IngresoFlota";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			crearCuenta.Empresa.inputRazonSocial(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			mensaje = "Flujo : Ingreso de Razon Social Aleatorio : " + empresaAleatorio;
			msj = "IngresoRazonSocial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			empresaAleatorio = empresaAleatorio.toLowerCase();
			crearCuenta.Empresa.inputSitioWeb(driver).sendKeys("www." + empresaAleatorio + ".com");
			mensaje = "Flujo : Ingreso de Sitio Web Empresa Aleatorio : " + "www." + empresaAleatorio + ".com";
			msj = "IngresoSitioWeb";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputNacionalidad(driver));

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver))).click();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver)))
					.sendKeys("CHI");
			Thread.sleep(500);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver)))
					.sendKeys("LE");
			Thread.sleep(500);
			mensaje = "Flujo : Ingreso Campo Nacionalidad : " + "CHILE";
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>");
			System.out.println(mensaje);

			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.nacionalidadSeleccion(driver))).click();
			mensaje = "Flujo : Seleccion de Pais Nacionalidad : " + "CHILE";
			msj = "SeleccionPaisNacionalidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String[] arregloCuentaPrincipal = new String[cuentaPrincipal.length()];
			for (int i = 0; i < cuentaPrincipal.length(); i++) {
				arregloCuentaPrincipal[i] = Character.toString(cuentaPrincipal.charAt(i));
			}

			for (int i = 0; i < cuentaPrincipal.length(); i++) {

				crearCuenta.Empresa.inputCuentaPrincipal(driver).sendKeys(arregloCuentaPrincipal[i]);
				Thread.sleep(300);

			}
			mensaje = "Flujo : Ingreso Campo Cuenta Principal : " + cuentaPrincipal;
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.cuentaPrincipalSeleccionParametro(driver, cuentaPrincipal).click();
			mensaje = "Flujo : Seleccion de Cuenta Principal  : " + cuentaPrincipal;
			msj = "SeleccionCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearCuenta.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo  al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearCuenta.Empresa.inputTelefono(driver).sendKeys("2" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngresoNumeroTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputPais(driver).click();
			crearCuenta.Empresa.inputPais(driver).sendKeys("CHI");
			Thread.sleep(300);
			crearCuenta.Empresa.inputPais(driver).sendKeys("LE");
			mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.inputPais(driver).sendKeys(" ");
			crearCuenta.Empresa.paisSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Pais  : " + "CHILE";
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputRegion(driver).click();
			crearCuenta.Empresa.inputRegion(driver).sendKeys("SANTIAGO");
			mensaje = "Flujo : Ingreso Campo Region : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputRegion(driver));

			Thread.sleep(2000);
			crearCuenta.Empresa.inputRegion(driver).sendKeys(" ");
			crearCuenta.Empresa.regionSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Region  : " + "SANTIAGO";
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputProvinciaDireccion(driver).click();
			crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys("SANT");
			Thread.sleep(1000);
			crearCuenta.Personal.inputProvinciaDireccion(driver).sendKeys("IAGO");
			mensaje = "Flujo : Ingreso Campo Provincia Direccion : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>");
			System.out.println(mensaje);

			Thread.sleep(3000);
			crearCuenta.Personal.provinciaDireccionSeleccion(driver).click();
			mensaje = "Flujo : Seleccion Campo provincia Direccion : " + "SANTIAGO";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputComuna(driver).click();
			crearCuenta.Empresa.inputComuna(driver).sendKeys("SANTIAGO");
			mensaje = "Flujo : Ingreso Campo Comuna : " + "SANTIAGO";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.inputComuna(driver).sendKeys(" ");
			crearCuenta.Empresa.comunaSeleccion(driver).click();
			mensaje = "Flujo : Seleccion de Comuna : " + "SANTIAGO";
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// Cadena de caracteres aleatoria Para Calle
			String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			crearCuenta.Empresa.inputCalleAleatorio(driver).sendKeys(calleAleatorio);
			mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
			msj = "IngresoCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String numDireccionAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numDireccionAzar = numDireccionAzar + azar;
			}
			crearCuenta.Empresa.inputNumeroDireccion(driver).sendKeys(numDireccionAzar);
			mensaje = "Flujo : Ingreso Numero Direccion Azar : " + calleAleatorio;
			msj = "IngresoNumDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String deptoCasaOficinaAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero casa
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
			}
			crearCuenta.Empresa.inputDeptoCasaOficina(driver).sendKeys("Oficina " + deptoCasaOficinaAzar);
			mensaje = "Flujo : Ingreso Casa Azar : " + "Oficina " + deptoCasaOficinaAzar;
			msj = "IngresoDetalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputComplementoDireccion(driver).sendKeys("inmueble interior");
			mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
			msj = "IngresoComplementoDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// Cadena de caracteres aleatoria Para Calle Laboral
			String calleComercialAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			String calleComercialAleatorioNumero = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				calleComercialAleatorioNumero = calleComercialAleatorioNumero + azar;
			}

			crearCuenta.Empresa.inputCalleComercial(driver)
					.sendKeys(calleComercialAleatorio + " " + calleComercialAleatorioNumero);
			mensaje = "Flujo: Ingreso Campo Calle Direccion Laboral Aleatorio: " + calleComercialAleatorio + " "
					+ calleComercialAleatorioNumero;
			msj = "IngresoCalleLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String codigoPostallAleatorio = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 6; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				codigoPostallAleatorio = codigoPostallAleatorio + azar;
			}
			crearCuenta.Empresa.inputCodigoPostalComercial(driver).sendKeys(codigoPostallAleatorio);
			mensaje = "Flujo : Ingreso Campo Codigo Postal Laboral Aleatorio: " + codigoPostallAleatorio;
			msj = "IngresoCodigoPostal";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputComunaComercial(driver).sendKeys("QUILICURA");
			mensaje = "Flujo : Ingreso Campo Comuna Laboral : " + "QUILICURA";
			msj = "IngresoComunaLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputProvinciaComercial(driver).sendKeys("QUILICURA");
			mensaje = "Flujo : Ingreso Campo Provincia Comercial : " + "QUILICURA";
			msj = "IngresoProvinciaComercial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputPaisEnvio(driver).sendKeys("CHILE");
			mensaje = "Flujo : Ingreso Campo Pais Envio : " + "CHILE";
			msj = "IngresoPaisEnvio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputReferenciaDireccionComercial(driver).sendKeys("SECTOR INDUSTRIAL");
			mensaje = "Flujo : Ingreso Campo Referencia Direccion Comercial : " + "SECTOR INDUSTRIAL";
			msj = "IngresoReferenciaDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			crearCuenta.Empresa.inputDescripcionDireccionComercial(driver).sendKeys("SECTOR INDUSTRIAL");
			mensaje = "Flujo : Ingreso Campo Descripcion Direccion Comercial : " + "SECTOR INDUSTRIAL";
			msj = "IngresoCDescripcionDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

//			String numSap = "";
//			azar = "";
//			// Cadena de numeros aleatoria Para telefono movil
//			for (int i = 0; i < 10; i++) {
//				azar = utilidades.Funciones.numeroAleatorio(1, 8);
//				numSap = numSap + azar;
//			}
//			crearCuenta.Empresa.inputIdExternoSap(driver).sendKeys(numSap);
//			mensaje = "Flujo : Ingreso ID EXterno SAP Aleatorio : " + numSap;
//			msj = "IngresoIdSap";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);

			crearCuenta.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String textoCuadroVerdePequeno = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerdePequeno(driver)))
					.getText();

			if (textoCuadroVerdePequeno.contains("Se cre� Cuenta")) {

				mensaje = "Flujo : Se cre� Cuenta";
				msj = "CreaCuenta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(3000);

				new WebDriverWait(driver, 20, 100)
						.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnCrearEnSap(driver))).click();
				mensaje = "Flujo : Click Boton Crear En Sap";
				msj = "ClickBtnCrearSap";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				int sw = 0;
				int contarTiempo = 0;
				String textoCuadroVerde = "";
				while (sw == 0) {
					if (contarTiempo > 30) {
						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, "
								+ "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}
					try {
						textoCuadroVerde = new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerde(driver)))
								.getText();
						sw = 1;

					} catch (NoSuchElementException | ScreenshotException e) {
						sw = 0;
						contarTiempo = contarTiempo + 1;
					}
				}

				mensaje = "Flujo : Se revisa creacion correcta de nueva cuenta personal";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				if (textoCuadroVerde.equals("Envio correcto a SAP")) {
					mensaje = "Ok :" + textoCuadroVerde;
					msj = "OkIngresoASap";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
					ValorEncontrado = true;
				} else {
					mensaje = "<p style='color:red;'>" + "Error : No Ingreso Datos A SAP " + "</p>";
					msj = "ErrorIngresoASap";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
				}

			}else {
				
				mensaje = "<p style='color:red;'>" + "Error : No Crea Cuenta " + "</p>";
				msj = "ErrorIngresoASap";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src="
						+ rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Cuenta Exitoso  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Cuenta Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
