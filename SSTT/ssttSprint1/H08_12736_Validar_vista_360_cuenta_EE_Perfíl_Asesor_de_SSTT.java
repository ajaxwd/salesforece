package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
//import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H08_12736_Validar_vista_360_cuenta_EE_Perf�l_Asesor_de_SSTT {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H08_12736_Validar_vista_360_cuenta_EE_Perf�l_Asesor_de_SSTT.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H08_12736_Validar_vista_360_cuenta_EE_Perf�l_Asesor_de_SSTT.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);
//		String numeroOrden = ExcelUtils.getCellData(1, 6);
//		String fechaApertura = ExcelUtils.getCellData(1, 7);
//		String nombreActivo = ExcelUtils.getCellData(1, 8);
//		String matricula = ExcelUtils.getCellData(1, 9);
//		String clasePedido = ExcelUtils.getCellData(1, 10);
//		String estadoPedido = ExcelUtils.getCellData(1, 11);
//		String motivoPedido = ExcelUtils.getCellData(1, 12);
//		String fechaActualizacionPedido = ExcelUtils.getCellData(1, 13);
//		String fechaCierrePedido = ExcelUtils.getCellData(1, 14);
//		String fechaEntregaPedido = ExcelUtils.getCellData(1, 15);
//		String nombreAsesorPedido = ExcelUtils.getCellData(1, 16);
//		String codigoCentroOtPedido = ExcelUtils.getCellData(1, 17);
//		String localPedido = ExcelUtils.getCellData(1, 18);
//		String valorPedido = ExcelUtils.getCellData(1, 19);
//		String recallIdPedido = ExcelUtils.getCellData(1, 20);
//		String vinPedido = ExcelUtils.getCellData(1, 21);
//		String marcaPedido = ExcelUtils.getCellData(1, 22);
//		String modeloPedido = ExcelUtils.getCellData(1, 23);
//		String kilometrajePedido = ExcelUtils.getCellData(1, 24);
//		String requerimientoPedido = ExcelUtils.getCellData(1, 25);
//		String detallePedido = ExcelUtils.getCellData(1, 25);
		
		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
					.click();
			mensaje = "Flujo : Click link Relacionado Cuenta";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(10000);

			int contadorDeErroresTotal = 0;
			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H08_12736_Validar_vista_360_cuenta_EE_Perf�l_Asesor_de_SSTT.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			recorreExcell = 0;
			String nombreLink = "";
			while (recorreExcell == 0) {

				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLink = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink));
						principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink).getText();

						mensaje = "OK : Link esperado '" + nombreLink + "' Existe en 'Relacionado'";
						msj = nombreLink + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Link esperado '" + nombreLink
								+ "' NO Existe en 'Relacionado'" + "</p>";
						System.err.println(mensaje);

						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}

				} else {

					recorreExcell = 1;
					break;
				}

				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			// Campa�a se toma elemento Manual

			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver));
				principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver).getText();
				mensaje = "OK : Link esperado 'Campa�a' Existe en 'Relacionado'";
				msj = "CampanaExiste";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				contadorCorrectos = contadorCorrectos + 1;
				correctos = correctos + "<p style='color:blue;'>" + "- " + "Campa�a" + "</p>";
				System.out.println(correctos);
				ValorEncontrado = true;

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

				mensaje = "<p style='color:red;'>" + "Error : Link esperado 'Campa�a' NO Existe en 'Relacionado'"
						+ "</p>";
				System.err.println(mensaje);
				contadorDeErroresTotal = contadorDeErroresTotal + 1;
				erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
				contadorIncorrectos = contadorIncorrectos + 1;
			}

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'RELACIONADO VISTA 360 CLIENTE':" + "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.err.println(mensajeResumen);

			Thread.sleep(2000);

			if (contadorIncorrectos > 0) {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
						+ " 'Label' " + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;

			} else {

				mensaje = "<p style='color:blue;'>" + "OK: Coinciden " + contadorCorrectos	+ " 'Etiquetas' " + "</p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

				
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; campos verificados" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}

			
			
//-------------------------------------------------------------------CODIGO OBSOLETO----------------------------------------			
////			JavascriptExecutor jse = (JavascriptExecutor) driver;
////			jse.executeScript("arguments[0].scrollIntoView();",
////					principalCuenta.MenuPrincipalCuenta.btnConsultarOt(driver));
//			
////			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
////					driver.findElement(By.xpath("//span[@title='Historial de cuentas']")));
////			
//			
////			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
////					driver.findElement(By.xpath("//span[@title='�rdenes del �ltimo a�o']")));
////			System.err.println("Sube a �rdenes del �ltimo a�o");
////			mensaje = "TEST1";
////			msj = "TEST1";
////			posicionEvidencia = posicionEvidencia + 1;
////			posicionEvidenciaString = Integer.toString(posicionEvidencia);
////			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
////			Thread.sleep(2000);
////			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
////			Log.info(mensaje);
////			Reporter.log(
////					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
////			System.out.println(mensaje);
//			Thread.sleep(2000);
//			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)", "");
////			System.err.println("sube 100 pt");
//			Thread.sleep(2000);
//			
////			if (driver.findElement(By.xpath("//span[@title='Oportunidades']")).isDisplayed()) {
////				System.err.println("Good ");
////				System.err.println("Good value  "+driver.findElement(By.xpath("//span[@title='�rdenes del �ltimo a�o']")).getText());
////				mensaje = "TEST2";
////				msj = "TEST2";
////				posicionEvidencia = posicionEvidencia + 1;
////				posicionEvidenciaString = Integer.toString(posicionEvidencia);
////				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
////				Thread.sleep(2000);
////				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
////				Log.info(mensaje);
////				Reporter.log(
////						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
////				System.out.println(mensaje);
////				driver.findElement(By.xpath("//span[@title='Oportunidades']")).click();
////				throw new AssertionError(errorBuffer.toString());
////			}else {
////				System.err.println("Fail");
////			}
////			JavascriptExecutor jse = (JavascriptExecutor)driver;
////			jse.executeScript("window.scrollBy(0,250)", "");
////			jse.executeScript("scroll(0, 250);");
//			
//			
//			Thread.sleep(5000);
//
//			String textoOrdenesUltimoAno = new WebDriverWait(driver, 20, 100).until(
//					ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkOrdenesUltimoAnoCantidad(driver)))
//					.getText();
//			System.err.println("Cantidad de ordenes "+textoOrdenesUltimoAno);
//			
//			if (textoOrdenesUltimoAno.contains("(0)")) {
//
//				mensaje = "<p style='color:red;'>" + "Error: No Existen Ordenes en el Ultimo A�o " + "<p>";
//				msj = "ErrorNOHayOT";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.err.println(mensaje);
//				errorBuffer.append("\n" + mensaje + "\n");
//				ValorEncontrado = false;
//			} else {
//				
//				try {
//					String ordenPrincipal = principalCuenta.MenuPrincipalCuenta
//							.linkNumeroDeOrdenPrincipal(driver, numeroOrden).getText();
//					
//					String fechaPrincipal = principalCuenta.MenuPrincipalCuenta
//							.txtFechaAperturaPrincipal(driver, fechaApertura).getText();
//					
//					String activoPrincipal = "";
//					if (nombreActivo == "") {
//						activoPrincipal = nombreActivo;
//					} else {
//						activoPrincipal = principalCuenta.MenuPrincipalCuenta
//								.txtNombreDelActivoPrincipal(driver, nombreActivo).getText();
//					}
//
//					String matriculaPrincipal = "";
//					if (nombreActivo == "") {
//						matriculaPrincipal = matricula;
//					} else {
//						matriculaPrincipal = principalCuenta.MenuPrincipalCuenta
//								.txtMatriculaPrincipal(driver, matricula).getText();
//					}
//
//					System.err.println("ordenPrincipal " + ordenPrincipal +"  numeroOrden "+numeroOrden);
//					System.err.println("fechaPrincipal " + fechaPrincipal+"  fechaApertura "+fechaApertura);
//					System.err.println("activoPrincipal " + activoPrincipal+"  nombreActivo "+nombreActivo);
//					System.err.println("matriculaPrincipal " + matriculaPrincipal+"  matricula "+matricula);
//					
//					if (ordenPrincipal.equals(numeroOrden) && fechaPrincipal.equals(fechaApertura) && activoPrincipal.equals(nombreActivo) && matriculaPrincipal.equals(matricula) ) {
//						
//						mensaje = "Flujo : Coincide Vista 360 ";
//						msj = "Coincide360";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						Thread.sleep(2000);
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log(
//								"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//						System.out.println(mensaje);
//						
//						ValorEncontrado = true;
//						
//					}else {
//						mensaje = "<p style='color:red;'>" + "Error: No coincide vista 360 " + "<p>";
//						msj = "ErrorNoCoicide360";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log(
//								"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//						System.err.println(mensaje);
//						errorBuffer.append("\n" + mensaje + "\n");
//						ValorEncontrado = false;
//					}
//					
//				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
//					ValorEncontrado = false;
//				}
//				
//			}
//
//			Thread.sleep(2000);
//
//			if (ValorEncontrado == true) {				
//
//				mensaje = "Flujo : Click Ordenes del �ltimo a�o ";
//				msj = "ClickOrdenesUltimoAno";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				Thread.sleep(2000);
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);
//				new WebDriverWait(driver, 20, 100)
//						.until(ExpectedConditions
//								.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkOrdenesUltimoAno(driver)))
//						.click();
//
//				Thread.sleep(5000);
//
//				sw = 0;
//				contarTiempo = 0;
//				while (sw == 0) {
//					if (contarTiempo > 10) {
//						mensaje = "<p style='color:red;'>" + "Error : No Encuentra link o No esta Visible "
//								+ numeroOrden + "</p>";
//						msj = "LinkNoEncontrado";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						Thread.sleep(2000);
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.err.println(mensaje);
//						ValorEncontrado = false;
//						errorBuffer.append("\n" + mensaje + "\n");
//						driver.close();
//						throw new AssertionError(errorBuffer.toString());
//					}
//					try {
//						Thread.sleep(500);						
//						principalCuenta.MenuPrincipalCuenta.linkNumeroDeOrden(driver, numeroOrden).getText();						
//						sw = 1;
//
//					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
//						sw = 0;
//						contarTiempo = contarTiempo + 1;
//
//					}
//				}
//
//				mensaje = "Flujo : Click Elemento Encontrado " + numeroOrden;
//				msj = "ClickElementoencontrado";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				Thread.sleep(2000);
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//						+ rutaEvidencia + ">");
//				System.out.println(mensaje);
//
//				principalCuenta.MenuPrincipalCuenta.linkNumeroDeOrden(driver, numeroOrden).click();
//				
//				Thread.sleep(5000);
//				
//				if (nombreCuenta.equals("")) {
//					
//				}else {
//				String nombreOrdenCompara = principalCuenta.MenuPrincipalCuenta.txtNombreDetallesHeader(driver, nombreCuenta).getText();			
//				if (nombreOrdenCompara.equals(nombreCuenta)) {
//				
//					mensaje = "Flujo : Nombre " + nombreOrdenCompara + " Coincide con "+nombreCuenta;
//					msj = "CoincideNombreOrden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Nombre encontrado " +nombreOrdenCompara+ " No Coincide con "+ nombreCuenta+ "</p>";
//					msj = "NombreNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				if (nombreActivo.equals("")) {
//					
//				}else {
//					
//				String nombreActivoCompara = principalCuenta.MenuPrincipalCuenta.txtNombreDelActivoPrincipalHeader(driver, nombreActivo).getText();			
//				if (nombreActivoCompara.equals(nombreActivo)) {
//				
//					mensaje = "Flujo : Nombre de Activo " + nombreActivoCompara + " Coincide con "+nombreActivo;
//					msj = "CoincideNombreActivoOrden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Nombre Activo encontrado " +nombreActivoCompara+ " No Coincide con "+ nombreActivo+ "</p>";
//					msj = "NombreActivoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (nombreAsesorPedido.equals("")) {
//					
//					
//				}else {
//				
//				String nombreAsesorPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtNombreAsesorPedidoHeader(driver).getText();			
//				if (nombreAsesorPedidoCompara.equals(nombreAsesorPedido)) {
//				
//					mensaje = "Flujo : Nombre Asesor Pedido " + nombreAsesorPedidoCompara + " Coincide con "+nombreAsesorPedido;
//					msj = "CoincideAsesorPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Nombre Asesor Pedido " +nombreAsesorPedidoCompara+ " No Coincide con "+ nombreAsesorPedido+ "</p>";
//					msj = "AsesorNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				
//				if (numeroOrden.equals("")) {
//					
//				}else {
//				String numeroOrdenCompara = principalCuenta.MenuPrincipalCuenta.txtNumeroDeOrden(driver,numeroOrden).getText();
//				
//				if (numeroOrdenCompara.equals(numeroOrden)) {
//				
//					mensaje = "Flujo : Numero Orden " + numeroOrdenCompara + " Coincide con "+numeroOrden;
//					msj = "CoincideNumeroDeOrden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Numero de Orden encontrado " +numeroOrdenCompara+ " No Coincide con "+ numeroOrden+ "</p>";
//					msj = "NumeroOrdenNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				
//				}
//				
//				Thread.sleep(2000);
//				
//				
//				if (nombreCuenta.equals("")) {
//					
//				}else {
//								
//				String nombreOrdenCompara = principalCuenta.MenuPrincipalCuenta.txtNombreDetalles(driver, nombreCuenta).getAttribute("text");			
//				
//				if (nombreOrdenCompara.equals(nombreCuenta)) {
//					mensaje = "Flujo : Nombre " + nombreOrdenCompara + " Coincide con "+nombreCuenta;
//					msj = "CoincideNombreOrden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Nombre encontrado " +nombreOrdenCompara+ " No Coincide con "+ nombreCuenta+ "</p>";
//					msj = "NombreNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (clasePedido.equals("")) {
//					
//				} else {
//				
//				String clasePedidoCompara = principalCuenta.MenuPrincipalCuenta.txtClasePedido(driver, clasePedido).getText();			
//				if (clasePedidoCompara.equals(clasePedido)) {
//				
//					mensaje = "Flujo : Clase Pedido " + clasePedidoCompara + " Coincide con "+clasePedido;
//					msj = "CoincideClasePedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Clase Pedido " +clasePedidoCompara+ " No Coincide con "+ clasePedido+ "</p>";
//					msj = "ClasesNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				
//				}
//				
//				Thread.sleep(2000);
//				
//				if (estadoPedido.equals("")) {
//											
//				}else {
//					
//				String estadoPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtEstadoPedido(driver, estadoPedido).getText();
//				
//				if (estadoPedidoCompara.equals(estadoPedido)) {
//				
//					mensaje = "Flujo : Estado Pedido " + estadoPedidoCompara + " Coincide con "+estadoPedido;
//					msj = "CoincideEstadoPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Estado Pedido " +estadoPedidoCompara+ " No Coincide con "+ estadoPedido+ "</p>";
//					msj = "EstadoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (motivoPedido.equals("")) {
//					
//				}else {
//				
//				String motivoPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtMotivoPedido(driver, motivoPedido).getText();	
//				if (motivoPedidoCompara.equals(motivoPedido)) {
//				
//					mensaje = "Flujo : Motivo Pedido " + motivoPedidoCompara + " Coincide con "+motivoPedido;
//					msj = "CoincideMotivoPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Motivo Pedido " +motivoPedidoCompara+ " No Coincide con "+ motivoPedido+ "</p>";
//					msj = "MotivoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (nombreAsesorPedido.equals("")) {
//					
//				}else {
//				
//				String nombreAsesorPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtNombreAsesorPedido(driver, nombreAsesorPedido).getText();			
//				if (nombreAsesorPedidoCompara.equals(nombreAsesorPedido)) {
//				
//					mensaje = "Flujo : Nombre Asesor Pedido " + nombreAsesorPedidoCompara + " Coincide con "+nombreAsesorPedido;
//					msj = "CoincideAsesorPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Fecha Entrega Pedido " +nombreAsesorPedidoCompara+ " No Coincide con "+ nombreAsesorPedido+ "</p>";
//					msj = "AsesorNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (codigoCentroOtPedido.equals("")) {
//					
//				}else {
//				
//				String codigoCentroOtPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtCodigoCentroOtPedido(driver, codigoCentroOtPedido).getText();			
//				if (codigoCentroOtPedidoCompara.equals(codigoCentroOtPedido)) {
//				
//					mensaje = "Flujo : Centro Ot Pedido " + codigoCentroOtPedidoCompara + " Coincide con "+codigoCentroOtPedido;
//					msj = "CoincideCentroOtPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Centro Ot Pedido " +codigoCentroOtPedidoCompara+ " No Coincide con "+ codigoCentroOtPedido+ "</p>";
//					msj = "CentroOtPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (localPedido.equals("")) {
//					
//				}else {
//				
//				String localPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtlocalPedido(driver, localPedido).getText();			
//				if (localPedidoCompara.equals(localPedido)) {
//				
//					mensaje = "Flujo : Local Pedido " + localPedidoCompara + " Coincide con "+localPedido;
//					msj = "CoincidelocalPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Local Pedido " +localPedidoCompara+ " No Coincide con "+ localPedido+ "</p>";
//					msj = "localPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (valorPedido.equals("")) {
//					
//				}else {
//				
//				String valorPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtValorPedido(driver, valorPedido).getText();			
//				if (valorPedidoCompara.equals(valorPedido)) {
//				
//					mensaje = "Flujo : Valor Pedido " + valorPedidoCompara + " Coincide con "+valorPedido;
//					msj = "CoincideValorPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Valor Pedido " +valorPedidoCompara+ " No Coincide con "+ valorPedido+ "</p>";
//					msj = "ValorPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (recallIdPedido.equals("")) {
//					
//				}else {
//				
//				String recallIdPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtRecallIdPedido(driver, recallIdPedido).getText();			
//				if (recallIdPedidoCompara.equals(recallIdPedido)) {
//				
//					mensaje = "Flujo : Recall Id Pedido " + recallIdPedidoCompara + " Coincide con "+recallIdPedido;
//					msj = "CoincideRecallIdPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Recall Id Pedido " +recallIdPedidoCompara+ " No Coincide con "+ recallIdPedido+ "</p>";
//					msj = "RecallIdPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				jse.executeScript("arguments[0].scrollIntoView();",
//						principalCuenta.MenuPrincipalCuenta.txtFechaActualizacionPedido(driver, fechaActualizacionPedido));
//				
//				if (fechaActualizacionPedido.equals("")) {
//					
//				}else {
//				
//				String fechaActualizacionPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtFechaActualizacionPedido(driver, fechaActualizacionPedido).getText();			
//				if (fechaActualizacionPedidoCompara.equals(fechaActualizacionPedido)) {
//				
//					mensaje = "Flujo : Fecha Actualizacion Pedido " + fechaActualizacionPedidoCompara + " Coincide con "+fechaActualizacionPedido;
//					msj = "CoincideFechaActPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Fecha Actualizacion Pedido " +fechaActualizacionPedidoCompara+ " No Coincide con "+ fechaActualizacionPedido+ "</p>";
//					msj = "FechaActNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (fechaCierrePedido.equals("")) {
//					
//				}else {
//				
//				String fechaCierrePedidoCompara = principalCuenta.MenuPrincipalCuenta.txtFechaCierrePedido(driver, fechaCierrePedido).getText();			
//				if (fechaCierrePedidoCompara.equals(fechaCierrePedido)) {
//				
//					mensaje = "Flujo : Fecha Cierre Pedido " + fechaCierrePedidoCompara + " Coincide con "+fechaCierrePedido;
//					msj = "CoincideFechaCierrePedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Fecha Cierre Pedido " +fechaCierrePedidoCompara+ " No Coincide con "+ fechaCierrePedido+ "</p>";
//					msj = "FechaCierreNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//
//				Thread.sleep(2000);
//				
//				if (fechaEntregaPedido.equals("")) {
//					
//				}else {
//				
//				String fechaEntregaPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtFechaEntregaPedido(driver, fechaEntregaPedido).getText();			
//				if (fechaEntregaPedidoCompara.equals(fechaCierrePedido)) {
//				
//					mensaje = "Flujo : Fecha Entrega Pedido " + fechaEntregaPedidoCompara + " Coincide con "+fechaEntregaPedido;
//					msj = "CoincideFechaEntregaPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Fecha Entrega Pedido " +fechaEntregaPedidoCompara+ " No Coincide con "+ fechaEntregaPedido+ "</p>";
//					msj = "FechaEntregaNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (vinPedido.equals("")) {
//					
//				}else {
//				
//				String vinPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtVinPedido(driver, vinPedido).getText();			
//				if (vinPedidoCompara.equals(vinPedido)) {
//				
//					mensaje = "Flujo : Vin Pedido " + vinPedidoCompara + " Coincide con "+vinPedido;
//					msj = "CoincideVinPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Vin Pedido " +vinPedidoCompara+ " No Coincide con "+ vinPedido+ "</p>";
//					msj = "VinPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				jse.executeScript("arguments[0].scrollIntoView();",
//						principalCuenta.MenuPrincipalCuenta.txtMatriculaPrincipalDetalle(driver, matricula));
//				
//				if (matricula.equals("")) {
//					
//				}else {
//				
//				String matriculaCompara = principalCuenta.MenuPrincipalCuenta.txtMatriculaPrincipalDetalle(driver, matricula).getText();			
//				if (matriculaCompara.equals(matricula)) {
//				
//					mensaje = "Flujo : Matricula Pedido " + matriculaCompara + " Coincide con "+matricula;
//					msj = "CoincideMatriculaPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Matricula Pedido " +matriculaCompara+ " No Coincide con "+ matricula+ "</p>";
//					msj = "matriculaPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (marcaPedido.equals("")) {
//					
//				}else {
//				
//				String marcaPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtMarcaPedido(driver, marcaPedido).getText();			
//				if (marcaPedidoCompara.equals(marcaPedido)) {
//				
//					mensaje = "Flujo : Marca Pedido " + marcaPedidoCompara + " Coincide con "+marcaPedido;
//					msj = "CoincideMarcaPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Marca Pedido " +marcaPedidoCompara+ " No Coincide con "+ marcaPedido+ "</p>";
//					msj = "MarcaPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (modeloPedido.equals("")) {
//					
//				}else {
//				
//				String modeloPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtModeloPedido(driver, modeloPedido).getText();			
//				if (modeloPedidoCompara.equals(modeloPedido)) {
//				
//					mensaje = "Flujo : Modelo Pedido " + modeloPedidoCompara + " Coincide con "+modeloPedido;
//					msj = "CoincideModeloPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Modelo Pedido " +modeloPedidoCompara+ " No Coincide con "+ modeloPedido+ "</p>";
//					msj = "ModeloPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (kilometrajePedido.equals("")) {
//					
//				}else {
//				
//				String kilometrajePedidoCompara = principalCuenta.MenuPrincipalCuenta.txtKilometrajePedido(driver, kilometrajePedido).getText();			
//				if (kilometrajePedidoCompara.equals(kilometrajePedido)) {
//				
//					mensaje = "Flujo : Kilometraje Pedido " + kilometrajePedidoCompara + " Coincide con "+kilometrajePedido;
//					msj = "CoincideKilometrajePedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Kilometraje Pedido " +kilometrajePedidoCompara+ " No Coincide con "+ kilometrajePedido+ "</p>";
//					msj = "KilometrajePedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				
//				Thread.sleep(2000);
//				
//				if (requerimientoPedido.equals("")) {
//					
//				}else {
//				
//				String requerimientoPedidoCompara = principalCuenta.MenuPrincipalCuenta.txtRequerimientoPedido(driver, requerimientoPedido).getText();			
//				if (requerimientoPedidoCompara.equals(requerimientoPedido)) {
//				
//					mensaje = "Flujo : Requerimiento Pedido " + requerimientoPedidoCompara + " Coincide con "+requerimientoPedido;
//					msj = "CoincideRequerimientoPedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Requerimiento Pedido " +requerimientoPedidoCompara+ " No Coincide con "+ requerimientoPedido+ "</p>";
//					msj = "RequerimientoPedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				Thread.sleep(2000);
//				
//				if (detallePedido.equals("")) {
//					
//				}else {
//				
//				String detallePedidoCompara = principalCuenta.MenuPrincipalCuenta.txtDetallePedido(driver, requerimientoPedido).getText();			
//				if (detallePedidoCompara.equals(detallePedido)) {
//				
//					mensaje = "Flujo : Detalle Pedido " + detallePedidoCompara + " Coincide con "+detallePedido;
//					msj = "CoincideDetallePedido";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//					ValorEncontrado = true;
//					
//					
//				}else {
//					
//					mensaje = "<p style='color:red;'>"
//							+ "Error : Detalle Pedido " +detallePedidoCompara+ " No Coincide con "+ detallePedido+ "</p>";
//					msj = "DetallePedidoNoCoinciden";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.err.println(mensaje);
//					driver.quit();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				}
//				
//				if (ValorEncontrado == true) {
//					
//					mensaje = "Flujo : Se revisan todos los campos de manera satisfactoria ";
//					msj = "terminoDeCampos";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log(
//							"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//					System.out.println(mensaje);
//					
//				}
//			}
//
//			try {
//				Assert.assertTrue(ValorEncontrado);
//				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; campos verificados"
//						+ "<p>";
//				msj = "PruebaCorrecta";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);
//
//			} catch (AssertionError e) {
//				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida" + "<p>";
//				msj = "PruebaErronea";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.err.println(mensaje);
//				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
//
//			}
//
//		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
//			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
//			msj = "ElementoNoEncontrado";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.err.println(mensaje);
//			ValorEncontrado = false;
//			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
//			driver.close();
//			throw new AssertionError(errorBuffer.toString());
//		}
//
//		if (errorBuffer.length() > 0) {
//			mensaje = "<p style='color:red;'>"
//					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
//			msj = "ErroresEncontrados";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.err.println(mensaje);
//			driver.close();
//			throw new AssertionError(errorBuffer.toString());
//
//		}
//
//		driver.quit();
//	}
//
//
//}
//---------------------------------------------------FIN CODIGO OBSOLETO------------------------------------------------