package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H03_12779_Crear_cuenta_nueva_en_salesforce_pero_ya_existe_en_SAP {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H03_12779_Crear_cuenta_nueva_en_salesforce_pero_ya_existe_en_SAP.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		String nombreAleatorio = "";
		String apellidoPaternoAleatorio = "";
		String apellidoMaternoAleatorio = "";

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<br>" + mensaje);
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H03_12779_Crear_cuenta_nueva_en_salesforce_pero_ya_existe_en_SAP.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		//String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);
		String rutIngreso = ExcelUtils.getCellData(1, 6);
		String giroIngreso = ExcelUtils.getCellData(1, 7);
		String tratamiento = ExcelUtils.getCellData(1, 8);
		String genero = ExcelUtils.getCellData(1, 9);
		String nombreIngreso = ExcelUtils.getCellData(1, 10);
		String apellidoPaternoIngreso = ExcelUtils.getCellData(1, 11);
		String apellidoMaternoIngreso = ExcelUtils.getCellData(1, 12);
		String fechaNacimientoIngreso = ExcelUtils.getCellData(1, 13);
		String nacionalidadIngreso = ExcelUtils.getCellData(1, 14);
		String estadoCivilIngreso = ExcelUtils.getCellData(1, 15);
		//String idExternoSapIngreso = ExcelUtils.getCellData(1, 16);
		String viaPreferenciaComunicacionIngreso = ExcelUtils.getCellData(1, 17);
		String movilIngreso = ExcelUtils.getCellData(1, 18);
		String correElectronicoIngreso = ExcelUtils.getCellData(1, 19);
		String telefonoIngreso = ExcelUtils.getCellData(1, 20);
		String paisIngreso = ExcelUtils.getCellData(1, 21);
		String regionIngreso = ExcelUtils.getCellData(1, 22);
		String provinciaIngreso = ExcelUtils.getCellData(1, 23);
		String comunaIngreso = ExcelUtils.getCellData(1, 24);
		String calleIngreso = ExcelUtils.getCellData(1, 25);
		String numeroIngreso = ExcelUtils.getCellData(1, 26);
		String dptoCasaOficinaIngreso = ExcelUtils.getCellData(1, 27);
		String compleDireIngreso = ExcelUtils.getCellData(1, 28);
		String calleLabIngreso = ExcelUtils.getCellData(1, 29);
		String codPostalDirIngreso = ExcelUtils.getCellData(1, 30);
		String comunaLabIngreso = ExcelUtils.getCellData(1, 31);
		String estadoLabIngreso = ExcelUtils.getCellData(1, 32);
		String paisLabIngreso = ExcelUtils.getCellData(1, 33);
		String dirRefLabIngreso = ExcelUtils.getCellData(1, 34);
		String dirDescLabIngreso = ExcelUtils.getCellData(1, 35);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<br>" + mensaje);
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);
			
			Thread.sleep(5000);
			
			mensaje = "Flujo : Resultado Busqueda ";
			msj = "ResultadoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>" + "<img src="
					+ rutaEvidencia + ">");
			System.out.println(mensaje);
			

			try {
				String textoNohayResultadosBusqueda = paginaPrincipal.MenuPrincipal.txtNohayResutados(driver).getText();
				if (textoNohayResultadosBusqueda.contains("a�n")) {

					try {

						Thread.sleep(5000);

						new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions
										.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
								.sendKeys(Keys.ENTER);
						mensaje = "Flujo : Click Boton Cuentas ";
						msj = "ClickBtnCuentas";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(3000);

						try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
								// flujo

							paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

							mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
							Log.info(mensaje);
							Reporter.log("<br>" + mensaje);
							System.out.println(mensaje);

						} catch (NoSuchElementException | ScreenshotException e) {

						}
						new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions
										.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearCuentas(driver)))
								.sendKeys(Keys.ENTER);
						mensaje = "Flujo : Click Boton Crear Cuentas ";
						msj = "ClickBtnCrearCuentas";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(3000);

						new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions
										.visibilityOf(paginaPrincipal.MenuPrincipal.rdCuentasPersonales(driver)))
								.click();
						paginaPrincipal.MenuPrincipal.rdCuentasPersonales(driver).click();
						mensaje = "Flujo : Click Boton Radio Crear Cuenta Personal";
						msj = "ClickBtnRadioCtaPersonal";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(3000);

						new WebDriverWait(driver, 20, 100)
								.until(ExpectedConditions
										.visibilityOf(paginaPrincipal.MenuPrincipal.btnSiguienteCrearCuenta(driver)))
								.click();
						mensaje = "Flujo : Click Boton Siguiente ";
						msj = "ClickBtnSiguiente";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						
						Thread.sleep(5000);

						// Inicializa Variable azar para llenado de datos al azar
						String azar = "";

						mensaje = "Flujo : Llenado de datos ";
						Log.info(mensaje);
						Reporter.log("<p>" +"<br>" + mensaje+"</p>");
						System.out.println(mensaje);

						if (rutIngreso.equals("")) {
							// Cadena de caracteres aleatoria Para Rut
							String rut = utilidades.Funciones.generadorDeRut(9000000, 22000000);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputDocumentoIdentidad(driver)))
									.sendKeys(rut);
							mensaje = "Flujo : Ingreso de Rut Aleatorio : " + rut;
							msj = "IngresoRut";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputDocumentoIdentidad(driver)))
									.sendKeys(rutIngreso);
							mensaje = "Flujo : Ingreso de Rut : " + rutIngreso;
							msj = "IngresoRut";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						if (giroIngreso.equals("")) {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputGiro(driver)))
									.sendKeys("Testing QA");
							mensaje = "Flujo : Ingreso de Giro : " + "Testing QA";
							msj = "IngresoGiro";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputGiro(driver)))
									.sendKeys(giroIngreso);
							mensaje = "Flujo : Ingreso de Giro : " + giroIngreso;
							msj = "IngresoGiro";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						if (tratamiento.equals("")) {

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.selectTratamiento(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.rolSR(driver))).click();
							mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
							msj = "IngresoTratamiento";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.selectTratamiento(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.rolTratamiento(driver, tratamiento)))
									.click();
							mensaje = "Flujo : Ingreso de Tratamiento : " + tratamiento;
							msj = "IngresoTratamiento";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						if (genero.equals("")) {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectGenero(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.generoMasculino(driver)))
									.click();
							mensaje = "Flujo : Ingreso de Genero : " + "Masculino";
							msj = "IngresoGenero";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectGenero(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.generoIngreso(driver, genero)))
									.click();
							mensaje = "Flujo : Ingreso de Genero : " + genero;
							msj = "IngresoGenero";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (nombreIngreso.equals("")) {

							// Cadena de caracteres aleatoria Para Nombre
							nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNombre(driver)))
									.sendKeys(nombreAleatorio);
							Thread.sleep(2000);
							mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
							msj = "IngresoNombre";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNombre(driver)))
									.sendKeys(nombreIngreso);
							Thread.sleep(2000);
							mensaje = "Flujo : Ingreso de Nombre : " + nombreIngreso;
							msj = "IngresoNombre";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (apellidoPaternoIngreso.equals("")) {

							// Cadena de caracteres aleatoria Para Apellido Paterno
							apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputApellidoPaterno(driver)))
									.sendKeys(apellidoPaternoAleatorio);
							mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
							msj = "IngresoApellidoPaterno";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputApellidoPaterno(driver)))
									.sendKeys(apellidoPaternoIngreso);
							mensaje = "Flujo : Ingreso de Apellido Paterno : " + apellidoPaternoIngreso;
							msj = "IngresoApellidoPaterno";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (apellidoMaternoIngreso.equals("")) {

							// Cadena de caracteres aleatoria Para Apellido Materno
							apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
									.click();
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
									.sendKeys(apellidoMaternoAleatorio);
							mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
							msj = "IngresoApellidoMaterno";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
									.click();
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
									.sendKeys(apellidoMaternoIngreso);
							mensaje = "Flujo : Ingreso de Apellido Materno : " + apellidoMaternoIngreso;
							msj = "IngresoApellidoMaterno";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (fechaNacimientoIngreso.equals("")) {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputFechaNacimiento(driver)))
									.sendKeys("18-08-1978");
							mensaje = "Flujo : Ingreso de Fecha Nacimiento : " + "18-08-1978";
							msj = "IngresoFachaNacimiento";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputFechaNacimiento(driver)))
									.sendKeys(fechaNacimientoIngreso);
							mensaje = "Flujo : Ingreso de Fecha Nacimiento : " + fechaNacimientoIngreso;
							msj = "IngresoFachaNacimiento";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (nacionalidadIngreso.equals("")) {

							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("arguments[0].scrollIntoView();",
									crearCuenta.Personal.inputNacionalidad(driver));

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNacionalidad(driver)))
									.click();
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputNacionalidad(driver)))
									.sendKeys("CHI");
							Thread.sleep(500);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions
											.visibilityOf(crearCuenta.Personal.inputNacionalidad(driver)))
									.sendKeys("LE");
							Thread.sleep(500);
							mensaje = "Flujo : Ingreso Campo Nacionalidad : " + "CHILE";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.nacionalidadSeleccion(driver)))
									.click();
							mensaje = "Flujo : Seleccion de Pais Nacionalidad : " + "CHILE";
							msj = "SeleccionPaisNacionalidad";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("arguments[0].scrollIntoView();",
									crearCuenta.Personal.inputNacionalidad(driver));

							String[] arregloNacionalidadIngreso = new String[nacionalidadIngreso.length()];
							for (int i = 0; i < nacionalidadIngreso.length(); i++) {
								arregloNacionalidadIngreso[i] = Character.toString(nacionalidadIngreso.charAt(i));
							}

							for (int i = 0; i < nacionalidadIngreso.length(); i++) {

								crearCuenta.Personal.inputNacionalidad(driver).sendKeys(arregloNacionalidadIngreso[i]);
								Thread.sleep(500);

							}

							mensaje = "Flujo : Ingreso Campo Nacionalidad : " + nacionalidadIngreso;
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions.visibilityOfElementLocated(
									By.xpath("//div[@title='" + nacionalidadIngreso + "']"))).click();
							mensaje = "Flujo : Seleccion de Pais Nacionalidad : " + nacionalidadIngreso;
							msj = "SeleccionPaisNacionalidad";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (estadoCivilIngreso.equals("")) {

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.selectEstadoCivil(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.soltero(driver)))
									.click();
							mensaje = "Flujo : Seleccion de Estado Civil : " + "Soltero";
							msj = "IngresoEstadoCivil";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							new WebDriverWait(driver, 20, 100).until(
									ExpectedConditions.visibilityOf(crearCuenta.Personal.selectEstadoCivil(driver)))
									.click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
									.visibilityOfElementLocated(By.xpath("//a[@title='" + estadoCivilIngreso + "']")))
									.click();

							mensaje = "Flujo : Seleccion de Estado Civil : " + estadoCivilIngreso;
							msj = "IngresoEstadoCivil";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

//						if (idExternoSapIngreso.equals("")) {
//
//							mensaje = "<p style='color:green;'>"
//									+ "WARNING : Campo 'Ingreso ID Externo SAP' no ingresado, no se encuentra en la data de entrada"
//									+ "</p>";
//							Log.info(mensaje);
//							Reporter.log("<br>" + mensaje);
//							System.out.println(mensaje);
//
//						} else {
//
//							new WebDriverWait(driver, 20, 100).until(
//									ExpectedConditions.visibilityOf(crearCuenta.Personal.inputIdExternoSap(driver)))
//									.click();
//							new WebDriverWait(driver, 20, 100)
//									.until(ExpectedConditions
//											.visibilityOf(crearCuenta.Personal.inputIdExternoSap(driver)))
//									.sendKeys(idExternoSapIngreso);
//
//							mensaje = "Flujo : ID Sap : " + idExternoSapIngreso;
//							msj = "IngresoIdSap";
//							posicionEvidencia = posicionEvidencia + 1;
//							posicionEvidenciaString = Integer.toString(posicionEvidencia);
//							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//							Thread.sleep(2000);
//							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//							Log.info(mensaje);
//							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//									+ rutaEvidencia + ">");
//							System.out.println(mensaje);
//
//						}

						Thread.sleep(2000);

						if (viaPreferenciaComunicacionIngreso.equals("")) {

							crearCuenta.Personal.selectPreferenciaComunicacion(driver).click();
							Thread.sleep(2000);
							crearCuenta.Personal.viaTelefono(driver).click();
							mensaje = "Flujo : Seleccion de Preferencia de Comunicacion : " + "Via Telefono";
							msj = "SeleccionPrefComunicacion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.selectPreferenciaComunicacion(driver).click();
							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOfElementLocated(
											By.xpath("//a[@title='" + viaPreferenciaComunicacionIngreso + "']")))
									.click();
							mensaje = "Flujo : Seleccion de Preferencia de Comunicacion : "
									+ viaPreferenciaComunicacionIngreso;
							msj = "SeleccionPrefComunicacion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (movilIngreso.equals("")) {

							String numMovil = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono movil
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numMovil = numMovil + azar;
							}
							crearCuenta.Personal.inputNumeroMovil(driver).sendKeys("9" + numMovil);
							mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
							msj = "IngresoMovil";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputNumeroMovil(driver).sendKeys(movilIngreso);
							mensaje = "Flujo : Ingreso Numero Movil : " + movilIngreso;
							msj = "IngresoMovil";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (correElectronicoIngreso.equals("")) {

							String correo = "";
							correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
							String correoAzar = "";
							azar = "";
							// Cadena de numeros aleatoria Para Correo
							for (int i = 0; i < 4; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								correoAzar = correoAzar + azar;
							}

							correo = correo + correoAzar + "@derco.cl";
							correo = correo.toLowerCase();
							crearCuenta.Personal.inputCorreo(driver).sendKeys(correo);
							mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : "
									+ correo;
							msj = "IngresoCorreo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputCorreo(driver).sendKeys(correElectronicoIngreso);
							mensaje = "Flujo : Ingreso Correo : " + correElectronicoIngreso;
							msj = "IngresoCorreo";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (telefonoIngreso.equals("")) {

							String numTelefono = "";
							azar = "";
							// Cadena de numeros aleatoria Para telefono
							for (int i = 0; i < 8; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numTelefono = numTelefono + azar;
							}

							crearCuenta.Personal.inputNumTelefono(driver).sendKeys("2" + numTelefono);
							mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
							msj = "IngressoTelefono";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputNumTelefono(driver).sendKeys(telefonoIngreso);
							mensaje = "Flujo : Ingreso Numero Telefono: " + telefonoIngreso;
							msj = "IngressoTelefono";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						JavascriptExecutor jse = (JavascriptExecutor) driver;
						jse.executeScript("arguments[0].scrollIntoView();",
								crearCuenta.Personal.inputPaisDireccion(driver));

						Thread.sleep(2000);

						if (paisIngreso.equals("")) {

							crearCuenta.Personal.inputPaisDireccion(driver).click();
							crearCuenta.Personal.inputPaisDireccion(driver).sendKeys("CHI");
							Thread.sleep(1000);
							crearCuenta.Personal.inputPaisDireccion(driver).sendKeys("LE");
							mensaje = "Flujo : Ingreso Campo Pais Direccion : " + "CHILE";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);

							crearCuenta.Personal.inputPaisDireccion(driver).sendKeys(" ");
							crearCuenta.Personal.paisDireccionSeleccion(driver).click();
							mensaje = "Flujo : Seleccion Campo Pais Direccion : " + "CHILE";
							msj = "SeleccionPais";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							String[] arreglopaisIngreso = new String[paisIngreso.length()];
							for (int i = 0; i < paisIngreso.length(); i++) {
								arreglopaisIngreso[i] = Character.toString(paisIngreso.charAt(i));
							}

							for (int i = 0; i < paisIngreso.length(); i++) {

								crearCuenta.Personal.inputPaisDireccion(driver).sendKeys(arreglopaisIngreso[i]);
								Thread.sleep(500);

							}

							mensaje = "Flujo : Ingreso Campo Pais Direccion : " + paisIngreso;
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
									.visibilityOfElementLocated(By.xpath("//div[@title='" + paisIngreso + "']")))
									.click();
							mensaje = "Flujo : Seleccion Campo Pais Direccion : " + paisIngreso;
							msj = "SeleccionPais";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (calleIngreso.equals("")) {

							// Cadena de caracteres aleatoria Para Calle
							String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
							crearCuenta.Personal.inputCalleAleatorio(driver).sendKeys(calleAleatorio);
							mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
							msj = "SeleccionCalle";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputCalleAleatorio(driver).sendKeys(calleIngreso);
							mensaje = "Flujo : Ingreso Campo Calle Direccion : " + calleIngreso;
							msj = "SeleccionCalle";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (numeroIngreso.equals("")) {

							String numDireccionAzar = "";
							azar = "";
							// Cadena de numeros aleatoria Para numero
							for (int i = 0; i < 4; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								numDireccionAzar = numDireccionAzar + azar;
							}
							crearCuenta.Personal.inputNumeroDireccion(driver).sendKeys(numDireccionAzar);
							mensaje = "Flujo : Ingreso Numero Direccion Azar : " + numDireccionAzar;
							msj = "IngresoDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputNumeroDireccion(driver).sendKeys(numeroIngreso);
							mensaje = "Flujo : Ingreso Numero Direccion : " + numeroIngreso;
							msj = "IngresoDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}

						Thread.sleep(2000);

						if (dptoCasaOficinaIngreso.equals("")) {

							String deptoCasaOficinaAzar = "";
							azar = "";
							// Cadena de numeros aleatoria Para numero casa
							for (int i = 0; i < 2; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
							}
							crearCuenta.Personal.inputDeptoCasaOficina(driver).sendKeys("casa " + deptoCasaOficinaAzar);
							mensaje = "Flujo : Ingreso Casa Azar : " + "casa " + deptoCasaOficinaAzar;
							msj = "DetallesDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputDeptoCasaOficina(driver).sendKeys(dptoCasaOficinaIngreso);
							mensaje = "Flujo : Ingreso Casa/Dpto/Of : " + dptoCasaOficinaIngreso;
							msj = "DetallesDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (compleDireIngreso.equals("")) {

							crearCuenta.Personal.inputComplementoDireccion(driver).sendKeys("inmueble interior");
							mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
							msj = "ComplementoDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							crearCuenta.Personal.inputComplementoDireccion(driver).sendKeys(compleDireIngreso);
							mensaje = "Flujo : Ingreso Complemento Direccion : " + compleDireIngreso;
							msj = "ComplementoDireccion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}

						Thread.sleep(2000);

						if (regionIngreso.equals("")) {

							crearCuenta.Personal.inputRegionDireccion(driver).click();
							crearCuenta.Personal.inputRegionDireccion(driver).sendKeys("SANT");
							Thread.sleep(1000);
							crearCuenta.Personal.inputRegionDireccion(driver).sendKeys("IAGO");
							mensaje = "Flujo : Ingreso Campo Region Direccion : " + "SANTIAGO";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(3000);
							
							crearCuenta.Personal.regionDireccionSeleccion(driver).click();
							mensaje = "Flujo : Seleccion Campo Region Direccion : " + "SANTIAGO";
							msj = "SeleccionRegion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							String[] arregloRegionIngreso = new String[regionIngreso.length()];
							for (int i = 0; i < regionIngreso.length(); i++) {
								arregloRegionIngreso[i] = Character.toString(regionIngreso.charAt(i));
							}

							for (int i = 0; i < regionIngreso.length(); i++) {

								crearCuenta.Personal.inputRegionDireccion(driver).sendKeys(arregloRegionIngreso[i]);
								Thread.sleep(500);

							}

							mensaje = "Flujo : Ingreso Campo Region Direccion : " + regionIngreso;
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
									.visibilityOfElementLocated(By.xpath("//div[@title='" + regionIngreso + "']")))
									.click();
							mensaje = "Flujo : Seleccion Campo Region Direccion : " + paisIngreso;
							msj = "SeleccionRegion";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}
						
						Thread.sleep(2000);

						if (provinciaIngreso.equals("")) {

							crearCuenta.Personal.inputProvinciaDireccion(driver).click();
							crearCuenta.Personal.inputProvinciaDireccion(driver).sendKeys("SANT");
							Thread.sleep(1000);
							crearCuenta.Personal.inputProvinciaDireccion(driver).sendKeys("IAGO");
							mensaje = "Flujo : Ingreso Campo Provincia Direccion : " + "SANTIAGO";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(3000);
							crearCuenta.Personal.provinciaDireccionSeleccion(driver).click();
							mensaje = "Flujo : Seleccion Campo provincia Direccion : " + "SANTIAGO";
							msj = "SeleccionComuna";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						} else {

							String[] arregloProvinciaIngreso = new String[provinciaIngreso.length()];
							for (int i = 0; i < provinciaIngreso.length(); i++) {
								arregloProvinciaIngreso[i] = Character.toString(provinciaIngreso.charAt(i));
							}

							for (int i = 0; i < provinciaIngreso.length(); i++) {

								crearCuenta.Personal.inputProvinciaDireccion(driver).sendKeys(arregloProvinciaIngreso[i]);
								Thread.sleep(500);

							}

							mensaje = "Flujo : Ingreso Campo Provincia Direccion : " + provinciaIngreso;
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
									.visibilityOfElementLocated(By.xpath("//div[@title='" + provinciaIngreso + "']")))
									.click();
							mensaje = "Flujo : Seleccion Campo Region Direccion : " + provinciaIngreso;
							msj = "SeleccionProvincia";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}
						
						
						Thread.sleep(2000);

						if (comunaIngreso.equals("")) {

							crearCuenta.Personal.inputComunaDireccion(driver).click();
							crearCuenta.Personal.inputComunaDireccion(driver).sendKeys("SANT");
							Thread.sleep(1000);
							crearCuenta.Personal.inputComunaDireccion(driver).sendKeys("IAGO");
							mensaje = "Flujo : Ingreso Campo Comuna Direccion : " + "SANTIAGO";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(3000);
							crearCuenta.Personal.comunaDireccionSeleccion(driver).click();
							mensaje = "Flujo : Seleccion Campo Comuna Direccion : " + "SANTIAGO";
							msj = "SeleccionComuna";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							String[] arregloComunaIngreso = new String[comunaIngreso.length()];
							for (int i = 0; i < comunaIngreso.length(); i++) {
								arregloComunaIngreso[i] = Character.toString(comunaIngreso.charAt(i));
							}

							for (int i = 0; i < comunaIngreso.length(); i++) {

								crearCuenta.Personal.inputComunaDireccion(driver).sendKeys(arregloComunaIngreso[i]);
								Thread.sleep(500);

							}

							mensaje = "Flujo : Ingreso Campo Comuna Direccion : " + comunaIngreso;
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							Thread.sleep(2000);
							
							new WebDriverWait(driver, 20, 100).until(ExpectedConditions
									.visibilityOfElementLocated(By.xpath("//div[@title='" + comunaIngreso + "']")))
									.click();
							mensaje = "Flujo : Seleccion Campo Comuna Direccion : " + comunaIngreso;
							msj = "SeleccionComuna";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
						}
						
						Thread.sleep(2000);

						if (calleLabIngreso.equals("")) {

							// Cadena de caracteres aleatoria Para Calle Laboral
							String calleLaboralAleatorio = utilidades.Funciones.funcionCalleAleatorio();
							String calleLaboralAleatorioNumero = "";
							azar = "";
							// Cadena de numeros aleatoria Para numero Calle Laboral
							for (int i = 0; i < 2; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								calleLaboralAleatorioNumero = calleLaboralAleatorioNumero + azar;
							}

							crearCuenta.Personal.inputCalleLaboral(driver)
									.sendKeys(calleLabIngreso);
							mensaje = "Flujo: Ingreso Campo Calle Direccion Laboral Aleatorio: " + calleLaboralAleatorio
									+ " " + calleLaboralAleatorioNumero;
							msj = "IngresoDireccionLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							crearCuenta.Personal.inputCalleLaboral(driver).sendKeys(calleLabIngreso);
							mensaje = "Flujo: Ingreso Campo Calle Direccion Laboral : " + calleLabIngreso;
							msj = "IngresoDireccionLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

						}
						
						Thread.sleep(2000);

						if (codPostalDirIngreso.equals("")) {

							String codigoPostallAleatorio = "";
							azar = "";
							// Cadena de numeros aleatoria Para numero Calle Laboral
							for (int i = 0; i < 6; i++) {
								azar = utilidades.Funciones.numeroAleatorio(1, 8);
								codigoPostallAleatorio = codigoPostallAleatorio + azar;
							}
							crearCuenta.Personal.inputCodigoPostalLaboral(driver).sendKeys(codigoPostallAleatorio);
							mensaje = "Flujo : Ingreso Campo Codigo Postal Laboral Aleatorio: " + codigoPostallAleatorio;
							msj = "IngresoCodigoPostal";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							crearCuenta.Personal.inputCodigoPostalLaboral(driver).sendKeys(codPostalDirIngreso);
							mensaje = "Flujo : Ingreso Campo Codigo Postal Laboral : " + codPostalDirIngreso;
							msj = "IngresoCodigoPostal";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						Thread.sleep(2000);

						if (comunaLabIngreso.equals("")) {

							crearCuenta.Personal.inputComunaLaboral(driver).sendKeys("QUILICURA");
							mensaje = "Flujo : Ingreso Campo Comuna Laboral : " + "QUILICURA";
							msj = "IngresoComunaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							
						} else {

							crearCuenta.Personal.inputComunaLaboral(driver).sendKeys(comunaLabIngreso);
							mensaje = "Flujo : Ingreso Campo Comuna Laboral : " + comunaLabIngreso;
							msj = "IngresoComunaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						Thread.sleep(2000);

						if (estadoLabIngreso.equals("")) {

							crearCuenta.Personal.inputProvinciaLaboral(driver).sendKeys("QUILICURA");
							mensaje = "Flujo : Ingreso Campo Provincia Laboral : " + "QUILICURA";
							msj = "IngresoProvinciaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							
						} else {

							crearCuenta.Personal.inputProvinciaLaboral(driver).sendKeys(estadoLabIngreso);
							mensaje = "Flujo : Ingreso Campo Provincia Laboral : " + estadoLabIngreso;
							msj = "IngresoProvinciaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						Thread.sleep(2000);

						if (paisLabIngreso.equals("")) {

							crearCuenta.Personal.inputPaisLaboral(driver).sendKeys("CHILE");
							mensaje = "Flujo : Ingreso Campo Pais Laboral : " + "CHILE";
							msj = "IngresoPaisLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							crearCuenta.Personal.inputPaisLaboral(driver).sendKeys(paisLabIngreso);
							mensaje = "Flujo : Ingreso Campo Pais Laboral : " + paisLabIngreso;
							msj = "IngresoPaisLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						Thread.sleep(2000);

						if (dirRefLabIngreso.equals("")) {

							crearCuenta.Personal.inputReferenciaDireccionLaboral(driver).sendKeys("SECTOR INDUSTRIAL");
							mensaje = "Flujo : Ingreso Campo Referencia Direccion Laboral : " + "SECTOR INDUSTRIAL";
							msj = "IngresoReferenciaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							crearCuenta.Personal.inputReferenciaDireccionLaboral(driver).sendKeys(dirRefLabIngreso);
							mensaje = "Flujo : Ingreso Campo Referencia Direccion Laboral : " + dirRefLabIngreso;
							msj = "IngresoReferenciaLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						Thread.sleep(2000);

						if (dirDescLabIngreso.equals("")) {

							crearCuenta.Personal.inputDescripcionDireccionLaboral(driver).sendKeys("SECTOR INDUSTRIAL");
							mensaje = "Flujo : Ingreso Campo Descripcion Direccion Laboral : " + "SECTOR INDUSTRIAL";
							msj = "IngersoDireccionLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						} else {

							crearCuenta.Personal.inputDescripcionDireccionLaboral(driver).sendKeys(dirDescLabIngreso);
							mensaje = "Flujo : Ingreso Campo Descripcion Direccion Laboral : " + dirDescLabIngreso;
							msj = "IngersoDireccionLaboral";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							
						}
						
						mensaje = "<p style='color:green;'>"
								+ "WARNING : Informacion en Campo 'Ingreso Campo Bussines Partner' No se Ingresara"
								+ "</p>";
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>");
						System.out.println(mensaje);

						crearCuenta.Personal.btnGuardar(driver).click();
						mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
						msj = "ClickBtnGuardar";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);
						
						Thread.sleep(2000);
						
						String textoCuadroVerdePequeno = new WebDriverWait(driver, 20, 100).until(
								ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerdePequeno(driver)))
								.getText();

						if (textoCuadroVerdePequeno.equals("Se cre� Cuenta personal .")) {

							mensaje = "Flujo : Se cre� Cuenta personal . ";
							msj = "CreaCuenta";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							Thread.sleep(3000);

							new WebDriverWait(driver, 20, 100)
									.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnCrearEnSap(driver)))
									.click();
							mensaje = "Flujo : Click Boton Crear En Sap";
							msj = "ClickBtnCrearSap";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							int sw = 0;
							int contarTiempo = 0;
							String textoCuadroVerde = "";
							while (sw == 0) {
								if (contarTiempo > 30) {
									mensaje = "<p style='color:red;'>"
											+ "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
									msj = "ElementoNoEncontrado";
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(2000);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,
											nombreCaptura + "_" + msj);
									Log.info(mensaje);
									Reporter.log("<p>" + mensaje + "</p>"
											+ "<img src=" + rutaEvidencia + ">");
									System.err.println(mensaje);
									ValorEncontrado = false;
									errorBuffer.append("\n" + mensaje + "\n");
									driver.close();
									throw new AssertionError(errorBuffer.toString());
								}
								try {
									textoCuadroVerde = new WebDriverWait(driver, 20, 100)
											.until(ExpectedConditions
													.visibilityOf(MenuPrincipal.textoNotificacionesVerde(driver)))
											.getText();
									sw = 1;

								} catch (NoSuchElementException | ScreenshotException e) {
									sw = 0;
									contarTiempo = contarTiempo + 1;
								}
							}
							mensaje = "Flujo : Se revisa creacion correcta de nueva cuenta personal";
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>");
							System.out.println(mensaje);

							if (textoCuadroVerde.contains("Envio correcto a SAP")) {
								mensaje = "Ok :" + textoCuadroVerde;
								msj = "OkIngresoASap";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.out.println(mensaje);
								ValorEncontrado = true;
							} else {
								mensaje = "<p style='color:red;'>" + "Error : No Ingreso Datos A SAP " + "</p>";
								msj = "ErrorIngresoASap";
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Log.info(mensaje);
								Reporter.log("<p>" + mensaje + "</p>" + "<img src="
										+ rutaEvidencia + ">");
								System.err.println(mensaje);
								errorBuffer.append("\n" + mensaje + "\n");
								ValorEncontrado = false;
							}

						}

					} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
								+ "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());

					}

				} else {
					mensaje = "<p style='color:red;'>"
							+ "ERROR: Prueba Fallida ; No Aparece Mensaje 'No hay resultados a�n...'" + "<p>";
					msj = "PruebaErronea";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append("\n" + mensaje + "\n");
				}

			} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {

				try {

					WebElement resultadoBuscar = driver
							.findElement(By.xpath("(//*[text()[contains(.,'resultado')]])[3]"));
					System.err.println("Texto " + resultadoBuscar.getText());

					if (resultadoBuscar.getText().contains("resultado")) {

						mensaje = "<p style='color:red;'>"
								+ "ERROR: Prueba Fallida ; Busqueda BP tiene resultados en busqueda" + "<p>";
						msj = "PruebaErronea";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
						ValorEncontrado = false;

					}

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e1) {

					mensaje = "<p style='color:red;'>"
							+ "ERROR: Prueba Fallida ; Busqueda BP tiene resultados en busqueda" + "<p>";
					msj = "PruebaErronea";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
					ValorEncontrado = false;
				}

				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Busqueda BP tiene resultados en busqueda"
						+ "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
				ValorEncontrado = false;

			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
						+ "OK: Prueba Correcta ; Crear Cuenta Exitoso con BP Existente En SAP " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>"
						+ "ERROR: Prueba Fallida ; Crear Cuenta Fallida con BP Existente En SAP" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
