package ssttSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

//import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H03_12676_Modificar_todos_los_campos_editables_en_cuenta_empresa {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H03_12676_Modificar_todos_los_campos_editables_en_cuenta_empresa.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H03_12676_Modificar_todos_los_campos_editables_en_cuenta_empresa.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			// int swTiempoEspera = 0;
			// int contarTiempoEspera = 0;

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);
					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
			msj = "ClickElementoencontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnModificarCuenta(driver)));
			crearCuenta.Personal.btnModificarCuenta(driver).click();
			mensaje = "Flujo : Click Modificar Cuentas ";
			msj = "ClickBtnModificarCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			String nombreAntiguo = crearCuenta.Empresa.inputNombreEmpresaModificar(driver).getAttribute("value");
			mensaje = "Flujo : Nombre Antiguo : " + nombreAntiguo;
			msj = "NombreAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			String empresaAleatorio = nombreAntiguo;

			while (empresaAleatorio.equals(nombreAntiguo)) {
				empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
				empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio()
						+ " Cia Ltda";
			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNombreEmpresaModificar(driver)))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"), empresaAleatorio);

			mensaje = "Flujo : Ingreso de Razon Social Aleatorio : " + empresaAleatorio;
			msj = "IngresoRazonSocial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String giroAntiguo = crearCuenta.Empresa.inputGiroModificar(driver).getAttribute("value");
			mensaje = "Flujo : Giro Antiguo : " + giroAntiguo;
			msj = "GiroAntiguo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			if (giroAntiguo.equals("Testing QA")) {

				crearCuenta.Empresa.inputGiroModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"Testing QA Empresa");

				mensaje = "Flujo : Ingreso Giro : " + "Testing QA Empresa";
				msj = "IngresoGiro";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				crearCuenta.Empresa.inputGiroModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"), "Testing QA");

				mensaje = "Flujo : Ingreso Giro : " + "Testing QA";
				msj = "IngresoGiro";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String nacionalidadAntigua = crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Nacionalidad Antigua : " + nacionalidadAntigua;
			msj = "NacionalidadAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// String nacionalidadModificada = "";
			// if (nacionalidadAntigua.equals("Chile")) {
			//
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).click();
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("ARGEN");
			// Thread.sleep(1000);
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("TINA");
			// mensaje = "Flujo : Ingreso Campo Pais : " + "ARGENTINA";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			// nacionalidadModificada = "Argentina";
			// crearCuenta.Empresa.selectElementoNacionalidadModificarCuenta(driver,
			// nacionalidadModificada).click();
			// mensaje = "Flujo : Seleccion de Pais : " + "ARGENTINA";
			// msj = "SeleccionPais";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// } else {
			//
			// nacionalidadModificada = "Chile";
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).click();
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("CHI");
			// Thread.sleep(1000);
			// crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("LE");
			// mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearCuenta.Empresa.selectElementoNacionalidadModificarCuenta(driver,
			// nacionalidadModificada).click();
			// mensaje = "Flujo : Seleccion de Pais : " + "CHILE";
			// msj = "SeleccionPais";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// }

			Thread.sleep(2000);

			String correoAntiguo = crearCuenta.Empresa.inputCorreoModificar(driver).getAttribute("value");
			mensaje = "Flujo : correo Antiguo : " + correoAntiguo;
			msj = "CorreoAntiguo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String azar;
			String correo = "";
			String correoAzar = "";
			String correoAleatorio = correoAntiguo;

			while (correoAleatorio.equals(correoAntiguo)) {
				correoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
				correoAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio()
						+ " Cia Ltda";

				// Cadena de caracteres aleatoria Para Nombre
				String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
				// Cadena de caracteres aleatoria Para Apellido Paterno
				String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();

				correo = nombreAleatorio + "." + apellidoPaternoAleatorio;

				azar = "";
				// Cadena de numeros aleatoria Para Correo
				for (int i = 0; i < 4; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					correoAzar = correoAzar + azar;
				}

				empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
				correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
				correo = correo.toLowerCase();
				correo = correo.replace(" ", "");

			}

			crearCuenta.Empresa.inputCorreoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"), correo);

			mensaje = "Flujo : Ingreso Correo  al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String telefonoAntiguo = crearCuenta.Empresa.inputTelefonoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Telefono  Antiguo : " + telefonoAntiguo;
			msj = "TelefonoAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}
			crearCuenta.Empresa.inputTelefonoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					"2" + numTelefono);

			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngressoTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();",
					crearCuenta.Empresa.inputRegionDireccionModificar(driver));

			String regionAntiguo = crearCuenta.Empresa.inputRegionDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Region Antiguo : " + regionAntiguo;
			msj = "RegionAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// String regionModificado = "";
			// if (regionAntiguo.equals("RM - Santiago")) {
			//
			// crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("V -
			// VALP");
			// Thread.sleep(1000);
			// crearCuenta.Personal.inputRegionDireccionModificar(driver).sendKeys("ARAISO");
			// mensaje = "Flujo : Ingreso Campo Region : " + "V - Valparaiso";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			// regionModificado = "V - Valparaiso";
			// crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver,
			// regionModificado).click();
			// mensaje = "Flujo : Seleccion de Region : " + "V - Valparaiso";
			// msj = "SeleccionRegion";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// } else {
			//
			// regionModificado = "RM - Santiago";
			// crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("RM -
			// San");
			// Thread.sleep(1000);
			// crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("tiago");
			// mensaje = "Flujo : Ingreso Campo Region : " + "RM - Santiago";
			// Log.info(mensaje);
			// Reporter.log("<br>" + mensaje);
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver,
			// regionModificado).click();
			// mensaje = "Flujo : Seleccion de Region : " + "RM - Santiago";
			// msj = "SeleccionRegion";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// }
			//
			// Thread.sleep(2000);
			//
			// String provinciaAntiguo =
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver)
			// .getAttribute("placeholder");
			// mensaje = "Flujo : Privincia Antiguo : " + regionAntiguo;
			// msj = "ProvinciaAModificar";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// String provinciaModificado = "";
			// if (provinciaAntiguo.equals("Santiago")) {
			//
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Valpa");
			// Thread.sleep(1000);
			// crearCuenta.Personal.inputProvinciaDireccionModificar(driver).sendKeys("ra�so");
			// mensaje = "Flujo : Ingreso Campo Provincia : " + "Valpara�so";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			// provinciaModificado = "Valpara�so";
			// crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver,
			// provinciaModificado).click();
			// mensaje = "Flujo : Seleccion de Provincia : " + "Valpara�so";
			// msj = "SeleccionProvincia";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// } else {
			//
			// provinciaModificado = "Santiago";
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Sant");
			// Thread.sleep(1000);
			// crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("iago");
			// mensaje = "Flujo : Ingreso Campo Provincia : " + "Santiago";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver,
			// provinciaModificado).click();
			// mensaje = "Flujo : Seleccion de Provincia : " + "Santiago";
			// msj = "SeleccionProvincia";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// }
			//
			// Thread.sleep(2000);
			//
			// String comunaAntiguo =
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver)
			// .getAttribute("placeholder");
			// mensaje = "Flujo : Comuna Antiguo : " + comunaAntiguo;
			// msj = "ComunaAModificar";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// String comunaModificado = "";
			// if (comunaAntiguo.equals("Santiago")) {
			//
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("Valpar");
			// Thread.sleep(1000);
			// crearCuenta.Personal.inputComunaDireccionModificar(driver).sendKeys("a�so");
			// mensaje = "Flujo : Ingreso Campo Comuna : " + "Valpara�so";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			// comunaModificado = "Valpara�so";
			// crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver,
			// comunaModificado).click();
			// mensaje = "Flujo : Seleccion de Region : " + "Valpara�so";
			// msj = "SeleccionComuna";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// } else {
			//
			// comunaAntiguo = "Santiago";
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("San");
			// Thread.sleep(1000);
			// crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("tiago");
			// mensaje = "Flujo : Ingreso Campo Comuna : " + "Santiago";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// Thread.sleep(2000);
			//
			// crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver,
			// comunaModificado).click();
			// mensaje = "Flujo : Seleccion de Comuna : " + "Santiago";
			// msj = "SeleccionComuna";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// }

			String regionModificado = "";
			if (regionAntiguo.equals("RM - Santiago")) {

				crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("V - VALP");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("ARAISO");
				mensaje = "Flujo : Ingreso Campo Region : " + "V - Valparaiso";
				Log.info(mensaje);
				Reporter.log("<br>" + "<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				regionModificado = "V - Valparaiso";
				crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver, regionModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "V - Valparaiso";
				msj = "SeleccionRegion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				regionModificado = "RM - Santiago";
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("RM - San");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("tiago");
				mensaje = "Flujo : Ingreso Campo Region : " + "RM - Santiago";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

				Thread.sleep(2000);

				crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver, regionModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "RM - Santiago";
				msj = "SeleccionRegion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String provinciaAntiguo = crearCuenta.Empresa.inputProvinciaDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Provincia Antiguo : " + regionAntiguo;
			msj = "ProvinciaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String provinciaModificado = "";
			if (provinciaAntiguo.equals("Santiago")) {

				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Valpa");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("ra�so");
				mensaje = "Flujo : Privincia Nuevo : " + "Valpara�so";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				provinciaModificado = "Valpara�so";
				crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver, provinciaModificado).click();
				mensaje = "Flujo : Seleccion de Provincia  : " + "Valpara�so";
				msj = "SeleccionProvincia";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				if (regionModificado.equals("V - Valparaiso")) {
					provinciaModificado = "Valpara�so";
					crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
					crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Valpa");
					Thread.sleep(1000);
					crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("ra�so");
					mensaje = "Flujo : Ingreso Campo Provincia : " + "Valpara�so";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					Thread.sleep(2000);

					crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver, provinciaModificado).click();
					mensaje = "Flujo : Seleccion de Provincia  : " + "Valpara�so";
					msj = "SeleccionProvincia";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

				} else {

					provinciaModificado = "Santiago";
					crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
					crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Sant");
					Thread.sleep(1000);
					crearCuenta.Personal.inputProvinciaDireccionModificar(driver).sendKeys("iago");
					mensaje = "Flujo : Ingreso Campo Provincia : " + "Santiago";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					Thread.sleep(2000);

					crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver, provinciaModificado).click();
					mensaje = "Flujo : Seleccion de Provincia  : " + "Santiago";
					msj = "SeleccionProvincia";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);
				}
			}

			Thread.sleep(2000);

			String comunaAntiguo = crearCuenta.Empresa.inputComunaDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Comuna Antiguo : " + comunaAntiguo;
			msj = "ComunaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String comunaModificado = "";
			if (comunaAntiguo.equals("Santiago")) {

				crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("Valpar");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("a�so");
				mensaje = "Flujo : Ingreso Campo Comuna : " + "Valpara�so";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				comunaModificado = "Valpara�so";
				crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver, comunaModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "Valpara�so";
				msj = "SeleccionComuna";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				if (regionModificado.equals("V - Valparaiso")) {

					comunaModificado = "Quintero";
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("Quin");
					Thread.sleep(1000);
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("tero");
					mensaje = "Flujo : Ingreso Campo Comuna : " + "Quintero";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					Thread.sleep(2000);

					crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver, comunaModificado).click();
					mensaje = "Flujo : Seleccion de Comuna  : " + "Quintero";
					msj = "SeleccionComuna";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

				} else {

					comunaModificado = "Macul";
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("Ma");
					Thread.sleep(1000);
					crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("cul");
					mensaje = "Flujo : Ingreso Campo Comuna : " + "Macul";
					// comunaModificado = "Santiago";
					// crearCuenta.Personal.inputComunaDireccionModificar(driver).click();
					// crearCuenta.Personal.inputComunaDireccionModificar(driver).sendKeys("San");
					// Thread.sleep(1000);
					// crearCuenta.Personal.inputComunaDireccionModificar(driver).sendKeys("tiago");
					// mensaje = "Flujo : Ingreso Campo Comuna : " + "Santiago";
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					Thread.sleep(2000);

					crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver, comunaModificado).click();
					mensaje = "Flujo : Seleccion de Comuna  : " + "Macul";
					// mensaje = "Flujo : Seleccion de Comuna : " + "Santiago";
					msj = "SeleccionComuna";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.out.println(mensaje);

				}
			}

			Thread.sleep(2000);

			String calleAntiguo = crearCuenta.Empresa.inputCalleModificar(driver).getAttribute("value");
			mensaje = "Flujo : Calle Antiguo : " + calleAntiguo;
			msj = "CalleAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String calleAleatorio = calleAntiguo;
			while (calleAleatorio.equals(calleAntiguo)) {
				// Cadena de caracteres aleatoria Para calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();

			}
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputCalleModificar(driver)))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"), calleAleatorio);

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
			msj = "SeleccionCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numeroCalleAntiguo = crearCuenta.Empresa.inputNumeroCalleModificar(driver).getAttribute("value");
			mensaje = "Flujo : Numero Calle Antiguo : " + numeroCalleAntiguo;
			msj = "NumeroCalleAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numDireccionAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numDireccionAzar = numDireccionAzar + azar;
			}
			crearCuenta.Empresa.inputNumeroCalleModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					numDireccionAzar);

			mensaje = "Flujo : Ingreso Numero Direccion Azar : " + numDireccionAzar;
			msj = "IngresoNumeroDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numeroDeptoAntiguo = crearCuenta.Empresa.inputDptoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Numero Oficina Antiguo : " + numeroDeptoAntiguo;
			msj = "NumeroOficinaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String deptoCasaOficinaAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Ofocina
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
			}
			crearCuenta.Empresa.inputDptoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					"Oficina " + deptoCasaOficinaAzar);

			mensaje = "Flujo : Ingreso Oficina Azar : " + "Oficina " + deptoCasaOficinaAzar;
			msj = "DetallesDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String complementoAntiguo = crearCuenta.Empresa.inputComplementoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Complemento Antiguo : " + complementoAntiguo;
			msj = "ComplementoAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			if (complementoAntiguo.equals("inmueble interior")) {

				crearCuenta.Empresa.inputComplementoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"inmueble exterior");

				mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble exterior";
				msj = "ComplementoDireccion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				crearCuenta.Empresa.inputComplementoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"inmueble interior");

				mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
				msj = "ComplementoDireccion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			WebElement grabarModificar = driver.findElement(By.xpath("//button[contains(text(),'Guardar')]"));
			grabarModificar.click();

			// sw = 0;
			// contarTiempo = 0;
			// String textoCuadroVerde = "";
			// String textoCuadroVerdeDos = "";
			// while (sw == 0) {
			// if (contarTiempo > 30) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible " + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }
			// try {
			// textoCuadroVerde = new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerde(driver)))
			// .getText();
			// sw = 1;
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			// sw = 0;
			// contarTiempo = contarTiempo + 1;
			// }
			// }
			//
			// mensaje = "Flujo : Se revisa Edicion correcta de cuenta Empresa";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			//
			// if (textoCuadroVerde.equals("Registro guardado en SAP.")) {
			//
			// Thread.sleep(2000);
			//
			// mensaje = "Ok :" + textoCuadroVerde;
			// msj = "OkguardadoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(7000);
			//
			// swTiempoEspera = 0;
			// contarTiempoEspera = 0;
			// while (swTiempoEspera == 0) {
			//
			// try {
			// textoCuadroVerdeDos =
			// MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
			// swTiempoEspera = 1;
			// } catch (TimeoutException | NoSuchElementException e) {
			// contarTiempoEspera = contarTiempoEspera + 1;
			// swTiempoEspera = 0;
			// }
			//
			// if (contarTiempoEspera > 30) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible "
			// + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }
			//
			// if (textoCuadroVerdeDos.contains("Guardado")) {
			//
			// mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" +
			// "<p>";
			// msj = "GuardadoSalesForce";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// swTiempoEspera = 0;
			// contarTiempoEspera = 0;
			// while (swTiempoEspera == 0) {
			//
			// try {
			//
			// textoCuadroVerdeDos =
			// MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
			// swTiempoEspera = 1;
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			// contarTiempoEspera = contarTiempoEspera + 1;
			// swTiempoEspera = 0;
			// }
			//
			// if (contarTiempoEspera > 15) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible En Sales Force "
			// + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="+ rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// driver.close();
			// errorBuffer.append("\n" + mensaje + "\n");
			// throw new AssertionError(errorBuffer.toString());
			// }
			// }
			//
			// if (textoCuadroVerdeDos.contains("Registro guardado en Salesforce.")) {
			//
			// mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" +
			// "<p>";
			// msj = "GuardadoSalesForce";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// ValorEncontrado = true;
			//
			// } else {
			//
			// mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force
			// " + "</p>";
			// msj = "ErrorIngresoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// errorBuffer.append("\n" + mensaje + "\n");
			// ValorEncontrado = false;
			// }
			// }
			// }
			// } else {
			// if (textoCuadroVerde.equals("Business Partner no existe, no se realizaran
			// cambios en SAP")) {
			//
			// Thread.sleep(3000);
			//
			// mensaje = "Warning : " + textoCuadroVerde;
			// msj = "WarningGuardadoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p style='color:green; text-stroke: 1px black'>" + mensaje +
			// "</p>" + "<img src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// swTiempoEspera = 0;
			// contarTiempoEspera = 0;
			// while (swTiempoEspera == 0) {
			//
			// try {
			//
			// textoCuadroVerdeDos =
			// MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
			// swTiempoEspera = 1;
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			// contarTiempoEspera = contarTiempoEspera + 1;
			// swTiempoEspera = 0;
			// }
			//
			// if (contarTiempoEspera > 15) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible En Sales Force "
			// + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="+ rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// driver.close();
			// errorBuffer.append("\n" + mensaje + "\n");
			// throw new AssertionError(errorBuffer.toString());
			// }
			// }
			//
			//
			// if (textoCuadroVerdeDos.contains("Registro guardado en Salesforce.")) {
			//
			// mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" +
			// "<p>";
			// msj = "GuardadoSalesForce";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// ValorEncontrado = true;
			//
			// } else {
			//
			// mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force
			// " + "</p>";
			// msj = "ErrorIngresoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// errorBuffer.append("\n" + mensaje + "\n");
			// ValorEncontrado = false;
			//
			// }
			//
			// } else {
			// mensaje = "<p style='color:red;'>" + "Error : No Interactua en SAP " +
			// "</p>";
			// msj = "ErrorIngresoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// errorBuffer.append("\n" + mensaje + "\n");
			// ValorEncontrado = false;
			// }
			// }

			Thread.sleep(2000);
			mensaje = "Flujo : Se presiona Boton 'Guardar' ";
			msj = "SePresionaBotonGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			// java.util.Iterator<WebElement> i = null;
			// int validaPopUp = 1;
			// int largo = 0;
			// WebElement mensajeDeGuardadoWebElement;
			// String mensajeDeGuardado = "";
			// contarTiempo = 0;
			// String segundoMensaje = "";
			// String tercerMensaje = "";
			// int bussinesNoExiste = 1;
			// int guardadoEnSap = 1;
			// int errorEnSap = 1;
			// while (validaPopUp == 1) {
			//
			// if (contarTiempo > 60) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible" + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }

			mensaje = "Flujo : Se revisa Edicion correcta de cuenta Empresa";
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>");
			System.out.println(mensaje);
			
			
			String textoMensajeSap = "";
			String textoMensajeExactoSap = "";
			String textoMensajeExactoSap2 = "";
			int esperarMensaje = 0;
			int contadorQuiebre = 0;
			boolean guardadoOk = false;
			while (esperarMensaje == 0) {
				if (driver.findElements(By.xpath("//span[@class='toastMessage forceActionsText']")).size() < 1) {
					esperarMensaje = 0;
					contadorQuiebre = contadorQuiebre + 1;
				} else {
					
					textoMensajeSap = driver
							.findElement(By.xpath("(//div[@class='toastTitle slds-text-heading--small'])[1]"))
							.getText();
					textoMensajeExactoSap = driver
							.findElement(By.xpath("(//span[@class='toastMessage forceActionsText'])[1]")).getText();
					textoMensajeExactoSap2 = driver
							.findElement(By.xpath("(//span[@class='toastMessage forceActionsText'])[2]")).getText();
					System.err.println("textoMensajeSap *" + textoMensajeSap + "*");
					System.err.println("textoMensajeExactoSap *" + textoMensajeExactoSap + "*");
					System.err.println("textoMensajeExactoSap2 *" + textoMensajeExactoSap2 + "*");
					
					if ((textoMensajeExactoSap.contains("Registro guardado en Salesforce"))) {
						
						mensaje = "Flujo : Registro guardado en Salesforce";
						msj = "MensajeRegistroGuardadoEnSalesforce";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						if (textoMensajeExactoSap2.contains("Registro guardado en SAP")) {

							mensaje = "Flujo : Registro guardado en SAP";
							msj = "MensajeRegistroGuardadoEnSalesSap";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							esperarMensaje = 1;
							guardadoOk = true;
							
						}

						if (textoMensajeExactoSap2.contains("Business Partner no existe, no se realizaran cambios en SAP")) {							
							
							mensaje = "Flujo : 'WARNING' Business Partner no existe, no se realizaran cambios en SAP";
							msj = "MensajeBusinessPartnerNoExiste";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							esperarMensaje = 1;
							guardadoOk = true;
						}

					}
					
					if ((textoMensajeExactoSap2.contains("Registro guardado en Salesforce"))) {
						
						mensaje = "Flujo : Registro guardado en Salesforce";
						msj = "MensajeRegistroGuardadoEnSalesforce";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						if (textoMensajeExactoSap.contains("Registro guardado en SAP")) {

							mensaje = "Flujo : Registro guardado en SAP";
							msj = "MensajeRegistroGuardadoEnSalesSap";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							esperarMensaje = 1;
							guardadoOk = true;
						}

						if (textoMensajeExactoSap.contains("Business Partner no existe, no se realizaran cambios en SAP")) {

							mensaje = "Flujo : 'WARNING' Business Partner no existe, no se realizaran cambios en SAP";
							msj = "MensajeBusinessPartnerNoExiste";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);							
							esperarMensaje = 1;
							guardadoOk = true;
						}

					}

					
				}

				if (contadorQuiebre == 20) {

					mensaje = "<p style='color:red;'>" + "Error : Mensaje desconocido o no existe" + "</p>";
					msj = "ErroresMensajesEncontrados";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
				}
			}
			
			if (guardadoOk == true) {
				
				mensaje = "Flujo : Registro De Manera Correcta";
				msj = "MensajeRegistroGuardadoOk";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src="
						+ rutaEvidencia + ">");
				System.out.println(mensaje);
				
				ValorEncontrado = true;
				
			}else {
				mensaje = "<p style='color:red;'>" + "Error : Guardado incorrecto" + "</p>";
				msj = "GuardadoIncorrecto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>" + "<img src="
						+ rutaEvidencia + ">");
				System.err.println(mensaje);
				driver.close();
				throw new AssertionError(errorBuffer.toString());
			}


//			String textoMensaje = "";
//			String textoMensajeExacto = "";
//			esperarMensaje = 0;
//			contadorQuiebre = 0;
//			while (esperarMensaje == 0) {
//				if (driver.findElements(By.xpath("//span[@class='toastMessage forceActionsText']")).size() < 0) {
//					esperarMensaje = 0;
//					contadorQuiebre = contadorQuiebre + 1;
//				} else {
//
//					textoMensaje = driver
//							.findElement(By.xpath("(//div[@class='toastTitle slds-text-heading--small'])[1]"))
//							.getText();
//					textoMensajeExacto = driver
//							.findElement(By.xpath("(//span[@class='toastMessage forceActionsText'])[2]")).getText();
//					System.err.println("textoMensaje *" + textoMensaje + "*");
//					System.err.println("textoMensajeExacto *" + textoMensajeExacto + "*");
//
//					//if (textoMensaje.contains("Guardado")) {
//					if ((textoMensajeExacto.contains("Registro guardado en Salesforce"))) {
//
//						mensaje = "Flujo : Registro guardado en Salesforce";
//						msj = "MensajeRegistroGuardadoEnSalesforce";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						Thread.sleep(2000);
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);
//
//						esperarMensaje = 1;
//
//					} else {
//						mensaje = "<p style='color:red;'>" + "Error : Guardado incorrecto" + "</p>";
//						msj = "GuardadoIncorrecto";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						Thread.sleep(2000);
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.err.println(mensaje);
//						driver.close();
//						throw new AssertionError(errorBuffer.toString());
//					}
//
//				}
//
//				if (contadorQuiebre == 20) {
//
//					mensaje = "<p style='color:red;'>" + "Error : Mensaje desconocido o no existe" + "</p>";
//					msj = "ErroresMensajesEncontrados";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.err.println(mensaje);
//				}
//			}

			

			// try {
			//
			// // largo = driver.findElements(By.xpath("//span[@class='toastMessage
			// // forceActionsText'][contains(text(),'Business Partner no existe, no se
			// // realizaran cambios en SAP')]|//span[@class='toastMessage
			// // forceActionsText'][contains(text(),'Registro guardado en SAP.')]"))
			// // .size();
			// try {
			// largo = driver.findElements(By.xpath("//span[@class='toastMessage
			// forceActionsText'][contains(text(),'Business Partner no existe, no se
			// realizaran cambios en SAP')]"))
			// .size();
			// if (largo > 0) {
			// bussinesNoExiste = 0;
			// System.err.println("bussinesNoExiste");
			// }
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			// try {
			// largo = driver.findElements(By.xpath(
			// "//span[@class='toastMessage forceActionsText'][contains(text(),'Registro
			// guardado en SAP.')]"))
			// .size();
			//
			// if (largo > 0) {
			// guardadoEnSap = 0;
			// System.err.println("guardadoEnSap");
			// }
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// try {
			// largo =
			// driver.findElements(By.xpath("//span[contains(text(),'Error')]")).size();
			// if (largo > 0) {
			// errorEnSap = 0;
			// System.err.println("errorEnSap");
			// }
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// System.err.println("Elemento SAP " + largo);
			// if (largo > 0) {
			//
			// validaPopUp = 1;
			//
			// // java.util.Iterator<WebElement> i =
			// // driver.findElements(By.xpath("//span[@class='toastMessage
			// // forceActionsText'][contains(text(),'Business Partner no existe, no se
			// // realizaran cambios en SAP')]|//span[@class='toastMessage
			// // forceActionsText'][contains(text(),'Registro guardado en SAP.')]"))
			// // .iterator();
			//
			// if (bussinesNoExiste == 0) {
			//
			// i = driver.findElements(By.xpath(
			// "//span[@class='toastMessage forceActionsText'][contains(text(),'Business
			// Partner no existe, no se realizaran cambios en SAP')]"))
			// .iterator();
			// }
			//
			// if (guardadoEnSap == 0) {
			// i = driver.findElements(By.xpath(
			// "//span[@class='toastMessage forceActionsText'][contains(text(),'Registro
			// guardado en SAP.')]"))
			// .iterator();
			// }
			//
			// if (errorEnSap == 0) {
			// i =
			// driver.findElements(By.xpath("//span[contains(text(),'Error')]")).iterator();
			// }
			//
			// while (i.hasNext()) {
			// mensajeDeGuardadoWebElement = i.next();
			// mensajeDeGuardado = mensajeDeGuardadoWebElement.getText();
			// System.err.println("Mensaje Pop up ciclo : " + mensajeDeGuardado);
			// }
			//
			// // try {
			// // System.err.println("Msje lightning-icon
			// // "+driver.findElement(By.xpath("//*[@data-key='success']")).getText());
			// //
			// // } catch (TimeoutException | NoSuchElementException e) {
			// //
			// // }
			// //
			// // try {
			// // System.err.println("Msje lightning-icon
			// // "+driver.findElement(By.xpath("//lightning-icon[@class='toastIcon
			// // slds-m-right--small slds-no-flex slds-align-top slds-icon-utility-success
			// // slds-icon_container']")).getText());
			// //
			// // } catch (TimeoutException | NoSuchElementException e) {
			// //
			// // }
			//
			// try {
			// segundoMensaje = driver
			// .findElement(By.xpath("//span[@class='toastMessage
			// forceActionsText']")).getText();
			// // System.err.println("Msje span
			// // "+driver.findElement(By.xpath("//span[@class='toastMessage
			// // forceActionsText']")).getText());
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// // try {
			// // System.err.println("Msje Katalon 0
			// // "+driver.findElement(By.xpath("(.//*[normalize-space(text()) and
			// // normalize-space(.)='Cuenta personal'])[1]/following::div[1]")).getText());
			// //
			// // } catch (TimeoutException | NoSuchElementException e) {
			// //
			// // }
			//
			// // try {
			// // tercerMensaje = driver.findElement(By.xpath("(.//*[normalize-space(text())
			// // and normalize-space(.)='Cargando...'])[2]/following::div[13]")).getText();
			// // System.err.println("Msje Katalon 1
			// // "+driver.findElement(By.xpath("(.//*[normalize-space(text()) and
			// // normalize-space(.)='Cargando...'])[2]/following::div[13]")).getText());
			// //
			// // } catch (TimeoutException | NoSuchElementException e) {
			// //
			// // }
			// //
			// try {
			// tercerMensaje = driver.findElement(By.xpath(
			// "(.//*[normalize-space(text()) and
			// normalize-space(.)='success'])[1]/following::div[3]"))
			// .getText();
			// // System.err.println("Msje Katalon 2
			// // "+driver.findElement(By.xpath("(.//*[normalize-space(text()) and
			// // normalize-space(.)='success'])[1]/following::div[3]")).getText());
			//
			// } catch (TimeoutException | NoSuchElementException e) {
			//
			// }
			//
			// } else {
			//
			// if (mensajeDeGuardado.equals("Business Partner no existe, no se realizaran
			// cambios en SAP")) {
			// mensaje = "Warning : " + mensajeDeGuardado;
			// msj = "WarningGuardadoASap";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			// }
			//
			// mensaje = "Flujo : Ingreso SAP : " + mensajeDeGuardado;
			// msj = "IngresoSap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.out.println(mensaje);
			// validaPopUp = 0;
			// }
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			// contarTiempo = contarTiempo + 1;
			// }
			//
			// }
			//
			// System.err.println("Mensaje Pop up Fuera ciclo : " + mensajeDeGuardado);
			// System.err.println("Mensaje Valida Guardado : " + segundoMensaje);
			// System.err.println("Mensaje Guardado : " + tercerMensaje);
			//
			// if ((mensajeDeGuardado.equals("Business Partner no existe, no se realizaran
			// cambios en SAP"))
			// || (mensajeDeGuardado.contains("Registro guardado en SAP"))) {
			// if (segundoMensaje.contains("Registro guardado en Salesforce")) {
			// mensaje = "OK : Guardado Correcto";
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>");
			// System.out.println(mensaje);
			// ValorEncontrado = true;
			// } else {
			// mensaje = "<p style='color:red;'>" + "Error : Guardado incorrecto" + "</p>";
			// msj = "GuardadoIncorrecto";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }
			// } else {
			// mensaje = "<p style='color:red;'>" + "Error : Guardado incorrecto" + "</p>";
			// msj = "GuardadoIncorrecto";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }

//			mensaje = "Flujo : Se revisa Edicion correcta de cuenta Empresa";
//			Log.info(mensaje);
//			Reporter.log("<p>" + mensaje + "</p>");
//			System.out.println(mensaje);

			// if (segundoMensaje.contains("Registro guardado en Salesforce")) {
			//
			// mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" +
			// "<p>";
			// msj = "GuardadoSalesForce";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// ValorEncontrado = true;
			//
			// } else {
			//
			// mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force
			// " + "</p>";
			// msj = "ErrorIngresoASap";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.err.println(mensaje);
			// errorBuffer.append("\n" + mensaje + "\n");
			// ValorEncontrado = false;
			//
			// }

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Edita Cuenta" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: No Edita Cuenta" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}

}
