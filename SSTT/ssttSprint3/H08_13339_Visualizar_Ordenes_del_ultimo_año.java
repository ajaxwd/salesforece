package ssttSprint3;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H08_13339_Visualizar_Ordenes_del_ultimo_año {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H08_13339_Visualizar_Ordenes_del_ultimo_año.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H08_13339_Visualizar_Ordenes_del_ultimo_año.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);		
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
			.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			mensaje = "Flujo : Resultado Busqueda ";
			msj = "ResultadoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
					+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);

					new WebDriverWait(driver, 40, 100)
					.until(ExpectedConditions.visibilityOf(
					paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
					.getText();

					mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
					msj = "ClickElementoencontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					new WebDriverWait(driver, 40, 100)
					.until(ExpectedConditions.visibilityOf(
					paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
					.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;
				}
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(10000);

			int contadorDeErroresTotal = 0;
			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H08_13339_Visualizar_Ordenes_del_ultimo_año.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			recorreExcell = 0;
			String nombreLink = "";
			while (recorreExcell == 0) {

				if ("Link Relacionado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLink = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
						principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink));
						principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLink).getText();
						mensaje = "OK : Link esperado '" + nombreLink + "' Existe en 'Relacionado'";
						msj = nombreLink + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
						mensaje = "<p style='color:red;'>" + "Error : Link esperado '" + nombreLink
						+ "' NO Existe en 'Relacionado'" + "</p>";
						System.err.println(mensaje);
						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}
				} else {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			// Campa�a se toma elemento Manual

			try {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver));
				principalCuenta.MenuPrincipalCuenta.linkCampanaRelacionado(driver).getText();
				mensaje = "OK : Link esperado 'Campaña' Existe en 'Relacionado'";
				msj = "CampanaExiste";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				contadorCorrectos = contadorCorrectos + 1;
				correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
				System.out.println(correctos);
				ValorEncontrado = true;
			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
				mensaje = "<p style='color:red;'>" + "Error : Link esperado 'Campaña' NO Existe en 'Relacionado'"
						+ "</p>";
				System.err.println(mensaje);
				contadorDeErroresTotal = contadorDeErroresTotal + 1;
				erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
				contadorIncorrectos = contadorIncorrectos + 1;
			}

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'RELACIONADO':" + "</p></b>";
			Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
					Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
			Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Funciones.mensajesLog(mensajeResumen);

			 if (contadorIncorrectos > 0) {
			
			 mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra "
			 + contadorIncorrectos
			 + " 'Label' " + "</p>";
			 msj = "PruebaErronea";
			 posicionEvidencia = posicionEvidencia + 1;
			 posicionEvidenciaString = Integer.toString(posicionEvidencia);
			 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			 errorBuffer.append("\n" + mensaje + "\n");
			 ValorEncontrado = false;
			 } else {

			Thread.sleep(3000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.xpath("//span[@title='Órdenes del último año']")));

			String textoElementoColumna = "";
			String conteoRecorridoString = "";
			String elementosOTVista360 = "";
			WebElement posicionColumna = null;

			Thread.sleep(3000);
			
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",principalCuenta.MenuPrincipalCuenta.linkOrdenesUltimoAnoCantidad(driver));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-200)", "");

			conteoRecorridoString = principalCuenta.MenuPrincipalCuenta.linkOrdenesUltimoAnoCantidad(driver).getAttribute("title");
			System.err.println("al año "+conteoRecorridoString);
			 if (conteoRecorridoString.equals("(6+)")) {
			 conteoRecorridoString = "(6)";
			
			 for (int i = 1; i <= 6; i++) {			
					posicionColumna = null;
					textoElementoColumna = "";
					mensajeResumen = "";
					contadorCorrectos = 0;
					contadorIncorrectos = 0;
					correctos = "";
					erroneos = "";

					Thread.sleep(3000);
					
					posicionColumna = principalCuenta.MenuPrincipalCuenta.linkRecorridoOrdenesUltimoAno(driver, i);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", posicionColumna);
					textoElementoColumna = posicionColumna.getText();
					elementosOTVista360 = elementosOTVista360 + "<p style='color:blue;'>" + "- " + textoElementoColumna
							+ "</p>";
					ValorEncontrado = true;
				}

				mensajeResumen = "";

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
				driver.findElement(By.xpath("//span[@title='Órdenes del último año']")));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)", "");

				mensaje = "Flujo : Resumen Revision";
				msj = "ResumenRevision";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				mensajeResumen = "<p><b>" + "RESUMEN REVISION OT EN 'ORDENES DEL ULTIMO AñO': " + "</p></b>";
				Funciones.mensajesLog(mensajeResumen);

				mensajeResumen = elementosOTVista360;
				Funciones.mensajesLog(mensajeResumen);

				if (contadorIncorrectos > 0) {

					mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
							+ " 'Label' " + "</p>";
					msj = "PruebaErronea";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
				} else {

					Thread.sleep(3000);

					mensajeResumen = "";
					contadorCorrectos = 0;
					contadorIncorrectos = 0;
					nombreLabel = "";
					correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
					erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
					contadorCorrectos = 0;
					contadorIncorrectos = 0;
					recorreExcell = 0;
					inicioExcell = 0;

					utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
					utilidades.DatosInicialesYDrivers.File_TestData = "H08_13339_Visualizar_Ordenes_del_ultimo_año.xlsx";
					ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
							"Hoja1");
					while (recorreExcell == 0) {
						if ("Label Orden 360".equals(ExcelUtils.getCellData(0, inicioExcell))) {
							recorreExcell = 1;
							break;
						}
						inicioExcell = inicioExcell + 1;
						recorreExcell = 0;
					}

					recorreExcell = 0;
					nombreLink = "";
					while (recorreExcell == 0) {

						if ("Label Orden 360".equals(ExcelUtils.getCellData(0, inicioExcell))) {
							try {
								nombreLink = ExcelUtils.getCellData(1, inicioExcell);
								principalCuenta.MenuPrincipalCuenta.labelOrdenesUltimoAnoVista360(driver, nombreLink)
										.getText();
								mensaje = "OK : Label esperado '" + nombreLink
										+ "' Existe en 'Ordenes del Ultimo A�o 360'";
								msj = nombreLink + "Existe";
								msj = msj.replace(" ", "");
								posicionEvidencia = posicionEvidencia + 1;
								posicionEvidenciaString = Integer.toString(posicionEvidencia);
								nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
								Thread.sleep(2000);
								rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
								Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
								contadorCorrectos = contadorCorrectos + 1;
								correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
								System.out.println(correctos);
								ValorEncontrado = true;

							} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

								mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLink
								+ " NO  Existe en 'Ordenes del Ultimo Año 360'" + "</p>";
								System.err.println(mensaje);
								erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
								contadorIncorrectos = contadorIncorrectos + 1;
							}
						} else {
							recorreExcell = 1;
							break;
						}
						inicioExcell = inicioExcell + 1;
						recorreExcell = 0;
					}

					Thread.sleep(2000);

					mensajeResumen = "<p><b>" + "RESUMEN REVISION LABEL VISTA 360 'OREDENES DEL ULTIMO AñO':"
							+ "</p></b>";
							Funciones.mensajesLog(mensajeResumen);

					mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
							+ "</p></b>";
							Funciones.mensajesLog(mensajeResumen);

					mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
					Funciones.mensajesLog(mensajeResumen);

					mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
							+ "</p></b>";
							Funciones.mensajesLog(mensajeResumen);

					mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
					Funciones.mensajesLog(mensajeResumen);

					 if (contadorIncorrectos > 0) {
					
					 mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra "
					 + contadorIncorrectos + " 'Label' " + "</p>";
					 msj = "PruebaErronea";
					 posicionEvidencia = posicionEvidencia + 1;
					 posicionEvidenciaString = Integer.toString(posicionEvidencia);
					 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					 errorBuffer.append("\n" + mensaje + "\n");
					 ValorEncontrado = false;
					 } else {

					Thread.sleep(3000);

					mensaje = "Flujo : Click 'Ver Todos' ";
					msj = "ClickVerTodos";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

					new WebDriverWait(driver, 40, 100)
					.until(ExpectedConditions
					.visibilityOf(principalCuenta.MenuPrincipalCuenta.linkVerTodoOrdenAnuales(driver)))
					.click();

					Thread.sleep(5000);

					String ordenadoPor = principalCuenta.MenuPrincipalCuenta.txtOrdenadoPorEnOrdenesAnuales(driver)
					.getText();

					if (ordenadoPor.contains("Ordenado por Fecha Apertura")) {

						mensaje = "OK : Vista esta 'Ordenado por Fecha Apertura' en Indicador De 'En Ordenes del Ultimo A�o' ";
						msj = "OrdenadoPorFecha";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						mensajeResumen = "";
						contadorCorrectos = 0;
						contadorIncorrectos = 0;
						nombreLabel = "";
						correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
						erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
						contadorCorrectos = 0;
						contadorIncorrectos = 0;
						recorreExcell = 0;
						inicioExcell = 0;

						utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
						utilidades.DatosInicialesYDrivers.File_TestData = "H08_13339_Visualizar_Ordenes_del_ultimo_a�o.xlsx";
						ExcelUtils.setExcelFile(
								DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
						while (recorreExcell == 0) {
							if ("Label Orden Principal".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
						}

						recorreExcell = 0;
						nombreLink = "";
						while (recorreExcell == 0) {

							if ("Label Orden Principal".equals(ExcelUtils.getCellData(0, inicioExcell))) {
								try {
									nombreLink = ExcelUtils.getCellData(1, inicioExcell);
									((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
											principalCuenta.MenuPrincipalCuenta.linkRevisionPrincipalActivo(driver,
													nombreLink));
									principalCuenta.MenuPrincipalCuenta.linkRevisionPrincipalActivo(driver, nombreLink)
											.getText();
									mensaje = "OK : Label esperado '" + nombreLink
											+ "' Existe en 'Ordenes de Trabajo Principal'";
									msj = nombreLink + "Existe";
									msj = msj.replace(" ", "");
									posicionEvidencia = posicionEvidencia + 1;
									posicionEvidenciaString = Integer.toString(posicionEvidencia);
									nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
									Thread.sleep(2000);
									rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver,nombreCaptura + "_" + msj);
									Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
									contadorCorrectos = contadorCorrectos + 1;
									correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
									System.out.println(correctos);
									ValorEncontrado = true;
								} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
									mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLink
									+ " NO  Existe en 'Ordenes de Trabajo Historica 360'" + "</p>";
									System.err.println(mensaje);
									erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
									contadorIncorrectos = contadorIncorrectos + 1;
								}
							} else {
								recorreExcell = 1;
								break;
							}
							inicioExcell = inicioExcell + 1;
							recorreExcell = 0;
						}

						mensajeResumen = "<p><b>" + "RESUMEN REVISION LABEL 'OREDENES DE TRABAJO PRINCIPAL':"
								+ "</p></b>";
								Funciones.mensajesLog(mensajeResumen);

						mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
								+ "</p></b>";
								Funciones.mensajesLog(mensajeResumen);

						mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
						Funciones.mensajesLog(mensajeResumen);

						mensajeResumen = "<p><b>" + contadorIncorrectos
								+ " Titulos de campo NO coinciden con los esperados" + "</p></b>";
								Funciones.mensajesLog(mensajeResumen);

						mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
						Funciones.mensajesLog(mensajeResumen);

						if (contadorIncorrectos > 0) {

							mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra "
									+ contadorIncorrectos + " 'Label' " + "</p>";
							msj = "PruebaErronea";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
						} 
					} else {
						mensaje = "<p style='color:red;'>"
								+ "Error: Vista NO esta 'Ordenado por Fecha Apertura' en Indicador De 'Ordenes de Trabajo Historica'"
								+ "</p>";
						msj = "PruebaErronea";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						errorBuffer.append("\n" + mensaje + "\n");
						ValorEncontrado = false;
					}
					 }
				}
			} else {
				mensaje = "<p style='color:red;'>"
						+ "Error: NO cumple Criterio de Aceptacion se Necesitan a lo menos 6 Registros desplegados en '�rdenes del �ltimo a�o'"
						+ "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Todos los Campos son Verificados" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Prueba Fallida" + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			// driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
			// throw new AssertionError();
		}
		driver.quit();
	}
}
