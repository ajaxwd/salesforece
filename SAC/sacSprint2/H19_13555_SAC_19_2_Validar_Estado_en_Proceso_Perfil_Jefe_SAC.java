package sacSprint2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H19_13555_SAC_19_2_Validar_Estado_en_Proceso_Perfil_Jefe_SAC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H19_13555_SAC_19_2_Validar_Estado_en_Proceso_Perfil_Jefe_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H19_13555_SAC_19_2_Validar_Estado_en_Proceso_Perfil_Jefe_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);
		String negocio = ExcelUtils.getCellData(1, 6);
		String area = ExcelUtils.getCellData(1, 7);
		String origen = ExcelUtils.getCellData(1, 8);
		String categoria = ExcelUtils.getCellData(1, 9);
		String subtipo = ExcelUtils.getCellData(1, 10);
		String subcategoria = ExcelUtils.getCellData(1, 11);
		String activo = ExcelUtils.getCellData(1, 12);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + Usuario + "</p>";
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		String seleccion = "";

		Thread.sleep(5000);

		try {

			String centro = "";
			centro = Funciones.getExcel(driver, "Centro", posicionExcel);

//			new WebDriverWait(driver, 20, 100)
//					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicaciones(driver)))
//					.click();
//
//			mensaje = "Flujo : Click Menu Aplicaciones ";
//			msj = "ClickMenuApp";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);
//
//			Thread.sleep(3000);
//
//			new WebDriverWait(driver, 20, 100)
//					.until(ExpectedConditions
//							.visibilityOf(paginaPrincipal.MenuPrincipal.linkServicioMenuAplicaciones(driver)))
//					.sendKeys(Keys.ENTER);
//
//			mensaje = "Flujo : Click Link Servicio";
//			msj = "ClickServicio";
//			posicionEvidencia = posicionEvidencia + 1;
//			posicionEvidenciaString = Integer.toString(posicionEvidencia);
//			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//			Thread.sleep(2000);
//			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//			Log.info(mensaje);
//			Reporter.log(
//					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//			System.out.println(mensaje);
//
//			Thread.sleep(5000);

//			try {
//
//				new WebDriverWait(driver, 20, 100)
//						.until(ExpectedConditions.elementToBeClickable(paginaPrincipal.MenuPrincipal.btnMostrarMenuNavegacionConsolaAlternativo(driver)))
//						.sendKeys(Keys.ENTER);
//				mensaje = "Flujo : Click Boton Mostrar men� de navegaci�n ";
//				msj = "ClickBtnMostrarMenu";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				Thread.sleep(2000);
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);
//
//			} catch (Exception e) {
//				// TODO: handle exception
//			}

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
			.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
			.sendKeys(Keys.ENTER);

			Thread.sleep(3000);

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
					+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)
					.sendKeys(Keys.ENTER);
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;
				}
			}

			mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
			msj = "ClickElementoencontrado";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			// new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions
			// .elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
			// .sendKeys(Keys.ENTER);

			principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver).click();

			Thread.sleep(1000);

			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(7000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
			driver.findElement(By.xpath("//span[@title='Relaciones']")));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy (0,-900)");

			Thread.sleep(10000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(principalCuenta.MenuPrincipalCuenta.btnCasoNuevo(driver)))
			.click();

			mensaje = "Flujo : Click Boton Caso Nuevo";
			msj = "ClickBotonCasoNuevo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			crearCaso.CrearCaso.rdNuevoCaso(driver).click();
			mensaje = "Flujo : Seleccion radio button Reclamo ";
			msj = "SeleccionReclamo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(1000);

			crearCaso.CrearCaso.btnSiguiente(driver).click();
			mensaje = "Flujo : Seleccion boton Siguiente en la seleccion del tipo de Nuevo Caso ";
			msj = "btnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(5000);

			seleccion = negocio;

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectElementoCaso(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Negocio :" + seleccion;
			msj = "SeleccionNegocio";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			seleccion = area;

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectElementoArea(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Area :" + seleccion;
			msj = "SeleccionArea";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);
			
			crearCaso.CrearCaso.inputCentro(driver).sendKeys(centro);
			mensaje = "Flujo : Seleccionar input Centro ";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(1000);

			crearCaso.CrearCaso.selectCentro(driver, centro).click();
			mensaje = "Flujo : Se selecciona centro :  " + centro;
			msj = "selectCentro";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			seleccion = origen;

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectElementoOrigen(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Origen :" + seleccion;
			msj = "SeleccionOrigen";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			seleccion = categoria;

			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectSegundoElementoCaso(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Categoria :" + seleccion;
			msj = "SeleccionCategoria";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			seleccion = subtipo;

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectSubTipo(driver))).click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Subtipo :" + seleccion;
			msj = "SeleccionSubtipo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			seleccion = subcategoria;

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.selectSubcategoria(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.elementoSelectCaso(driver, seleccion)))
			.click();

			mensaje = "Flujo : Seleccion Subcategoria :" + seleccion;
			msj = "SeleccionSubcategoria";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH-mm-ss");
			Date date = new Date();
			String dateTime = dateFormat.format(date);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.textAreaDescripcion(driver)))
			.sendKeys("Test " + nombreCuenta + " " + dateTime);

			mensaje = "Flujo : Ingreso Descripcion : " + "Test " + nombreCuenta + " " + dateTime;
			msj = "IngresoDescripcion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearCaso.CrearCaso.inputActivoCaso(driver));

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.inputActivoCaso(driver)))
			.sendKeys(Keys.ENTER);

			Thread.sleep(2000);

			String[] arregloActivo = new String[activo.length()];
			for (int i = 0; i < activo.length(); i++) {
				arregloActivo[i] = Character.toString(activo.charAt(i));
			}
			for (int i = 0; i < activo.length(); i++) {
				crearCaso.CrearCaso.inputActivoCaso(driver).sendKeys(arregloActivo[i]);
				Thread.sleep(500);
			}

			Thread.sleep(2000);

			WebElement elementoActivo = driver.findElement(By.xpath("//div[@title='" + activo + "']"));
			elementoActivo.click();
			mensaje = "Flujo : Ingreso Activo";
			msj = "IngresoActivo";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCaso.CrearCaso.btnGuardarCaso(driver))).click();

			mensaje = "Flujo : Click 'Guardar'";
			msj = "ClickGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Thread.sleep(4000);
			//
			// sw = 0;
			// contarTiempo = 0;
			// String textoCuadroVerde = "";
			//
			// while (sw == 0) {
			// if (contarTiempo > 30) {
			// mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta
			// Visible" + "</p>";
			// msj = "ElementoNoEncontrado";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log("<p>" + mensaje + "</p>" + "<img
			// src="
			// + rutaEvidencia + ">");
			// System.err.println(mensaje);
			// ValorEncontrado = false;
			// errorBuffer.append("\n" + mensaje + "\n");
			// driver.close();
			// throw new AssertionError(errorBuffer.toString());
			// }
			// try {
			// textoCuadroVerde = new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions.visibilityOf(crearCaso.CrearCaso.txtMensajeGuardado(driver)))
			// .getText();
			// sw = 1;
			//
			// } catch (NoSuchElementException | ScreenshotException e) {
			// sw = 0;
			// contarTiempo = contarTiempo + 1;
			// }
			// }

			Thread.sleep(2000);

			sw = 0;
			contarTiempo = 0;
			String textoCuadroVerde = "";

			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible" + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}

				int esperarMensajeSimulacion = 0;
				while (esperarMensajeSimulacion == 0) {
					int vueltas = 0;
					if (driver.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']"))
						.size() > 0) {
						vueltas = vueltas + 1;
						System.out.println(vueltas);
						esperarMensajeSimulacion = 0;
					} else {
						mensaje = "Flujo : Mensaje 'De Guardado'";
						msj = "MsjGuardado";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						esperarMensajeSimulacion = 1;
					}
				}

				try {
					WebElement ElelentTextoNotificacionesVerdeCss = driver.findElement(By.cssSelector(
					"div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage"));
					String textoNotificacionesVerdeCss = ElelentTextoNotificacionesVerdeCss.getText();
					System.err.println("textoNotificacionesVerdeCss " + textoNotificacionesVerdeCss);

					System.out.println(textoNotificacionesVerdeCss);
					System.out.println("Se creó Caso.");

					if (textoNotificacionesVerdeCss.contains("Se creó Caso.")) {
						textoCuadroVerde = textoNotificacionesVerdeCss;
						sw = 1;
						break;
					} else {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Registro NO Crea Caso. " + "<p>";
						msj = "ErrorNoGuardaRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				} catch (TimeoutException | NoSuchElementException e) { sw = 0; }

				try {
					String textoNotificacionesWarning = driver.findElement(By.xpath(
					"(.//*[normalize-space(text()) and normalize-space(.)='warning'])[1]/following::div[3]"))
					.getText();
					System.err.println("textoNotificacionesVerdeDos " + textoNotificacionesWarning);
					mensaje = "<p style='color:green;'>" + "Warnng:  " + textoNotificacionesWarning + "<p>";
					msj = "WarningGuardaRegistro";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				} catch (TimeoutException | NoSuchElementException e) { }

				try {
					String textoNotificacionesVerdePequeno = MenuPrincipal.textoNotificacionesVerdePequeno(driver)
							.getText();
					System.err.println("textoNotificacionesVerdePequeno " + textoNotificacionesVerdePequeno);
					if (textoNotificacionesVerdePequeno.contains("Se creó Caso.")) {
						textoCuadroVerde = textoNotificacionesVerdePequeno;
						sw = 1;
						break;
					} else {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Registro NO Crea Caso. " + "<p>";
						msj = "ErrorNoGuardaRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
				} catch (TimeoutException | NoSuchElementException e) { sw = 0; }

				try {

					String textoError = driver
					.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']")).getText();
					String contenidoError = driver.findElement(By.xpath(
					"(//div[@class='toastTitle slds-text-heading--small'])[1]/following::span[@class='toastMessage forceActionsText'][1]"))
					.getText();
					System.err.println("Cont Error " + contenidoError);
					if (textoError.contains("Error") || textoError.contains("error")) {
						mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida " + contenidoError + "<p>";
						msj = "Error";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						driver.close();
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						throw new AssertionError(errorBuffer.toString());
					}
					sw = 1;
				} catch (NoSuchElementException | ScreenshotException e1) { sw = 0; }
				contarTiempo = contarTiempo + 1;
			}

			mensaje = "Flujo : Se revisa Guardado Correcto";
			Funciones.mensajesLog(mensaje);

			if (textoCuadroVerde.contains("Se creó Caso.")) {

				Thread.sleep(2000);

				mensaje = "Ok :" + textoCuadroVerde;
				msj = "OkGuardadoCorrecto";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				crearCaso.CrearCaso.linkMensajeGuardado(driver).click();

				Thread.sleep(7000);

				mensaje = "Flujo : Click " + textoCuadroVerde;
				msj = "ClickCaso";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				crearCaso.CrearCaso.linkActividad(driver).click();
				mensaje = "Flujo : Click 'Actividades'";
				msj = "ClickActividades";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				crearCaso.CrearCaso.linkNuevaTarea(driver).click();
				mensaje = "Flujo : Click 'Nueva Tarea'";
				msj = "ClickNuevaTarea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				crearCaso.CrearCaso.inputAsunto(driver).click();
				mensaje = "Flujo : Click 'Input Asunto'";
				msj = "ClickInputAsunto";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				crearCaso.CrearCaso.selectPrimerContacto(driver).click();
				mensaje = "Flujo : Click 'Primer Contacto'";
				msj = "ClickPrimerContacto";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(1000);

				jse.executeScript("arguments[0].scrollIntoView();",
				driver.findElement(By.xpath("//span[@class='title'][contains(text(),'Relacionado')]")));

				Thread.sleep(4000);

				crearCaso.CrearCaso.inputFechaVencimiento(driver).click();
				mensaje = "Flujo : Click 'Fecha Vencimiento'";
				msj = "ClickFechaVencimiento";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				crearCaso.CrearCaso.linkHoy(driver).click();
				mensaje = "Flujo : Click 'Hoy'";
				msj = "ClickHoy";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(3000);

				jse.executeScript("arguments[0].scrollIntoView();",
				driver.findElement(By.xpath("//div/div/div/div/div[2]/div/div/div/div/label/span")));

				Thread.sleep(4000);

				int sw1 = 0;
				while (sw1 == 0) {
					try {
						int ok_size = driver.findElements(By.xpath("//div[3]/div/div/div[2]/div[2]/button/span"))
								.size();
						driver.findElements(By.xpath("//div[3]/div/div/div[2]/div[2]/button/span")).get(ok_size - 1)
								.click();
						sw1 = 1;
					} catch (NoSuchElementException | ScreenshotException e) { sw1 = 0; }
				}

				mensaje = "Flujo : Click 'Guardar Tarea'";
				msj = "ClickGuardarTarea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				sw = 0;
				contarTiempo = 0;
				textoCuadroVerde = "";

				while (sw == 0) {
					if (contarTiempo > 15) {
						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible" + "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}

					int esperarMensajeSimulacion = 0;
					while (esperarMensajeSimulacion == 0) {
						int vueltas = 0;
						if (driver.findElements(By.xpath("//lightning-spinner[@class='slds-spinner_container']"))
								.size() > 0) {
							vueltas = vueltas + 1;
							System.out.println(vueltas);
							esperarMensajeSimulacion = 0;
						} else {
							mensaje = "Flujo : Mensaje 'De Guardado'";
							msj = "MsjGuardado";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							esperarMensajeSimulacion = 1;
						}
					}

					try {
						WebElement ElelentTextoNotificacionesVerdeCss = driver.findElement(By.cssSelector(
						"div.slds-theme--success.slds-notify--toast.slds-notify.slds-notify--toast.forceToastMessage"));
						String textoNotificacionesVerdeCss = ElelentTextoNotificacionesVerdeCss.getText();
						System.err.println("textoNotificacionesVerdeCss " + textoNotificacionesVerdeCss);
						if (textoNotificacionesVerdeCss.contains("Se creó Tarea SAC - Primer Contacto.")) {
							textoCuadroVerde = textoNotificacionesVerdeCss;
							sw = 1;
							break;
						} else {
							mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Registro NO Crea Caso. "
									+ "<p>";
							msj = "ErrorNoGuardaRegistro";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							driver.close();
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							throw new AssertionError(errorBuffer.toString());
						}
					} catch (TimeoutException | NoSuchElementException e) { sw = 0; }

					try {
						String textoNotificacionesWarning = driver.findElement(By.xpath(
						"(.//*[normalize-space(text()) and normalize-space(.)='warning'])[1]/following::div[3]"))
						.getText();
						System.err.println("textoNotificacionesVerdeDos " + textoNotificacionesWarning);
						mensaje = "<p style='color:green;'>" + "Warnng:  " + textoNotificacionesWarning + "<p>";
						msj = "WarningGuardaRegistro";
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
						Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					} catch (TimeoutException | NoSuchElementException e) { }

					try {
						String textoNotificacionesVerdePequeno = MenuPrincipal.textoNotificacionesVerdePequeno(driver)
						.getText();
						System.err.println("textoNotificacionesVerdePequeno " + textoNotificacionesVerdePequeno);
						if (textoNotificacionesVerdePequeno.contains("Se creó Tarea SAC - Primer Contacto.")) {
							textoCuadroVerde = textoNotificacionesVerdePequeno;
							sw = 1;
							break;
						} else {
							mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Registro NO Crea Caso. "
									+ "<p>";
							msj = "ErrorNoGuardaRegistro";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							driver.close();
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							throw new AssertionError(errorBuffer.toString());
						}
					} catch (TimeoutException | NoSuchElementException e) { sw = 0; }

					try {

						String textoError = driver
						.findElement(By.xpath("//div[@class='toastTitle slds-text-heading--small']")).getText();
						String contenidoError = driver.findElement(By.xpath(
						"(//div[@class='toastTitle slds-text-heading--small'])[1]/following::span[@class='toastMessage forceActionsText'][1]"))
						.getText();
						System.err.println("Cont Error " + contenidoError);
						if (textoError.contains("Error") || textoError.contains("error")) {
							mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida " + contenidoError + "<p>";
							msj = "Error";
							posicionEvidencia = posicionEvidencia + 1;
							rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
							Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
							driver.close();
							ValorEncontrado = false;
							errorBuffer.append("\n" + mensaje + "\n");
							throw new AssertionError(errorBuffer.toString());
						}
						sw = 1;
					} catch (NoSuchElementException | ScreenshotException e1) { sw = 0; }
					contarTiempo = contarTiempo + 1;
				}

				if (textoCuadroVerde.contains("Se creó Tarea SAC - Primer Contacto.")) {

					mensaje = "Ok : Se creó Tarea SAC - Primer Contacto.";
					msj = "OkGuardadoCorrecto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				} else {
					mensaje = "<p style='color:red;'>" + "Error : error al guardar " + textoCuadroVerde + "</p>";
					msj = "ErrorGuardado";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
					Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;

				}

				Thread.sleep(3000);
				driver.navigate().refresh();
				Thread.sleep(15000);

				new WebDriverWait(driver, 20, 100)
				.until(ExpectedConditions.elementToBeClickable(crearCaso.CrearCaso.linkEnProceso(driver)))
				.sendKeys(Keys.ENTER);
				mensaje = "Flujo : Click 'Link En Preoceso'";
				msj = "ClickEnProceso";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				crearCaso.CrearCaso.btnMarcarEstado(driver).click();
				mensaje = "Flujo : Click 'Marcar Estado'";
				msj = "ClickMarcarestado";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

				Thread.sleep(4000);

				String mensajeTarea = crearCaso.CrearCaso.txtMensajeTarea(driver).getText();
				mensaje = "Flujo : Revisa Mensaje";
				msj = "RevisaMensaje";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				if (mensajeTarea.equals("Estado cambió correctamente.")) {
					mensaje = "Ok : " + mensajeTarea;
					msj = "OkCambioEstadoCorrecto";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = true;
				} else {
					mensaje = "<p style='color:red;'>" + "Error : error al cambiar " + mensajeTarea + "</p>";
					msj = "ErrorCambio";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
				}
				Thread.sleep(2000);
			} else {
				mensaje = "<p style='color:red;'>" + "Error : error al guardar " + textoCuadroVerde + "</p>";
				msj = "ErrorGuardado";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
				+ "OK: Prueba Correcta ; Crea, Guarda Caso, genera primer contacto y cambia estado a 'EN PROCESO'"
				+ "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: No Crea Caso, por lo tanto no cambia de estado" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
