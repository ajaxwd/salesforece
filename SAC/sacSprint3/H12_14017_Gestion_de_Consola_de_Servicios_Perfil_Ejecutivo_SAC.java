package sacSprint3;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H12_14017_Gestion_de_Consola_de_Servicios_Perfil_Ejecutivo_SAC {

	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H12_14017_Gestion_de_Consola_de_Servicios_Perfil_Ejecutivo_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H12_14017_Gestion_de_Consola_de_Servicios_Perfil_Ejecutivo_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel);
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		mensaje = "<p>" + "Usuario test en Ejecucion : " + Usuario + "</p>";
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
			+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
					Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicaciones(driver)))
					.click();

			mensaje = "Flujo : Click Menu Aplicaciones ";
			msj = "ClickMenuApp";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			try {
				new WebDriverWait(driver, 20, 100)
				.until(ExpectedConditions
				.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicacionesAlternativo(driver)))
				.click();

				mensaje = "Flujo : Click Menu Aplicaciones ";
				msj = "ClickMenuApp";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e2) {
				mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
				msj = "ElementoNoEncontrado";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = false;
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
				driver.close();
				throw new AssertionError(errorBuffer.toString());
			}
		}
		try {

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions
			.visibilityOf(paginaPrincipal.MenuPrincipal.linkConsolaServicioMenuAplicaciones(driver)))
			.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click Consola Servicio";
			msj = "ClickConsolaServicio";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(10000);

			// new WebDriverWait(driver, 20, 100)
			// .until(ExpectedConditions
			// .elementToBeClickable(paginaPrincipal.MenuPrincipal.btnMostrarMenuNavegacionConsola(driver)))
			// .sendKeys(Keys.ENTER);

			// driver.findElement(By.xpath("(.//*[normalize-space(text()) and
			// normalize-space(.)='Casos'])[1]/following::lightning-primitive-icon[1]")).click();
			// mensaje = "Flujo : Click Boton Mostrar Menu Navegacion Consola ";
			// msj = "ClickBtnMenuNavegacion";
			// posicionEvidencia = posicionEvidencia + 1;
			// posicionEvidenciaString = Integer.toString(posicionEvidencia);
			// nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			// Thread.sleep(2000);
			// rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura +
			// "_" + msj);
			// Log.info(mensaje);
			// Reporter.log(
			// "<p>" + mensaje + "</p>" + "<img src=" +
			// rutaEvidencia + ">");
			// System.out.println(mensaje);
			//
			// Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			Thread.sleep(3000);

			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H12_14017_Gestion_de_Consola_de_Servicios_Perfil_Ejecutivo_SAC.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Menu de Navegacion Desplegable".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			driver.findElement(By.xpath(
					"(.//*[normalize-space(text()) and normalize-space(.)='Casos'])[1]/following::lightning-primitive-icon[1]"))
					.click();
			mensaje = "Flujo : Click Boton Mostrar Menu Navegacion Consola ";
			msj = "ClickBtnMenuNavegacion";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);
			recorreExcell = 0;
			String nombreLink = "";
			while (recorreExcell == 0) {

				if ("Menu de Navegacion Desplegable".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLink = ExcelUtils.getCellData(1, inicioExcell);
						principalCuenta.MenuPrincipalCuenta.linkARevisarMenuNavegacionDesplegable(driver, nombreLink)
								.getText();
						mensaje = "OK : Link esperado '" + nombreLink + "' Existe en 'Menu de Navegacion Desplegable'";
						msj = nombreLink + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLink + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Link esperado '" + nombreLink
								+ "' NO Existe en 'Menu de Navegacion Desplegable'" + "</p>";
						System.err.println(mensaje);
						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLink + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}
				} else {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 100) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No Encuentra Elemento En Hoja Excell Revise Data de entrada " + "</p>";
					msj = "DatoNoEncontradoEnEntradaDeDatos";
					posicionEvidencia = posicionEvidencia + 1;
					rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
			}

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'Menu de Navegacion Desplegable':" + "</p></b>";
			Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
					Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
					Funciones.mensajesLog(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Funciones.mensajesLog(mensajeResumen);

			if (contadorIncorrectos > 0) {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
						+ " 'Label' " + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			} else {
				mensaje = "OK : No Hay Errores en Revision de Elementos ";
				Funciones.mensajesLog(mensaje);
				ValorEncontrado = true;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>"
						+ "OK: Prueba Correcta ; Existen Todos Los Links En Menu de Navegacion Desplegable" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: Elementos No encontrados en Menu de Navegacion Desplegable"
						+ "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			rutaEvidencia = Funciones.evidencia(msj, driver, nombreCapturaClase, posicionEvidencia);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
