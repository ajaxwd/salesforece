package certificacion_SAC;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class Cert_SAC_13505_Validacion_de_Campos_tabla_de_busqueda_con_perfil_Jefe_SAC_Cuenta_EE {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "Cert_SAC_13505_Validacion_de_Campos_tabla_de_busqueda_con_perfil_Jefe_SAC_Cuenta_EE.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "Cert_SAC_13505_Validacion_de_Campos_tabla_de_busqueda_con_perfil_Jefe_SAC_Cuenta_EE.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		mensaje = "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			int recorreExcell = 0;
			int inicioExcell = 0;
			String numeroDeCaso = "";
			while (recorreExcell == 0) {
				if ("Numero de Caso".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					numeroDeCaso = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Numero de Caso' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String nombreActivo = "";
			while (recorreExcell == 0) {
				if ("Nombre Activo".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					nombreActivo = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Nombre Activo' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String numeroVin = "";
			while (recorreExcell == 0) {
				if ("Numero de VIN".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					numeroVin = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Numero de VIN' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			// recorreExcell = 0;
			// inicioExcell = 0;
			// String local = "";
			// while (recorreExcell == 0) {
			// if ("Local".equals(ExcelUtils.getCellData(0, inicioExcell))) {
			// local = ExcelUtils.getCellData(1, inicioExcell);
			// recorreExcell = 1;
			// break;
			// }
			// inicioExcell = inicioExcell + 1;
			// recorreExcell = 0;
			// if (inicioExcell > 150) {
			// mensaje = "<p style='color:red;'>"
			// + "Error : No encuentra elemento 'Local' en Archivo de Ingreso de datos Favor
			// Revisar"
			// + "</p>";
			// Log.info(mensaje);
			// Reporter.log(mensaje);
			// System.err.println(mensaje);
			// driver.quit();
			// throw new AssertionError();
			// }
			// }

			recorreExcell = 0;
			inicioExcell = 0;
			String patente = "";
			while (recorreExcell == 0) {
				if ("Patente".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					patente = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Patente' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String cuenta = "";
			while (recorreExcell == 0) {
				if ("Cuenta".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					cuenta = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Cuenta' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			recorreExcell = 0;
			inicioExcell = 0;
			String documentoIdentidad = "";
			while (recorreExcell == 0) {
				if ("Documento Identidad".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					documentoIdentidad = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Documento Identidad' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			int cierraLlamado = 0;
			while (cierraLlamado == 0) {
				Thread.sleep(2000);
				try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
						// flujo

					driver.findElement(By.xpath(
							"(//h2[contains(text(),'Llamada de Tel�fono')]//following::button[contains(@title,'Descartar notificaci�n')])[1]"))
							.click();
					// paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

					mensaje = "Flujo : Se Cierra 'Pop Up' LLamado Pendiente  ";
					Log.info(mensaje);
					Reporter.log("<br>" + "<p>" + mensaje + "</p>");
					System.out.println(mensaje);

					cierraLlamado = 0;
				} catch (NoSuchElementException | ScreenshotException e) {
					cierraLlamado = 1;
				}
			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(numeroDeCaso);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Buscar Numero de Caso : " + numeroDeCaso;
			msj = "BuscarNroCaso";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			if (paginaPrincipal.MenuPrincipal.linkNumeroCaso(driver, numeroDeCaso).getText().contains(numeroDeCaso)) {

				mensaje = "Flujo : Coinciden numeros de caso : " + numeroDeCaso;
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

			} else {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No coincide numero de caso : "
						+ numeroDeCaso + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
				driver.quit();
				throw new AssertionError(errorBuffer.toString());
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkInicioPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Inicio ";
			msj = "ClickBtnInicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(numeroVin);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Buscar Numero Vin : " + numeroVin;
			msj = "BuscarNroCaso";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			if (paginaPrincipal.MenuPrincipal.linkActivo(driver, nombreActivo).getText().contains(nombreActivo)) {

				mensaje = "Flujo : Coinciden numero de vin : " + numeroVin + " para activo " + nombreActivo;
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

			} else {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No coincide numero de vin : " + numeroVin
						+ " para activo " + nombreActivo + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
				driver.quit();
				throw new AssertionError(errorBuffer.toString());
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkInicioPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Inicio ";
			msj = "ClickBtnInicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(patente);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Buscar Patente : " + patente;
			msj = "BuscarPatente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			if (paginaPrincipal.MenuPrincipal.linkActivo(driver, nombreActivo).getText().contains(nombreActivo)) {

				mensaje = "Flujo : Coinciden numeros de patente : " + patente + " para activo " + nombreActivo;
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

			} else {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No coincide numero de patente : "
						+ patente + " para activo " + nombreActivo + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
				driver.quit();
				throw new AssertionError(errorBuffer.toString());
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkInicioPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Inicio ";
			msj = "ClickBtnInicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(cuenta + " " + Keys.ENTER);

			Thread.sleep(2000);

			mensaje = "Flujo : Buscar Cuenta : " + cuenta;
			msj = "BuscarCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			if (paginaPrincipal.MenuPrincipal.linkCuenta(driver, cuenta).getText().contains(cuenta)) {

				mensaje = "Flujo : Coinciden Nombres de Cuenta : " + cuenta ;
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

			} else {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No coincide Nombres de Cuenta : " + cuenta + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
				driver.quit();
				throw new AssertionError(errorBuffer.toString());
			}

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkInicioPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Inicio ";
			msj = "ClickBtnInicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(documentoIdentidad);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPaginaPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Buscar Documento Identidad : " + documentoIdentidad;
			msj = "BuscarDocumentoIdentidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			// int contadorDeErroresTotal = 0;
			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			recorreExcell = 0;
			inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "Cert_SAC_13505_Validacion_de_Campos_tabla_de_busqueda_con_perfil_Jefe_SAC_Cuenta_EE.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Label Cuenta".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			recorreExcell = 0;
			nombreLabel = "";
			while (recorreExcell == 0) {

				if ("Label Cuenta".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLabel = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLabel));
						principalCuenta.MenuPrincipalCuenta.linkARevisarRelacionado(driver, nombreLabel).getText();
						mensaje = "OK : Label esperado '" + nombreLabel + "' Existe en 'Cuenta'";
						msj = nombreLabel + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLabel + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLabel
								+ "' NO Existe en 'Patente'" + "</p>";
						System.err.println(mensaje);

						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}

				} else {

					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			Thread.sleep(2000);

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'LABEL CUENTA':" + "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de colunas coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de columnas NO coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.err.println(mensajeResumen);

			if (contadorIncorrectos > 0) {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
						+ " 'Label' " + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			Thread.sleep(5000);

			paginaPrincipal.MenuPrincipal.seleccionCuenta(driver, cuenta).click();
			mensaje = "Flujo : Seleccionar Cuenta : " + cuenta;
			msj = "SeleccionCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click link Relacionado Cuenta ";
			msj = "ClickLinkRelacionados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(10000);

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By
					.xpath("(//span[contains(text(),'Relacionado')]//following::span[contains(text(),'Casos')])[1]")));
			Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-200)", "");

			Thread.sleep(2000);

			mensaje = "Flujo : Revision Numero de Caso ";
			msj = "LinkNumeroCaso";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			if (paginaPrincipal.MenuPrincipal.linkRevisionNumeroCaso(driver, numeroDeCaso).getText()
					.equals(numeroDeCaso)) {

				paginaPrincipal.MenuPrincipal.linkRevisionNumeroCaso(driver, numeroDeCaso).click();
				mensaje = "Flujo : Click Numero de Caso ";
				msj = "ClickLinkNumeroCaso";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				Thread.sleep(5000);

				paginaPrincipal.MenuPrincipal.linkActividad(driver).click();
				mensaje = "Flujo : Click Actividad ";
				msj = "ClickActividad";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				nombreLabel = "";
				correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
				erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
				contadorCorrectos = 0;
				contadorIncorrectos = 0;

				Thread.sleep(3000);

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By
						.xpath("(//span[contains(text(),'Relacionado')]//following::span[contains(text(),'Informaci�n del caso')])[1]")));
				Thread.sleep(2000);
				((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-200)", "");

				Thread.sleep(3000);

				recorreExcell = 0;
				inicioExcell = 0;
				utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
				utilidades.DatosInicialesYDrivers.File_TestData = "Cert_SAC_13505_Validacion_de_Campos_tabla_de_busqueda_con_perfil_Jefe_SAC_Cuenta_EE.xlsx";
				ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
						"Hoja1");
				while (recorreExcell == 0) {
					if ("Label Informacion del caso".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
				}

				recorreExcell = 0;
				nombreLabel = "";
				while (recorreExcell == 0) {

					if ("Label Informacion del caso".equals(ExcelUtils.getCellData(0, inicioExcell))) {
						try {
							nombreLabel = ExcelUtils.getCellData(1, inicioExcell);
							((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
									principalCuenta.MenuPrincipalCuenta.labelARevisarRelacionado(driver, nombreLabel));
							principalCuenta.MenuPrincipalCuenta.labelARevisarRelacionado(driver, nombreLabel).getText();
							mensaje = "OK : Label esperado '" + nombreLabel + "' Existe en 'Informacion del caso'";
							msj = nombreLabel + "Existe";
							msj = msj.replace(" ", "");
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							Thread.sleep(2000);
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);

							contadorCorrectos = contadorCorrectos + 1;
							correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLabel + "</p>";
							System.out.println(correctos);
							ValorEncontrado = true;

						} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

							mensaje = "<p style='color:red;'>" + "Error : Label esperado '" + nombreLabel
									+ "' NO Existe en 'Informacion del caso'" + "</p>";
							System.err.println(mensaje);

							erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
							contadorIncorrectos = contadorIncorrectos + 1;
						}

					} else {

						recorreExcell = 1;
						break;
					}
					inicioExcell = inicioExcell + 1;
					recorreExcell = 0;
				}

				Thread.sleep(2000);

				mensajeResumen = "<p><b>" + "RESUMEN REVISION 'LABEL INFORMACION DEL CASO':" + "</p></b>";
				Log.info(mensajeResumen);
				Reporter.log(mensajeResumen);
				System.out.println(mensajeResumen);

				mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de colunas coinciden con los esperados"
						+ "</p></b>";
				Log.info(mensajeResumen);
				Reporter.log(mensajeResumen);
				System.out.println(mensajeResumen);

				mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
				Log.info(mensajeResumen);
				Reporter.log(mensajeResumen);
				System.out.println(mensajeResumen);

				mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de columnas NO coinciden con los esperados"
						+ "</p></b>";
				Log.info(mensajeResumen);
				Reporter.log(mensajeResumen);
				System.out.println(mensajeResumen);

				mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
				Log.info(mensajeResumen);
				Reporter.log(mensajeResumen);
				System.err.println(mensajeResumen);

				if (contadorIncorrectos > 0) {

					mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
							+ " 'Label' " + "</p>";
					msj = "PruebaErronea";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					errorBuffer.append("\n" + mensaje + "\n");
					ValorEncontrado = false;
				} else {
					ValorEncontrado = true;
				}

			} else {

				mensaje = "<p style='color:red;'>" + "Error : No Coincide Numero de Caso" + "</p>";
				msj = "SeEncuentranErrores";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				driver.quit();
				throw new AssertionError(errorBuffer.toString());
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Se revisan Todos Los Labels  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; No se encontraron Labels" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}


}
