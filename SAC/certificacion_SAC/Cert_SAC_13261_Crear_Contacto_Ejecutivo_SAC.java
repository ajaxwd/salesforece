package certificacion_SAC;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class Cert_SAC_13261_Crear_Contacto_Ejecutivo_SAC {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "Cert_SAC_13261_Crear_Contacto_Ejecutivo_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "Cert_SAC_13261_Crear_Contacto_Ejecutivo_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);

		mensaje = "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario;
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnContactos(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Contactos ";
			msj = "ClickBtnContactos";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearContactos(driver)))
					.click();
			mensaje = "Flujo : Click Boton Crear Contactos ";
			msj = "ClickBtnCrearContactos";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";

			mensaje = "Flujo : LLenado de datos ";
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			int recorreExcell = 0;
			int inicioExcell = 0;
			String cuentaPrincipal = "";
			while (recorreExcell == 0) {
				if ("Cuenta Principal".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					cuentaPrincipal = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Cuenta Principal' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			Thread.sleep(3000);

			String[] arregloCuentaPrincipal = new String[cuentaPrincipal.length()];
			for (int i = 0; i < cuentaPrincipal.length(); i++) {
				arregloCuentaPrincipal[i] = Character.toString(cuentaPrincipal.charAt(i));
			}

			for (int i = 0; i < cuentaPrincipal.length(); i++) {

				crearCuenta.Personal.inputCuentaPrincipal(driver).sendKeys(arregloCuentaPrincipal[i]);
				Thread.sleep(300);

			}
			mensaje = "Flujo : Ingreso Campo Cuenta Principal : " + cuentaPrincipal;
			Log.info(mensaje);
			Reporter.log("<br>" + mensaje);
			System.out.println(mensaje);

			Thread.sleep(2000);
			crearCuenta.Personal.cuentaPrincipalSeleccionParametro(driver, cuentaPrincipal).click();
			mensaje = "Flujo : Seleccion de Cuenta Principal  : " + cuentaPrincipal;
			msj = "SeleccionCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			recorreExcell = 0;
			inicioExcell = 0;
			String elementoEstado = "";
			while (recorreExcell == 0) {
				if ("Estado".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					elementoEstado = ExcelUtils.getCellData(1, inicioExcell);
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
				if (inicioExcell > 150) {
					mensaje = "<p style='color:red;'>"
							+ "Error : No encuentra elemento 'Estado' en Archivo de Ingreso de datos Favor Revisar"
							+ "</p>";
					Log.info(mensaje);
					Reporter.log(mensaje);
					System.err.println(mensaje);
					driver.quit();
					throw new AssertionError();
				}
			}

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectEstado(driver))).click();
			mensaje = "Flujo : Click Select Estado ";
			msj = "ClickSelectEstado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.elementoEstado(driver, elementoEstado)))
					.click();
			mensaje = "Flujo : Click Elemento Estado ";
			msj = "ClickElementoEstado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectTratamiento(driver))).click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.rolSR(driver))).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNombre(driver)))
					.sendKeys(nombreAleatorio);
			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoPaterno(driver)))
					.sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPaterno";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver))).click();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
					.sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMaterno";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String cargo = "Tester";
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputCargo(driver))).click();
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputCargo(driver))).sendKeys(cargo);
			mensaje = "Flujo : Ingreso de Cargo : " + cargo;
			msj = "IngresoCargo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			crearCuenta.Personal.selectPreferenciaComunicacion(driver).click();
			Thread.sleep(2000);
			crearCuenta.Personal.viaTelefono(driver).click();
			mensaje = "Flujo : Seleccion de Preferencia de Comunicacion : " + "Via Telefono";
			msj = "SeleccionPrefComunicacion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numMovil = numMovil + azar;
			}
			crearCuenta.Personal.inputNumeroMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			correo = correo + correoAzar + "@derco.cl";
			correo = correo.toLowerCase();
			crearCuenta.Personal.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			
			Thread.sleep(2000);
			
			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearCuenta.Personal.inputNumTelefono(driver).sendKeys("2" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngressoTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);
			
			crearCuenta.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String textoCuadroVerdePequeno = new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerdePequeno(driver)))
					.getText();

			if (textoCuadroVerdePequeno.contains("Se cre�")) {

				mensaje = "Flujo : Se cre� Contacto";
				msj = "CreaContacto";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);
				ValorEncontrado = true;

				Thread.sleep(3000);

			} else {

				mensaje = "<p style='color:red;'>" + "Error : No Aparece Mensaje de Creacion de Cuenta Exitoso "
						+ "</p>";
				msj = "ErrorIngresoASap";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Contacto Exitoso  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Contacto Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}

		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.quit();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}


}
