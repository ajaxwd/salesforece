package sacSprint1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H08_13255_Actualizar_cuenta_EE_Perfil_Jefe_SAC {
	
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H08_13255_Actualizar_cuenta_EE_Perfil_Jefe_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();

		mensaje = "<p>" + "Test en Ejecucion : " + sNombreTest + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H08_13255_Actualizar_cuenta_EE_Perfil_Jefe_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		utilidades.DatosInicialesYDrivers.Browser = ExcelUtils.getCellData(1, 0);
		utilidades.DatosInicialesYDrivers.URL = ExcelUtils.getCellData(1, 1);
		utilidades.DatosInicialesYDrivers.Usuario = ExcelUtils.getCellData(1, 2);
		utilidades.DatosInicialesYDrivers.Password = ExcelUtils.getCellData(1, 3);
		String nombreCuenta = ExcelUtils.getCellData(1, 4);
		String rutCuenta = ExcelUtils.getCellData(1, 5);

		mensaje = "<p>" + "Usuario test en Ejecucion : " + utilidades.DatosInicialesYDrivers.Usuario + "</p>";
		Log.info(mensaje);
		Reporter.log("<p>" + mensaje + "</p>");
		System.out.println(mensaje);

		// utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution +
		// "CARPETA DE SET\\";
		// utilidades.DatosInicialesYDrivers.File_TestData = "******.xlsx";
		// ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData +
		// DatosInicialesYDrivers.File_TestData, "Hoja1");

		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			driver = new ChromeDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			// mensaje = "<br>" + mensaje + "URL "+ utilidades.DatosInicialesYDrivers.URL;
			System.out.println(mensaje);
			Reporter.log(mensaje);
			Log.info(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */

		// llamadas a las clases genericas que forman un flujo de pruebas:
		//

		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase);

		Thread.sleep(5000);

		try {

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnMenuAplicaciones(driver)))
					.click();

			mensaje = "Flujo : Click Menu Aplicaciones ";
			msj = "ClickMenuApp";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions
							.visibilityOf(paginaPrincipal.MenuPrincipal.linkServicioMenuAplicaciones(driver)))
					.sendKeys(Keys.ENTER);

			mensaje = "Flujo : Click Link Servicio";
			msj = "ClickServicio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
					.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de
					// flujo

				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();

				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

			} catch (NoSuchElementException | ScreenshotException e) {

			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(rutCuenta);
			mensaje = "Flujo : Se ingresa elemento a buscar " + rutCuenta;
			msj = "InputElementoBusqueda";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.inputBuscarPrincipal(driver)))
					.sendKeys(Keys.ENTER);

			Thread.sleep(5000);

			int swTiempoEspera = 0;
			int contarTiempoEspera = 0;

			int sw = 0;
			int contarTiempo = 0;
			while (sw == 0) {
				if (contarTiempo > 15) {
					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
							+ nombreCuenta + "</p>";
					msj = "ElementoNoEncontrado";
					posicionEvidencia = posicionEvidencia + 1;
					posicionEvidenciaString = Integer.toString(posicionEvidencia);
					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
					Thread.sleep(2000);
					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
					Log.info(mensaje);
					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
							+ rutaEvidencia + ">");
					System.err.println(mensaje);
					ValorEncontrado = false;
					errorBuffer.append("\n" + mensaje + "\n");
					driver.close();
					throw new AssertionError(errorBuffer.toString());
				}
				try {
					Thread.sleep(500);
					new WebDriverWait(driver, 40, 100)
							.until(ExpectedConditions.visibilityOf(
									paginaPrincipal.MenuPrincipal.linkRegistroResultadoBusqueda(driver, nombreCuenta)))
							.click();
					sw = 1;

				} catch (ElementNotVisibleException | NoSuchElementException | ScreenshotException e) {
					sw = 0;
					contarTiempo = contarTiempo + 1;

				}
			}

			mensaje = "Flujo : Click Elemento Encontrado " + nombreCuenta;
			msj = "ClickElementoencontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(5000);

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.btnModificarCuenta(driver)));
			crearCuenta.Personal.btnModificarCuenta(driver).click();
			mensaje = "Flujo : Click Modificar Cuentas ";
			msj = "ClickBtnModificarCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			
			Thread.sleep(5000);

			try {

				mensaje = "Flujo : Se verifica si es posible modificar RUT ";
				msj = "ModificarRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

				new WebDriverWait(driver, 10, 100)
						.until(ExpectedConditions.elementToBeClickable(crearCuenta.Personal.inputRutModificar(driver)))
						.sendKeys("11111111");

				mensaje = "<p style='color:red;'>" + "Error : Se Modifica Rut " + "</p>";
				msj = "ErrorSeModificaRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				ValorEncontrado = false;
				driver.close();
				throw new AssertionError(errorBuffer.toString());

			} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

				mensaje = "OK : No es posible modificar RUT ";
				msj = "NoModificaRut";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(5000);

			String nombreLabel = "";
			String correctos = "<p><b>" + "Lista de Titulos encontrados :" + "</p></b>";
			String erroneos = "<p><b>" + "Lista de Titulos NO encontrados :" + "</p></b>";
			int contadorCorrectos = 0;
			int contadorIncorrectos = 0;

			int recorreExcell = 0;
			int inicioExcell = 0;
			utilidades.DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
			utilidades.DatosInicialesYDrivers.File_TestData = "H08_13255_Actualizar_cuenta_EE_Perfil_Jefe_SAC.xlsx";
			ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData,
					"Hoja1");
			while (recorreExcell == 0) {
				if ("Label Modificacion".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			recorreExcell = 0;
			nombreLabel = "";
			while (recorreExcell == 0) {

				Thread.sleep(2000);

				if ("Label Modificacion".equals(ExcelUtils.getCellData(0, inicioExcell))) {
					try {
						nombreLabel = ExcelUtils.getCellData(1, inicioExcell);
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								principalCuenta.MenuPrincipalCuenta.labelRevisionModificar(driver, nombreLabel));
						principalCuenta.MenuPrincipalCuenta.labelRevisionModificar(driver, nombreLabel).getText();
						mensaje = "OK : Label esperado '" + nombreLabel + "' Existe en 'Modificacion'";
						msj = nombreLabel + "Existe";
						msj = msj.replace(" ", "");
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						contadorCorrectos = contadorCorrectos + 1;
						correctos = correctos + "<p style='color:blue;'>" + "- " + nombreLabel + "</p>";
						System.out.println(correctos);
						ValorEncontrado = true;

					} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {

						mensaje = "<p style='color:red;'>" + "Error : Titulo esperado '" + nombreLabel
								+ "' NO Existe en 'Modificacion'" + "</p>";
						System.err.println(mensaje);

						erroneos = erroneos + "<p style='color:red;'>" + "- " + nombreLabel + "</p>";
						contadorIncorrectos = contadorIncorrectos + 1;
					}

				} else {

					recorreExcell = 1;
					break;
				}
				inicioExcell = inicioExcell + 1;
				recorreExcell = 0;
			}

			String mensajeResumen = "<p><b>" + "RESUMEN REVISION 'CAMPOS MODIFICACION':" + "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorCorrectos + " Titulos de campo coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:blue;'>" + correctos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p><b>" + contadorIncorrectos + " Titulos de campo NO coinciden con los esperados"
					+ "</p></b>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.out.println(mensajeResumen);

			mensajeResumen = "<p style='color:red;'>" + erroneos + "</p>";
			Log.info(mensajeResumen);
			Reporter.log(mensajeResumen);
			System.err.println(mensajeResumen);

			if (contadorIncorrectos > 0) {

				mensaje = "<p style='color:red;'>" + "Error: Fin de Ejecucion No Encuentra " + contadorIncorrectos
						+ " 'Label' " + "</p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			} else {
				mensaje = "OK : No Hay Errores en Revision de Elementos ";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);


			String nombreAntiguo = crearCuenta.Empresa.inputNombreEmpresaModificar(driver).getAttribute("value");
			mensaje = "Flujo : Nombre Antiguo : " + nombreAntiguo;
			msj = "NombreAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);
			String empresaAleatorio = nombreAntiguo;

			while (empresaAleatorio.equals(nombreAntiguo)) {
				empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
				empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio()
						+ " Cia Ltda";
			}

			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNombreEmpresaModificar(driver)))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"), empresaAleatorio);

			mensaje = "Flujo : Ingreso de Razon Social Aleatorio : " + empresaAleatorio;
			msj = "IngresoRazonSocial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String giroAntiguo = crearCuenta.Empresa.inputGiroModificar(driver).getAttribute("value");
			mensaje = "Flujo : Giro Antiguo : " + giroAntiguo;
			msj = "GiroAntiguo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			if (giroAntiguo.equals("Testing QA")) {

				crearCuenta.Empresa.inputGiroModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"Testing QA Empresa");

				mensaje = "Flujo : Ingreso Giro : " + "Testing QA Empresa";
				msj = "IngresoGiro";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				crearCuenta.Empresa.inputGiroModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"), "Testing QA");

				mensaje = "Flujo : Ingreso Giro : " + "Testing QA";
				msj = "IngresoGiro";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String nacionalidadAntigua = crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Nacionalidad Antigua : " + nacionalidadAntigua;
			msj = "NacionalidadAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String nacionalidadModificada = "";
			if (nacionalidadAntigua.equals("Chile")) {

				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).click();
				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("ARGEN");
				Thread.sleep(1000);
				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("TINA");
				mensaje = "Flujo : Ingreso Campo Pais : " + "ARGENTINA";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				nacionalidadModificada = "Argentina";
				crearCuenta.Empresa.selectElementoNacionalidadModificarCuenta(driver, nacionalidadModificada).click();
				mensaje = "Flujo : Seleccion de Pais  : " + "ARGENTINA";
				msj = "SeleccionPais";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				nacionalidadModificada = "Chile";
				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).click();
				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("CHI");
				Thread.sleep(1000);
				crearCuenta.Empresa.selectNacionalidadModificarCuenta(driver).sendKeys("LE");
				mensaje = "Flujo : Ingreso Campo Pais : " + "CHILE";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);

				crearCuenta.Empresa.selectElementoNacionalidadModificarCuenta(driver, nacionalidadModificada).click();
				mensaje = "Flujo : Seleccion de Pais  : " + "CHILE";
				msj = "SeleccionPais";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String correoAntiguo = crearCuenta.Empresa.inputCorreoModificar(driver).getAttribute("value");
			mensaje = "Flujo : correo Antiguo : " + correoAntiguo;
			msj = "CorreoAntiguo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String azar;
			String correo = "";
			String correoAzar = "";
			String correoAleatorio = correoAntiguo;

			while (correoAleatorio.equals(correoAntiguo)) {
				correoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
				correoAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio()
						+ " Cia Ltda";

				// Cadena de caracteres aleatoria Para Nombre
				String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
				// Cadena de caracteres aleatoria Para Apellido Paterno
				String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();

				correo = nombreAleatorio + "." + apellidoPaternoAleatorio;

				azar = "";
				// Cadena de numeros aleatoria Para Correo
				for (int i = 0; i < 4; i++) {
					azar = utilidades.Funciones.numeroAleatorio(1, 8);
					correoAzar = correoAzar + azar;
				}

				empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
				correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
				correo = correo.toLowerCase();
				correo = correo.replace(" ", "");

			}

			crearCuenta.Empresa.inputCorreoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"), correo);

			mensaje = "Flujo : Ingreso Correo  al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String telefonoAntiguo = crearCuenta.Empresa.inputTelefonoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Telefono  Antiguo : " + telefonoAntiguo;
			msj = "TelefonoAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 8; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}
			crearCuenta.Empresa.inputTelefonoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					"2" + numTelefono);

			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngressoTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();",
					crearCuenta.Empresa.inputRegionDireccionModificar(driver));

			String regionAntiguo = crearCuenta.Empresa.inputRegionDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Region Antiguo : " + regionAntiguo;
			msj = "RegionAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String regionModificado = "";
			if (regionAntiguo.equals("RM - Santiago")) {

				crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("V - VALP");
				Thread.sleep(1000);
				crearCuenta.Personal.inputRegionDireccionModificar(driver).sendKeys("ARAISO");
				mensaje = "Flujo : Ingreso Campo Region : " + "V - Valparaiso";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

				Thread.sleep(2000);
				regionModificado = "V - Valparaiso";
				crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver, regionModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "V - Valparaiso";
				msj = "SeleccionRegion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				regionModificado = "RM - Santiago";
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).click();
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("RM - San");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputRegionDireccionModificar(driver).sendKeys("tiago");
				mensaje = "Flujo : Ingreso Campo Region : " + "RM - Santiago";
				Log.info(mensaje);
				Reporter.log("<br>" + mensaje);
				System.out.println(mensaje);

				Thread.sleep(2000);

				crearCuenta.Empresa.selectElementoRegionModificarCuenta(driver, regionModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "RM - Santiago";
				msj = "SeleccionRegion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String provinciaAntiguo = crearCuenta.Empresa.inputProvinciaDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Privincia Antiguo : " + regionAntiguo;
			msj = "ProvinciaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String provinciaModificado = "";
			if (provinciaAntiguo.equals("Santiago")) {

				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Valpa");
				Thread.sleep(1000);
				crearCuenta.Personal.inputProvinciaDireccionModificar(driver).sendKeys("ra�so");
				mensaje = "Flujo : Ingreso Campo Provincia : " + "Valpara�so";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				provinciaModificado = "Valpara�so";
				crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver, provinciaModificado).click();
				mensaje = "Flujo : Seleccion de Provincia  : " + "Valpara�so";
				msj = "SeleccionProvincia";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				provinciaModificado = "Santiago";
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("Sant");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputProvinciaDireccionModificar(driver).sendKeys("iago");
				mensaje = "Flujo : Ingreso Campo Provincia : " + "Santiago";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);

				crearCuenta.Empresa.selectElementoProvinciaModificarCuenta(driver, provinciaModificado).click();
				mensaje = "Flujo : Seleccion de Provincia  : " + "Santiago";
				msj = "SeleccionProvincia";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String comunaAntiguo = crearCuenta.Empresa.inputComunaDireccionModificar(driver)
					.getAttribute("placeholder");
			mensaje = "Flujo : Comuna Antiguo : " + comunaAntiguo;
			msj = "ComunaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(4000);

			String comunaModificado = "";
			if (comunaAntiguo.equals("Santiago")) {

				crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("Valpar");
				Thread.sleep(1000);
				crearCuenta.Personal.inputComunaDireccionModificar(driver).sendKeys("a�so");
				mensaje = "Flujo : Ingreso Campo Comuna : " + "Valpara�so";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);

				Thread.sleep(2000);
				comunaModificado = "Valpara�so";
				crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver, comunaModificado).click();
				mensaje = "Flujo : Seleccion de Region  : " + "Valpara�so";
				msj = "SeleccionComuna";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				comunaAntiguo = "Santiago";
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).click();
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("San");
				Thread.sleep(1000);
				crearCuenta.Empresa.inputComunaDireccionModificar(driver).sendKeys("tiago");
				mensaje = "Flujo : Ingreso Campo Comuna : " + "Santiago";
				Log.info(mensaje);
				Reporter.log("<p>" + mensaje + "</p>");
				System.out.println(mensaje);
				comunaModificado = "Santiago";
				Thread.sleep(2000);

				crearCuenta.Empresa.selectElementoComunaModificarCuenta(driver, comunaModificado).click();
				mensaje = "Flujo : Seleccion de Comuna  : " + "Santiago";
				msj = "SeleccionComuna";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			String calleAntiguo = crearCuenta.Empresa.inputCalleModificar(driver).getAttribute("value");
			mensaje = "Flujo : Calle Antiguo : " + calleAntiguo;
			msj = "CalleAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			String calleAleatorio = calleAntiguo;
			while (calleAleatorio.equals(calleAntiguo)) {
				// Cadena de caracteres aleatoria Para calle
				calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();

			}
			new WebDriverWait(driver, 20, 100)
					.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputCalleModificar(driver)))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"), calleAleatorio);

			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
			msj = "SeleccionCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numeroCalleAntiguo = crearCuenta.Empresa.inputNumeroCalleModificar(driver).getAttribute("value");
			mensaje = "Flujo : Numero Calle Antiguo : " + numeroCalleAntiguo;
			msj = "NumeroCalleAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numDireccionAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numDireccionAzar = numDireccionAzar + azar;
			}
			crearCuenta.Empresa.inputNumeroCalleModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					numDireccionAzar);

			mensaje = "Flujo : Ingreso Numero Direccion Azar : " + numDireccionAzar;
			msj = "IngresoNumeroDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String numeroDeptoAntiguo = crearCuenta.Empresa.inputDptoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Numero Oficina Antiguo : " + numeroDeptoAntiguo;
			msj = "NumeroOficinaAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String deptoCasaOficinaAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Ofocina
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
			}
			crearCuenta.Empresa.inputDptoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
					"Oficina " + deptoCasaOficinaAzar);

			mensaje = "Flujo : Ingreso Oficina Azar : " + "Oficina " + deptoCasaOficinaAzar;
			msj = "DetallesDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			String complementoAntiguo = crearCuenta.Empresa.inputComplementoModificar(driver).getAttribute("value");
			mensaje = "Flujo : Complemento Antiguo : " + complementoAntiguo;
			msj = "ComplementoAModificar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.out.println(mensaje);

			Thread.sleep(2000);

			if (complementoAntiguo.equals("inmueble interior")) {

				crearCuenta.Empresa.inputComplementoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"inmueble exterior");

				mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble exterior";
				msj = "ComplementoDireccion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} else {

				crearCuenta.Empresa.inputComplementoModificar(driver).sendKeys(Keys.chord(Keys.CONTROL, "a"),
						"inmueble interior");

				mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
				msj = "ComplementoDireccion";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			}

			Thread.sleep(2000);

			WebElement grabarModificar = driver.findElement(By.xpath("//button[contains(text(),'Guardar')]"));
			grabarModificar.click();

//			sw = 0;
//			contarTiempo = 0;
//			
			String textoCuadroVerdeDos = "";
//			while (sw == 0) {
//				if (contarTiempo > 30) {
//					mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
//					msj = "ElementoNoEncontrado";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					Thread.sleep(2000);
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.err.println(mensaje);
//					ValorEncontrado = false;
//					errorBuffer.append("\n" + mensaje + "\n");
//					driver.close();
//					throw new AssertionError(errorBuffer.toString());
//				}
//				try {
//					textoCuadroVerde = new WebDriverWait(driver, 20, 100)
//							.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerde(driver)))
//							.getText();
//					sw = 1;
//
//				} catch (NoSuchElementException | ScreenshotException e) {
//					sw = 0;
//					contarTiempo = contarTiempo + 1;
//				}
//			}

			mensaje = "Flujo : Se revisa Edicion correcta de cuenta Empresa";
			Log.info(mensaje);
			Reporter.log("<p>" + mensaje + "</p>");
			System.out.println(mensaje);

//			if (textoCuadroVerde.equals("Registro guardado en SAP.")) {
//
//				Thread.sleep(2000);
//
//				mensaje = "Ok :" + textoCuadroVerde;
//				msj = "OkguardadoASap";
//				posicionEvidencia = posicionEvidencia + 1;
//				posicionEvidenciaString = Integer.toString(posicionEvidencia);
//				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//				Log.info(mensaje);
//				Reporter.log(
//						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
//				System.out.println(mensaje);

				Thread.sleep(4000);

				swTiempoEspera = 0;
				contarTiempoEspera = 0;
				while (swTiempoEspera == 0) {

					try {
						textoCuadroVerdeDos = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
						swTiempoEspera = 1;
					} catch (TimeoutException | NoSuchElementException e) {
						contarTiempoEspera = contarTiempoEspera + 1;
						swTiempoEspera = 0;
					}

					if (contarTiempoEspera > 30) {
						mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible "
								+ "</p>";
						msj = "ElementoNoEncontrado";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.err.println(mensaje);
						ValorEncontrado = false;
						errorBuffer.append("\n" + mensaje + "\n");
						driver.close();
						throw new AssertionError(errorBuffer.toString());
					}

					if (textoCuadroVerdeDos.contains("Guardado")) {

						mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" + "<p>";
						msj = "GuardadoSalesForce";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
								+ rutaEvidencia + ">");
						System.out.println(mensaje);

						Thread.sleep(5000);

						principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver).click();
						
//						new WebDriverWait(driver, 20, 100)
//								.until(ExpectedConditions
//										.elementToBeClickable(principalCuenta.MenuPrincipalCuenta.linkRelacionado(driver)))
//								.sendKeys(Keys.ENTER);

						mensaje = "Flujo : Click link Relacionado Cuenta ";
						msj = "ClickLinkRelacionados";
						posicionEvidencia = posicionEvidencia + 1;
						posicionEvidenciaString = Integer.toString(posicionEvidencia);
						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
						Thread.sleep(2000);
						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
						Log.info(mensaje);
						Reporter.log(
								"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
						System.out.println(mensaje);
						
						Thread.sleep(3000);
						
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								driver.findElement(By.xpath("//span[@title='Casos']")));
						
						
						String txtNombreAntiguo = principalCuenta.MenuPrincipalCuenta.txtNombreAntiguoEmpresa(driver).getText();
						String txtNombreNuevo = principalCuenta.MenuPrincipalCuenta.txtNombreNuevoEmpresa(driver).getText();
						
						if (txtNombreAntiguo.equals(nombreAntiguo) || txtNombreNuevo.equals(empresaAleatorio)) {
							
							mensaje = "<p style='color:blue;'>" + "OK: Coinciden Cambios en 'Historial'" + "<p>";
							msj = "CambiosEnHistorial";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.out.println(mensaje);
							ValorEncontrado = true;
						}
						
						} else {

							mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force " + "</p>";
							msj = "ErrorIngresoASap";
							posicionEvidencia = posicionEvidencia + 1;
							posicionEvidenciaString = Integer.toString(posicionEvidencia);
							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
							Log.info(mensaje);
							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
									+ rutaEvidencia + ">");
							System.err.println(mensaje);
							errorBuffer.append("\n" + mensaje + "\n");
							ValorEncontrado = false;
						}
					}
				}
//			} else {
//				if (textoCuadroVerde.equals("Business Partner no existe, no se realizaran cambios en SAP")) {
//
//					Thread.sleep(3000);
//
//					mensaje = "Warning : " + textoCuadroVerde;
//					msj = "WarningGuardadoASap";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p style='color:green; text-stroke: 1px black'>" + mensaje + "</p>"
//							+ "<img src=" + rutaEvidencia + ">");
//					System.out.println(mensaje);
//
//					swTiempoEspera = 0;
//					contarTiempoEspera = 0;
//					while (swTiempoEspera == 0) {
//
//						try {
//
//							textoCuadroVerdeDos = MenuPrincipal.textoNotificacionesVerdeDos(driver).getText();
//							swTiempoEspera = 1;
//
//						} catch (TimeoutException | NoSuchElementException e) {
//							contarTiempoEspera = contarTiempoEspera + 1;
//							swTiempoEspera = 0;
//						}
//
//						if (contarTiempoEspera > 15) {
//							mensaje = "<p style='color:red;'>"
//									+ "Error : No Encuentra Elemento o No esta Visible En Sales Force " + "</p>";
//							msj = "ElementoNoEncontrado";
//							posicionEvidencia = posicionEvidencia + 1;
//							posicionEvidenciaString = Integer.toString(posicionEvidencia);
//							nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//							Thread.sleep(2000);
//							rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//							Log.info(mensaje);
//							Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//									+ rutaEvidencia + ">");
//							System.err.println(mensaje);
//							ValorEncontrado = false;
//							driver.close();
//							errorBuffer.append("\n" + mensaje + "\n");
//							throw new AssertionError(errorBuffer.toString());
//						}
//					}
//
//					if (textoCuadroVerdeDos.contains("Registro guardado en Salesforce.")) {
//
//						mensaje = "<p style='color:blue;'>" + "OK: guardado en 'Sales Force'" + "<p>";
//						msj = "GuardadoSalesForce";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.out.println(mensaje);
//
//						ValorEncontrado = true;
//
//					} else {
//
//						mensaje = "<p style='color:red;'>" + "Error : No Guarda Datos en Sales Force " + "</p>";
//						msj = "ErrorIngresoASap";
//						posicionEvidencia = posicionEvidencia + 1;
//						posicionEvidenciaString = Integer.toString(posicionEvidencia);
//						nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//						rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//						Log.info(mensaje);
//						Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//								+ rutaEvidencia + ">");
//						System.err.println(mensaje);
//						errorBuffer.append("\n" + mensaje + "\n");
//						ValorEncontrado = false;
//
//					}
//
//				} else {
//					mensaje = "<p style='color:red;'>" + "Error : No Interactua en SAP " + "</p>";
//					msj = "ErrorIngresoASap";
//					posicionEvidencia = posicionEvidencia + 1;
//					posicionEvidenciaString = Integer.toString(posicionEvidencia);
//					nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
//					rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
//					Log.info(mensaje);
//					Reporter.log("<p>" + mensaje + "</p>" + "<img src="
//							+ rutaEvidencia + ">");
//					System.err.println(mensaje);
//					errorBuffer.append("\n" + mensaje + "\n");
//					ValorEncontrado = false;
//				}
//			}

			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Edita Cuenta" + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.out.println(mensaje);

			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "Error: No Edita Cuenta" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Log.info(mensaje);
				Reporter.log(
						"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
				System.err.println(mensaje);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");

			}		
		
		
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}

		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
					+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Log.info(mensaje);
			Reporter.log(
					"<p>" + mensaje + "</p>" + "<img src=" + rutaEvidencia + ">");
			System.err.println(mensaje);
			driver.close();
			throw new AssertionError(errorBuffer.toString());

		}

		driver.quit();
	}


}
