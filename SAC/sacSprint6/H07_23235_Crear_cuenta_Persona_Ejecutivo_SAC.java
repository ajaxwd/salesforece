package sacSprint6;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import crearCuenta.Personal;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H07_23235_Crear_cuenta_Persona_Ejecutivo_SAC {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H07_23235_Crear_cuenta_Persona_Ejecutivo_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H07_23235_Crear_cuenta_Persona_Ejecutivo_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;

		mensaje = "Usuario test en Ejecucion : " + Usuario;
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String tipoDocumento = "";
	
			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			tipoDocumento = Funciones.getExcel(driver, "Tipo de Documento", posicionExcel);

			 //Seleccionar menú de navegación "Contactos"
			 paginaPrincipal.MenuPrincipal.menuNavegacion(driver).click();
			 mensaje = "Flujo : Click Icono Menú de navegación ";
			 msj = "ClickBtnContactos";
			 posicionEvidencia = posicionEvidencia + 1;
			 posicionEvidenciaString = Integer.toString(posicionEvidencia);
			 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			 Thread.sleep(2000);
			 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
 
			 Thread.sleep(3000);
 
			 String opcionMenu = "Cuentas";
			 paginaPrincipal.MenuPrincipal.seleccionarMenu(driver, opcionMenu).click();
			 mensaje = "Flujo : Se selecciona Menú de navegación " + opcionMenu;
			 msj = "ClickBtnContactos";
			 posicionEvidencia = posicionEvidencia + 1;
			 posicionEvidenciaString = Integer.toString(posicionEvidencia);
			 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			 Thread.sleep(2000);
			 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			 
			 Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearCuentas(driver)))
			.click();
			mensaje = "Flujo : Click Boton Crear Cuentas ";
			msj = "ClickBtnCrearCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.rdCuentasPersonales(driver)))
			.click();
			paginaPrincipal.MenuPrincipal.rdCuentasPersonales(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Cuenta Persona";
			msj = "ClickBtnRadioCtaEmpresa";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnSiguienteCrearCuenta(driver)))
			.click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			//Seleccionar tipo de documento
			crearCuenta.Personal.inputTipoDocumento(driver).click();
			mensaje = "Flujo : Seleccionar Tipo de Documento";
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);

			crearCuenta.Personal.selectTipoDocumento(driver, tipoDocumento).click();
			mensaje = "Flujo : Tipo de documento seleccionado " + tipoDocumento;
			msj = "inputDocumento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			//Cadena de caracteres aleatoria Para Rut
			String rut = utilidades.Funciones.numeroAleatorio(111111, 999988888);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputDocumentoIdentidad(driver)))
			.sendKeys(rut);
			mensaje = "Flujo : Ingreso de Rut: " + rut;
			msj = "IngresoRut";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectTratamiento(driver))).click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.rolSR(driver))).click();
			mensaje = "Flujo : Ingreso de Tratamiento : " + "SR";
			msj = "IngresoTratamiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio1 = utilidades.Funciones.funcionNombreAleatorio();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputNombre(driver)))
			.sendKeys(nombreAleatorio1);
			Thread.sleep(2000);
			mensaje = "Flujo : Ingreso de Nombre Aleatorio : " + nombreAleatorio1;
			msj = "IngresoNombre";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoPaterno(driver)))
			.sendKeys(apellidoPaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Paterno Aleatorio : " + apellidoPaternoAleatorio;
			msj = "IngresoApellidoPaterno";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Apellido Materno
			String apellidoMaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver))).click();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputApellidoMaterno(driver)))
			.sendKeys(apellidoMaternoAleatorio);
			mensaje = "Flujo : Ingreso de Apellido Materno Aleatorio : " + apellidoMaternoAleatorio;
			msj = "IngresoApellidoMaterno";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectGenero(driver))).click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.generoMasculino(driver))).click();
			mensaje = "Flujo : Ingreso de Genero : " + "Masculino";
			msj = "IngresoGenero";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.selectEstadoCivil(driver))).click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.soltero(driver))).click();
			mensaje = "Flujo : Seleccion de Estado Civil : " + "Soltero";
			msj = "IngresoEstadoCivil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Personal.inputFechaNacimiento(driver)))
			.sendKeys("18-08-1978");
			mensaje = "Flujo : Ingreso de Fecha Nacimiento : " + "18-08-1978";
			msj = "IngresoFachaNacimiento";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputNacionalidad(driver));

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver))).click();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver)))
			.sendKeys(paisEjecucion);
			Thread.sleep(1000);
			
			mensaje = "Flujo : Ingreso Campo Nacionalidad : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.nacionalidadSeleccion(driver, paisEjecucion))).click();
			mensaje = "Flujo : Seleccion de Pais Nacionalidad : " + paisEjecucion;
			msj = "SeleccionPaisNacionalidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String numMovil = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono movil
			for (int i = 0; i < 9; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numMovil = numMovil + azar;
			}
			crearCuenta.Personal.inputNumeroMovil(driver).sendKeys("9" + numMovil);
			mensaje = "Flujo : Ingreso Numero Movil Aleatorio : " + "9" + numMovil;
			msj = "IngresoMovil";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 6; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearCuenta.Personal.inputTelefono(driver).sendKeys("2" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngresoNumeroTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String correo = "";
			correo = nombreAleatorio1 + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			correo = correo + correoAzar + "@derco.cl";
			correo = correo.toLowerCase();
			crearCuenta.Personal.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo Creado con Datos del Registro y numero al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputPais(driver).click();
			crearCuenta.Empresa.inputPais(driver).sendKeys(paisEjecucion);
			Thread.sleep(300);
			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			//crearCuenta.Empresa.inputPais(driver).sendKeys(paisEjecucion);
			crearCuenta.Empresa.paisSeleccion(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputRegion(driver).click();
			crearCuenta.Empresa.inputRegion(driver).sendKeys(regionEjecucion);
			mensaje = "Flujo : Ingreso Campo Region : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputRegion(driver));
			Thread.sleep(2000);

			crearCuenta.Empresa.inputRegion(driver).sendKeys(regionEjecucion);
			crearCuenta.Empresa.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(Keys.ENTER);
			crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(provinciaEjecucion);
			mensaje = "Flujo : Ingreso Campo Provincia Direccion : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputProvinciaDireccion(driver));
			Thread.sleep(2000);

			//crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(provinciaEjecucion);
			crearCuenta.Personal.provinciaDireccionSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion Campo provincia Direccion : " + provinciaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputComuna(driver).click();
			crearCuenta.Empresa.inputComuna(driver).sendKeys(comunaEjecucion);
			mensaje = "Flujo : Ingreso Campo Comuna : " + comunaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.inputComuna(driver).sendKeys(comunaEjecucion);
			crearCuenta.Empresa.comunaSeleccion(driver, comunaEjecucion).click();
			mensaje = "Flujo : Seleccion de Comuna : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Cadena de caracteres aleatoria Para Calle
			String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			crearCuenta.Empresa.inputCalleAleatorio(driver).sendKeys(calleAleatorio);
			mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
			msj = "IngresoCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numDireccionAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numDireccionAzar = numDireccionAzar + azar;
			}
			crearCuenta.Personal.inputNumeroDireccion(driver).sendKeys(numDireccionAzar);
			mensaje = "Flujo : Ingreso Numero Direccion Azar : " + calleAleatorio;
			msj = "IngresoNumDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String deptoCasaOficinaAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero casa
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
			}
			crearCuenta.Empresa.inputDeptoCasaOficina(driver).sendKeys("Oficina " + deptoCasaOficinaAzar);
			mensaje = "Flujo : Ingreso Casa Azar : " + "Oficina " + deptoCasaOficinaAzar;
			msj = "IngresoDetalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Personal.inputComplementoDireccion(driver).sendKeys("inmueble interior");
			mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
			msj = "IngresoComplementoDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Cadena de caracteres aleatoria Para Calle Laboral
			String calleComercialAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			String calleComercialAleatorioNumero = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				calleComercialAleatorioNumero = calleComercialAleatorioNumero + azar;
			}

			crearCuenta.Empresa.inputCalleComercial(driver)
			.sendKeys(calleComercialAleatorio + " " + calleComercialAleatorioNumero);
			mensaje = "Flujo: Ingreso Campo Calle Direccion Laboral Aleatorio: " + calleComercialAleatorio + " "
			+ calleComercialAleatorioNumero;
			msj = "IngresoCalleLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputCiudadFacturacion(driver).sendKeys(provinciaEjecucion);
			mensaje = "Flujo : Ingreso Campo Ciudad de Facturacion Aleatorio: " + provinciaEjecucion;
			msj = "IngresoCodigoPostal";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputEstado(driver).sendKeys(comunaEjecucion);
			mensaje = "Flujo : Ingreso Estado o provicncia de la facturación : " + comunaEjecucion;
			msj = "IngresoComunaLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String codigoPostal = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 6; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				codigoPostal = codigoPostal + azar;
			}

			crearCuenta.Empresa.codigoPostal(driver).sendKeys(codigoPostal);
			mensaje = "Flujo : Ingreso Campo Provincia Comercial : " + codigoPostal;
			msj = "IngresoProvinciaComercial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputPaisEnvio(driver).sendKeys(paisEjecucion);
			mensaje = "Flujo : Ingreso Campo Pais Envio : " + paisEjecucion;
			msj = "IngresoPaisEnvio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String textoCuadroVerdePequeno = new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerdePequeno(driver)))
			.getText();

			if (textoCuadroVerdePequeno.contains("Se creá Cuenta")) {
				mensaje = "Flujo : Se creá Cuenta";
				msj = "CreaCuenta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>" + "Error : No Crea Cuenta " + "</p>";
				msj = "ErrorIngresoASap";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Cuenta Empresa Exitoso  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Cuenta Empresa Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}

}
