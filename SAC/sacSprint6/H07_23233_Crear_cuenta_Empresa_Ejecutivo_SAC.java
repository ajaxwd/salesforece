package sacSprint6;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import paginaPrincipal.MenuPrincipal;
import utilidades.DatosInicialesYDrivers;
import utilidades.ExcelUtils;
import utilidades.Funciones;

public class H07_23233_Crear_cuenta_Empresa_Ejecutivo_SAC {
	public WebDriver driver;
	private static Logger Log = Logger.getLogger(Log.class.getName());
	public static String Path_TestDataExecution = "./DatosEjecucionSalesForce/";
	public static String File_TestDataExecution = "H07_23233_Crear_cuenta_Empresa_Ejecutivo_SAC.xlsx";
	public static String sNumeroEscenario = "";
	public static String sNombrePrueba = "";
	public static String sNombreTest = "";

	@Parameters({ "sNumeroEscenarioXml", "sNombrePruebaXml", "sNombreTestXml" })
	@BeforeTest
	public void parametrizacion(String sNumeroEscenarioParametrizacion, String sNombrePruebaParametrizacion,
			String sNombreTestParametrizacion) {
		sNumeroEscenario = sNumeroEscenarioParametrizacion;
		sNombrePrueba = sNombrePruebaParametrizacion;
		sNombreTest = sNombreTestParametrizacion;
	}

	@Test
	public void main() throws Exception {

		Logger.getRootLogger().setLevel(Level.OFF);
		String mensaje = "";
		String msj = "";
		String rutaEvidencia = "";
		int posicionEvidencia = 1;
		String posicionEvidenciaString = "";
		String nombreCaptura = "";
		String nombreCapturaClase = this.getClass().getSimpleName();
		boolean ValorEncontrado = true;
		StringBuffer errorBuffer = new StringBuffer();
		int posicionExcel = 1;

		mensaje = "Test en Ejecucion : " + sNombreTest;
		Funciones.mensajesLog(mensaje);

		utilidades.DatosInicialesYDrivers.File_TestData = "H07_23233_Crear_cuenta_Empresa_Ejecutivo_SAC.xlsx";
		DatosInicialesYDrivers.Path_TestData = Path_TestDataExecution;
		DatosInicialesYDrivers.File_TestData = File_TestDataExecution;
		ExcelUtils.setExcelFile(DatosInicialesYDrivers.Path_TestData + DatosInicialesYDrivers.File_TestData, "Hoja1");
		String Usuario = Funciones.getExcel(driver, "Usuario", posicionExcel); 
		String Password = Funciones.getExcel(driver, "Password", posicionExcel);
		String URL = Funciones.getExcel(driver, "URL", posicionExcel);
		String Browser = Funciones.getExcel(driver, "Browser", posicionExcel);
		utilidades.DatosInicialesYDrivers.Browser = Browser;
		utilidades.DatosInicialesYDrivers.URL = URL;
		utilidades.DatosInicialesYDrivers.Usuario = Usuario;
		utilidades.DatosInicialesYDrivers.Password = Password;
		String cuentaPrincipal = Funciones.getExcel(driver, "Cuenta Principal", posicionExcel);

		mensaje = "Usuario test en Ejecucion : " + Usuario;
		Funciones.mensajesLog(mensaje);
		/*
		 * 
		 * Esta parte del codigo se debe agregar para cada caso de pruebas, aqui se
		 * define el driver y browser a utlizar
		 * 
		 * INICIO
		 * 
		 */
		switch (utilidades.DatosInicialesYDrivers.Browser) {
		case "firefox":
			Thread.sleep(2000);
			Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.gecko.driver", utilidades.DatosInicialesYDrivers.RutaDriverFirefox);
			driver = new FirefoxDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "chrome":
			Thread.sleep(3000);
			Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
			Thread.sleep(4000);
			System.setProperty("webdriver.chrome.driver", utilidades.DatosInicialesYDrivers.RutaDriverChrome);
			//driver = new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless");
			driver = new ChromeDriver(options);
			driver.manage().window().setSize(new Dimension(1024, 768));
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE32":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE32);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		case "IE64":
			Runtime.getRuntime().exec("TASKKILL /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("TASKKILL /F /IM iexplore.exe");
			System.setProperty("webdriver.ie.driver", utilidades.DatosInicialesYDrivers.RutaDriverIE64);
			driver = new InternetExplorerDriver();
			mensaje = "<p>" + "Navegador : " + utilidades.DatosInicialesYDrivers.Browser + "<br>" + "URL "
					+ utilidades.DatosInicialesYDrivers.URL + "</p>";
			Funciones.mensajesLog(mensaje);
			Thread.sleep(2000);
			break;

		}

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(utilidades.DatosInicialesYDrivers.URL);
		//
		// /*
		// *
		// * FIN
		// *
		// */
		// llamadas a las clases genericas que forman un flujo de pruebas:
		// Inicio de Sesion
		login.IniciarSesion.Execute(driver, nombreCapturaClase, Usuario, Password);

		Thread.sleep(5000);

		try {

			String paisEjecucion = "";
			String regionEjecucion = "";
			String provinciaEjecucion = "";
			String comunaEjecucion = "";
			String rutEmpresa = "";
			/* String modeloInteres = "";
			String ingresoLocal = ""; */

			paisEjecucion = Funciones.getExcel(driver, "Pais", posicionExcel);
			regionEjecucion = Funciones.getExcel(driver, "Region", posicionExcel);
			provinciaEjecucion = Funciones.getExcel(driver, "Provincia", posicionExcel);
			comunaEjecucion = Funciones.getExcel(driver, "Comuna", posicionExcel);
			rutEmpresa = Funciones.getExcel(driver, "Rut Empresa", posicionExcel);
			System.out.println(rutEmpresa);
			/* modeloInteres = Funciones.getExcel(driver, "Marca de Interes", posicionExcel);
			ingresoLocal = Funciones.getExcel(driver, "Local", posicionExcel); */

			 //Seleccionar menú de navegación "Contactos"
			 paginaPrincipal.MenuPrincipal.menuNavegacion(driver).click();
			 mensaje = "Flujo : Click Icono Menú de navegación ";
			 msj = "ClickBtnContactos";
			 posicionEvidencia = posicionEvidencia + 1;
			 posicionEvidenciaString = Integer.toString(posicionEvidencia);
			 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			 Thread.sleep(2000);
			 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
 
			 Thread.sleep(3000);
 
			 String opcionMenu = "Cuentas";
			 paginaPrincipal.MenuPrincipal.seleccionarMenu(driver, opcionMenu).click();
			 mensaje = "Flujo : Se selecciona Menú de navegación " + opcionMenu;
			 msj = "ClickBtnContactos";
			 posicionEvidencia = posicionEvidencia + 1;
			 posicionEvidenciaString = Integer.toString(posicionEvidencia);
			 nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			 Thread.sleep(2000);
			 rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			 Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			 
			 Thread.sleep(3000);

			/* new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCuentas(driver)))
			.sendKeys(Keys.ENTER);
			mensaje = "Flujo : Click Boton Cuentas ";
			msj = "ClickBtnCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			Thread.sleep(3000);

			try {// presiona boton borrar todo para llamados anteriores y no ocultar botones de flujo
				paginaPrincipal.MenuPrincipal.btnBorrarTodo(driver).click();
				mensaje = "Flujo : Se persiona 'Borra Todo' para quitar llamdos agendados  ";
				Funciones.mensajesLog(mensaje);
			} catch (NoSuchElementException | ScreenshotException e) { }

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnCrearCuentas(driver)))
			.click();
			mensaje = "Flujo : Click Boton Crear Cuentas ";
			msj = "ClickBtnCrearCuentas";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.rdCuentasEmpresas(driver)))
			.click();
			paginaPrincipal.MenuPrincipal.rdCuentasEmpresas(driver).click();
			mensaje = "Flujo : Click Boton Radio Crear Cuenta Empresa";
			msj = "ClickBtnRadioCtaEmpresa";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(3000);

			new WebDriverWait(driver, 20, 100).until(
			ExpectedConditions.visibilityOf(paginaPrincipal.MenuPrincipal.btnSiguienteCrearCuenta(driver)))
			.click();
			mensaje = "Flujo : Click Boton Siguiente ";
			msj = "ClickBtnSiguiente";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Inicializa Variable azar para llenado de datos al azar
			String azar = "";
			mensaje = "Flujo : LLenado de datos ";
			Funciones.mensajesLog(mensaje);

			// Cadena de caracteres aleatoria Para Rut
			//String rut = utilidades.Funciones.generadorDeRut(49999999, 50000000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputDocumentoIdentidad(driver)))
			.sendKeys(rutEmpresa);
			System.out.println(rutEmpresa);
			mensaje = "Flujo : Ingreso de Rut: " + rutEmpresa;
			msj = "IngresoRut";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputGiro(driver)))
			.sendKeys("Testing QA");
			mensaje = "Flujo : Ingreso de Giro : " + "Testing QA";
			msj = "IngresoGiro";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			/* new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.selectClasificacionFlotas(driver)))
			.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.clasificacionFlotasSeleccion(driver, "A")))
			.click();
			mensaje = "Flujo : Ingreso de Flota : " + "A";
			msj = "IngresoFlota";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			Thread.sleep(2000);

			// Cadena de caracteres aleatoria Para Crear Nombre Empresa
			String empresaAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			empresaAleatorio = empresaAleatorio + " & " + utilidades.Funciones.funcionApellidoAleatorio();
			crearCuenta.Empresa.inputRazonSocial(driver).sendKeys(empresaAleatorio + " Cia Ltda");
			mensaje = "Flujo : Ingreso de Nombre de la cuenta : " + empresaAleatorio;
			msj = "IngresoRazonSocial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			/* empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			empresaAleatorio = empresaAleatorio.toLowerCase();
			crearCuenta.Empresa.inputSitioWeb(driver).sendKeys("www." + empresaAleatorio + ".com");
			mensaje = "Flujo : Ingreso de Sitio Web Empresa Aleatorio : " + "www." + empresaAleatorio + ".com";
			msj = "IngresoSitioWeb";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputNacionalidad(driver));

			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver))).click();
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver)))
			.sendKeys(paisEjecucion);
			Thread.sleep(500);
			/* new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.inputNacionalidad(driver)))
			.sendKeys("LE"); */
			Thread.sleep(500);
			mensaje = "Flujo : Ingreso Campo Nacionalidad : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(crearCuenta.Empresa.nacionalidadSeleccion(driver, paisEjecucion))).click();
			mensaje = "Flujo : Seleccion de Pais Nacionalidad : " + paisEjecucion;
			msj = "SeleccionPaisNacionalidad";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			String[] arregloCuentaPrincipal = new String[cuentaPrincipal.length()];
			for (int i = 0; i < cuentaPrincipal.length(); i++) {
				arregloCuentaPrincipal[i] = Character.toString(cuentaPrincipal.charAt(i));
			}
			for (int i = 0; i < cuentaPrincipal.length(); i++) {
				crearCuenta.Empresa.inputCuentaPrincipal(driver).sendKeys(arregloCuentaPrincipal[i]);
				Thread.sleep(300);
			}

			mensaje = "Flujo : Ingreso Campo Cuenta Principal : " + cuentaPrincipal;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.cuentaPrincipalSeleccionParametro(driver, cuentaPrincipal).click();
			mensaje = "Flujo : Seleccion de Cuenta Principal  : " + cuentaPrincipal;
			msj = "SeleccionCuenta";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);
			// Cadena de caracteres aleatoria Para Nombre
			String nombreAleatorio = utilidades.Funciones.funcionNombreAleatorio();//
			// Cadena de caracteres aleatoria Para Apellido Paterno
			String apellidoPaternoAleatorio = utilidades.Funciones.funcionApellidoAleatorio();
			String correo = "";
			correo = nombreAleatorio + "." + apellidoPaternoAleatorio;
			String correoAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para Correo
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				correoAzar = correoAzar + azar;
			}

			empresaAleatorio = empresaAleatorio.replaceAll("&", "y");
			empresaAleatorio = empresaAleatorio.replaceAll(" ", "");
			correo = correo + correoAzar + "@" + empresaAleatorio + ".com";
			correo = correo.toLowerCase();
			crearCuenta.Empresa.inputCorreo(driver).sendKeys(correo);
			mensaje = "Flujo : Ingreso Correo  al Azar : " + correo;
			msj = "IngresoCorreo";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numTelefono = "";
			azar = "";
			// Cadena de numeros aleatoria Para telefono
			for (int i = 0; i < 6; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numTelefono = numTelefono + azar;
			}

			crearCuenta.Empresa.inputTelefono(driver).sendKeys("2" + numTelefono);
			mensaje = "Flujo : Ingreso Numero Telefono Aleatorio : " + "2" + numTelefono;
			msj = "IngresoNumeroTelefono";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputPais(driver).click();
			crearCuenta.Empresa.inputPais(driver).sendKeys(paisEjecucion);
			Thread.sleep(300);
			mensaje = "Flujo : Ingreso Campo Pais : " + paisEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			//crearCuenta.Empresa.inputPais(driver).sendKeys(paisEjecucion);
			crearCuenta.Empresa.paisSeleccion(driver, paisEjecucion).click();
			mensaje = "Flujo : Seleccion de Pais  : " + paisEjecucion;
			msj = "SeleccionPais";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputRegion(driver).click();
			crearCuenta.Empresa.inputRegion(driver).sendKeys(regionEjecucion);
			mensaje = "Flujo : Ingreso Campo Region : " + regionEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputRegion(driver));
			Thread.sleep(2000);

			crearCuenta.Empresa.inputRegion(driver).sendKeys(regionEjecucion);
			crearCuenta.Empresa.regionSeleccion(driver, regionEjecucion).click();
			mensaje = "Flujo : Seleccion de Region  : " + regionEjecucion;
			msj = "SeleccionRegion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(Keys.ENTER);
			crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(provinciaEjecucion);
			mensaje = "Flujo : Ingreso Campo Provincia Direccion : " + provinciaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			jse.executeScript("arguments[0].scrollIntoView();", crearCuenta.Empresa.inputProvinciaDireccion(driver));
			Thread.sleep(2000);

			//crearCuenta.Empresa.inputProvinciaDireccion(driver).sendKeys(provinciaEjecucion);
			crearCuenta.Personal.provinciaDireccionSeleccion(driver, provinciaEjecucion).click();
			mensaje = "Flujo : Seleccion Campo provincia Direccion : " + provinciaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			Thread.sleep(2000);

			crearCuenta.Empresa.inputComuna(driver).click();
			crearCuenta.Empresa.inputComuna(driver).sendKeys(comunaEjecucion);
			mensaje = "Flujo : Ingreso Campo Comuna : " + comunaEjecucion;
			Funciones.mensajesLog(mensaje);

			Thread.sleep(2000);
			crearCuenta.Empresa.inputComuna(driver).sendKeys(comunaEjecucion);
			crearCuenta.Empresa.comunaSeleccion(driver, comunaEjecucion).click();
			mensaje = "Flujo : Seleccion de Comuna : " + comunaEjecucion;
			msj = "SeleccionComuna";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Cadena de caracteres aleatoria Para Calle
			String calleAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			crearCuenta.Empresa.inputCalleAleatorio(driver).sendKeys(calleAleatorio);
			mensaje = "Flujo : Ingreso Campo Calle Direccion Azar : " + calleAleatorio;
			msj = "IngresoCalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String numDireccionAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero
			for (int i = 0; i < 4; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				numDireccionAzar = numDireccionAzar + azar;
			}
			crearCuenta.Empresa.inputNumeroDireccion(driver).sendKeys(numDireccionAzar);
			mensaje = "Flujo : Ingreso Numero Direccion Azar : " + calleAleatorio;
			msj = "IngresoNumDireccion";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String deptoCasaOficinaAzar = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero casa
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				deptoCasaOficinaAzar = deptoCasaOficinaAzar + azar;
			}
			crearCuenta.Empresa.inputDeptoCasaOficina(driver).sendKeys("Oficina " + deptoCasaOficinaAzar);
			mensaje = "Flujo : Ingreso Casa Azar : " + "Oficina " + deptoCasaOficinaAzar;
			msj = "IngresoDetalle";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputComplementoDireccion(driver).sendKeys("inmueble interior");
			mensaje = "Flujo : Ingreso Complemento Direccion : " + "inmueble interior";
			msj = "IngresoComplementoDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			// Cadena de caracteres aleatoria Para Calle Laboral
			String calleComercialAleatorio = utilidades.Funciones.funcionCalleAleatorio();
			String calleComercialAleatorioNumero = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 2; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				calleComercialAleatorioNumero = calleComercialAleatorioNumero + azar;
			}

			crearCuenta.Empresa.inputCalleComercial(driver)
			.sendKeys(calleComercialAleatorio + " " + calleComercialAleatorioNumero);
			mensaje = "Flujo: Ingreso Campo Calle Direccion Laboral Aleatorio: " + calleComercialAleatorio + " "
			+ calleComercialAleatorioNumero;
			msj = "IngresoCalleLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputCiudadFacturacion(driver).sendKeys(provinciaEjecucion);
			mensaje = "Flujo : Ingreso Campo Ciudad de Facturacion Aleatorio: " + provinciaEjecucion;
			msj = "IngresoCodigoPostal";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputEstado(driver).sendKeys(comunaEjecucion);
			mensaje = "Flujo : Ingreso Estado o provicncia de la facturación : " + comunaEjecucion;
			msj = "IngresoComunaLaboral";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String codigoPostal = "";
			azar = "";
			// Cadena de numeros aleatoria Para numero Calle Laboral
			for (int i = 0; i < 6; i++) {
				azar = utilidades.Funciones.numeroAleatorio(1, 8);
				codigoPostal = codigoPostal + azar;
			}

			crearCuenta.Empresa.inputProvinciaComercial(driver).sendKeys(codigoPostal);
			mensaje = "Flujo : Ingreso Campo Provincia Comercial : " + codigoPostal;
			msj = "IngresoProvinciaComercial";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputPaisEnvio(driver).sendKeys(paisEjecucion);
			mensaje = "Flujo : Ingreso Campo Pais Envio : " + paisEjecucion;
			msj = "IngresoPaisEnvio";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			/* crearCuenta.Empresa.inputReferenciaDireccionComercial(driver).sendKeys("SECTOR INDUSTRIAL");
			mensaje = "Flujo : Ingreso Campo Referencia Direccion Comercial : " + "SECTOR INDUSTRIAL";
			msj = "IngresoReferenciaDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			crearCuenta.Empresa.inputDescripcionDireccionComercial(driver).sendKeys("SECTOR INDUSTRIAL");
			mensaje = "Flujo : Ingreso Campo Descripcion Direccion Comercial : " + "SECTOR INDUSTRIAL";
			msj = "IngresoCDescripcionDir";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia); */

			crearCuenta.Personal.btnGuardar(driver).click();
			mensaje = "Flujo : Se Realiza Click en Boton Guardar ";
			msj = "ClickBtnGuardar";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);

			String textoCuadroVerdePequeno = new WebDriverWait(driver, 20, 100)
			.until(ExpectedConditions.visibilityOf(MenuPrincipal.textoNotificacionesVerdePequeno(driver)))
			.getText();

			if (textoCuadroVerdePequeno.contains("Se creó Cuenta")) {
				mensaje = "Flujo : Se creó Cuenta";
				msj = "CreaCuenta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				Thread.sleep(2000);
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				ValorEncontrado = true;
			} else {
				mensaje = "<p style='color:red;'>" + "Error : No Crea Cuenta " + "</p>";
				msj = "ErrorIngresoASap";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append("\n" + mensaje + "\n");
				ValorEncontrado = false;
			}
			try {
				Assert.assertTrue(ValorEncontrado);
				mensaje = "<p style='color:blue;'>" + "OK: Prueba Correcta ; Crear Cuenta Empresa Exitoso  " + "<p>";
				msj = "PruebaCorrecta";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			} catch (AssertionError e) {
				mensaje = "<p style='color:red;'>" + "ERROR: Prueba Fallida ; Crear Cuenta Empresa Fallida" + "<p>";
				msj = "PruebaErronea";
				posicionEvidencia = posicionEvidencia + 1;
				posicionEvidenciaString = Integer.toString(posicionEvidencia);
				nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
				rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
				Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
				errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			}
		} catch (TimeoutException | NoSuchElementException | ScreenshotException e) {
			mensaje = "<p style='color:red;'>" + "Error : No Encuentra Elemento o No esta Visible, " + "</p>";
			msj = "ElementoNoEncontrado";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			ValorEncontrado = false;
			errorBuffer.append(e.getMessage() + "\n" + mensaje + "\n");
			driver.close();
			throw new AssertionError(errorBuffer.toString());
		}
		if (errorBuffer.length() > 0) {
			mensaje = "<p style='color:red;'>"
			+ "Error : Fin de ejecucion.......se encontraron errores, revisar logs y evidencias" + "</p>";
			msj = "ErroresEncontrados";
			posicionEvidencia = posicionEvidencia + 1;
			posicionEvidenciaString = Integer.toString(posicionEvidencia);
			nombreCaptura = posicionEvidenciaString + "-" + nombreCapturaClase;
			Thread.sleep(2000);
			rutaEvidencia = Funciones.funcionCapturar_Pantalla(driver, nombreCaptura + "_" + msj);
			Funciones.mensajesLogRuta(mensaje, rutaEvidencia);
			throw new AssertionError(errorBuffer.toString());
		}
		driver.quit();
	}
}
